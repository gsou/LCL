(in-package :lcl/compiler)

;; XXX This will probably need many passes btw as requirements may be nested.
;; If a block is promoted as a nlr block, then a nlr through it might not be possible anymore.

(defvar *resolve-cnt* 0)
(defun fresh-resolve-symbol () (concat-string "r" (number-to-lua (incf *resolve-cnt*))))

(defun traverse-ir (fn ir &optional (parent ir))
  (funcall fn ir parent)
  (cond
    ((scope-p ir) (mapc (lambda (b) (traverse-ir fn b ir)) (scope-body ir)))
    ((eq (car ir) 'if)
     (traverse-ir fn (second ir) parent)
     (traverse-ir fn (third ir) parent)
     (traverse-ir fn (fourth ir) parent))
    ((eq (car ir) 'while)
     (traverse-ir fn (second ir) parent)
     (traverse-ir fn (third ir) parent))
    ;; TODO TAG GO
    ((eq (car ir) 'local) (if (third ir) (traverse-ir fn (third ir) parent)))
    ((eq (car ir) '=) (traverse-ir fn (third ir) parent))
    ((eq (car ir) 'nlr) (traverse-ir fn (third ir) parent))
    ((eq (car ir) 'call) (mapc (lambda (b) (traverse-ir fn b parent)) (cdr ir)))
    ((eq (car ir) 'multiple-value-table) (traverse-ir fn (third ir) parent))
    ;; TODO We are still missing elements here...
    ;; NOTE: No traversal for lua, lua-push, pure
    ))

(defun traverse-resolve-nlr (ir &optional parent)
  (cond
    ;; Block
    ;; Tagbody
    ((and parent (listp ir) (eq (car ir) 'go))
     (let ((to-tag (second ir)))
       (unless (can-go-to parent to-tag)
         (setf (scope-nlr (find-scope-by-tag parent to-tag))
               (fresh-resolve-symbol)))))
    ;; Return
    ((and parent (listp ir) (eq (car ir) 'nlr))
     (let ((to-name (second ir)))
       ;; XXX With a can-go-to we could convert from return to goto
       ;; and optimize a few other cases
       (unless (can-return-to parent to-name)
         ;; Directly set the NLR
         (let ((to (find-scope-by-name parent to-name)))
           (when (and to (not (scope-nlr to)))
             (setf (scope-nlr to) (fresh-resolve-symbol)))))))))

(defun resolve (scope)
  (setq *resolve-cnt* 0)
  ;; TODO Check how many are needed.
  (traverse-ir #'traverse-resolve-nlr scope)
  (traverse-ir #'traverse-resolve-nlr scope)
  (traverse-ir #'traverse-resolve-nlr scope)
  (traverse-ir #'traverse-resolve-nlr scope)
  (traverse-ir #'traverse-resolve-nlr scope)
  scope)

(defun traverse-remove-parent (fn ir &optional (parent ir))
  (funcall fn ir parent)
  )
