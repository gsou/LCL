(in-package :lcl/compiler)

(defun compile-to-scope (ast)
  (setq *emit-count* 0)
  (let ((env (make-env)))
    (values
     (compile-scope-body (make-do-scope nil) (list ast) env)
     env)))

(defun compile-to-lua (ast &optional (return (list :return nil)) (env (make-env)))
  "Main entry point for the compilation engine."
  (setq *emit-count* 0)
  (let* ((scope (make-do-scope nil))
         (code (scope-to-lua (resolve (compile-scope-body scope (list ast) env)) return)))
    ;; TODO
    (values (case *compile-mode*
              (:serialize
               (concat-string "local q = {} " (second (env-serializer-state env)) " " code))
              (:eval (concat-string "return function(q) " code " end"))
              (t code))
            env)))

(defun compile-to-lua-in-env (ast env)
  (let ((*emit-count* *emit-count*)
        (*resolve-cnt* *resolve-cnt* ))
    (compile-to-lua ast (list :return nil) (copy-env-for-lexical-evaluation env))))

(defun compile-expect-statements (ast &optional (env (make-env)) (return nil))
  (scope-to-lua (resolve (compile-scope-body (make-raw-scope nil) (list ast) env)) return))

(defun compile-expect-expression (ast &optional (env (make-env)))
  "Compile expecting a raw expression. If it is not the case, results are undefined."
  (scope-to-lua (resolve (compile-scope-body (make-raw-scope nil) (list ast) env)) :raw))
