(in-package :lcl/compiler)

(defun fresh-lua-symbol () (concat-string "e" (number-to-lua (incf *emit-count*))))

(defun expression-inline (scope)
  "Can the expression be inlined directly as a lua expression when generated via `ir-to-lua'
and a NIL return ? This implies a 'pure' expression without side effects.
"
  (cond
    ;; Explicitly pure lua expression
    ((and (listp scope) (eq (car scope) 'pure)) (cadr scope))
    ;; TODO More cases for optimization
    (t nil)))

(defun statements-to-lua (statements)
  (let ((ctx ""))
    (labels ((add (s) (setq ctx (concat-string ctx s " ")))
             (iter (s) (when s (add (car s)) (iter (cdr s)))))
      (iter statements)
      ctx)))

(defun csl (statements)
  (if (listp statements)
      ;; Many statements
      (let ((ctx ""))
        (labels ((add (s more) (setq ctx (concat-string ctx s (if more ", " " "))))
                 (iter (s) (when s (add (car s) (cdr s)) (iter (cdr s)))))
          (iter statements)
          ctx))
      ;; Single string
      (concat-string statements " ")))

(defun single-expression-to-lua (string &optional scope return call-needs-nil-protect)
  "This resolves most of the return magic, everything ends up here."
  (cond
    ((null return)
     ;; NOTE: Wrap in a function to make sure it is a valid statement
     ;; (concat-string "id(" string ")")
     ;; NOTE: This does not consider that sometimes, indexing has metatable side effects.
     (if (find #\( string) string ""))
    ((eq return :raw) string)
    ((eq (car return) :return)
     (if (can-return-to scope (cadr return))
         (concat-string "do return " string " end")
         (let ((to (find-scope-by-name scope (cadr return))))
           (cond
             ((null to) (compile-error "Can't find scope with name ~S" (cadr return)))
             ((null (scope-nlr to)) (compile-error "Scope named ~S can't be returned to" (cadr return)))
             (t (concat-string "error({nlr=" (scope-nlr to) ", " string "})"))))))
    ((eq (car return) :multiple-value-table) (concat-string (cadr return) " = {" string "}"))
    ((eq (car return) :local)
     (if (or (cddr return) (not call-needs-nil-protect))
         ;; Note that if the number of values mismatch, some can be assigned "nil"
         (concat-string
          " " (csl (cdr return)) "= " string "
")
         (concat-string
          " " (csl (cdr return)) "= (" string ") or n
")))
    (t (compile-error "Invalid IR Return target ~S" return))))

(defun ir-to-lua (ir &optional scope return)
  "Generate the lua string code from the scope object.
"
  (cond
    ;; Directly compiling a scope
    ((scope-p ir)
     ;; Compile this scope in its parent context, wanting it to return in the same way as return.
     (scope-to-lua ir return))
    ;; Meta scope
    ((eq (car ir) 'if)
     (if (= 4 (length ir))
         (if (expression-inline (cadr ir))
             ;; TODO
             (error "TODO - inlinable if")
             (let ((e (fresh-lua-symbol)))
               (concat-string
                "local " e " "
                (scope-to-lua (cadr ir) `(:local ,e)) " "
                e " = " e " or CL.NIL if " e " ~= CL.NIL then " (scope-to-lua (caddr ir) return)
                " else " (scope-to-lua (cadddr ir) return) " end")))
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) 'while)
     (if (= 3 (length ir))
         (concat-string
          "while true do local c "
          (scope-to-lua (second ir) '(:local "c"))
          " if not c or c == CL.NIL then break end "
          (scope-to-lua (third ir) nil)
          " end")
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) 'lua-push)
     (if (= 3 (length ir))
         (concat-string
                      (statements-to-lua (cadr ir))
                      (single-expression-to-lua (caddr ir) scope return))
         (compile-error "Invalid IR ~s" ir)))
    ;; Optimize pure
    ((eq (car ir) 'lua) (single-expression-to-lua (cadr ir) scope return))
    ((eq (car ir) 'pure) (if return (single-expression-to-lua (cadr ir) scope return) ""))
    ;; Return modification
    ((eq (car ir) 'local)
     (cond
       ((= 2 (length ir))
        (concat-string "local " (csl (cadr ir))))
       ((= 3 (length ir))
        (concat-string
                     "local " (csl (cadr ir)) "
" (ir-to-lua (caddr ir) scope `(:local ,@(cadr ir)))))
       (t
        (compile-error "Invalid IR ~s" ir))))
    ((eq (car ir) 'multiple-value-table)
     (if (= 3 (length ir))
         (ir-to-lua (caddr ir) scope `(:multiple-value-table ,(cadr ir)))
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) '=)
     (if (= 3 (length ir))
         (ir-to-lua (caddr ir) scope `(:local ,@(cadr ir)))
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) 'nlr)
     (if (= 3 (length ir))
         (ir-to-lua (caddr ir) scope `(:return ,(cadr ir)))
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) 'go)
     ;; XXX With which return format is this valid ?
     (if (= 2 (length ir))
         ;; XXX If this is nil, doesn't mean we can actually find the label...
         (if (can-go-to scope (cadr ir))
             (concat-string "goto " (go-to-label scope (cadr ir)))
             ;; In this case the scope should have gen'd a nlr context.
             (concat-string "error({go=" (go-to-label scope (cadr ir)) "})"))
         (compile-error "Invalid IR ~s" ir)))
    ((eq (car ir) 'tag)
     (if (= 2 (length ir))
         (if return
             (compile-error "Tag at invalid position within scope ~s" ir)
             (concat-string "::" (cadr ir) "::"))
         (compile-error "Invalid IR ~s" ir)))
    ;; Call
    ((eq (car ir) 'call)
     (if (< 1 (length ir))
         (cond
           ((every 'expression-inline (cdr ir))
            (single-expression-to-lua
             (concat-string (expression-inline (cadr ir)) "(" (csl (mapcar 'expression-inline (cddr ir))) ")")
             scope return))
           ;; Ensure evaluation order
           ((expression-inline (cadr ir))
            (let ((vars (mapcar (lambda (_) (declare (ignore _)) (fresh-lua-symbol)) (cddr ir))))
              (concat-string
                           "do local " (csl vars)
                           (statements-to-lua (mapcar (lambda (symbol obj) (ir-to-lua obj scope `(:local ,symbol))) vars (cddr ir)))
                           (single-expression-to-lua
                            (concat-string (expression-inline (cadr ir)) "(" (csl vars) ")")
                            scope return t)
                           " end")))
           (t (error "TODO - uninlinable call")))
         (compile-error "Invalid IR ~s" ir)))
    (t (compile-error "Invalid IR ~s" ir))))

(defun body-to-lua (scope body &optional return)
  (cond
    ((and body (cdr body))
     ;; Any element within the body has a return value that can be ignored
     (setf (scope-ctx scope) (concat-string (scope-ctx scope) " " (ir-to-lua (car body) scope nil)))
     (body-to-lua scope (cdr body) return))
    (body
     ;; Last element, it needs to return a value from the scope if required to
     (setf (scope-ctx scope) (concat-string (scope-ctx scope) " " (ir-to-lua (car body) scope return)))
     (scope-ctx scope))
    (t
     ;; We done, output the context
     (scope-ctx scope))))

(defun scope-to-lua (scope &optional return)
  "Generate the lua string code matching the scope object."
  (if (or (scope-nlr scope) (scope-protect scope))
      (concat-string
                   (if (scope-nlr scope)
                       (if (eq +new+ (scope-nlr-value scope))
                           (concat-string "local " (scope-nlr scope) " = {} ")
                           ;; TODO Should be local as well
                           (concat-string
                            "local " (scope-nlr scope) " "
                            (ir-to-lua (scope-nlr-value scope) scope `(:local ,(scope-nlr scope)))))
                       "")
                   (if (scope-tags scope)
                       (concat-string
                        (apply 'concat-string
                               "local tag = nil "
                               (let (acc)
                                 (maphash (lambda (k v) (setq acc (cons (concat-string "local " v " = {} ") acc)))
                                          (scope-tags scope))
                                 acc))
                        " ::again:: ")
                       "")
                   " local v = {pcall(function() "
                   (if (scope-tags scope)
                       (concat-string
                        (apply 'concat-string
                               (let ((i 0)
                                     acc)
                                 (maphash (lambda (k v)
                                            (setq acc (cons (concat-string
                                                             (if (zerop i) "if tag == " "elseif tag == ")
                                                             v " then goto " v " ")
                                                            acc))
                                            (incf i))
                                          (scope-tags scope))
                                 (reverse acc))
                               ;; (loop
                               ;;   for i from 0
                               ;;   for v being the hash-values of (scope-tags scope)
                               ;;   collect
                               ;;   (concat-string
                               ;;    (if (zerop i) "if tag == " "elseif tag == ")
                               ;;    v " then goto " v " "))
                               )
                        "end ")
                       "")
                   (cond
                     ;; TODO Function
                     ((eq :raw (scope-kind scope)) (body-to-lua scope (scope-body scope) (list :return scope)))
                     ((eq :do (scope-kind scope)) (body-to-lua scope (scope-body scope) (list :return scope)))
                     ((eq :loop (scope-kind scope)) (body-to-lua scope (scope-body scope) (list :return scope)))
                     (t (compile-error "Invalid scope kind for scope with nlr: ~s" scope)))
                   " end)} "
                   ;; In any case, perform the protect
                   (if (scope-protect scope)
                       ;; XXX Return on that ?
                       (concat-string (scope-to-lua (scope-protect scope)) " ")
                       "")
                   ;; Check for error:
                   ;; If none we simply forward ret
                   "if v[1] then "
                   (single-expression-to-lua "(table.unpack or unpack)(v, 2)" (scope-parent scope) return)
                   (if (scope-nlr scope)
                       (concat-string
                        " elseif v[2].nlr == " (scope-nlr scope) " then "
                        (single-expression-to-lua "(table.unpack or unpack)(v[2])" (scope-parent scope) return))
                       "")
                   (if (scope-tags scope)
                       (let ((acc ""))
                         (maphash (lambda (k v)
                                    (setq acc (concat-string acc " elseif v[2].go == " v " then tag = " v " goto again ")))
                                  (scope-tags scope))
                         acc)
                       ;; (apply 'concat-string
                       ;;        (loop for v being the hash-values of (scope-tags scope)
                       ;;              collect
                       ;;              (concat-string " elseif v[2].go == " v " then tag = " v " goto again ")))
                       "")
                   " else error(v[2]) end")
      (cond
        ((eq :function (scope-kind scope))
         ;; We define a lambda, and the lambda is the value of this scope.
         (single-expression-to-lua
          (let ((a (scope-arglist scope)))
            (concat-string
             "CL_LIB[\"ALLOC-FUNCTION\"](" (arglist-lisp a) ",function (" (arglist-lua a) "...) " (arglist-ctx a) " "
             (body-to-lua scope (scope-body scope) `(:return ,scope))
             " end, " (arglist-pos a) ", " (arglist-opt a) ", " (if (arglist-rest a) "true" "false") ", " (or (arglist-key a) "false") ")"))
          (scope-parent scope) return))
        ((eq :raw (scope-kind scope)) (body-to-lua scope (scope-body scope) return))
        ((eq :do (scope-kind scope)) (concat-string "do" (body-to-lua scope (scope-body scope) return) " end"))
        ((eq :loop (scope-kind scope)) (body-to-lua scope (scope-body scope) return))
        (t (compile-error "Invalid scope kind for scope: ~s" scope)))))
