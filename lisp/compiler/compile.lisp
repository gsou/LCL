(in-package :lcl/compiler)

(defun genlocal (env) (concat-string "l" (number-to-lua (incf (env-count env)))))
(defun gentag (env) (concat-string "g" (number-to-lua (incf *emit-count*))))

(define-condition compile-error (error) ())
(define-condition simple-compile-error (simple-error compile-error) ())
(defun compile-error (fmt &rest args) (error 'simple-compile-error :format-control fmt :format-arguments args))

(defun symbol-value-to-lua (ast env)
  "Get the Lua expression that evaluates to the value of the symbol AST in
the lexical context of the ENV."
  (let ((local (find ast (env-locals env) :key #'env-def-name)))
    (cond
      (local (env-def-target local))
      ((keywordp ast) (symbol-to-lua ast env))
      (t (concat-string (symbol-to-lua ast env) ".bound")))))
(defun symbol-value-to-ir (ast env scope)
  (let ((sm (find ast (env-smlocals env) :key #'env-def-name)))
    (if sm
        ;; Local symbol macro expansion
        (compile-to-ir (env-def-target sm) env scope)
        (cond
          ;; Global symbol macro expansion
          #+lcl ((symbol-macro-boundp ast) (compile-to-ir (%symbol-macro ast) env scope))
          ;; Fallback to symbol-value-to-lua
          (t `(lua ,(symbol-value-to-lua ast env)))))))


(defun quote-to-ir (ast env scope)
  "Transform an existing read object to an IR statement(s) and expression.
We do this by leveraging the serializer state of the environment and returning a pure Lua expression."
  (declare (ignorable scope))
  `(pure ,(quote-to-lua ast env)))


;; XXX Environment declaration
(defun compile-arglist (arglist name old-env)
  "Compile the function arglist into:
1. The Lua function arguments
2. The Lua code function preamble
3. The Lua code function postamble
This also adds the definitions within the ENV."
  (declare (ignore name))
  (let* ((env (copy-env old-env))
         (positional (lambda-get-pos arglist))
         (optional (lambda-get-optional arglist))
         (rest (lambda-get-rest arglist))
         ;; XXX At the same time
         (key-p (lambda-has-key arglist))
         (keys (lambda-get-key arglist))
         (aux (lambda-get-aux arglist))
         (lua-arglist "")
         (ctx "")
         (specials ())
         (fast-pos (length positional))
         (fast-opt (length optional))
         (fast-rest (if rest t nil))
         (k-table-ix 2)
         (fast-key (if key-p "{nil ")))
    ;; Positional arguments
    (dolist (p positional)
      (let ((local (genlocal env)))
        (if (boundp p)
            ;; Push the special for storage and restoration
            (progn
              (push (list p (genlocal env)) specials)
              (setq ctx (concat-string ctx " " (symbol-value-to-lua p env) " = " local)))
            ;; Push the local for access
            (push (make-env-def :name p :target local) (env-locals env)))
        (setq lua-arglist (concat-string lua-arglist local ", "))))
    ;; Optional arguments add arguments in the lua-arglist
    ;; As well context to set the default value if needed
    (dolist (o optional)
      (let* ((target (if (consp o) (car o) o))
             (value (if (consp o) (cadr o) nil))
             (is-set (if (consp o) (caddr o) nil))
             (local (genlocal env))
             (default-eval (compile-expect-statements value env `(:local ,local))))
        (if (boundp target)
            (push (list target (genlocal env)) specials)
            (push (make-env-def :name target :target local) (env-locals env)))
        (setq lua-arglist (concat-string lua-arglist local ", "))
        (if is-set
            (let ((local-isset (genlocal env)))
              (push (make-env-def :name is-set :target local-isset) (env-locals env))
              (setq ctx (concat-string ctx " local " local-isset " = " local " and CL.T or n"))))
        (setq ctx (concat-string ctx " if not " local " then "
                                 default-eval
                                 ;; (compile-to-lua value newenv nil (concat-string local " = "))
                                 " end"))
        (when (boundp target) (setq ctx (concat-string ctx " " (symbol-value-to-lua target env) " = " local " ; ")))))
    (if rest
        (let ((local-rest (genlocal env)))
          (if (boundp rest)
              (progn
                (push (list rest (genlocal env)) specials)
                (setq ctx (concat-string ctx " " (symbol-value-to-lua rest env) " = " local-rest " ; ")))
              (push (make-env-def :name rest :target local-rest) (env-locals env)))
          (setq lua-arglist (concat-string lua-arglist local-rest ", "))))
    (if key-p
        (setq lua-arglist (concat-string lua-arglist "keys, ")))
    (dolist (ks keys)
      (let* ((k (if (consp ks) (car ks) ks))
             (k-keyword (if (consp k) (car k) (intern (symbol-name k) :keyword)))
             (k-var-symbol (if (consp k) (cadr k) k))
             (value (if (consp ks) (cadr ks)))
             (is-set (if (consp ks) (caddr ks)))
             ;; TODO Switch to numbering
             ;; (number k-table-ix)
             ;; (k-ix (concat-string "keys[" (number-to-lua number) "]"))
             (k-ix (concat-string "keys[" (symbol-to-lua k-var-symbol env) "]"))
             ;; XXX Check for no return values
             (default-eval (compile-expect-statements value env `(:local ,k-ix))))
        (setq k-table-ix (+ 1 k-table-ix))
        (setq fast-key (concat-string fast-key ", [" (symbol-to-lua k-keyword env) "] = "
                                      ;; (number-to-lua number)
                                      (symbol-to-lua k-var-symbol env)))
        (if (boundp k-var-symbol)
            (push (list k-var-symbol (genlocal env)) specials)
            (push (make-env-def :name k-var-symbol :target k-ix) (env-locals env)))
        (if is-set
            (let ((local-isset (genlocal env)))
              (push (make-env-def :name is-set :target local-isset) (env-locals env))
              (setq ctx (concat-string ctx " local " local-isset " = " k-ix " and CL.T or n"))))
        (setq ctx (concat-string ctx " if not " k-ix " then "
                                 default-eval
                                 ;; (compile-to-lua value newenv nil (concat-string k-ix " = "))
                                 " end"))
        (when (boundp k-var-symbol) (setq ctx (concat-string ctx " " (symbol-value-to-lua k-var-symbol env) " = " k-ix)))))
    (if key-p (setq fast-key (concat-string fast-key "}")))
    ;; XXX Can &AUX be special variables ?
    (dolist (a aux)
      (let ((target (if (listp a) (car a) a))
            (value (if (listp a) (cadr a)))
            (local (genlocal env)))
        (setq ctx (concat-string ctx
                                 (compile-expect-statements value env `(:local ,local))
                                 ;; (compile-to-lua value newenv nil (concat-string local " = "))
                                 ))
        (push (make-env-def :name target :target local) (env-locals env))))
    ;; Check mv as we enter a lambda block.
    ;; (env-checkmv newenv t)
    (values (make-arglist
             :lua lua-arglist
             :lisp (quote-to-lua arglist env)
             :pos fast-pos
             :opt fast-opt
             :rest fast-rest
             :special specials
             :key fast-key)
            env
            ctx)

    ;; (if specials
    ;;     ;; Create special variables bindings
    ;;     (concat-string "CL_LIB[\"ALLOC-FUNCTION\"](" (quote-to-lua arglist env)
    ;;                    ",function (" lua-arglist " ...) "
    ;;                    (apply #'concat-string
    ;;                           (mapcar
    ;;                            (lambda (s) (concat-string " local " (second s) " = " (symbol-value-to-lua (first s) env) " ; "))
    ;;                            specials))
    ;;                    "local status, ret = pcall(function () " ctx " " (compile-to-lua (cons 'let (cons () body)) newenv) " end)"
    ;;                    (apply #'concat-string
    ;;                           (mapcar
    ;;                            (lambda (s) (concat-string (symbol-value-to-lua (first s) env) " = " (second s) "; "))
    ;;                            specials))
    ;;                    "if status then return(ret) else error(ret) end end, " fast-pos ", " fast-opt ", " fast-rest ", " fast-key ")")
    ;;     ;; No need for specials wrapping
    ;;     (concat-string "CL_LIB[\"ALLOC-FUNCTION\"](" (quote-to-lua arglist env) ",function (" lua-arglist " ...) " ctx " " BODY
    ;;                    " end, " fast-pos ", " fast-opt ", " fast-rest ", " fast-key ")"))
    ))

(defun lambda-scope (name arglist body old-env parent)
  (let* ((function-scope (make-scope :parent parent :kind :function))
         (scope (make-scope :name name :parent function-scope :kind :do)))
    (multiple-value-bind (obj env preamble) (compile-arglist arglist nil old-env)
      (setf (scope-arglist function-scope) obj)
      (if (arglist-special obj)
          ;; Add special saves
          (setf (scope-body scope)
                (list
                 `(lua-push (,(apply #'concat-string
                                     (mapcar
                                      (lambda (sp)
                                        (concat-string
                                         "local " (cadr sp) " = "
                                         (symbol-value-to-lua (car sp) env)
                                         "
"
                                         ))
                                      (arglist-special obj))))
                   "")
                 (compile-to-ir
                  `(unwind-protect
                        (progn
                          (lua-push (,preamble) "")
                          ,@body)
                     ;; Restore specials
                     (lua-push (,(apply #'concat-string
                                        (mapcar
                                         (lambda (sp)
                                           (concat-string
                                            " " (symbol-value-to-lua (car sp) env) " = "
                                            (cadr sp)
                                            "
"
                                            ))
                                         (arglist-special obj))))
                               ""))
                  env scope)))
          ;; No need for specials
          (setf (scope-body scope) (cons
                                    `(lua-push (,preamble) "")
                                    ;; TODO Such bodies in other places.
                                    (if body
                                        (mapcar (lambda (b) (compile-to-ir b env scope)) body)
                                        (list (list 'pure "n"))))))
      (setf (scope-body function-scope) (list scope))
      function-scope)))
(defun function-name-block-name (function)
  (cond
    ((symbolp function) function)
    ((and (listp function)
          (= 2 (length function))
          (eq (car function) 'setf))
     (second function))
    (t (compile-error "Invalid function name ~S" function))))
(defun function-to-ir (ast env scope)
  "Get the IR for the function represented by the AST (symbol or lambda form)."
  (cond
    ((consp ast)
     (case (car ast)
       ((lambda nlambda)
        ;; XXX check length
        (if (eq (car ast) 'nlambda)
            (lambda-scope (function-name-block-name (cadr ast)) (caddr ast) (cdddr ast) env scope)
            (lambda-scope +unnamed+ (cadr ast) (cddr ast) env scope)))
       (setf
        ;; XXX check length
        (let ((f (find ast (env-flocals env) :key #'env-def-name :test #'equal)))
          (if f
              ;; Local setf function
              `(pure ,(env-def-target f))
              ;; Global function
              `(pure ,(concat-string (symbol-to-lua (cadr ast) env) ".setfbound")))))
       (t (compile-error "Invalid function representation ~S" ast))))
    ((symbolp ast)
     (let ((f (find ast (env-flocals env) :key #'env-def-name)))
       (if f
           ;; Local function
           `(pure ,(env-def-target f))
           ;; Global function
           `(pure ,(concat-string (symbol-to-lua ast env) ".fbound")))))
    (t (compile-error "Invalid function representation ~S" ast))))

(defun let-to-ir (defs body old-env parent)
  "Compile a let to ir, this does not consider the special variables."
  (let* ((env (copy-env old-env))
         (variables (mapcar (lambda (_) (declare (ignore _)) (genlocal env)) defs))
         (scope (make-do-scope parent)))
    (setf (scope-body scope)
          `(
            ,@(mapcar (lambda (v d)
                        (cond
                          ((atom d) `(local (,v) (pure "CL.NIL")))
                          ((= 1 (length d)) `(local (,v) (pure "CL.NIL")))
                          ((= 2 (length d))
                           `(local (,v) ,(compile-to-ir (cadr d) env scope))
                           )
                          (t (compile-error "Invalid let definition ~s" d))))
                      variables defs)
            ,@(progn
                (mapc (lambda (v d) (push (make-env-def :name (if (consp d) (car d) d) :target v) (env-locals env))) variables defs)
                (mapcar (lambda (b) (compile-to-ir b env scope)) (if body body (list nil))))))
    scope))
(defun flet-to-ir (lab defs body old-env parent)
  (if defs
      (let* ((env (copy-env old-env))
             (variables (mapcar (lambda (_) (declare (ignore _)) (genlocal env)) defs))
             (scope (make-do-scope parent)))
        (setf (scope-body scope)
              `(
                ,@(if lab
                      (progn
                        (mapc (lambda (def local)
                                (push (make-env-def :name (car def) :target local) (env-flocals env)))
                              defs variables)
                        `((local ,variables)
                          ,@(mapcar (lambda (v d) `(= (,v) ,(compile-to-ir `(function (nlambda ,@d)) env scope)))
                                    variables defs))
                        )
                      (prog1
                          `((local ,variables)
                            ,@(mapcar (lambda (v d) `(= (,v) ,(compile-to-ir `(function (nlambda ,@d)) env scope)))
                                      variables defs))
                        (mapc (lambda (def local)
                                (push (make-env-def :name (car def) :target local) (env-flocals env)))
                              defs variables)))
                ,@(mapcar (lambda (b) (compile-to-ir b env scope)) (or body (list 'nil)))))
        scope)
      (let ((env (copy-env old-env))
            (scope (make-do-scope parent)))
        (setf (scope-body scope) (mapcar (lambda (b) (compile-to-ir b env scope)) (or body (list 'nil))))
        scope)))
(defun macrolet-to-ir (defs body old-env parent)
  (let* ((env (copy-env old-env)))
    ;; Prepare the functions
    (dolist (d defs)
      (let ((name (first d))
            (arglist (second d))
            (func (cddr d)))
        (push (make-env-def :name name :target (#-lcl eval #+lcl cl-lib::eval-in-lexical-environment `(lambda-macro ,name ,arglist ,@func) #+lcl old-env)) (env-mlocals env))))
    (compile-to-ir `(progn ,@body) env parent)))
(defun symbol-macrolet-to-ir (defs body old-env parent)
  (let* ((env (copy-env old-env))
         (scope (make-do-scope parent)))
    ;; FIXME smlocals should be shadowable by let.
    (mapc (lambda (d) (push (make-env-def :name (car d) :target (cadr d)) (env-smlocals env))) defs)
    (setf (scope-body scope)
          (mapcar (lambda (b) (compile-to-ir b env scope)) (or body (list nil))))
    scope))
(defun mvb-to-ir (syms call body old-env parent)
  (let* ((env (copy-env old-env))
         (scope (make-do-scope parent))
         (vars (mapcar (lambda (_) (declare (ignore _)) (genlocal env)) syms)))
    (setf (scope-body scope)
          `(,@(if vars
                  `(
                    (local ,vars ,(compile-scope-body (make-do-scope scope) (list call) env))
                    ;; Make sure they are bound if needed
                    ,@(mapcar (lambda (v) `(= (,v) (pure ,(concat-string v " or CL.NIL")))) vars))
                  `(,(compile-scope-body (make-do-scope scope) (list call) env)))
            ,(progn
               (mapc (lambda (v s) (push (make-env-def :name s :target v) (env-locals env))) vars syms)
               (compile-scope-body (make-do-scope scope) body env))))
    scope))
(defun tagbody-to-ir (ast old-env parent)
  (let ((scope (make-tagbody-scope parent))
        (env (copy-env old-env)))
    (flet ((tagbody-element-to-ir (element)
             (if (symbolp element)
                 (let ((tag (gentag env)))
                   ;; HACK DEBUG
                   (setf (gethash element (scope-tags scope)) tag)
                   `(tag ,tag))
                 (compile-scope-body (make-do-scope scope) (list element) env) ;; (compile-to-ir element env scope)
                 )))
      (setf (scope-body scope)
            (concatenate 'list
                         (mapcar #'tagbody-element-to-ir ast)
                         (list `(pure "CL.NIL")))))
    scope))

(defun setq-to-ir (dest src env scope)
  (cond
    ((symbolp dest)
     (let ((sm (find dest (env-smlocals env) :key #'env-def-name)))
       (if sm
           ;; Local symbol macro expansion
           (compile-to-ir `(setf ,(env-def-target sm) ,src) env scope)
           (cond
             ;; Global symbol macro expansion
             #+lcl
             ((symbol-macro-boundp dest)
              (compile-to-ir `(setf ,(%symbol-macro dest) ,src) env scope))
             ;; Bound value within the symbols (also locals)
             (t
              (let ((inner (make-do-scope scope)))
                (setf (scope-body inner)
                      `((= (,(symbol-value-to-lua dest env)) ,(compile-to-ir src env scope))
                        (lua ,(symbol-value-to-lua dest env))))
                inner))))))
    ((atom dest) (compile-error "Invalid destination for setq ~s" dest))
    ((eq (car dest) 'function)
     (cond
       ((and (consp (cadr dest)) (eq 'setf (caadr dest))) `(call (pure "CL_LIB[\"SETF-FUNCTION-SETQ\"]") (pure ,(symbol-to-lua (cadadr dest) env)) ,(compile-to-ir src env scope)))
       ((symbolp (cadr dest)) `(call (pure "CL_LIB[\"FUNCTION-SETQ\"]") (pure ,(symbol-to-lua (cadr dest) env)) ,(compile-to-ir src env scope)))
       (t (compile-error "SETQ doesn't know how to set ~s" dest))))
    ((eq (car dest) 'macro-function)
     `(call (pure "CL_LIB[\"MACRO-SETQ\"]") ,(compile-to-ir (cadr dest) env scope) ,(compile-to-ir src env scope)))
    (t (compile-error "Invalid destination for setq ~s" dest))))
(defun long-function-call-to-ir (ast env parent)
  `(call (pure "funcall") ,@(mapcar (lambda (a) (compile-to-ir a env parent)) (cons (list 'function (car ast)) (cdr ast)))))
(defun gen-short-call-to-ir (ast env parent arglist)
  "Try to generate a short function call by precomputing everything in CL-LIB:FUNCALL-LUA if the function already exist.
This should also generate a warning if it fails or if the function does not exist. This could also be a problem if a function
is redefined with different arguments."
  (let ((scope (make-do-scope parent)))
    (let ((func-name (car ast))
          (args (cdr ast))
          (state 0)
          (key :no)
          nextkey
          funcall-args)
      (if (eq '&whole arglist)
          ;; Compile the arguments raw.
          (tagbody
           continue
             (unless args (go break))
             (push (compile-to-ir (car args) (copy-env env) scope) funcall-args)
             (setq args (cdr args))
             (go continue)
           break)
          (tagbody
           continue
             (unless (or arglist args) (go break))
             (cond
               ((eq (car arglist) '&OPTIONAL) (setq state 1))
               ((or (eq (car arglist) '&REST)
                    (eq (car arglist) '&BODY))
                (setq state 2))
               ((eq (car arglist) '&KEY) (setq state 3)
                ;; TODO Make key a scope
                (setq key nil))
               ((eq (car arglist) '&AUX) (go break))
               ((eq state 0)
                ;; Positional args
                (if args
                    (if arglist
                        (push (compile-to-ir (car args) (copy-env env) scope) funcall-args)
                        (progn
                          (warn "Invalid call to ~S, too many argument(s)." (car ast))
                          (return-from gen-short-call-to-ir nil)))
                    (progn
                      (warn "Invalid call to ~S, missing argument(s)." (car ast))
                      (return-from gen-short-call-to-ir nil)))
                (setq args (cdr args)))
               ((eq state 1)
                ;; Optional args
                (cond
                  ((not (car arglist))
                   (warn "Invalid call to ~S, too many argument(s)." (car ast))
                   (return-from gen-short-call-to-ir nil))
                  (args (push (compile-to-ir (car args) (copy-env env) scope) funcall-args))
                  (t (push `(pure "false") funcall-args)))
                (setq args (cdr args)))
               ((eq state 2)
                (setq state 5)
                ;; Generate list
                (let ((inner (make-do-scope scope)))
                  (setf (scope-body inner)
                        `((call (pure "l") ,@(mapcar (lambda (a) (compile-to-ir a (copy-env env) scope)) args))))
                  (push inner funcall-args)))
               ((eq state 3)
                ;; FIXME Aborting for keys now (also allow-other-keys.
                (return-from gen-short-call-to-ir nil)
                ;; (if args
                ;;            (if nextkey
                ;;                (progn
                ;;                  ;; TODO PUSH BOTH TO KEY
                ;;                  (setq nextkey nil)
                ;;                  )
                ;;                (progn
                ;;                  ;; TODO Identify the symbol
                ;;                  (setq nextkey (lua "funcall_key_get_actual_symbol(" func ", " (car args) ") or n" ))
                ;;                  (cond
                ;;                    ((and (not nextkey) (not (eq (car (last arglist)) '&allow-other-keys)))
                ;;                     (warn "Unknown keyword argument ~S on call to ~S. " (car args) (car ast))
                ;;                     (return-from gen-short-call-to-ir nil))
                ;;                    ((not nextkey) (setq args (cdr args)))))))
                (setq args (cdr args)))
               (t (go break)))
             (setq arglist (cdr arglist))
             (go continue)
           break))
      (setf (scope-body scope)
            `((call
               ,(function-to-ir func-name env scope)
               ,@(reverse funcall-args)))))
    scope))

(defun compile-scope-body (scope progn env)
  (setf (scope-body scope) (mapcar (lambda (ast) (compile-to-ir ast env scope)) (or progn (list 'nil))))
  scope)

(defun compile-to-ir (ast &optional (env (make-env)) parent)
  "Compile standard AST to either a scope object or other AST that can be emitted."
  (cond
    ;; Generate the symbol value (or expand symbol macro, which will loop back here)
    ((symbolp ast) (symbol-value-to-ir ast env parent))
    ;; Copy the object (or clone if we cant copy) to the ast gen
    ((atom ast) `(pure ,(quote-to-lua ast env)))
    ;; Raw lua expression
    ((eq (car ast) 'lua)
     (if (< 1 (length ast))
         `(lua
           ,(apply 'concat-string
                   (mapcar (lambda (a)
                             (if (stringp a)
                                 a
                                 (compile-expect-expression a env)))
                           (cdr ast))))
         (compile-error "Invalid special-form ~s" ast)))
    ;; Lua push wrapper is allowed for now
    ((eq (car ast) 'lua-push)
     (if (< 2 (length ast))
         `(lua-push
              ,(mapcar (lambda (a)
                         (if (stringp a)
                             a
                             (compile-expect-expression a env)))
                (cadr ast))
              ,(apply 'concat-string
                      (mapcar (lambda (a)
                                (if (stringp a)
                                    a
                                    (compile-expect-expression a env)))
                              (cddr ast))))
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'quote)
     (if (= 2 (length ast))
         (quote-to-ir (cadr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'function)
     (if (= 2 (length ast))
         (function-to-ir (cadr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'setq)
     (cond
       ((= 1 (length ast)) (compile-to-ir nil env parent))
       ((= 3 (length ast)) (setq-to-ir (cadr ast) (caddr ast) env parent))
       ;; HACK longer streaks are fed back to setf.
       ((< 3 (length ast)) (compile-to-ir `(setf ,@(cdr ast)) env parent))
       (t (compile-error "Invalid special-form ~s" ast))))
    ((eq (car ast) 'if)
     (cond
       ((or (= 3 (length ast)) (= 4 (length ast)))
        (list 'if
              (compile-scope-body (make-do-scope parent) (list (cadr ast)) (copy-env env))
              (compile-scope-body (make-do-scope parent) (list (caddr ast)) (copy-env env))
              (compile-scope-body (make-do-scope parent) (list (cadddr ast)) (copy-env env))))
       (t
        (compile-error "Invalid special-form ~s" ast))))
    ((eq (car ast) 'while)
     (if (< 1 (length ast))
         (list 'while
               (compile-scope-body (make-do-scope parent) (list (cadr ast)) (copy-env env))
               (compile-scope-body (make-loop-scope parent) (cddr ast) (copy-env env)))
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) '%let)
     (if (< 1 (length ast))
         (let-to-ir (cadr ast) (cddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) '%pairs)
     (if (< 3 (length ast))
         (let* ((key (cadr ast))
                (key-var (genlocal env))
                (val (caddr ast))
                (val-var (genlocal env))
                (table (cadddr ast))
                (table-var (genlocal env))
                (body (cddddr ast))
                (scope (make-do-scope parent))
                (ee (copy-env env)))
           ;; XXX Scope parent, or wrapping ?
           (setf (scope-body scope)
                 `((local (,table-var) ,(compile-scope-body (make-do-scope parent) (list table) (copy-env ee)))
                   (lua-push ("for " ,key-var ", " ,val-var " in pairs(" ,table-var ") do") "")
                   ,(progn
                      (push (make-env-def :name key :target key-var) (env-locals ee))
                      (push (make-env-def :name val :target val-var) (env-locals ee))
                      (compile-scope-body (make-do-scope parent) body (copy-env ee)))
                   (lua-push ("end") "")))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ;; TODO %loop
    ((eq (car ast) 'flet)
     (if (< 1 (length ast))
         (flet-to-ir nil (cadr ast) (cddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'labels)
     (if (< 1 (length ast))
         (flet-to-ir t (cadr ast) (cddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'macrolet)
     (if (< 1 (length ast))
         (macrolet-to-ir (cadr ast) (cddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'symbol-macrolet)
     (if (< 1 (length ast))
         (symbol-macrolet-to-ir (cadr ast) (cddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'block)
     (if (< 1 (length ast))
         (let* ((new-env (copy-env env))
                (scope (make-block-scope (cadr ast) parent)))
           (setf (scope-body scope) (mapcar (lambda (b) (compile-to-ir b new-env scope)) (cddr ast)))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'multiple-value-bind)
     (if (< 1 (length ast))
         (mvb-to-ir (cadr ast) (caddr ast) (cdddr ast) env parent)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'multiple-value-list)
     (if (= 2 (length ast))
         (let ((scope (make-do-scope parent))
               (values (genlocal env)))
           (setf (scope-body scope)
                 `((local (,values))
                   (multiple-value-table ,values ,(compile-scope-body (make-do-scope scope) (list (cadr ast)) env))
                   (call (pure "list_r") (pure ,values))))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'return-from)
     (if (or (= 2 (length ast)) (= 3 (length ast)))
         `(nlr ,(cadr ast) ,(compile-to-ir (caddr ast) env parent))
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'catch)
     (if (< 1 (length ast))
         ;; XXX using lua's equality for the tag match instead of eq
         (let ((scope (make-block-scope +catch+ parent)))
           (setf (scope-nlr scope) (genlocal env))
           (setf (scope-nlr-value scope) (compile-to-ir (cadr ast) env scope))
           (setf (scope-body scope) (list (compile-scope-body (make-do-scope scope) (cddr ast) env)))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'throw)
     (if (= 3 (length ast))
         (let ((scope (make-do-scope parent))
               (values (genlocal env))
               (tag (genlocal env)))
           (setf (scope-body scope)
                 `((local (,tag) ,(compile-scope-body (make-do-scope scope) (list (cadr ast)) env))
                   (local (,values))
                   (multiple-value-table ,values ,(compile-scope-body (make-do-scope scope) (list (caddr ast)) env))
                   (= (,(concat-string values ".nlr")) (pure ,tag))
                   (lua ,(concat-string " error(" values ")"))))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'unwind-protect)
     (if (< 1 (length ast))
         (let* ((scope (make-do-scope parent))
                (protected (make-do-scope parent))
                (env-protect (copy-env env))
                (env-protected (copy-env env)))
           (setf (scope-protect scope) (compile-scope-body protected (cddr ast) env-protected))
           (setf (scope-body scope) (list (compile-to-ir (cadr ast) env-protect scope)))
           scope)
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'go)
     (if (= 2 (length ast))
         ast
         (compile-error "Invalid special-form ~s" ast)))
    ((eq (car ast) 'tagbody) (tagbody-to-ir (cdr ast) env parent))
    (t
     ;; Function call or macro
     (call-to-ir ast env parent)
      ;; (multiple-value-bind (form expanded-p) (macroexpand ast env)
      ;;        (if expanded-p
      ;;            (compile-to-ir form env parent)
      ;;            (call-to-ir form env parent)))
     )))
