(in-package :lcl/compiler)

(defparameter +unnamed+ (gentemp "UNNAMED"))
(defparameter +new+ (gentemp "NEW"))
(defparameter +catch+ (gentemp "CATCH"))

(defstruct env-def
  (name nil :type (or symbol cons))
  ;; Target varies between kinds
  ;; For locals and flocals, they are the lua symbols,
  ;; but for macros and symbol macros, they represent
  ;; the expansion.
  (target nil :type t))

(defstruct env
  (smlocals nil :type list)
  (mlocals nil :type list)
  (flocals nil :type list)
  (locals nil :type list)
  (serializer-state (list 0 "" nil))
  (eval-state (make-array 0 :fill-pointer 0))
  (count 0 :type (integer 0)))

(defun copy-env-for-lexical-evaluation (env)
  (setq env (copy-env env))
  (setf (env-flocals env) nil)
  (setf (env-locals env) nil)
  (setf (env-serializer-state env) (list 0 "" nil))
  (setf (env-eval-state env) (make-array 0 :fill-pointer 0))
  env)

(defstruct arglist
  (lua "" :type string)
  (ctx "" :type string)
  (lisp nil :type string)
  (pos 0 :type (integer 0))
  (opt 0 :type (integer 0))
  (rest nil :type boolean)
  (special nil :type list)
  (key nil :type (or null string)))

(defstruct scope
  (name +unnamed+ :type t)
  (ctx "" :type string)
  (protect nil :type (or scope null))
  (nlr nil)                ; Lua symbol for the nlr
  (nlr-value +new+)
  ;; (nlr-mode nil :type (member nil :goto :call :nlr))
                                        ; How to trigger the nlr in this case
  (tags nil :type (or hash-table null))
  (kind :do :type (member :raw :function :do :loop))
  (parent nil :type (or scope null))
  (declarations nil :type list)
  (arglist nil :type (or null arglist))
  (body nil :type list))

(defun make-do-scope (parent) (make-scope :parent parent :kind :do))
(defun make-tagbody-scope (parent) (make-scope :parent parent :kind :do :tags (make-hash-table)))
(defun make-raw-scope (parent) (make-scope :parent parent :kind :raw))
(defun make-block-scope (name parent) (make-scope :name name :parent parent :kind :do))
(defun make-loop-scope (parent) (make-scope :parent parent :kind :loop))

(defun return-passthrough-scope-p (scope)
  (and
   (not (scope-nlr scope))
   (not (scope-protect scope))
   (or (eq (scope-kind scope) :do)
       (eq (scope-kind scope) :loop))))

;; FIXME This is still wrong, you'd need to check for last recursively.
(defun is-last-in-body (scope)
  "Extra condition to check that the block must be last for it to be returnable."
  (or
   ;; No parent
   (not (scope-parent scope))
   (let ((body (scope-body (scope-parent scope))))
     (eq (1- (length body))
         (position scope body)))))

(defun return-stops-at (scope)
  "This checks wether using return will stop at the given SCOPE.
For that to be true, it must be directly the last in the body of its parent, be a function kinded scope,
or be a top level scope."
  (or
   (null (scope-parent scope))
   (eq :function (scope-kind scope))
   (and
    (is-last-in-body scope)
    (return-stops-at (scope-parent scope)))))

(defun can-return-to (scope el)
  (cond
    ((eq el scope)
     ;; XXX Do we want to check stops at for this one too ?
     t)
    ((null scope) nil)
    ((eq el (scope-name scope))
     (or (not (eq :do (scope-kind scope)))
         ;; Only need to check is last in body if it's a do kinded block
         ;; (is-last-in-body scope)
         (return-stops-at scope)
         ))
    (t
     (if (return-passthrough-scope-p scope)
         (can-return-to (scope-parent scope) el)
         nil))))

(defun can-go-to (scope tag)
  (cond
    ;; FIXME This should be an error:
    ((null scope) nil)
    ((and (scope-tags scope) (gethash tag (scope-tags scope))) scope)
    (t
     (if (return-passthrough-scope-p scope)
         (can-go-to (scope-parent scope) tag)
         nil))))

(defun go-to-label (scope tag)
  (cond
    ((null scope) nil)
    ((and (scope-tags scope) (gethash tag (scope-tags scope))) (gethash tag (scope-tags scope)))
    (t (go-to-label (scope-parent scope) tag))))


(defun find-scope-by-tag (scope tag)
  (cond
    ((null scope) nil)
    ((and (scope-tags scope) (gethash tag (scope-tags scope))) scope)
    (t (find-scope-by-tag (scope-parent scope) tag))))

(defun find-scope-by-name (scope name)
  (cond
    ((eq name (scope-name scope)) scope)
    ((scope-parent scope) (find-scope-by-name (scope-parent scope) name))
    (t nil)))

(defvar *emit-count* 0
  "Global counter for Lua generation of unique identifier.")
