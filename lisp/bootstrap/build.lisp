(in-package #:lcl/bootstrap)


(defun compile-stmt (&rest asts) (lcl/compiler:compile-to-lua `(%let () ,@asts) nil))

(defun compile-stdlib-file-to-lua (path)
  "Cross compile the stdlib file at PATH"
  (format t "~&Compiling ~S~&" path)
  (format nil
          "~&print('Loading ' .. ~A .. '...')~&~A"
          (string-to-lua (namestring path))
          (apply 'compile-stmt (cdr (uiop:read-file-forms path)))))

(defmacro compile-all-files-to-lua ((&optional (target nil)) &body paths)
  "Helper macro to compile all the given files in order"
  `(format ,target "~A" (concatenate 'string ,@(mapcar (lambda (p) `(compile-stdlib-file-to-lua ,p)) paths))))

(defmacro compile-all-to-lua-file ((path) &body paths)
  `(with-open-file (file ,path :direction :output :if-exists :supersede)
     (compile-all-files-to-lua (file) ,@paths)))

(defun stdlib ()
  (mapcar
   (lambda (path) (asdf:system-relative-pathname :lcl (format nil "stdlib/~A.lisp" path)))
   '("init" "init2" "native" "variables" "list"
     "base" "hash" "predicate" "math" "predicate"
     "math" "type" "string" "compile" "destructuring"
     "eval" "setf" "loop" "array" "sequence"
     "setf-expanders" "struct" "byte" "stream"
     "reader" "printer" "load" "package" "system"
     "stubs")))

(defun bootstrap (file)
  "Generate the bootstrap LCL.lua from this lisp implementation."
  (setq *guest-macros* (make-hash-table))
  (alexandria:with-output-to-file (s file :if-exists :supersede)
    ;; Preamble library, as well as the stdlib
    (format s "~A~&LCL['LCL/BOOTSTRAP'] = CL_LIB LCL['LCL/LUA'] = CL_LIB LCL['LCL/COMPILER'] = CL_LIB~&" (uiop:read-file-string (asdf:system-relative-pathname :lcl "lcl_lib.lua")))
    ;; Ensure all CL Symbols exist.
    (do-external-symbols (symbol :cl) (format s "~&id(CL['~A'])~&" symbol))
    (loop for file in (stdlib) do (format s "~A~&" (compile-stdlib-file-to-lua file)))
    ;; Compiler
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/lua/serialize.lisp")))
    ;; (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/lua/special.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/environment.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/arglist.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/compile.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/resolve.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/emit.lisp")))
    (format s "~A~&" (compile-stdlib-file-to-lua (asdf:system-relative-pathname :lcl "lisp/compiler/interface.lisp")))
    t))
