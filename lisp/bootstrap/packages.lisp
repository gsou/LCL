(defpackage #:lcl/bootstrap
  (:use :cl :lcl/lua :lcl/compiler)
  (:export
   #:compile-stdlib-file-to-lua
   #:compile-all-to-lua-file
   #:lambda-get-pos
   #:lambda-grab-optional
   #:lambda-get-optional
   #:lambda-get-rest
   #:lambda-grab-key
   #:lambda-get-key
   #:lambda-grab-aux
   #:lambda-get-aux
   #:bootstrap))
