(in-package #:lcl/bootstrap)

#+sbcl
(defparameter *guest-macros* (make-hash-table))

#+sbcl
(defparameter *guest-functions* (make-hash-table :test #'equal))

#+sbcl
(defun traverse-quotes (m)
  "Remove the quasiquotation from the read code"
  (cond
    ;; FIXME this is specific to SBCL
    ((and (listp m) (eq (car m) 'sb-int:quasiquote))
     (traverse-quotes (macroexpand-1 m)))
    ((listp m) (mapcar #'traverse-quotes m))
    (t m)))

(defun rename-symbols (symbol)
  (if (symbolp symbol)
      (if (str:starts-with-p "LCL" (package-name (symbol-package symbol)))
          (intern (symbol-name symbol) :lcl/bootstrap)
          symbol)
      symbol))

(defun lcl/compiler::function-call-to-ir (ast env parent)
  ;; TODO Local functions.
  (or
   (let ((arglist (gethash (car ast) *guest-functions* :undefined)))
     (and (not (eq arglist :undefined))
          (lcl/compiler::gen-short-call-to-ir ast env parent arglist)))
   (progn
     ;; Already covering most of the funcall delay.
     ;; (warn "Can't fold function: ~S" ast)
     nil)
   (lcl/compiler::long-function-call-to-ir ast env parent)))

;; SBCL replaces the function to provide additionnal context for compiling the stdlib
#+sbcl
(defun lcl/compiler::call-to-ir (ast env parent)
  (let ((func (car ast))
         (args (cdr ast)))
    (when (and (eq func 'defun) (consp (car args)) (eq 'setf (caar args)))
       (setf (gethash (cadar args) *lua-setf-functions*) t))
    (when (eq func 'defun)
      (setf (gethash (rename-symbols (first args)) *guest-functions*)
            (second args)))
    (cond
      ((eq func 'defmacro)
       ;; Register the macrofunction
       (let* ((macro (car args))
              (arglist (cadr args))
              (body (traverse-quotes (cddr args)))
              (lbd (list* 'lambda (substitute '&rest '&body arglist) body)))
         (setf (gethash macro *guest-macros*)
               (handler-bind ((style-warning #'muffle-warning))
                 (eval lbd)))
         (lcl/compiler::compile-to-ir
          (list 'setq (list 'macro-function (list 'quote macro))
                (list 'function
                      (list 'lambda '(form &optional env) (list 'apply (list 'function (traverse-quotes lbd)) '(cdr form)))))
          env parent)))
      ;; TODO Register setf function
      ((eq func 'sb-int:quasiquote)
       (lcl/compiler::compile-to-ir (traverse-quotes ast) env parent))
      ((gethash func *guest-macros*)
       (lcl/compiler::compile-to-ir (apply (gethash func *guest-macros*) args) env parent))
      (t (lcl/compiler::function-call-to-ir ast env parent)))))
