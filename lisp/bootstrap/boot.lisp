(in-package #:lcl/bootstrap)

;; Some macros use this function defined within the stdlib itself.
(defun %stringp (string) (stringp string))

(defvar *eval-when-compile-or-load* nil)
