(in-package :lcl/java)

(defun class (class-name)
  "Get the class object my name."
  (funcall (l "luajava" "bindClass") (string class-name)))

(defmacro new (class-name &rest arguments)
  "Instantiate the Java class named CLASS-NAME, with the given ARGUMENTS"
  `(l "luajava" "newInstance" (,class-name ,@arguments)))

(defmacro proxy (interface-name &bind bindings)
  "Create a proxy interface.
The bindings have the following format:
(name (8rest arglist) &body body)"
  `(l "luajava" "createProxy" (,interface-name
                              (table ,@(mapcan (lambda (q) (list (string (car q)) (cons 'lambda (cdr q)))))))))
