(defpackage #:lcl/java
  (:use :cl :lcl/l)
  (:export
   #:class
   #:new
   #:proxy))
