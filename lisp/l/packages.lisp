(defpackage #:lcl/l
  (:use :cl :cl-lib)
  (:nicknames :l)
  (:export
   #:l
   #:@
   #:table))
