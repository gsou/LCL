(in-package :lcl/l)

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defun element-to-lua (form &optional (index t))
    (etypecase form
      (string
       (let ((start (char form 0)))
         (if (or (char= start #\.) (char= start #\:))
             (values (list (string (subseq form 1))) nil (string start))
             (values (list (string form)) nil "."))))
      (number (values (list (cl-lib:number-to-lua form) ) nil))
      (list
       (if index
           (let ((vars (mapcar (lambda (x) (gensym)) form)))
             (values (concatenate 'list (list "(") (cdr (mapcan (lambda (x) (list "," x)) vars)) (list ")"))
                     (mapcar #'list vars form)
                     ""))
           (let ((val (gensym "SOURCE"))) (values (list val) (list (list val form))))))
      (symbol (let ((val (gensym "ELEMENT")))
                (values (list val) `((,val ,form)))))))

  (defun l-to-lua (forms)
    (let ((eq (position 'cl:= forms)))
      (if eq
          ;; Assignment
          (let ((result (gensym "RESULT")))
            (multiple-value-bind (lvalue lvars) (l-to-lua (subseq forms 0 eq))
              (multiple-value-bind (rvalue rvars) (l-to-lua (subseq forms (1+ eq)))
                (values
                 (concatenate 'list lvalue (list " = ") rvalue)
                 (concatenate 'list lvars rvars)
                 t))))
          ;; Index sequence
          (let ((acc nil)
                (vars nil)
                (ii nil))
            (dolist (form forms)
              (if (eq form '@)
                  (setq ii nil)
                  (multiple-value-bind (strs new-vars index) (element-to-lua form ii)
                    (setq ii t)
                    (setq vars (concatenate 'list vars new-vars))
                    (setq acc (concatenate 'list acc
                                           (if acc (if index
                                                       (cons index strs)
                                                       (concatenate 'list (list "[") strs (list "]")))
                                               strs))))))
            (values acc vars nil))))))

(defmacro l! (&rest forms)
  (multiple-value-bind (binds vars push) (l-to-lua forms)
    (if push
        `(let ,vars (cl-lib:lua-push (,@binds) nil))
        `(let ,vars (cl-lib:lua ,@binds)))))

(defmacro l (&rest forms)
  "Lua expression generation macro. Each form can be:
- A string
  In this case, the string is taken as a raw Lua expression.
  Subsequent elements are indexed.
- A string starting with : or .
  Index, but using : or . specifically
- The symbol CL:=
  Maks the forms to the left as an lvalue and the forms
  to the right as an rvalue. Only the first = is considered
- An atom
  The value of the atom is used as a source or index
- A splice (@ lisp)
  The result of the evaluation is taken as the index (or source) value
- A list
  Indicates to lua-call the given value, the inner elements
  are evaluated as lisp."
  `(or (values (l! ,@forms)) nil))

(defconstant true (l "true") "Lua true")
(defconstant false (l "false") "Lua false")
(defconstant lnil (l "nil") "Lua nil (DANGEROUS)")

(defun rawget (table index) (funcall (l "rawget") table index))
(defun rawset (table index value) (funcall (l "rawset") table index value))
(defun (setf rawget) (new-value table index)
  (rawset table index new-value)
  new-value)

(defun type (object) (l "type" (object)))

(defun lfunction (function)
  "Retrieve the raw Lua function of FUNCTION"
  (cond
    ((functionp function) (l function ".func"))
    ((string= "function" (type funtion)) function)
    (t (error 'type-error :form 'function :datum function :expected-type 'function))))


(defun metatable (table)
  "Retrieve the Lua metatable of TABLE"
  (l "getmetatable" (table)))
(defun (setf metatable) (new-value table)
  (l "setmetatable" (table new-value))
  new-value)

(defun _G () "The global environment" (l "_G"))
(defun pcall (function &rest arguments)
  "Returns (values TRUE . result-values) if function returns locally,
or (values NIL error) if the function returns non-locally."
  (apply (l "pcall") function arguments))
(defun lprint (&rest prints)
  "Call Lua's print on the given arguments"
  (apply (l "print") prints))

(defun table (&rest plist)
  "Create a Lua table.
The content of the table is initialized with
the content of the PLIST argument."
  (if plist
      (let ((tbl (cl-lib::new)))
        (tagbody
         next
           (l tbl @(first plist) = @(second plist))
           (setq plist (cddr plist))
           (unless plist (return-from table tbl))
           (go next)))
      (cl-lib::new)))

;; table.concat, insert, move, pack, remove, sort, unpack
