(in-package :lcl/l)

(defun coroutine-create (function) (l "coroutine" "create" ((lfunction function))))
(defun coroutine-wrap (function) (l "coroutine" "wrap" ((lfunction function))))

(defun coroutinep (coroutine) (string= "thread" (type coroutine)))

(defun coroutine-resume (coroutine &rest arguments)
  "Start or resume the COROUTINE"
  (if (coroutinep coroutine)
      (apply (l "coroutine" "resume") coroutine arguments)
      (error 'type-error :form 'coroutine :datum coroutine :expected-type 'coroutine)))

(defun coroutine-yield (&rest values)
  "Yield from the current COROUTINE."
  (apply (l "coroutine" "yield") values))

(defun coroutine-isyieldable (&optional (coroutine nil coroutine-p))
  (if coroutine-p
   (if (coroutinep coroutine)
       (l "coroutine" "isyieldable" (coroutine))
       (error 'type-error :form 'coroutine :datum coroutine :expected-type 'coroutine))
   (l "coroutine" "isyieldable" ())))

(defun coroutine-status (coroutine)
  (if (coroutinep coroutine)
      (l "coroutine" "status" (coroutine))
      (error 'type-error :form 'coroutine :datum coroutine :expected-type 'coroutine)))

(defun coroutine-running () (l "coroutine" "running" ()))
