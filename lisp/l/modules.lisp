(in-package :lcl/l)

(defun lrequire (modname) (l "require" (modname)))
