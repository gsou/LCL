(in-package :lcl/clos)


(defun subclassp (c1 c2)
  (not (null (find c2 (class-precedence-list c1)))))

(defun sub-specializer-p (c1 c2 c-arg)
  (cond
    ((and (listp c1) (listp c2)) nil)
    ((and (listp c1)) t)
    ((and (listp c2)) nil)
    (t (let ((cpl (class-precedence-list c-arg)))
         (not (null (find c2 (cdr (member c1 cpl)))))))))

(defparameter the-defclass-standard-class ;standard-class's defclass form
  '(defclass standard-class ()
    ((name :initarg :name)              ; :accessor class-name
     (direct-superclasses               ; :accessor class-direct-superclasses
      :initarg :direct-superclasses)
     (direct-slots)                     ; :accessor class-direct-slots
     (class-precedence-list)            ; :accessor class-precedence-list
     (effective-slots)                  ; :accessor class-slots
     (index-metatable)
     (direct-subclasses :initform ())   ; :accessor class-direct-subclasses
     (direct-methods :initform ()))))   ; :accessor class-direct-methods



;;;; Basic class operation

(defun class-name (class)
  "Get the symbol that names CLASS."
  (std-slot-value class 'name))

(defun (setf class-name) (new-value class)
  "Sets the symbol that names CLASS"
  ;; This does not update *class-table*
  (setf (std-slot-value class 'name) new-value))

(defun class-direct-superclasses (class)
  (slot-value class 'direct-superclasses))

(defun (setf class-direct-superclasses) (new-value class)
  (setf (slot-value class 'direct-superclasses) new-value))

(defun class-direct-slots (class)
  (slot-value class 'direct-slots))

(defun (setf class-direct-slots) (new-value class)
  (setf (slot-value class 'direct-slots) new-value))

(defun class-precedence-list (class)
  (slot-value class 'class-precedence-list))

(defun (setf class-precedence-list) (new-value class)
  (setf (slot-value class 'class-precedence-list) new-value))

(defun class-slots (class)
  (slot-value class 'effective-slots))

(defun (setf class-slots) (new-value class)
  (setf (slot-value class 'effective-slots) new-value))

(defun class-direct-subclasses (class)
  (slot-value class 'direct-subclasses))

(defun (setf class-direct-subclasses) (new-value class)
  (setf (slot-value class 'direct-subclasses) new-value))

(defun class-direct-methods (class)
  (slot-value class 'direct-methods))

(defun (setf class-direct-methods) (new-value class)
  (setf (slot-value class 'direct-methods) new-value))


;;;; Find-class

(defparameter *class-table* (make-hash-table :test #'eq)
  "Global table storing the class objects.")

(defun find-class (symbol &optional (errorp t) environment)
  "Find the class object from its name."
  (declare (ignore environment))
  (let ((class (gethash symbol *class-table* nil)))
    (if (and (null class) errorp)
        (error "No class named ~S." symbol)
        class)))
(defun %set-find-class (symbol new-value)
  (setf (gethash symbol *class-table*) new-value))
(defsetf find-class %set-find-class)

(defun forget-all-classes () (clrhash *class-table*))


;;;; defclass macro

(defun canonicalize-direct-superclasses (direct-superclasses)
  `(list ,@(mapcar #'canonicalize-direct-superclass direct-superclasses)))
(defun canonicalize-direct-superclass (class-name)
  `(find-class ',class-name))
(defun canonicalize-defclass-option (option)
  (case (car option)
    (:metaclass
     (list ':metaclass
           `(find-class ',(cadr option))))
    (:default-initargs
     (list
      ':direct-default-initargs
      `(list ,@(mapappend
                #'(lambda (x) x)
                (mapplist
                 #'(lambda (key value)
                     `(',key ,value))
                 (cdr option))))))
    (t (list `',(car option) `',(cadr option)))))
(defun canonicalize-defclass-options (options)
  (mapappend #'canonicalize-defclass-option options))
(defun canonicalize-direct-slots (direct-slots)
  `(list ,@(mapcar #'canonicalize-direct-slot direct-slots)))
(defun canonicalize-direct-slot (spec)
  (if (symbolp spec)
      `(list :name ',spec)
      (let ((name (car spec))
            (initfunction nil)
            (initform nil)
            (initargs ())
            (readers ())
            (writers ())
            (other-options ()))
        (do ((olist (cdr spec) (cddr olist)))
            ((null olist))
          (case (car olist)
            (:initform
             (setq initfunction
                   `(function (lambda () ,(cadr olist))))
             (setq initform `',(cadr olist)))
            (:initarg
             (push-on-end (cadr olist) initargs))
            (:reader
             (push-on-end (cadr olist) readers))
            (:writer
             (push-on-end (cadr olist) writers))
            (:accessor
             (push-on-end (cadr olist) readers)
             (push-on-end `(setf ,(cadr olist)) writers))
            (otherwise
             (push-on-end `',(car olist) other-options)
             (push-on-end `',(cadr olist) other-options))))
        `(list
          :name ',name
          ,@(when initfunction
              `(:initform ,initform
                :initfunction ,initfunction))
          ,@(when initargs `(:initargs ',initargs))
          ,@(when readers `(:readers ',readers))
          ,@(when writers `(:writers ',writers))
          ,@other-options))))

(defun ensure-class (name &rest all-keys
                          &key (metaclass the-class-standard-class)
                          &allow-other-keys)
  (if (find-class name nil)
      (progn (warn "Can't redefine the class named ~S." name) (find-class name nil))
      (let ((class (apply (if (eq metaclass the-class-standard-class)
                              #'make-instance-standard-class
                              #'make-instance)
                          metaclass :name name all-keys)))
        (setf (find-class name) class)
        class)))

(defmacro defclass (name direct-superclasses direct-slots &rest options)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (ensure-class ',name
                   :direct-superclasses
                   ,(canonicalize-direct-superclasses direct-superclasses)
                   :direct-slots
                   ,(canonicalize-direct-slots direct-slots)
                   ,@(canonicalize-defclass-options options))))


;;;; Class instantiation

(defun create-instance-metatable (class table)
  (dolist (c (class-direct-superclasses class))
    (create-instance-metatable c table))
  (dolist (s (class-direct-slots class))
    (lua-set-table table (slot-definition-name s) +slot-unbound+))
  table)

(defun create-or-get-class-instance-metatable (class)
  "Create (or get) a table that will be set as the metatable of the slots table."
  (if (std-slot-boundp class 'index-metatable)
      (std-slot-value class 'index-metatable)
      (let* ((table (create-instance-metatable class (new)))
             (ix-table (new)))
        (lua-set-table ix-table "__index" table)
        (setf (std-slot-value class 'index-metatable) ix-table)
        ix-table)))

(defun std-after-initialization-for-classes
       (class &key direct-superclasses direct-slots &allow-other-keys)
  (let ((supers
          (or direct-superclasses
              (list (find-class 'standard-object)))))
    (setf (class-direct-superclasses class) supers)
    (dolist (superclass supers)
      (push class (class-direct-subclasses superclass))))
  (let ((slots
          (mapcar #'(lambda (slot-properties)
                      (apply #'make-direct-slot-definition
                             slot-properties))
                    direct-slots)))
    (setf (class-direct-slots class) slots)
    (dolist (direct-slot slots)
      (dolist (reader (slot-definition-readers direct-slot))
        (add-reader-method
          class reader (slot-definition-name direct-slot)))
      (dolist (writer (slot-definition-writers direct-slot))
        (add-writer-method
          class writer (slot-definition-name direct-slot)))))
  (funcall (if (eq (class-of class) the-class-standard-class)
              #'std-finalize-inheritance
              #'finalize-inheritance)
           class)
  (values))

(defun make-instance-standard-class (metaclass &key name direct-superclasses direct-slots &allow-other-keys)
  "Create an instance of a standard-class"
  (declare (ignore metaclass))
  (let ((class (allocate-std-instance the-class-standard-class)))
    (setf (class-name class) name)
    (setf (class-direct-subclasses class) ())
    (setf (class-direct-methods class) ())
    (std-after-initialization-for-classes class
                                          :direct-slots direct-slots
                                          :direct-superclasses direct-superclasses)
    class))


(defun std-tie-breaker-rule (minimal-elements cpl-so-far)
  "In the event of a tie while topologically sorting class precedence lists, the
CLOS Specification says to \"select the one that has a direct subclass rightmost
in the class precedence list computed so far.\"  The same result is obtained by
inspecting the partially constructed class precedence list from right to left,
looking for the first minimal element to show up among the direct superclasses
of the class precedence list constituent. (There's a lemma that shows that
this rule yields a unique result.)"
  (block rule
    (dolist (cpl-constituent (reverse cpl-so-far))
      (let* ((supers (class-direct-superclasses cpl-constituent))
             (common (intersection minimal-elements supers)))
        (when (not (null common))
          (return-from rule (car common)))))))

;;; This version of collect-superclasses* isn't bothered by cycles in the class
;;; hierarchy, which sometimes happen by accident.
(defun collect-superclasses* (class)
  (labels ((all-superclasses-loop (seen superclasses)
             (let ((to-be-processed
                     (set-difference superclasses seen)))
               (if (null to-be-processed)
                   superclasses
                   (let ((class-to-process
                           (car to-be-processed)))
                     (all-superclasses-loop
                      (cons class-to-process seen)
                      (union (class-direct-superclasses
                              class-to-process)
                             superclasses)))))))
    (all-superclasses-loop () (list class))))

;;; The local precedence ordering of a class C with direct superclasses C_1,
;;; C_2, ..., C_n is the set ((C C_1) (C_1 C_2) ...(C_n-1 C_n)).
(defun local-precedence-ordering (class)
  (mapcar #'list
          (cons class
                (butlast (class-direct-superclasses class)))
          (class-direct-superclasses class)))

(defun std-compute-class-precedence-list (class)
  (let ((classes-to-order (collect-superclasses* class)))
    (topological-sort classes-to-order
                      (remove-duplicates
                        (mapappend #'local-precedence-ordering
                                   classes-to-order))
                      #'std-tie-breaker-rule)))


(defun std-compute-effective-slot-definition (class direct-slots)
  (declare (ignore class))
  (let ((initer (find-if-not #'null direct-slots
                             :key #'slot-definition-initfunction)))
    (make-effective-slot-definition
     :name (slot-definition-name (car direct-slots))
     :initform (if initer
                   (slot-definition-initform initer)
                   nil)
     :initfunction (if initer
                       (slot-definition-initfunction initer)
                       nil)
     :initargs (remove-duplicates
                (mapappend #'slot-definition-initargs
                           direct-slots))
     :allocation (slot-definition-allocation (car direct-slots)))))
(defun std-compute-slots (class)
  (let* ((all-slots (mapappend #'class-direct-slots
                               (class-precedence-list class)))
         (all-names (remove-duplicates
                      (mapcar #'slot-definition-name all-slots))))
    (mapcar #'(lambda (name)
                (funcall
                  (if (eq (class-of class) the-class-standard-class)
                      #'std-compute-effective-slot-definition
                      #'compute-effective-slot-definition)
                  class
                  (remove name all-slots
                          :key #'slot-definition-name
                          :test-not #'eq)))
            all-names)))

(defun std-finalize-inheritance (class)
  (setf (class-precedence-list class)
        (funcall (if (eq (class-of class) the-class-standard-class)
                     #'std-compute-class-precedence-list
                     #'compute-class-precedence-list)
                 class))
  (setf (class-slots class)
        (funcall (if (eq (class-of class) the-class-standard-class)
                     #'std-compute-slots
                     #'compute-slots)
                 class))
  (values))
