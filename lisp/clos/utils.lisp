;;;
;;; Utilities
;;;

(in-package :lcl/clos)
;; (in-package :closette)

;;; push-on-end is like push except it uses the other end:

(eval-when (:compile-toplevel :execute)
  (defmacro push-on-end (value location)
    `(setf ,location (nconc ,location (list ,value))))

;;; (setf getf*) is like (setf getf) except that it always changes the list,
;;;              which must be non-nil.

  (defun setgetf* (plist key new-value)
    (let ((none (gensym)))
      (if (eq (getf plist key none) none)
          (progn
            (push-on-end key plist)
            (push-on-end new-value plist))
          (setf (getf plist key) new-value)))))

(defsetf getf* setgetf*)

;;; mapappend is like mapcar except that the results are appended together:

(defun mapappend (fun &rest args)
  (if (some #'null args)
      ()
      (append (apply fun (mapcar #'car args))
              (apply #'mapappend fun (mapcar #'cdr args)))))

;;; mapplist is mapcar for property lists:

(defun mapplist (fun x)
  (if (null x) ()
      (cons (funcall fun (car x) (cadr x))
            (mapplist fun (cddr x)))))


;; Add the print-unreadable-object macro
(defmacro cl::print-unreadable-object (params &body body)
  (destructuring-bind (object stream &key type identity) params
    (let ((o (gensym "object"))
          (s (gensym "stream")))
      `(let ((,o ,object)
             (,s ,stream))
         (prog2
             (progn
               (write-string "#<" ,s)
               (when ,type (write-string (string (class-name (class-of ,o))) ,s))
               (write-string " " ,s))
             (progn ,@body)
           (write-string " " ,s)
           ;; TODO
           ;; (when ,identity )
           (write-string ">" ,s))))))

(defun topological-sort (elements constraints tie-breaker)
  "TOPOLOGICAL-SORT implements the standard algorithm for topologically sorting
an arbitrary set of ELEMENTS while honoring the precedence CONSTRAINTS given by
a set of (X,Y) pairs that indicate that element X must precede element Y.  The
TIE-BREAKER procedure is called when it is necessary to choose from multiple
minimal elements; both a list of candidates and the ordering so far are provided
as arguments."
  (let ((remaining-constraints constraints)
        (remaining-elements elements)
        (result ()))
    (cl-lib::%loop
     (let ((minimal-elements
             (remove-if
              #'(lambda (class)
                  (member class remaining-constraints
                          :key #'cadr))
              remaining-elements)))
       (when (null minimal-elements)
         (if (null remaining-elements)
             (cl-lib:%return result)
             (error "Inconsistent precedence graph.")))
       (let ((choice (if (null (cdr minimal-elements))
                         (car minimal-elements)
                         (funcall tie-breaker
                                  minimal-elements
                                  result))))
         (setq result (append result (list choice)))
         (setq remaining-elements
               (remove choice remaining-elements))
         (setq remaining-constraints
               (remove choice
                       remaining-constraints
                       :test #'member)))))))
