(in-package :lcl/clos)


;;;; Slot definition

(defstruct slot-definition
  name
  (initfunction nil)
  (initform nil)
  (initargs ())
  (readers ())
  (writers ())
  (type t)
  (allocation :instance)
  documentation)

(defun make-direct-slot-definition
       (&rest properties
        &key name (initargs ()) (initform nil) (initfunction nil)
             (readers ()) (writers ()) (allocation :instance)
        &allow-other-keys)
  (apply #'make-slot-definition :allow-other-keys t properties))

(defun make-effective-slot-definition
       (&rest properties
        &key name (initargs ()) (initform nil) (initfunction nil)
             (allocation :instance))
  (apply #'make-slot-definition :allow-other-keys t properties))
