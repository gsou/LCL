
(in-package :lcl/clos)


;;;; Standard Instances
(defconstant instance-metatable 'instance-metatable
  "Metatable used to identify table corresponding to CLOS instances.")
(lua-set-table instance-metatable "instance" t)


;;;; Basic operations on standard instances.

(defparameter +slot-unbound+ (new)
  "This is a unique value (according to Lua's ==) to indicate that the slot is unbound.")
(defparameter +slot-missing+ (new)
  "This is a unique value (according to Lua's ==) to indicate that the slot is missing.")

(defparameter +undef-table-metatable+ (let ((table (new))) (lua-set-table table "__index" (cl-lib:lua "function (table, index) return LCL['LCL/CLOS']['+SLOT-UNBOUND+'] end"))
                                        table))

;; Stubs, will be replaced
(defun slot-missing (class object slot-name operation &optional new-value)
  (error "~A: The slot ~S is missing in the object ~S." operation slot-name object))
(defun slot-unbound (class object slot-name)
  (error "SLOT-VALUE: The slot ~S is missing in the object ~S." slot-name object))

(defun allocate-std-instance (class)
  "Create a table instance of class CLASS."
  (let ((this (new instance-metatable)))
    (lua-set-table this "class" class)
    (lua-set-table this "slots" (new (if class (create-or-get-class-instance-metatable class) +undef-table-metatable+)))
    this))

(defun instance-p (instance) (eq instance-metatable (lua-get-metatable instance)))
(defun std-instance-p (instance) (and (instance-p instance) (eq the-class-standard-class (lua-index-table (lua-index-table instance "class") "class"))))

(defun std-slot-value (instance slot-name)
  "Retrieve the value of a slot (by name) within the instance."
  (if (instance-p instance)
      (let ((value (lua-index-table (lua-index-table instance "slots") slot-name +slot-missing+)))
        (if (eq value +slot-unbound+)
            (slot-unbound (class-of instance) instance slot-name)
            (if (eq value +slot-missing+)
                (slot-missing (class-of instance) instance slot-name 'slot-value)
                value)))
      (error 'simple-type-error
             :format-control "SLOT-VALUE: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun (setf std-slot-value) (new-value instance slot-name)
  "Writes the value of a slot (by name) within the instance"
  (if (instance-p instance)
      (let ((value (lua-index-table (lua-index-table instance "slots") slot-name +slot-missing+)))
        (if (eq value +slot-missing+)
            (slot-missing (class-of instance) instance slot-name 'setf)
            (lua-set-table (lua-index-table instance "slots") slot-name new-value))
        new-value)
      (error 'simple-type-error
             :format-control "(SETF SLOT-VALUE): ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun std-slot-boundp (instance slot-name)
  (if (instance-p instance)
      (let ((value (lua-index-table (lua-index-table instance "slots") slot-name +slot-missing+)))
        (if (eq value +slot-missing+)
            (slot-missing (class-of instance) instance slot-name 'slot-boundp)
            (not (eq value +slot-unbound+))))
      (error 'simple-type-error
             :format-control "SLOT-BOUNDP: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun std-slot-makunbound (instance slot-name)
  (if (instance-p instance)
      (let ((value (lua-index-table (lua-index-table instance "slots") slot-name +slot-missing+)))
        (if (eq value +slot-missing+)
            (slot-missing (class-of instance) instance slot-name 'slot-makunbound)
            (setf (std-slot-value instance slot-name) +slot-unbound+)))
      (error 'simple-type-error
             :format-control "SLOT-MAKUNBOUND: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance))
  instance)
(defun std-slot-exists-p (instance slot-name)
  (if (instance-p instance)
      (eq +slot-missing+ (lua-index-table (lua-index-table instance "slots") slot-name +slot-missing+))
      (error 'simple-type-error
             :format-control "SLOT-EXISTS-P: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun std-instance-class (instance)
  (if (instance-p instance)
      (lua-index-table instance "class")
      (error 'simple-type-error
             :format-control "CLASS-OF: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun (setf std-instance-class) (new-class instance)
  (if (instance-p instance)
      (progn
        ;; XXX Check if class is class
        (lua-set-table instance "class" new-class)
        ;; Only happen during bootstrapping (creating instances with no classes).
        (let ((slots (lua-index-table instance "slots")))
          (when (eq +undef-table-metatable+ (lua-get-metatable slots))
            (funcall (cl-lib:lua "setmetatable") slots (create-or-get-class-instance-metatable new-class))))
        new-class)
      (error 'simple-type-error
             :format-control "(SETF CLASS-OF): ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))

(defun std-instance-slots (instance)
  (if (instance-p instance)
      (lua-index-table instance "slots")
      (error 'simple-type-error
             :format-control "STD-INSTANCE-SLOTS: ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))
(defun (setf std-instance-slots) (new-slots instance)
  (if (instance-p instance)
      (progn
        (lua-set-table instance "slots" new-class)
        new-slots)
      (error 'simple-type-error
             :format-control "(SETF STD-INSTANCE-SLOTS): ~S not an object"
             :format-arguments (list instance)
             :form 'instance :datum instance :expected-type 'standard-instance)))



;;;; Generic instance operations

(defun slot-value (object slot-name)
  (if (eq (class-of (class-of object)) the-class-standard-class)
      (std-slot-value object slot-name)
      (slot-value-using-class (class-of object) object slot-name)))

(defun (setf slot-value) (new-value object slot-name)
  (if (eq (class-of (class-of object)) the-class-standard-class)
      (setf (std-slot-value object slot-name) new-value)
      (setf-slot-value-using-class
       new-value (class-of object) object slot-name)))

(defun slot-boundp (object slot-name)
  (if (eq (class-of (class-of object)) the-class-standard-class)
      (std-slot-boundp object slot-name)
      (slot-boundp-using-class (class-of object) object slot-name)))

(defun slot-makunbound (object slot-name)
  (if (eq (class-of (class-of object)) the-class-standard-class)
      (std-slot-makunbound object slot-name)
      (slot-makunbound-using-class (class-of object) object slot-name)))

(defun slot-exists-p (object slot-name)
  (if (eq (class-of (class-of object)) the-class-standard-class)
      (std-slot-exists-p object slot-name)
      (slot-exists-p-using-class (class-of object) object slot-name)))



(defun built-in-class-of (x)
  "Retrieve the class of a non clos object X"
  (typecase x
    (null                                          (find-class 'null))
    ((and symbol (not null))                       (find-class 'symbol))
    ((complex *)                                   (find-class 'complex))
    ((integer * *)                                 (find-class 'integer))
    ((float * *)                                   (find-class 'float))
    (rational                                      (find-class 'rational))
    (cons                                          (find-class 'cons))
    (character                                     (find-class 'character))
    (hash-table                                    (find-class 'hash-table))
    (package                                       (find-class 'package))
    (pathname                                      (find-class 'pathname))
    (readtable                                     (find-class 'readtable))
    (stream                                        (find-class 'stream))
    ((and number (not (or integer complex float))) (find-class 'number))
    ((string *)                                    (find-class 'string))
    ((bit-vector *)                                (find-class 'bit-vector))
    ((and (vector * *) (not (or string vector)))   (find-class 'vector))
    ((and (array * *) (not vector))                (find-class 'array))
    ((and sequence (not (or vector list)))         (find-class 'sequence))
    (function                                      (find-class 'function))
    (random-state                                  (find-class 'random-state))
    (structure-object                              (structure-class (lua-index-table x "name")))
    (t                                             (find-class 't))))

(defun class-of (x)
  (if (std-instance-p x)
      (std-instance-class x)
      (built-in-class-of x)))
