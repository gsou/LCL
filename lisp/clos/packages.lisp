(eval-when (:compile-toplevel :load-toplevel :execute)
  (defpackage #:lcl/clos
    (:use #:CL #:CL-LIB)
    (:nicknames #:closette)
    (:export
     #:class-direct-superclasses #:class-direct-slots #:class-precedence-list
     #:class-slots #:class-direct-subclasses #:class-direct-methods

     #:generic-function-name #:generic-function-lambda-list
     #:generic-function-methods #:generic-function-discriminating-function
     #:generic-function-method-class
     #:method-lambda-list #:method-qualifiers #:method-specializers #:method-body
     #:method-environment #:method-generic-function #:method-function
     #:slot-definition-name #:slot-definition-initfunction
     #:slot-definition-initform #:slot-definition-initargs
     #:slot-definition-readers #:slot-definition-writers
     #:slot-definition-allocation

    #:compute-class-precedence-list #:compute-slots
    #:compute-effective-slot-definition
    #:finalize-inheritance #:allocate-instance
    #:slot-value-using-class #:slot-boundp-using-class
    #:slot-exists-p-using-class #:slot-makunbound-using-class

    #:compute-discriminating-function
    #:compute-applicable-methods-using-classes #:method-more-specific-p
    #:ensure-generic-function
    #:add-method #:remove-method #:find-method
    #:compute-effective-method-function #:compute-method-function
    #:apply-methods #:apply-method
    #:find-generic-function

    #:instance-p
    #:subclassp))

  (import '(cl-lib:lua-set-table
            cl-lib:lua-index-table
            cl-lib:lua-get-metatable
            cl-lib:lua-set-metatable
            cl-lib:new)
          '#:lcl/clos))
