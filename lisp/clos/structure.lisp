(in-package :lcl/clos)

(defclass structure-class (standard-class) ()
    (:documentation "Metaclass applied to the structure-object"))

(defclass structure-object (t) ()
  (:metaclass structure-class)
  (:documentation "Parent class for all structures"))

(defparameter *structure-classes* (make-hash-table))

(defun generate-structure-class (structure-name &optional parents)
  (or (gethash structure-name *structure-classes*)
      (setf (gethash structure-name *structure-classes*)
            (ensure-class structure-name
                          :direct-superclasses `(,@(mapcar #'find-class parents) ,(find-class 'structure-object))
                          :direct-slots ()
                          :metaclass (find-class 'structure-class)))))

(defun structure-class (structure-name)
  "Generate the structure class for a structure named STRUCTURE-NAME."
  (if (subtypep structure-name 'structure-object)
      (generate-structure-class structure-name ())
      (error "STRUCTURE-CLASS: ~S not a structure designator" structure-name)))

;; On structure creation, create the class as well.
(setf (fdefinition 'cl-lib::defstruct-pre-init) #'generate-structure-class)
