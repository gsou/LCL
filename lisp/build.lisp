;; Build LCL from LCL
(in-package :cl-lib)

(defun copy-file (stream source)
  "Copy the file in SOURCE to the STREAM."
  (write-string (concat-string "Generated " (namestring source) "
"))
  (with-open-file (i source)
    (tagbody
     next
       (let ((line (read-line i nil)))
         (when line
           (write-line line stream)
           (go next))))))

(defun add-feature (stream feature)
  "Add the feature FEATURE to the list of feature that will be loaded within the result."
  (write-string (compile-to-lua (list 'push feature '*features*) nil) stream)
  (write-char #\Newline stream))

(defun stdlib ()
  (mapcar
   (lambda (path) (concat-string "stdlib/" path ".lisp"))
   '("init" "init2" "native" "variables" "list"
     "base" "hash" "predicate" "math" "type"
     "string" "compile" "destructuring"
     "eval" "setf" "loop" "array" "sequence"
     "setf-expanders" "struct" "byte" "stream"
     "reader" "printer" "load" "package" "system"
     "stubs")))

(defun compile-and-generate (stream path)
  (let ((file (compile-file path)))
    (write-string (concat-string "
print('Loading ' .. " (string-to-lua (namestring path)) " .. '...')
") stream)
    (copy-file stream file)
    (write-char #\Newline stream)
    file))

(defun build/lcl-clos (out)
  (flet ((load-compile-and-generate (file)
           #-clos
           (load (compile-and-generate out file))
           #+clos
           (compile-and-generate out file)
           )
         (load-then-compile-and-generate (file)
           #-clos (load file)
           (compile-and-generate out file)))
    (load-compile-and-generate "lisp/clos/packages.lisp")
    (load-compile-and-generate "lisp/clos/utils.lisp")
    (load-compile-and-generate "lisp/clos/slot-definition.lisp")
    (load-then-compile-and-generate "lisp/clos/instance.lisp")
    (load-then-compile-and-generate "lisp/clos/class.lisp")
    (load-then-compile-and-generate "lisp/clos/gf.lisp")
    (load-then-compile-and-generate "lisp/clos/bootstrap.lisp")
    (load-then-compile-and-generate "lisp/clos/final.lisp")
    (load-then-compile-and-generate "lisp/clos/structure.lisp")
    t))

(defun build/clos (out)
  #-clos
  (progn
    (write-string "Loading clos/package.lisp") (terpri)
    (load (compile-and-generate out "clos/package.lisp")))
  #+clos (compile-and-generate out "clos/package.lisp")

  ;; Add clos, next version will have clos.
  (add-feature out :clos)

  ;; Compile the files
  (flet ((load-run (file)
           #-clos
           (progn
             (write-string (concat-string "Loading " file)) (terpri)
             (load file))
           (write-string (concat-string "Compiling " file)) (terpri)
           (compile-and-generate out file)))
    (load-run "clos/utils.lisp")
    (load-run "clos/closette.lisp")
    (load-run "clos/bootstrap.lisp")
    (load-run "clos/closette-final.lisp")))

(with-open-file (out "/tmp/lcl.lua" :direction :output)
  (copy-file out "./LICENSE.lua")
  (copy-file out "./lcl_lib.lua")
  (dolist (file (stdlib)) (compile-and-generate out file))
  (write-string "
LCL['LCL/BOOTSTRAP'] = CL_LIB LCL['LCL/LUA'] = CL_LIB LCL['LCL/COMPILER'] = CL_LIB
" out)
  (do-external-symbols (symbol :cl) (write-string (concat-string "id(CL['" (symbol-name symbol) "'])") out))
  (add-feature out :lcl-lcl)
  (dolist (file '(
                  ;; "lisp/lua/packages.lisp"
                  "lisp/lua/serialize.lisp"
                  ;; "lisp/compiler/packages.lisp"
                  "lisp/compiler/environment.lisp"
                  "lisp/compiler/compile.lisp"
                  "lisp/compiler/resolve.lisp"
                  "lisp/compiler/emit.lisp"
                  "lisp/compiler/interface.lisp"
                  "lisp/compiler/dump.lisp"
                  ))
    (compile-and-generate out file))
  #+lcl-lcl
  ;; Starting from here only works from a LCL generated LCL.
  (progn
    (build/lcl-clos out)
    ;; (build/clos out)
    (add-feature out :clos)
    #-clos (load "stdlib/conditions.lisp")
    (compile-and-generate out "stdlib/conditions.lisp")
    (load (compile-and-generate out "stdlib/debug.lisp"))

    #-format (load "ext/loop.lisp")
    (compile-and-generate out "ext/loop.lisp")
    ;; #-format (load "ext/cmu_loop.cl")
    ;; (compile-and-generate out "ext/cmu_loop.cl")

    (progn
      #-format
      (eval
       ;; Need to muffle any warnings here, as any warning is likely
       ;; to call format to display itself, but we most likely have an
       ;; incomplete format function during loading.
       '(handler-bind ((warning #'muffle-warning))
         (load "ext/format.lisp")))
      (add-feature out :format)
      (compile-and-generate out "ext/format.lisp"))

    (compile-and-generate out "stdlib/post.lisp")
    (compile-and-generate out "stdlib/main.lisp")

    ;; Bootstrap in the LCL/L library
    (compile-and-generate out "lisp/l/packages.lisp")
    (compile-and-generate out "lisp/l/table.lisp")
    (compile-and-generate out "lisp/l/coroutines.lisp")
    (compile-and-generate out "lisp/l/modules.lisp")

    ;; TOPLEVEL
    (write-string "
if not LCL_LIB then CL_LIB.MAIN() end
" out)))
