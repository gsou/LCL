(in-package #:lcl/lua)
;; NOTE: CROSS COMPILE ONLY

(defun group (list) (if list (acons (car list) (cadr list) (group (cddr list)))))

(defun concat-string (&rest strs)
  (apply #'concatenate 'string (mapcar (lambda (s)
                                         (etypecase s
                                           ;; (null "")
                                           (string s)
                                           (number (format nil "~A" s))))
                                       strs)))

(defun number-to-lua (number)
  "Convert a number to a Lua number literal"
  (if (complexp number)
      (format nil "c(~A,~A)" (realpart number) (imagpart number))
      (if (floatp number)
          (format nil "~,,,,,,'eE" number)
          (format nil "~A" number))))

#-lcl
(defun char-string (str) (format nil "~c" str))

(defun string-to-lua (str)
  "String to a lua literal."
  (let ((acc "\""))
    (dotimes (ix (length str))
     (let ((char (char str ix)))
       (cond
         ;; ((null char))
         ((equal #\" char) (setq acc (concat-string acc "\\\"")))
         ((equal #\\ char) (setq acc (concat-string acc "\\\\")))
         ((equal 10 (char-code char)) (setq acc (concat-string acc "\\n")))
         (t (setq acc (concat-string acc (char-string char)))))))
    (concat-string acc "\"")))

(defun symbol-to-lua (sym &optional env)
  "Get the lua expression that will index a symbol"
  (let ((pn (package-name (symbol-package sym))))
    (cond
      ;; Nil shorthands
      ((not sym) "n")
      ((string= pn "CL-LIB")
       (concat-string "CL_LIB[\"" (symbol-name sym) "\"]"))
      ((string= pn "COMMON-LISP")
       (concat-string "CL[\"" (symbol-name sym) "\"]"))
      (t (concat-string "LCL[\"" (package-name (symbol-package sym)) "\"][\"" (symbol-name sym) "\"]")))))

(defun charp (char) (typep char 'character))
(defun char-to-lua (char)
  "Convert a character to lua code generating the char"
  (concat-string "LCL['CL-LIB']['MAKE-CHAR'](" (string-to-lua (string char)) ")"))

(defun %quote-to-lua (obj env) (error "Can't bootstrap raw LCL object ~S !" obj))

(defun atom-to-lua-p (obj) (or (numberp obj) (stringp obj) (symbolp obj) (charp obj)))
(defun atom-to-lua (obj &optional env)
  (cond
    ((numberp obj) (number-to-lua obj))
    ((#+lcl %stringp #-lcl stringp obj) (string-to-lua obj))
    ((symbolp obj) (symbol-to-lua obj))
    ((charp obj) (char-to-lua obj))
    ;; If we have any other object we could just send it as it at this point
    ;; FIXME ERROR HERE
    (t (%quote-to-lua obj nil))
    ;; (t (lua-error (concat-string "Invalid atom" (tostring obj))))
    ))

(defvar *lua-setf-functions* (make-hash-table))
(defun %is-setf-function-defined (place)
  (and (consp place) (gethash (car place) *lua-setf-functions*)))
(defvar *setf-expander* (make-hash-table))
(defun sethash (key table val) (setf (gethash key table) val))
(defun %register-setf-expander (key function)
  (sethash key *setf-expander* function))
(defun %get-setf-expansion (place &optional env)
  (cond
    ((symbolp place)
     (let ((ss (gensym)))
       (values nil nil (list ss) (list 'setq place ss) place)))
    ((and (listp place) (gethash (car place) *setf-expander*))
     (apply (gethash (car place) *setf-expander*) (cdr place)))
    ((and (listp place) (gethash (car place) *lua-setf-functions*))
     (let ((obj (gensym))
           (new (gensym)))
       ;; Incorrect but ok for bootstrapping
       (values (list obj) (list (cadr place)) (list new)
               `(funcall (function (setf ,(car place))) ,new ,obj ,@(cddr place))
               `(,(car place) ,obj ,@(cddr place)))))
    (t (error "Invalid setf expansion ~A" place))))
(defun get-setf-expansion-or-eval (place &optional env)
  (declare (ignore env))
  (cond
    ((symbolp place)
     (let ((ss (gensym)))
       (values nil nil (list ss) (list 'setq place ss) place)))
    ((and (listp place) (gethash (car place) *setf-expander*))
     (apply (gethash (car place) *setf-expander*) (cdr place)))
    (t (values nil nil nil nil place))))
(defun get-setf-all-expansion (places &optional env)
  (declare (ignore env))
  (let (dummies vals newval setter getter)
    (dolist (p places)
      (multiple-value-bind (d v n s g) (get-setf-expansion-or-eval p)
        (setq dummies (append d dummies))
        (setq vals (append v vals))
        (setq newval (append n newval))
        (setq setter (append (list s) setter))
        (setq getter (append (list g) getter))))
    (values dummies vals newval setter getter)))
(defun gen-sym-all-getters (getters dummies vals)
  (let (syms)
    (dolist (g getters)
      (let ((s (gensym)))
        (push s syms)
        (push s dummies)
        (push g vals)))
    (values syms dummies vals)))

(defparameter *structure-slots-table* (make-hash-table))
(defparameter *structure-slots-type* (make-hash-table))
(defun %traverse-replace (key new list)
  (cond
    ((null list) nil)
    ((atom list) (if (eq list key) new list))
    (t (mapcar #'(lambda (el) (%traverse-replace key new el)) list))))

(defvar *eval-when-enable-compile* nil)
(defvar *eval-when-enable-load* nil)
