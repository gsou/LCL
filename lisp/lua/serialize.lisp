(in-package :lcl/lua)

(defvar *compile-mode* nil)

(defun %cons-quote-to-lua (obj &optional env)
  (cond
    ((consp obj)
     (concat-string (quote-to-lua (car obj) env) "," (%cons-quote-to-lua (cdr obj) env)))
    (t (quote-to-lua obj env))))

(defun serialize-to-lua (name obj &optional env (saved (make-hash-table)))
  "Serialize the object so it is built when the compiled lua code is evaled.
This function is able to compile objects with cycles."
  (if (gethash obj saved)
      (if (atom-to-lua-p obj)
          (concat-string name " = " (atom-to-lua obj) "
")
          (concat-string name " = " (gethash obj saved) "
"))
      (progn
        (setf (gethash obj saved) name)
        (cond
          ;; Serialize setup
          ((atom-to-lua-p obj) (concat-string name " = " (atom-to-lua obj) "
"))
          ;; [ ] TODO Optimize some consp cases.
          ;; [ ] TODO Package
          ;; [ ] TODO Function
          ((typep obj 'hash-table) (%serialize-table-to-lua name obj env saved 'hash-table-metatable))
          ((typep obj 'array) (%serialize-table-to-lua name obj env saved 'array-metatable))
          ((typep obj 'structure-object) (%serialize-table-to-lua name obj env saved 'struct-metatable))
          ((consp obj)
           (concat-string name " = {}
"
                          (serialize-to-lua (concat-string name "[1]") (car obj) env saved)
                          (serialize-to-lua (concat-string name "[2]") (cdr obj) env saved)))
          (t (error "Compile-error: Non-serializable object ~A" obj))))))

(defun compile-mode-eval-add (env value)
  "Create the symbol for passing to evaluation in the q table."
  (if (equal *compile-mode* :eval)
      (concat-string "q[" (vector-push-extend value (env-eval-state env)) "]")
      (compile-error "Trying to add eval element where mode is not an :eval mode")))

(defun force-quote-to-lua (obj env)
  "quote-to-lua but no shorts for atoms."
  (cond
    ((eq *compile-mode* :serialize)
     (let* ((v (env-serializer-state env))
            (id (incf (first v))))
       (setf (second v)
             (concat-string
              (second v)
              " "
              (serialize-to-lua (concat-string "q[" id "]") obj env)))
       (concat-string "q[" id "]")))
    ((eq *compile-mode* :eval) (compile-mode-eval-add env obj))
    ;; Naive compile mode
    ((consp obj) (concat-string "list_star_r({" (%cons-quote-to-lua obj env) "})"))
    ;; At least try to fallback.
    (t (%quote-to-lua obj nil))
    ;; (t (error "Invalid input to quote-to-lua ~A" (lua "dump(" obj ")")))
    ))

(defun quote-to-lua (obj env)
        "Transform an existing read object to a lua string.
Normally we would only link the list to a variable and pass it along and pass everything
else in the same way as above. But now, we sadly have to generate a new list literal from scratch.
We don't have much choice unless we compile in the same lua env as we are running, so this is used
to generate the closest that we can."
  (if (atom-to-lua-p obj)
      (atom-to-lua obj env)
      (force-quote-to-lua obj env)))
