(in-package #:lcl/lua)

;; Special symbols

(defmacro %let ((&rest binds) &body body)
  "Standard CL `let' macro without special variables management."
  (declare (ignore binds body))
  (error "Special operator `%let' must be managed by the compiler."))

(defmacro lua (string)
  "Inject the Lua expression in STRING during compilation."
  (declare (ignore string))
  (error "Special operator `lua' must be managed by the compiler."))

(defmacro lua-push ((&rest push) string)
  "Inject the Lua statements and expression in PUSH and STRING respectively
during compilation."
  (declare (ignore push string))
  (error "Special operator `lua-push' must be managed by the compiler."))

(defmacro while (condition &body body)
  "Loop the BODY as long as CONDITION evaluates truthy."
  (declare (ignore condition body))
  (error "Special operator `while' must be managed by the compiler."))

(defmacro %pairs (key value table &body body)
  "Run the BODY with the KEY and VALUE bound to each pairs in TABLE."
  (declare (ignore key value table body))
  (error "Special operator `%pairs' must be managed by the compiler."))

(defmacro nlambda (name (&rest arglist) &body body)
  "Named lambda"
  (declare (ignore name arglist body))
  (error "Special operator `nlambda' must be managed by the compiler."))
