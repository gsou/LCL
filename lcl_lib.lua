-- Boot Lua code for LCL. This file defines the metatables for the common
-- required basic Common Lisp types and basic functions to manipulate
-- lisp data.

-----------------------
-- TEMPORARY HELPERS --
-----------------------

if bit32 then bit = bit32 end
if bit then
  function bit.intdiv(x,y)
    -- XXX There must be a better way
    return math.floor(x/y)
  end
else
  bit = {}
  load('function bit.intdiv(x,y) return x // y end function bit.bnot(x) return ~x end function bit.band(x,y) return x & y end function bit.bor(x,y) return x | y end function bit.bxor(x,y) return x ~ y end function bit.lshift(x,y) return x << y end function bit.rshift(x,y) return x >> y end')()
end
if not math.pow then
  load('function math.pow(x,y) return x ^ y end')()
end
local _unpack = table.unpack or unpack


function dump(o, maxdepth, car)
  maxdepth = maxdepth or 16
  car = not car
  if getmetatable(o) == CL_LIB["SYMBOL-METATABLE"] then
    return o.package.name .. "." .. o.name
  elseif getmetatable(o) == CL_LIB["FUNCTION-METATABLE"] then
    return "#'" .. tostring(o.debugName) .. dump(o.arglist)
  elseif type(o) == 'table' and getmetatable(o) == nil and maxdepth > 0 then
    if #o == 2 then
      local s = ''
      s = s .. dump(o[1], maxdepth-1)
      if o[2] ~= CL.NIL then
        local cr = false
        if type(o[2]) == 'table' and getmetatable(o[2]) == nil then
          s = s .. ' '
          cr = true
        else
          s = s .. ' . '
        end
        s = s .. dump(o[2], maxdepth, cr)
      end
      if car then
        return '(' .. s ..')'
      else
        return s
      end
    else
      local s = '{ '
      for k,v in pairs(o) do
        if type(k) ~= 'number' then k = '"'..dump(k)..'"' end
        s = s .. '['..k..'] = ' .. dump(v, maxdepth-1) .. ','
      end
      return s .. '} '
    end
  else
    if type(o) == 'string' then
      return '"' .. o .. '"'
    else
      return tostring(o)
    end
  end
end

---------------
-- BOOT CODE --
---------------

LCL = {}

-- Create cl_lib package
LCL["CL-LIB"] = {name = "CL-LIB"}
CL_LIB = LCL["CL-LIB"]

-- Define metatables
CL_LIB["SYMBOL-METATABLE"] = {
  package = CL_LIB,
  name = "SYMBOL-METATABLE"
}

-- alloc-symbol
setmetatable(CL_LIB["SYMBOL-METATABLE"], CL_LIB["SYMBOL-METATABLE"])
function alloc_symbol (package, name)
  local this = {}
  setmetatable(this, CL_LIB["SYMBOL-METATABLE"])
  this.package = package
  this.name = name
  return this
end

-- Call a symbol directly
CL_LIB["SYMBOL-METATABLE"].__call = function (symbol, ...) return symbol.fbound(...) end
CL_LIB["SYMBOL-METATABLE"].__tostring = function (o)
  return o.package.name .. "." .. o.name
end

CL_LIB["SYMBOL-METATABLE"].bound = CL_LIB["SYMBOL-METATABLE"]
CL_LIB["FUNCTION-METATABLE"] = alloc_symbol(CL_LIB, "FUNCTION-METATABLE")
CL_LIB["FUNCTION-METATABLE"].bound = CL_LIB["FUNCTION-METATABLE"]
CL_LIB["MACRO-METATABLE"] = alloc_symbol(CL_LIB, "MACRO-METATABLE")
CL_LIB["MACRO-METATABLE"].bound = CL_LIB["MACRO-METATABLE"]
CL_LIB["CHAR-METATABLE"] = alloc_symbol(CL_LIB, "CHAR-METATABLE")
CL_LIB["CHAR-METATABLE"].bound = CL_LIB["CHAR-METATABLE"]
CL_LIB["PACKAGE-METATABLE"] = alloc_symbol(CL_LIB, "PACKAGE-METATABLE")
CL_LIB["PACKAGE-METATABLE"].bound = CL_LIB["PACKAGE-METATABLE"]
CL_LIB["HASH-TABLE-METATABLE"] = alloc_symbol(CL_LIB, "HASH-TABLE-METATABLE")
CL_LIB["HASH-TABLE-METATABLE"].bound = CL_LIB["HASH-TABLE-METATABLE"]
CL_LIB["STRUCT-METATABLE"] = alloc_symbol(CL_LIB, "STRUCT-METATABLE")
CL_LIB["STRUCT-METATABLE"].bound = CL_LIB["STRUCT-METATABLE"]
CL_LIB["COMPLEX-METATABLE"] = alloc_symbol(CL_LIB, "COMPLEX-METATABLE")
CL_LIB["COMPLEX-METATABLE"].bound = CL_LIB["COMPLEX-METATABLE"]
CL_LIB["RATIONAL-METATABLE"] = alloc_symbol(CL_LIB, "RATIONAL-METATABLE")
CL_LIB["RATIONAL-METATABLE"].bound = CL_LIB["RATIONAL-METATABLE"]
CL_LIB["ARRAY-METATABLE"] = alloc_symbol(CL_LIB, "ARRAY-METATABLE")
CL_LIB["ARRAY-METATABLE"].bound = CL_LIB["ARRAY-METATABLE"]

CL_LIB["FUNCTION-METATABLE"].__call = function (fbound, ...)
  return fbound.func(...)
end
CL_LIB["PACKAGE-METATABLE"].__index = function (table, key)
  -- Fake import CL until packages are supported
  if table ~= KEYWORD and rawget(LCL['COMMON-LISP'], key) then
    return LCL['COMMON-LISP'][key]
  else
    table[key] = alloc_symbol(table, key)
    if table == KEYWORD then table[key].bound = table[key] end
    return table[key]
  end
end


setmetatable(CL_LIB, CL_LIB["PACKAGE-METATABLE"])
-- Create keyword package
LCL["KEYWORD"] = {name = "KEYWORD"}
KEYWORD = LCL["KEYWORD"]
setmetatable(KEYWORD, CL_LIB["PACKAGE-METATABLE"])

-- Create the actual CL package
COMMON_LISP = {name = "COMMON-LISP"}
CL = COMMON_LISP
LCL["COMMON-LISP"] = CL
LCL["CL"] = CL -- Synonym

-- Make sure the special form symbols are defined
CL.LET = alloc_symbol(CL, "LET")
CL.FLET = alloc_symbol(CL, "FLET")
CL.LABELS = alloc_symbol(CL, "LABELS")
CL.FUNCTION = alloc_symbol(CL, "FUNCTION")
CL.QUOTE = alloc_symbol(CL, "QUOTE")
CL.SETQ = alloc_symbol(CL, "SETQ")
CL.IF = alloc_symbol(CL, "IF")

-- Complete packages
function complete_package(p)
  p.external = {}
  p.shadowing = CL.NIL
  p.nicknames = CL.NIL
  p.used = CL.NIL
  p.used_by = CL.NIL
end

setmetatable(CL, CL_LIB["PACKAGE-METATABLE"])
-- Create gensym package
LCL["NIL"] = {name = "NIL"}
NIL = LCL["NIL"]
setmetatable(LCL["NIL"], CL_LIB["PACKAGE-METATABLE"])
-- Block return symbols
LCL["%BR"] = {name = "%BR"}
setmetatable(LCL["%BR"], CL_LIB["PACKAGE-METATABLE"])

complete_package(CL)
complete_package(CL_LIB)
complete_package(LCL["NIL"])
complete_package(LCL["%BR"])
complete_package(KEYWORD)
CL_LIB.used = {CL, CL.NIL}
CL.used_by = {CL_LIB, CL.NIL}
CL.nicknames = {"CL", CL.NIL}

CL_LIB["ALLOC-FUNCTION"].fbound = function (arglist,func, pos, opt, rest, key)
  local this = {}
  this.arglist = arglist
  this.func = func
  this.pos = pos
  this.opt = opt
  this.rest = rest
  this.key = key
  setmetatable(this, CL_LIB["FUNCTION-METATABLE"])
  return this
end
CL_LIB["LUA-DEFUN"].fbound = function (symbol, arglist, func, ...)
  local this = CL_LIB["ALLOC-FUNCTION"](arglist,func,...)
  this.debugName = tostring(symbol)
  symbol.fbound = this
  return this
end

CL_LIB["MACRO-METATABLE"].__call = function (fbound, ...)
  return fbound.macro_function(...)
end

function eval (str)
  if str then
    str = CL['READ-FROM-STRING'](str, CL.T, CL.NIL, {})
  else
    CL.PRINC("EVAL> ")
    str = CL.READ()
  end
  local ok, result = pcall(function ()
      return {CL.EVAL(str)}
  end)
  if ok then
    for i = 1, #result do
      if not pcall(function() CL.PRINT(result[i]) end) then
        print("#<unprintable>")
      end
    end
  else
    if pcall(function () funcall(CL_LIB['REPORT-CONDITION'], result, CL['*STANDARD-OUTPUT*'].bound) end) then
    else
      print("Error: " .. dump(result))
    end
  end
  print()
  return result[1] or n
end
function repl() while true do eval() end end

function lclc (str)
  str = str and CL['READ-FROM-STRING'](str, CL.T, CL.NIL, {}) or CL.READ()
  return CL_LIB["COMPILE-TO-LUA"](str, CL.NIL, CL.NIL)
end

function read (str)
  CL.PRINT(str and CL['READ-FROM-STRING'](str, CL.T, CL.NIL, {}) or CL.READ())
  print()
end

-- Self evaluating symbols
CL.NIL.bound = CL.NIL
-- This allows to optimize (car list) to list[1]
CL.NIL[1] = CL.NIL
CL.NIL[2] = CL.NIL
n = CL.NIL
CL.T.bound = CL.T
CL["&ALLOW-OTHER-KEYS"].bound = nil
CL["&AUX"]        .bound = nil
CL["&BODY"]       .bound = nil
CL["&ENVIRONMENT"].bound = nil
CL["&KEY"]        .bound = nil
CL["&OPTIONAL"]   .bound = nil
CL["&REST"]       .bound = nil
CL["&WHOLE"]      .bound = nil

---------------------------------
-- NATIVE TYPES IMPLEMENTATION --
---------------------------------

function get_complex_lhsrhs (lhs, rhs)
  local x1, y1, x2, y2
  if getmetatable(lhs) == CL_LIB["COMPLEX-METATABLE"] then
    x1 = lhs.r y1 = lhs.i
  elseif type(lhs) == 'number' then
    x1 = lhs y1 = 0
  else
    error("1070")
  end
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    x2 = rhs.r y2 = rhs.i
  elseif type(rhs) == 'number' then
    x2 = rhs y2 = 0
  elseif getmetatable(rhs) == CL_LIB["RATIONAL-METATABLE"] then
    x2 = rhs.n/rhs.d y2 = 0
  else
    error("1071")
  end
  return x1, y1, x2, y2
end

function get_rational_lhsrhs (lhs, rhs)
  local n1, d1, n2, d2
  if getmetatable(lhs) == CL_LIB["RATIONAL-METATABLE"] then
    n1 = lhs.n d1 = lhs.d
  elseif type(lhs) == 'number' then
    n1 = lhs d1 = 1
  else
    error("1072")
  end
  if getmetatable(rhs) == CL_LIB["RATIONAL-METATABLE"] then
    n2 = rhs.n d2 = rhs.d
  elseif type(rhs) == 'number' then
    n2 = rhs d2 = 1
  else
    error("1073")
  end
  return n1, d1, n2, d2
end

CL_LIB["COMPLEX-METATABLE"].__add = function (lhs, rhs)
  local a, b, c, d = get_complex_lhsrhs(lhs,rhs)
  local r = a + c
  local i = b + d
  if i == 0 then
    return r
  else
    return CL_LIB["MAKE-COMPLEX"](r, i)
  end
end

CL_LIB["COMPLEX-METATABLE"].__sub = function (lhs, rhs)
  local a, b, c, d = get_complex_lhsrhs(lhs,rhs)
  local r = a - c
  local i = b - d
  if i == 0 then
    return r
  else
    return CL_LIB["MAKE-COMPLEX"](r, i)
  end
end

CL_LIB["COMPLEX-METATABLE"].__mul = function (lhs, rhs)
  local a, b, c, d = get_complex_lhsrhs(lhs,rhs)
  local r = a*c - b*d
  local i = a*d + b*c
  if i == 0 then
    return r
  else
    return CL_LIB["MAKE-COMPLEX"](r, i)
  end
end

CL_LIB["COMPLEX-METATABLE"].__div = function (lhs, rhs)
  local a, b, c, d = get_complex_lhsrhs(lhs,rhs)
  local off = c*c + d*d
  local r = (a*c + b*d) / off
  local i = (b*c - a*d) / off
  if i == 0 then
    return r
  else
    return CL_LIB["MAKE-COMPLEX"](r, i)
  end
end

CL_LIB["COMPLEX-METATABLE"].__eq = function (lhs, rhs)
  if CL.NUMBERP(lhs) ~= CL.NIL and CL.NUMBERP(rhs) ~= CL.NIL then
    local a, b, c, d = get_complex_lhsrhs(lhs,rhs)
    return (a == c) and (b == d)
  else
    return false
  end
end

CL_LIB["RATIONAL-METATABLE"].__add = function (lhs, rhs)
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    return CL_LIB["COMPLEX-METATABLE"].__add(lhs.n/lhs.d, rhs)
  else
    local a, b, c, d = get_rational_lhsrhs(lhs, rhs)
    local n = a * d + c * b
    local d = b * d
    return CL_LIB["MAKE-RATIONAL"](n, d)
  end
end

CL_LIB["RATIONAL-METATABLE"].__sub = function (lhs, rhs)
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    return CL_LIB["COMPLEX-METATABLE"].__sub(lhs.n/lhs.d, rhs)
  else
    local a, b, c, d = get_rational_lhsrhs(lhs, rhs)
    local n = a * d - c * b
    local d = b * d
    return CL_LIB["MAKE-RATIONAL"](n, d)
  end
end

CL_LIB["RATIONAL-METATABLE"].__mul = function (lhs, rhs)
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    return CL_LIB["COMPLEX-METATABLE"].__mul(lhs.n/lhs.d, rhs)
  else
    local a, b, c, d = get_rational_lhsrhs(lhs, rhs)
    local n = a * c
    local d = b * d
    return CL_LIB["MAKE-RATIONAL"](n, d)
  end
end

CL_LIB["RATIONAL-METATABLE"].__div = function (lhs, rhs)
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    return CL_LIB["COMPLEX-METATABLE"].__div(lhs.n/lhs.d, rhs)
  else
    local a, b, c, d = get_rational_lhsrhs(lhs, rhs)
    local n = a * d
    local d = b * c
    return CL_LIB["MAKE-RATIONAL"](n, d)
  end
end

CL_LIB["RATIONAL-METATABLE"].__eq = function (lhs, rhs)
  if getmetatable(rhs) == CL_LIB["COMPLEX-METATABLE"] then
    -- ?
    return false
  else
    local a, b, c, d = get_rational_lhsrhs(lhs, rhs)
    local left = a * d
    local right = b * c
    return (left == right)
  end
end

CL_LIB["RATIONAL-METATABLE"].__lt = function (lhs, rhs)
  local a, b, c, d = get_rational_lhsrhs(lhs,rhs)
  local left = a * d
  local right = b * c
  return (left < right)
end

CL_LIB["CHAR-METATABLE"].__eq = function (lhs, rhs)
  if getmetatable(lhs) == CL_LIB["CHAR-METATABLE"]
  and getmetatable(rhs) == CL_LIB["CHAR-METATABLE"] then
    return lhs.code == rhs.code
  else
    return false
  end
end

------------------------------------
-- NATIVELY IMPLEMENTED FUNCTIONS --
------------------------------------

function no_values() return end

-- Interop between Lisp lists and Lua
function unpack_lisp_list(args)
  if args == CL.NIL then
    return
  elseif type(args) ~= 'table' or #args ~= 2 then
    CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "values-list, ~S not a proper list", l(args))
  else
    return args[1] , unpack_lisp_list(args[2])
  end
end

CL_LIB['*MULTIPLE-VALUES-COUNT*'].bound = 1
mvc = CL_LIB['*MULTIPLE-VALUES-COUNT*']
CL_LIB['*MULTIPLE-VALUES-STORAGE*'].bound = { [0] = CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL, CL.NIL}
function single_value ()
  mvc.bound = 20
end


function unpack_mv(vals, len)
  vals = {vals}
  if  CL_LIB['*MULTIPLE-VALUES-COUNT*'].bound ~= 20 then
    vals = {(table.unpack or unpack)(CL_LIB['*MULTIPLE-VALUES-STORAGE*'].bound,0,CL_LIB['*MULTIPLE-VALUES-COUNT*'].bound-1)}
    -- Reset here as it's not required normally
    mvc.bound = 20
  end
  if len then
    for i = 1,len  do vals[i] = vals[i] or CL.NIL end
  end
  return (table.unpack or unpack)(vals)
  -- return vals[1]
end

function pcallmv(func)
  -- inline single_value
  mvc.bound = 20
  local ep, res = pcall(func)
  if ep then
    -- No errors, the return value is as is, we have to take the multiple values if any
    return {error = CL.NIL, unpack_mv(res)}
  else
    if type(res) == 'table' and res.tag then
      -- Standard Common Lisp throw
      res.error = KEYWORD.TAG
      return res
    else
      -- Some native error
      return { res, error = CL.T }
    end
  end
end

function list_star_r(lst, range)
  range = range or 1
  local len = #lst
  local ret = lst[len]
  for i = #lst-1,range,-1 do
    ret = {lst[i], ret}
  end
  return ret
end

function list_r(lst, range)
  range = range or 1
  local ret = CL.NIL
  for i = #lst,range,-1 do
    ret = {lst[i], ret}
  end
  return ret
end

function list(...)
  return list_r({...}, 1)
end
function list_star(...)
  return list_star_r({...}, 1)
end
l = list
s = list_star

function k(...)
  local lst = {...}
  local this = {}
  for i = 1,#lst,2 do
    this[lst[i]] = lst[i+1]
  end
  return this
end

function c(r, i) return CL_LIB["MAKE-COMPLEX"](r, i) end
function r(n, d) return CL_LIB["MAKE-RATIONAL"](n, d) end

-- count_fl_fast = 0
-- count_fc_fast = 0
-- count_fc_slow = 0
-- count_fc_fb = 0

-- XXX Maybe even switch to select...
function lcl_dynamic_funcall (func, args)
  -- TODO Short funcalls to funcall.

  if getmetatable(func) == CL_LIB['SYMBOL-METATABLE']  then
    func = func.fbound
  end

  if getmetatable(func) == CL_LIB['FUNCTION-METATABLE'] and func.pos then
    -- Defined function definition
    -- We start with the positional arguments.
    if #args < func.pos then
      CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "Missing argument(s) for function ~A called with ~A.", l(func, args))
    end
    -- Supply the unsupplied optional arguments
    for i = #args+1,func.pos+func.opt do
      args[i] = false
    end
    local extra = 1+func.pos+func.opt
    local allow_extra = false

    local key if func.key then
      -- TODO, do that better
      local allow_other_keys = false
      local traverse = func.arglist
      while traverse ~= CL.NIL do
        if traverse[1] == CL['&ALLOW-OTHER-KEYS'] then allow_other_keys = true end
        traverse = traverse[2]
      end
      local other_keys = false
      key = {}

      if 0 ~= (#args - extra + 1) % 2 then
        CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "Calling ~A with odd number of key arguments", l(func))
      end

      for ix = extra,#args,2 do
        local nextkey = func.key[args[ix]]
        local value = args[ix+1]
        if args[ix] == KEYWORD['ALLOW-OTHER-KEYS'] then
          -- Overwrite allow-other-keys
          allow_other_keys = allow_other_keys or value ~= CL.NIL
        end
        if not nextkey then
          other_keys = args[ix]
        elseif not key[nextkey] then
          key[nextkey] = value
        end
      end

      if other_keys and not allow_other_keys then
        CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "Unknown keyword argument ~A for function ~A.", l(other_keys, func))
      end
      allow_extra = true
    end

    local rest if func.rest then
      rest = list_r(args, extra)
      allow_extra = true
    end

    if not allow_extra and extra <= #args then
      CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "Extra argument(s) for function ~A called with ~A.", l(func, args))
    end


    if func.rest then
      args[extra] = rest
      extra = extra + 1
    end
    if func.key then
      args[extra] = key
      extra = extra + 1
    end
    args[extra] = nil

    local ret =  {func.func(_unpack(args))}
    return _unpack(ret)
  else
    if type(func) == 'table' then
      if not getmetatable(func) then
        CL.ERROR("Calling cons cell/raw table ~S with args ~S", l(func, args))
      end
      if not rawget(getmetatable(func), "__call") then
        CL.WARN("Calling non-callable table ~S with args ~S", l(func, args))
      end
    elseif type(func) ~= 'function' then
      CL.ERROR("Calling non callable object ~S with args ~S", l(func, ares))
    end
    return func(_unpack(args))
  end
end

CL_LIB['LUA-DEFUN'](CL_LIB["ALLOC-SYMBOL"], nil, alloc_symbol)
CL_LIB["LUA-DEFUN"](CL_LIB["FUNCALL-LUA"], nil,
function (func, args) return lcl_dynamic_funcall(func, args) end)

function funcall_key_get_actual_symbol(func, key)
  arglist = func.arglist
  if getmetatable(key) ~= CL_LIB["SYMBOL-METATABLE"] then
    CL_LIB['SIMPLE-ERROR'](CL_LIB['SIMPLE-PROGRAM-ERROR'], "&key parameter is not a symbol ~A.", l(key))
  end
  while arglist ~= CL.NIL do
    symb = arglist[1]
    if getmetatable(symb) ~= CL_LIB["SYMBOL-METATABLE"] then
      symb = symb[1]
    end
    if symb.name == key.name then
      return symb
    elseif symb[1] == key then
      return symb[2][1]
    end
    arglist = arglist[2]
  end
  return nil
end

-- cl-lib:funcall
-- CL.FUNCALL = CL_LIB.FUNCALL
CL_LIB["LUA-DEFUN"](CL.FUNCALL, l(CL_LIB.FUNC, CL['&REST'], CL_LIB.ARGS), function(func,args)
                      return lcl_dynamic_funcall(func, {unpack_lisp_list(args)})
end, 1, 0, true, false)

function funcall(func, ...) return lcl_dynamic_funcall(func, {...}) end
local funcall = funcall

CL_LIB["LUA-DEFUN"](CL["ERROR"], l(CL.DATUM, CL["&REST"], CL_LIB["ARGS"]),
function (datum, rest)
  error("SIMPLE-ERROR " .. dump(datum) .. " " .. dump(rest))
end, 1, 0, true, false)
CL_LIB["LUA-DEFUN"](CL["SIMPLE-ERROR"], l(CL_LIB.TYP, CL.FORMAT, CL["&REST"], CL_LIB["ARGS"]),
function (ttt, fmt, rest)
  if rawget(CL.FORMAT, 'fbound') then
    error("fallback error: " .. dump(ttt) .. ": " .. CL.FORMAT(n,fmt,rest))
  else
    error("fallback error (nofmt): " .. dump(ttt) .. ": " .. dump(fmt) .. " - " .. dump(rest))
  end
end, 2, 0, true, false)

CL_LIB["LUA-DEFUN"](CL["WARN"], l(CL.DATUM, CL["&REST"], CL_LIB["ARGS"]),
function (datum, rest)
  print("WARNING: " .. dump(datum) .. " " .. dump(rest))
end, 1, 0, true, false)

CL_LIB["LUA-DEFUN"](CL_LIB["DBG"], l(CL["&REST"], CL["VALUES"]),
function (vals)
  local out = ""
  while vals ~= CL.NIL do
    out = out .. dump(vals[1]) .. " "
    vals = vals[2]
  end
  print(out)
  return CL.NIL
end, 0, 0, true, false)

-- cl-lib:id. Identity function
CL_LIB["LUA-DEFUN"](CL_LIB.ID, l(CL_LIB.A), function (a) return a end, 1, 0, false, false)
i = CL_LIB.ID
id = i
local id = id

-- cl-lib:lua-null
-- Return a lua nil when the lisp value is cl:nil
CL_LIB["LUA-DEFUN"](CL_LIB["LUA-NULL"], l(CL_LIB.VALUE), function(value)
  if value == CL.NIL then
    return nil
  else
      return value
  end
end, 1, 0, false, false)

-- cl:values-list
CL_LIB["LUA-DEFUN"](CL["VALUES-LIST"], l(CL.LIST), function (l)
  -- local tbl = {unpack_lisp_list(l)}
  -- Only pass through the first value, the rest uses the multiple-values-storage array
  -- local tbl = {l[1]}
  local tbl = l[1]

  local i = 0
  while l ~= CL.NIL do
    CL_LIB['*MULTIPLE-VALUES-STORAGE*'].bound[i] = l[1]
    l = l[2]
    i = i + 1
  end
  CL_LIB['*MULTIPLE-VALUES-COUNT*'].bound = i

  -- setmetatable(tbl, CL_LIB["MULTIPLE-VALUES-METATABLE"])
  return tbl
end, 1, 0, false, false)

-- cl-lib:macro-setq
-- setq, but sets the macro metatable for the function
CL_LIB["LUA-DEFUN"](CL_LIB["MACRO-SETQ"], l(CL.SYMBOL, CL_LIB.FUNC),
function (symbol, func)
  local f = {macro_function = func}
  setmetatable(f, CL_LIB["MACRO-METATABLE"])
  symbol.fbound = f
  return CL.NIL
end, 2, 0, false, false)

-- cl-lib:function-setq
-- setq, but sets the function binding for the symbol
CL_LIB["LUA-DEFUN"](CL_LIB["FUNCTION-SETQ"], l(CL.SYMBOL, CL_LIB.FUNC), function(symbol, func)
  symbol.fbound = func
  if getmetatable(func) == CL_LIB["FUNCTION-METATABLE"] then
    func.debugName = tostring(symbol)
  end
  return func
end, 2, 0, false, false)

CL_LIB["LUA-DEFUN"](CL_LIB["SETF-FUNCTION-SETQ"], l(CL.SYMBOL, CL_LIB.FUNC), function(symbol, func)
  symbol.setfbound = func
  if getmetatable(func) == CL_LIB["FUNCTION-METATABLE"] then
    func.debugName = "(SETF " .. tostring(symbol) .. ")"
  end
  return func
end, 2, 0, false, false)

-- cl-lib:def
CL_LIB["LUA-DEFUN"](CL_LIB["DEF"], l(CL.SYMBOL, CL["&OPTIONAL"], CL_LIB.VALUE, CL_LIB.DOCUMENTATION),
function(symbol, value, doc)
  value = value or CL.NIL
  symbol.bound = value
  return value
end, 1, 2, false, false)

-- cl:list
CL_LIB["LUA-DEFUN"](CL.LIST, { CL["&REST"] , {CL["REST"], CL.NIL} },
function (rest) return rest end, 0, 0, true, false)

-- cl:cons
CL_LIB["LUA-DEFUN"](CL.CONS, { CL.CAR , {CL.CDR , CL.NIL} },
function (car, cdr) return {car, cdr} end, 2, 0, false, false)

-- cl-lib:make-char
CL_LIB["LUA-DEFUN"](CL_LIB["MAKE-CHAR"], l(CL_LIB.STR, CL["&OPTIONAL"], CL_LIB.OFFSET),
function (str, offset)
  local this = {}
  offset = offset or 0
  offset = offset + 1
  this.char = string.sub(str, offset, offset)
  this.code = string.byte(this.char)
  setmetatable(this, CL_LIB["CHAR-METATABLE"])
  if this.char == "" then
    return CL.NIL
  else
    return this
  end
end, 1, 1, false, false)

-- cl-lib:gensym
CL_LIB["LUA-DEFUN"](CL.GENSYM, { CL["&OPTIONAL"] , {CL_LIB.X , CL.NIL }},
function (x)
  x = x or "G"
  local gensymid = CL["*GENSYM-COUNTER*"].bound
  if type(x) == 'number' and x >= 0 and x == bit.intdiv(x,1) then
    return CL_LIB["ALLOC-SYMBOL"](CL.NIL, "G" .. x)
  elseif type(gensymid) ~= 'number' then
    CL["*GENSYM-COUNTER*"].bound = 99999
    CL.ERROR(CL['TYPE-ERROR'], l(KEYWORD.FORM, CL["*GENSYM-COUNTER*"], KEYWORD['EXPECTED-TYPE'], CL.INTEGER, KEYWORD.DATUM, gensymid))
  elseif type(x) == 'string' then
    CL["*GENSYM-COUNTER*"].bound = CL["*GENSYM-COUNTER*"].bound + 1
    return CL_LIB["ALLOC-SYMBOL"](CL.NIL, tostring(x) .. gensymid)
  else
    CL.ERROR(CL['TYPE-ERROR'], l(KEYWORD.FORM, CL_LIB.X, KEYWORD['EXPECTED-TYPE'], l(CL.OR, CL.INTEGER, CL.STRING), KEYWORD.DATUM, x))
  end
end, 0, 1, false, false)
-- CL.GENSYM = CL_LIB.GENSYM
CL["*GENSYM-COUNTER*"].bound = 0

CL_LIB["LUA-DEFUN"](CL['MAKE-PACKAGE'], { CL['PACKAGE-NAME'] , {CL['&KEY'] , {CL_LIB.NICKNAMES, {CL_LIB.USE ,CL.NIL}}}},
function (name, key)
  -- Use symbol name
  if getmetatable(name) == CL_LIB["SYMBOL-METATABLE"] then
    name = name.name
  end
  if LCL[name] then
    CL.ERROR("MAKE-PACKAGE: Package ~S already exists", l(name))
  end
  lst = key[CL_LIB.NICKNAMES] or CL.NIL
  this = {}
  this.name = name
  this.external = {}
  this.shadowing = l()
  this.nicknames = lst
  -- By default use CL
  this.used = key[CL_LIB.USE] or {CL,CL.NIL}
  this.used_by = CL.NIL
  LCL[name] = this
  setmetatable(this, CL_LIB['PACKAGE-METATABLE'])
  while lst ~= CL.NIL do
    if LCL[lst[1]] then
      CL.ERROR("MAKE-PACKAGE: Package (nickname) ~S already exists", l(lst[1]))
    end
    LCL[lst[1]] = this
    lst = lst[2]
  end
  return this
end, 1, 0, false, {[KEYWORD.NICKNAMES] = CL_LIB.NICKNAMES, [KEYWORD.USE] = CL_LIB.USE})



CL_LIB["LUA-DEFUN"](CL_LIB["MAKE-COMPLEX"], l(CL["REAL"], CL_LIB["IMAG"]),
function (r, i)
  local this = { ['r'] = r, ['i'] = i }
  setmetatable(this, CL_LIB["COMPLEX-METATABLE"])
  return this
end, 2, 0, false, false)

function gcd(n, d)
  if d == 0 then return n else return gcd(d, n % d) end
end
local function valid_rational_component(el)
  return
    bit.intdiv(el, 1) == el
  and el < 0x10000000000000
  and el > -0x10000000000000

end
CL_LIB["LUA-DEFUN"](CL_LIB["MAKE-RATIONAL"], l(CL_LIB["NUM"], CL_LIB["DENUM"]),
function (n, d)
    if n == 0 then
      -- XXX 0/0
      return 0
    elseif d == 1 then
      return n
    elseif valid_rational_component(n) and valid_rational_component(d) then
      local gcd = gcd(n, d)
      local this = { ['n'] = bit.intdiv(n, gcd), ['d'] = bit.intdiv(d, gcd) }
      setmetatable(this, CL_LIB["RATIONAL-METATABLE"])
      return this
    else
      return n / d
    end
  end, 2, 0, false, false)

-- cl:gethash
CL_LIB["LUA-DEFUN"](CL.GETHASH, l(CL_LIB.KEY, CL_LIB.HASHTABLE, CL["&OPTIONAL"], CL_LIB.DEFAULT),
function (key, ht, default)
  default = default or CL.NIL
  local dat = ht.data[key]
  return CL.VALUES(l(dat or default, dat and CL.T or CL.NIL))
end, 2, 1, false, false)

-----------------
-- QUOTED DATA --
-----------------

QUOTE = {name = "QUOTE", data = {}}
setmetatable(QUOTE, CL_LIB["PACKAGE-METATABLE"])
CL_LIB["LUA-DEFUN"](QUOTE.ALLOC, l(CL_LIB.DATA),
function (data)
  newnum = 1 + #QUOTE.data
  table.insert(QUOTE.data, data)
  return "QUOTE.data[" .. newnum .. "]"
end)
do
  local b = {}
  setmetatable(QUOTE.data, b)
  b.__mode = "v"
end

-------------------------------
-- COMPILER HELPER FUNCTIONS --
-------------------------------

-- TODO Not a list here maybe a problem.
CL_LIB["LUA-DEFUN"](CL_LIB["CONCAT-STRING"], CL['&WHOLE'],
function (...)
  local lst = {...}
  for i = 1,#lst do
    if type(lst[i]) ~= 'number' and type(lst[i]) ~= 'string' then
      funcall(CL.ERROR, "In concat-string ~S, argument #~D of ~D, ~S, is not a number or a string", funcall(CL.VECTOR, ...), i, #lst, lst[i])
    end
  end
  return table.concat(lst)
  -- XXX Add pcall for invalid elements in the table for the error
  -- if a then
  --   if b then
  --     if type(a) == 'table' then
  --       CL.ERROR("Trying to concat table: ~A", l(a))
  --     end
  --     if type(b) == 'table' then
  --       CL.ERROR("Trying to concat table: ~A", l(b))
  --     end
  --     return CL_LIB["CONCAT-STRING"](a .. b, ...)
  --   else
  --     return a
  --   end
  -- else
  --   return ""
  -- end
end)

function number_to_lua (number)
  if getmetatable(number) == CL_LIB["COMPLEX-METATABLE"] then
    return "c(" .. number_to_lua(number.r) .. "," .. number_to_lua(number.i) .. ")"
  elseif getmetatable(number) == CL_LIB["RATIONAL-METATABLE"] then
    return "r(" .. number_to_lua(number.n) .. "," .. number_to_lua(number.d) .. ")"
  elseif number == bit.intdiv(number,1) and number <= 9007199254740991 and number >= -9007199254740991 then
    return string.format("%d", number)
  else
    return string.format("%a", number)
  end
end

CL_LIB["LUA-DEFUN"](CL_LIB["NUMBER-TO-LUA"], l(CL.NUMBER), number_to_lua, 1, 0, false, false)

CL_LIB["LUA-DEFUN"](CL_LIB["STRING-TO-LUA"], l(CL_LIB.STR),
function (str) return CL_LIB["%QUOTE-TO-LUA"](str) end, 1, 0, false, false)

CL_LIB["LUA-DEFUN"](CL_LIB["%QUOTE-TO-LUA"], l(CL_LIB.STR, CL_LIB.ENV),
function (str, env) return QUOTE.ALLOC(str) end, 1, 0, false, false)

CL_LIB["LUA-DEFUN"](CL_LIB["%IN-PACKAGE"], l(CL_LIB.PACK),
function (pack) return pack end, 1, 0, false, false)

CL_LIB["LUA-DEFUN"](CL_LIB["CALL-TO-LUA"], l(CL_LIB.AST, CL_LIB.ENV, CL_LIB.CTX),
function (ast, env, ctx)
  local func = ast[1]
  -- FIXME To allow for local macro defintions, this has to change
  if getmetatable(rawget(func,'fbound')) == CL_LIB["MACRO-METATABLE"] then
    -- Execute the macro and call compile again with the result
    return CL_LIB["COMPILE-TO-LUA"](func.fbound.macro_function(ast, env), env, ctx)
  else
    return CL_LIB["FUNCTION-CALL-TO-LUA"](ast, env, ctx)
  end
end, 3, 0, false, false)

-- Manually include some required funcitons for later bootstrap steps (init.lisp)
-- CL_LIB["MACRO-SETQ"]((LCL["COMMON-LISP"]["DEFMACRO"]),CL_LIB["ALLOC-FUNCTION"]({(LCL["CL-LIB"]["NAME"]),{(LCL["CL-LIB"]["ARGLIST"]),{(LCL["COMMON-LISP"]["&BODY"]),{(LCL["CL-LIB"]["BODY"]),(LCL["COMMON-LISP"]["NIL"])}}}},function (l1, l2, l3,  ...)   return CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["SETQ"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["MACRO-FUNCTION"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["QUOTE"]),l1)),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["FUNCTION"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["CONS"]).fbound,(LCL["COMMON-LISP"]["LAMBDA"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["CONS"]).fbound,l2,l3)))) end)) CL_LIB.ID( CL_LIB["MACRO-SETQ"]((LCL["COMMON-LISP"]["LET"]),CL_LIB["ALLOC-FUNCTION"]({(LCL["COMMON-LISP"]["&REST"]),{(LCL["COMMON-LISP"]["REST"]),(LCL["COMMON-LISP"]["NIL"])}},function (l1,  ...)   return CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST*"]).fbound,(LCL["CL-LIB"]["%LET"]),l1) end)) );
CL_LIB["MACRO-SETQ"]((LCL["COMMON-LISP"]["DEFMACRO"]),CL_LIB["ALLOC-FUNCTION"]({(LCL["CL-LIB"]["FORM"]),{(LCL["COMMON-LISP"]["&OPTIONAL"]),{(LCL["CL-LIB"]["ENV"]),(LCL["COMMON-LISP"]["NIL"])}}},function (l1, l2,  ...)  if not l2 then  l2 = (LCL["COMMON-LISP"]["NIL"]).bound end  local l3 = CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["SECOND"]).fbound,l1); local l4 = CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["THIRD"]).fbound,l1); local l5 = CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["CDDDR"]).fbound,l1); return CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["SETQ"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["MACRO-FUNCTION"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["QUOTE"]),l3)),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["FUNCTION"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["LAMBDA"]),{(LCL["CL-LIB"]["FORM"]),{(LCL["COMMON-LISP"]["&OPTIONAL"]),{(LCL["CL-LIB"]["ENV"]),(LCL["COMMON-LISP"]["NIL"])}}},CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST*"]).fbound,(LCL["COMMON-LISP"]["APPLY"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST"]).fbound,(LCL["COMMON-LISP"]["FUNCTION"]),CL_LIB["FUNCALL-LUA"]( (LCL["COMMON-LISP"]["LIST*"]).fbound,(LCL["COMMON-LISP"]["LAMBDA"]),l4,l5)),{{(LCL["COMMON-LISP"]["CDR"]),{(LCL["CL-LIB"]["FORM"]),(LCL["COMMON-LISP"]["NIL"])}},(LCL["COMMON-LISP"]["NIL"])})))) end))

local mvc = mvc
local CL = CL
local CL_LIB = CL_LIB
local n = n
local i = i
local l = l
local s = s
