;; Copyright (C) 2001-2018, Arthur Lemmens et al.
;;
;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.

;;; SPLIT-SEQUENCE
;;;
;;; This code was based on Arthur Lemmens' in
;;; <URL:http://groups.google.com/groups?as_umsgid=39F36F1A.B8F19D20%40simplex.nl>;
;;;
;;; changes include:
;;;
;;; * altering the behaviour of the :from-end keyword argument to
;;; return the subsequences in original order, for consistency with
;;; CL:REMOVE, CL:SUBSTITUTE et al. (:from-end being non-NIL only
;;; affects the answer if :count is less than the number of
;;; subsequences, by analogy with the above-referenced functions).
;;;
;;; * changing the :maximum keyword argument to :count, by analogy
;;; with CL:REMOVE, CL:SUBSTITUTE, and so on.
;;;
;;; * naming the function SPLIT-SEQUENCE rather than PARTITION rather
;;; than SPLIT.
;;;
;;; * adding SPLIT-SEQUENCE-IF and SPLIT-SEQUENCE-IF-NOT.
;;;
;;; * The second return value is now an index rather than a copy of a
;;; portion of the sequence; this index is the `right' one to feed to
;;; CL:SUBSEQ for continued processing.

;;; There's a certain amount of code duplication in the vector and
;;; extended sequence modules, which is kept to illustrate the
;;; relationship between the SPLIT-SEQUENCE functions and the
;;; CL:POSITION functions.

(defpackage :split-sequence
  (:use :common-lisp)
  (:export :split-sequence
           :split-sequence-if
           :split-sequence-if-not))

;; vector.lisp
(in-package :split-sequence)

(declaim (inline
          split-vector split-vector-if split-vector-if-not
          split-vector-from-end split-vector-from-start))

(deftype array-index (&optional (length array-dimension-limit))
  `(integer 0 (,length)))

(declaim (ftype (function (&rest t) (values list unsigned-byte))
                split-vector split-vector-if split-vector-if-not))

(declaim (ftype (function (function vector array-index
                                    (or null array-index) (or null array-index) boolean)
                          (values list unsigned-byte))
                split-vector-from-start split-vector-from-end))

(defun split-vector-from-end (position-fn vector start end count remove-empty-subseqs)
  (declare (optimize (speed 3) (debug 0))
           (type (function (vector fixnum) (or null fixnum)) position-fn))
  (loop
    :with end = (or end (length vector))
    :for right := end :then left
    :for left := (max (or (funcall position-fn vector right) -1)
                      (1- start))
    :unless (and (= right (1+ left)) remove-empty-subseqs)
      :if (and count (>= nr-elts count))
        :do (return (values (nreverse subseqs) right))
      :else
        :collect (subseq vector (1+ left) right) into subseqs
        :and :sum 1 :into nr-elts :of-type fixnum
    :until (< left start)
    :finally (return (values (nreverse subseqs) (1+ left)))))

(defun split-vector-from-start (position-fn vector start end count remove-empty-subseqs)
  (declare (optimize (speed 3) (debug 0))
           (type vector vector)
           (type (function (vector fixnum) (or null fixnum)) position-fn))
  (let ((length (length vector)))
    (loop
      :with end = (or end (length vector))
      :for left := start :then (1+ right)
      :for right := (min (or (funcall position-fn vector left) length)
                         end)
      :unless (and (= right left) remove-empty-subseqs)
        :if (and count (>= nr-elts count))
          :do (return (values subseqs left))
        :else
          :collect (subseq vector left right) :into subseqs
          :and :sum 1 :into nr-elts :of-type fixnum
      :until (>= right end)
      :finally (return (values subseqs right)))))

(defun split-vector-if
    (predicate vector start end from-end count remove-empty-subseqs key)
  (if from-end
      (split-vector-from-end (lambda (vector end)
                               (position-if predicate vector :end end :from-end t :key key))
                             vector start end count remove-empty-subseqs)
      (split-vector-from-start (lambda (vector start)
                                 (position-if predicate vector :start start :key key))
                               vector start end count remove-empty-subseqs)))

(defun split-vector-if-not
    (predicate vector start end from-end count remove-empty-subseqs key)
  (if from-end
      (split-vector-from-end (lambda (vector end)
                               (position-if-not predicate vector :end end :from-end t :key key))
                             vector start end count remove-empty-subseqs)
      (split-vector-from-start (lambda (vector start)
                                 (position-if-not predicate vector :start start :key key))
                               vector start end count remove-empty-subseqs)))

(defun split-vector
    (delimiter vector start end from-end count remove-empty-subseqs test test-not key)
  (cond
    ((and (not from-end) (null test-not))
     (split-vector-from-start (lambda (vector start)
                                (position delimiter vector :start start :key key :test test))
                              vector start end count remove-empty-subseqs))
    ((and (not from-end) test-not)
     (split-vector-from-start (lambda (vector start)
                                (position delimiter vector :start start :key key :test-not test-not))
                              vector start end count remove-empty-subseqs))
    ((and from-end (null test-not))
     (split-vector-from-end (lambda (vector end)
                              (position delimiter vector :end end :from-end t :key key :test test))
                            vector start end count remove-empty-subseqs))
    (t
     (split-vector-from-end (lambda (vector end)
                              (position delimiter vector :end end :from-end t :key key :test-not test-not))
                            vector start end count remove-empty-subseqs))))

;; list.lisp
(in-package :split-sequence)

(declaim (inline
          collect-until count-while
          split-list split-list-if split-list-if-not
          split-list-from-end split-list-from-start split-list-internal))

(declaim (ftype (function (&rest t) (values list unsigned-byte))
                split-list split-list-if split-list-if-not))

(declaim (ftype (function (function list unsigned-byte (or null unsigned-byte) (or null unsigned-byte)
                                    boolean)
                          (values list unsigned-byte))
                split-list-from-start split-list-from-end split-list-internal))

(defun collect-until (predicate list end)
  "Collect elements from LIST until one that satisfies PREDICATE is found.

  At most END elements will be examined. If END is null, all elements will be examined.

  Returns four values:

  * The collected items.
  * The remaining items.
  * The number of elements examined.
  * Whether the search ended by running off the end, instead of by finding a delimiter."
  (let ((examined 0)
        (found nil))
    (flet ((examine (value)
             (incf examined)
             (setf found (funcall predicate value))))
      (loop :for lst :on list
            :until (eql examined end)
            :until (examine (car lst))
            :collect (car lst) :into result
            :finally (return (values result
                                     (cdr lst)
                                     examined
                                     (and (not found)
                                          (or (null end)
                                              (= end examined)))))))))

(defun count-while (predicate list end)
  "Count the number of elements satisfying PREDICATE at the beginning of LIST.

  At most END elements will be counted. If END is null, all elements will be examined."
  (if end
      (loop :for value :in list
            :for i :below end
            :while (funcall predicate value)
            :summing 1)
      (loop :for value :in list
            :while (funcall predicate value)
            :summing 1)))

(defun split-list-internal (predicate list start end count remove-empty-subseqs)
  (let ((count count)
        (done nil)
        (index start)
        (end (when end (- end start)))
        (list (nthcdr start list)))
    (flet ((should-collect-p (chunk)
             (unless (and remove-empty-subseqs (null chunk))
               (when (numberp count) (decf count))
               t))
           (gather-chunk ()
             (multiple-value-bind (chunk remaining examined ran-off-end)
                 (collect-until predicate list end)
               (incf index examined)
               (when end (decf end examined))
               (setf list remaining
                     done ran-off-end)
               chunk)))
      (values (loop :with chunk
                    :until (or done (eql 0 count))
                    :do (setf chunk (gather-chunk))
                    :when (should-collect-p chunk)
                      :collect chunk)
              (+ index
                 (if remove-empty-subseqs
                     (count-while predicate list end) ; chew off remaining empty seqs
                     0))))))

(defun split-list-from-end (predicate list start end count remove-empty-subseqs)
  (let ((length (length list)))
    (multiple-value-bind (result index)
        (split-list-internal predicate (reverse list)
                             (if end (- length end) 0)
                             (- length start) count remove-empty-subseqs)
      (loop :for cons on result
            :for car := (car cons)
            :do (setf (car cons) (nreverse car)))
      (values (nreverse result) (- length index)))))

(defun split-list-from-start (predicate list start end count remove-empty-subseqs)
  (split-list-internal predicate list start end count remove-empty-subseqs))

(defun split-list-if (predicate list start end from-end count remove-empty-subseqs key)
  (let ((predicate (lambda (x) (funcall predicate (funcall key x)))))
    (if from-end
        (split-list-from-end predicate list start end count remove-empty-subseqs)
        (split-list-from-start predicate list start end count remove-empty-subseqs))))

(defun split-list-if-not (predicate list start end from-end count remove-empty-subseqs key)
  (split-list-if (complement predicate) list start end from-end count remove-empty-subseqs key))

(defun split-list
    (delimiter list start end from-end count remove-empty-subseqs test test-not key)
  (let ((predicate (if test-not
                       (lambda (x) (not (funcall test-not delimiter (funcall key x))))
                       (lambda (x) (funcall test delimiter (funcall key x))))))
    (if from-end
        (split-list-from-end predicate list start end count remove-empty-subseqs)
        (split-list-from-start predicate list start end count remove-empty-subseqs))))

;; api.lisp
(in-package :split-sequence)

(defun list-long-enough-p (list length)
  (or (zerop length)
      (not (null (nthcdr (1- length) list)))))

(defun check-bounds (sequence start end)
  (progn
    (check-type start unsigned-byte "a non-negative integer")
    (check-type end (or null unsigned-byte) "a non-negative integer or NIL")
    (typecase sequence
      (list
       (when end
         (unless (<= start end)
           (error "Wrong sequence bounds. START: ~S END: ~S" start end))
         (unless (list-long-enough-p sequence end)
           (error "The list is too short: END was ~S but the list is ~S elements long."
                  end (length sequence)))))
      (t
       (let ((length (length sequence)))
         (unless end (setf end length))
         (unless (<= start end length)
           (error "Wrong sequence bounds. START: ~S END: ~S" start end)))))))

(define-condition simple-program-error (program-error simple-condition) ())

(defmacro check-tests (test test-p test-not test-not-p)
  `(if ,test-p
       (if ,test-not-p
           (error (make-condition 'simple-program-error
                                  :format-control "Cannot specify both TEST and TEST-NOT."))
           (check-type ,test (or function (and symbol (not null)))))
       (when ,test-not-p
         (check-type ,test-not (or function (and symbol (not null)))))))

(declaim (ftype (function (&rest t) (values list unsigned-byte))
                split-sequence split-sequence-if split-sequence-if-not))

(defun split-sequence (delimiter sequence &key (start 0) (end nil) (from-end nil)
                                            (count nil) (remove-empty-subseqs nil)
                                            (test #'eql test-p) (test-not nil test-not-p)
                                            (key #'identity))
  (check-bounds sequence start end)
  (check-tests test test-p test-not test-not-p)
  (etypecase sequence
    (list (split-list delimiter sequence start end from-end count
                      remove-empty-subseqs test test-not key))
    (vector (split-vector delimiter sequence start end from-end count
                          remove-empty-subseqs test test-not key))
    #+(or abcl sbcl)
    (extended-sequence (split-extended-sequence delimiter sequence start end from-end count
                                                remove-empty-subseqs test test-not key))))

(defun split-sequence-if (predicate sequence &key (start 0) (end nil) (from-end nil)
                                               (count nil) (remove-empty-subseqs nil) (key #'identity))
  (check-bounds sequence start end)
  (etypecase sequence
    (list (split-list-if predicate sequence start end from-end count
                         remove-empty-subseqs key))
    (vector (split-vector-if predicate sequence start end from-end count
                             remove-empty-subseqs key))
    #+(or abcl sbcl)
    (extended-sequence (split-extended-sequence-if predicate sequence start end from-end count
                                                   remove-empty-subseqs key))))

(defun split-sequence-if-not (predicate sequence &key (start 0) (end nil) (from-end nil)
                                                   (count nil) (remove-empty-subseqs nil) (key #'identity))
  (check-bounds sequence start end)
  (etypecase sequence
    (list (split-list-if-not predicate sequence start end from-end count
                             remove-empty-subseqs key))
    (vector (split-vector-if-not predicate sequence start end from-end count
                                 remove-empty-subseqs key))
    #+(or abcl sbcl)
    (extended-sequence (split-extended-sequence-if-not predicate sequence start end from-end count
                                                       remove-empty-subseqs key))))

(pushnew :split-sequence *features*)
