(in-package :cl-user)

(use-package :l)

(defmacro with-ngx-output ((stream) &body body)
  "Execute the body with the stream bound to a special stream
that writes as if ngx.say was called."
  `(let ((,stream (cl-lib::make-file-stream
                   :file (table "write" (lambda (tbl w) (l "ngx" "print" (w))))
                   :pathname #P"<nginx>")))
     ,@body))

(defun example ()
  (with-ngx-output (*standard-output*)
    (format t "~A ~A~&~A~%~%~A"
            (lisp-implementation-type)
            (lisp-implementation-version)
            (l "ngx" "today" ())
            *features*)))
