(in-package :cl-user)
(use-package :l)

(let ((px 400)
      (py 300)
      (speed 32))

  (l "love" "draw" =
     (lambda ()
       (l "love" "graphics" "print" ("Hello World" px py))))

  (l "love" "update" =
     (lambda (dt &aux (dp (round (* speed dt))))
       (format t "FPS: ~D~&" (floor (/ dt)))
       (flet ((down-p (key) (l "love" "keyboard" "isDown" (key))))
         (when (down-p "w") (decf py dp))
         (when (down-p "a") (decf px dp))
         (when (down-p "s") (incf py dp))
         (when (down-p "d") (incf px dp))))))
