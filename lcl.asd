(asdf:defsystem #:lcl
  :description "This loads LCL within your host Lisp."
  :version "0.6.1"
  :depends-on (#:alexandria #:str)
  :serial t
  :components
  ((:module "lisp/"
    :serial t
    :components
    ((:module "lua/"
      :serial t
      :components
      ((:file "packages")
       (:file "source")
       (:file "serialize")
       (:file "special")))
     (:module "compiler/"
      :serial t
      :components
      ((:file "packages")
       (:file "environment")
       (:file "arglist")
       (:file "compile")
       (:file "resolve")
       (:file "emit")
       (:file "interface")))
     #-lcl (:module "bootstrap/"
            :serial t
            :components
            ((:file "packages")
             (:file "boot")
             (:file "call")
             (:file "build")))
     ))))
