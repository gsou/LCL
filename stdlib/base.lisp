;;;; Base CL library macros and functions

(cl:in-package :cl-lib)

(defun apply-gen-list (args)
  (cond
    ((null args) nil)
    ((null (cdr args)) (car args))
    (t (cons (car args) (apply-gen-list (cdr args))))))
(defun apply (func arg &rest args)
  (cond
    ((endp args) (lua "CL.FUNCALL(" func ", " arg ")"))
    (t (let ((a (cons arg (apply-gen-list args)))) (lua "CL.FUNCALL(" func ", " a ")")))))


;;; Basic functions
(defun identity (a) a)
(defun complement (function)
  (lambda (&rest args) (not (apply function args))))
(defun constantly (value) (lambda (&rest args) value))
(defun not (a) (if a nil t))
(defun null (a) (not a))

(defun compiled-function-p (fun) (functionp fun))

(defun constantp (form &optional environment)
  (cond
    ;; TODO CONSTANT VARIABLES, AND THEN REMOVE SYMBOLS
    ((atom form) t)
    ((eq 'quote (car form )) t)
    (t nil)))

;;; Identification functions
(defun lisp-implementation-type () "" "LCL")
(defun lisp-implementation-version () ""
  #+asdf
  #.(asdf:component-version (asdf:find-system :lcl))
  #-asdf
  "0.6")
(defun short-site-name () nil)
(defun long-site-name () nil)
(defun room (&optional (x :default)) nil)
(defun software-type () nil)
(defun software-version () nil)
(defun machine-instance () nil)
(defun machine-type () nil)
(defun machine-version () nil)

(defun function-lambda-expression (function) (values nil t nil))

(defun ed (&optional x))

(defmacro in-package (package))

;;; Quasiquotation
;; XXX Nesting seems incorrect
(defun perform-quasiquote (arg &optional (level 0))
  (cond
    ((not (consp arg)) (if (eq 0 level)
                           (list 'quote arg)
                           arg))
    ((eq (car arg) 'quasiquote) (list 'quote (list '%quasiquote (perform-quasiquote (cadr arg) (+ level 1)))))
    ((eq (car arg) 'unquote)
     (if (eq 0 level)
         (cadr arg)
         (list 'unquote (perform-quasiquote (cadr arg) (- level 1)))))
    ((eq (car arg) 'unquote-list) (error "Malformed quasiquote expression"))
    ((and (consp (car arg)) (eq (caar arg) 'unquote-list))
     (if (eq 0 level)
         (if (not (null (cdr arg)))
             (list 'append (cadar arg) (perform-quasiquote (cdr arg) level))
             (cadar arg))
         (list* (list 'unquote-list (perform-quasiquote (cadar arg) level)) (perform-quasiquote (cdr arg) level))))
    (t
     (if (eq 0 level)
         (list 'cons (perform-quasiquote (car arg) level) (perform-quasiquote (cdr arg) level))
         (cons (perform-quasiquote (car arg) level) (perform-quasiquote (cdr arg) level))))))
(defmacro quasiquote (arg) (perform-quasiquote arg))
(defmacro %quasiquote (arg) (perform-quasiquote arg))
(defmacro unquote (arg) (error ", not inside a quasiquote"))
(defmacro unquote-list (arg) (error ",@ not inside a quasiquote"))

;; TODO Switch to lisp gensym
;; (defun gensym (&optional (x "G"))
;;   (cond
;;     ((%numberp x) (alloc-symbol nil (concat-string "G" (tostring x))))
;;     ((not (%numberp *gensym-counter*))
;;      ;; Set gensym-counter to something reasonable before throwing, in case throwing requires
;;      ;; gensym
;;      (let ((*gensym-counter* 999999))
;;        (error 'type-error :form '*gensym-counter* :datum *gensym-counter* :expected-type 'number)))
;;     (t
;;      (let ((id *gensym-counter*))
;;        (setq *gensym-counter* (%+ 1 *gensym-counter*))
;;        (alloc-symbol nil (concat-string (tostring x) id))))))



(lua-set-table symbol-metatable "__index"
               (lua-index-table
                (lambda (table key)
                  (cond
                    ((%= key "fbound") (error 'undefined-function :name table))
                    ((%= key "setfbound") (error 'undefined-function :name (list 'setf table)))
                    ((%= key "bound") (error 'unbound-variable :name table)))
                  ;; Make sure to return nil so it quickly crashes.
                  (values))
                "func"))
