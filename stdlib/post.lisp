(in-package :cl-lib)
;; Post init

(ignore-errors (import closette::+closette-symbols+ :common-lisp))


(defun %find-class (class)
  (if (typep class 'closette::standard-class)
      class
      (closette:find-class class nil)))
(defun %print-clos-object (object stream)
  (write-string "#<" stream)
  (write-string (string (closette::class-name (closette::class-of object))) stream)
  (write-string ">" stream))
(defun %subclassp (t1 t2) (closette:subclassp t1 t2))
;;; Install the typep-extension
(defun typep-ext (object spec-symbol spec-args)
  ;; XXX Hack to support classes
  (if (and (null spec-args) (find-class spec-symbol nil))
      (closette:subclassp (closette:class-of object) (find-class spec-symbol))))

;; Enable non serializing optimization.
;; (setq *compile-serializing* nil)

(define-condition compile-error (error) ())
(define-condition simple-compile-error (simple-error compile-error) ())

;; HACK Small hack for speed for now, only go through the slow generic-function
;; dispatch if it is at least an instance.
;; Once a new method is defined, this will be disabled again anyway.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *print-object-replaced* nil)
  (unless *print-object-replaced*
    (let ((generic-print-object #'cl:print-object))
      (defun cl:print-object (&rest rest)
        (let ((instance (first rest))
              (stream (second rest)))
          (if (lcl/clos::instance-p instance)
              (funcall generic-print-object instance stream)
              (cl-lib::%print-object instance stream)))))
    (setq *print-object-replaced* t)))

(defun compile (name &optional definition)
  (let ((warnings-p nil)
        (failure-p nil))
    (values
     (handler-case
         (handler-bind ((warning (lambda (c) (setq warnings-p t))))
           (cond
             ((and name definition) (%set-fdefinition name (eval definition)))
             ((and (not name) definition) (eval definition))
             ((symbolp name) name)
             (t (error "Invalid argument to compile ~A" name))))
       (error (c) (setq failure-p t)
         (format *error-output* "~&; Error in compile: ~A~%" c)
         (lambda (&rest) (error c))))
     warnings-p
     failure-p)))

(defun type-of (object)
  (let ((mt (lua-get-metatable object)))
    (cond
      ((eq object nil) 'null)
      ((eq object t) 'boolean)
      ((eq mt struct-metatable) (structure-name object))
      ((eq mt lcl/clos::instance-metatable) (class-name (class-of object)))
      ;; Additional cases

      (t (class-name (class-of object))))))
