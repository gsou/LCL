(in-package :cl-lib)
;;;; Functions that are implemented using native hooks.

;;; Lua helper functions
(defmacro lua-index-table (table index &optional default)
  (let ((ix (gensym))
        (def (gensym))
        (tbl (gensym)))
    `(let ((,tbl ,table) (,ix ,index) (,def ,default))
       (lua "((" ,tbl ")[" ,ix "] or " ,def ")"))))
(defmacro lua-raw-index-table (table index) `(lua "((" ,table ")[" (identity ,index) "])"))
(defmacro lua-set-table (table index value)
  (let ((s (gensym))
        (i (gensym))
        (tbl (gensym)))
    `(let ((,s ,value)
           (,i ,index)
           (,tbl ,table))
       (lua-push (,tbl "[" ,i "] = " ,s) ,s))))
(defmacro lua-get-metatable (table) `(lua "(getmetatable(" ,table ") or n)"))
(defmacro lua-set-metatable (table metatable) `(lua "(setmetatable(" ,table ", " ,metatable ") or n)"))
(defmacro lua-call (table &rest args)
  (let ((syms (mapcar (lambda (_) (declare (ignore _)) (gensym)) args)))
    `(let ,(mapcar #'list syms args) (funcall (lua ,table) ,@syms))))
(defmacro lua-mcall (obj fn &rest args)
  (let ((o (gensym))) `(let ((,o ,obj)) (funcall (lua ,o "." ,fn) ,@(cons o args)))))
(defmacro %modf (number) `(lua "(({math.modf(" ,number ")})[2])"))
;; (defmacro lua-nil () `(lua "(nil)"))
(defmacro %break (&optional value) `(lua-push ("do break end;") ,value))
(defmacro %return (&optional value) `(let ((v ,value)) (lua-push ("do return " v " end;") nil)))
(defun tostring (obj) (lua "tostring(" obj ")" ))
(defmacro lua-array (&rest elements) `(lua  "{" ,@(mapcan (lambda (el) (list `(let ((el ,el)) el) ", ")) elements) "}"))
(defmacro lua-nilp (val) `(lua "nil == " (identity ,val) " and CL.T or n"))

(defun new (&optional metatable &aux (table (lua "{}")))
  (when metatable (lua-call "setmetatable" table metatable))
  table)

(defun %pairs-value-list (table &key test)
  "Get the values in a TABLE into a list. The values can be filtered with the TEST predicate."
  (let (list)
    (%pairs key val table
            (when (or (not test) (funcall test val))
              (push val list)))
    list))
(defmacro %loop (&body body) `(while t ,@body))

;;; Special forms that can be macros

;; (defmacro unwind-protect (protect &body cleanup)
;;   `(%nlr () ,cleanup ,protect))
(defmacro let (binds &body body)
  (if (and binds (some (lambda (el) (boundp (if (consp el) (car el) el))) binds))
      ;; Manage special variables in let somehow
      (let* ((specials ())
             (binds-notsp
               (mapcar
                (lambda (el)
                  (let ((var (if (consp el) (car el) el))
                        (val (if (consp el) (cadr el))))
                    (if (boundp var)
                        (let ((varsym (gensym)))
                          (setq specials (cons (list varsym var) specials))
                          (list varsym `(prog1 ,var (setq ,var ,val)) ))
                        el)))
                binds)))
        `(%let ,specials
               (unwind-protect
                    (%let ,binds-notsp (progn ,@body))
                 ,@(mapcar (lambda (sp) `(setq ,(cadr sp) ,(car sp))) specials))))
      `(%let ,binds ,@body)))

(defmacro return (&optional exp)
  `(return-from nil ,exp))

;;; Basic
(defun car (list)
  "Returns the car of the given cons cell."
  ;; (lua list " == n and n or ('table' == type(" list ") and " list "[1]) or CL.ERROR(l(\"Taking CAR of not a list ~S\", " list "))")
  ;; (lua list " == n and n or " list "[1] or CL.ERROR(l(\"Taking CAR of not a list ~S\", " list "))")
  (if list (if (consp list) (lua list "[1]") (error 'type-error :form 'list :datum list :expected-type 'list))))

(defun cdr (list)
  "Returns the cdr of the given cons cell."
  ;; (lua list " == n and n or ('table' == type(" list ") and " list "[2]) or CL.ERROR(l(\"Taking CDR of not a list ~S\", " list "))")
  ;; (lua list " == n and n or " list "[2] or CL.ERROR(l(\"Taking CDR of not a list ~S\", " list "))")
  (if list (if (consp list) (lua list "[2]") (error 'type-error :form 'list :datum list :expected-type 'list))))

(defun rplaca (cons object)
  (if (consp cons) (lua-push (" " cons "[1] = " object) cons)
      (error 'type-error :form 'cons :datum cons :expected-type 'cons)))
(defun rplacd (cons object)
  (if (consp cons) (lua-push (" " cons "[2] = " object) cons)
      (error 'type-error :form 'cons :datum cons :expected-type 'cons)))

(defun cons (left right)
  "Construct a cons cell"
  (lua "{" left "," right "}"))

(defun list (&rest list) list)


(defun consp (cons)
  (lua "('table' == type(" cons ") and nil == getmetatable(" cons ")) and CL.T or n")
  ;; (and
  ;;  (eq "table" (lua "type(" cons ")"))
  ;;  (eq nil (lua-get-metatable cons)))
  )

(defun %consp (cons)
  "More precise but slow version of consp"
  (and
   (eq "table" (lua "type(" cons ")"))
   (eq 2 (%len cons))
   (null (lua-get-metatable cons))))

(defun eq (x y)
  (lua "((" x " == " y " ) and CL.T or n)"))

(defun characterp (char)
  (eq (lua-get-metatable char) char-metatable))
(defun char-code (char)
  (if (characterp char)
      (lua-index-table char "code")
      ;; FIXME Gets called with nil a lot.
      (if char
          (error 'type-error :form 'char :datum char :expected-type 'character))))
(defun code-char (code)
  (if (and (integerp code) (> char-code-limit code -1))
      (make-char (lua "string.char(" code ")"))
      (error 'type-error :form 'code :datum code :expected-type `(integer 0 (,char-code-limit)))))
(defun char-string (char)
  (if (characterp char)
      (lua-index-table char "char")
      (error 'type-error :form 'char :datum char :expected-type 'character)))
(defun char-downcase (char)
  (make-char (lua-call "string.lower" (char-string char))))
(defun char-upcase (char)
  (make-char (lua-call "string.upper" (char-string char))))

;;; Variables
(defun %stringp (string) (lua "((type(" string ") == 'string') and CL.T or n)"))
(defun complexp (number) (eq (lua-get-metatable number) complex-metatable))
(defun %rationalp (number) (eq (lua-get-metatable number) rational-metatable))
(defun %numberp (number) (lua "((type(" number ") == 'number') and CL.T or n)"))
(defun charp (char) (eq (lua-get-metatable char) char-metatable))

;;; Symbol

(defun symbolp (symbol) (eq (lua-get-metatable symbol) symbol-metatable))
(defun symbol-name (symbol)
  (if (symbolp symbol)
      (lua-index-table symbol "name")
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun symbol-package (symbol)
  (if (symbolp symbol)
      (lua-index-table symbol "package")
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun package-name (package) (lua-index-table package "name"))

(defun %get-package-by-name (name) (lua-index-table (lua "LCL") name))
;; TODO Support array strings
(defun intern (string &optional (package *package*))
  ;; XXX This is really dependant on the core implementation and is quite sensible to
  ;; changes there.
  (setq string (string string))
  (let ((pack (or (and (%stringp package) (%get-package-by-name package))
                  (and (symbolp package) (%get-package-by-name (symbol-name package)))
                  package)))
    (lua-index-table pack string)))

(defun get (symbol indicator &optional default)
  (if (symbolp symbol)
      (lua "(" symbol ".plist[" indicator "] or " default ")")
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))

(defun boundp (symbol)
  (if (symbolp symbol)
      (lua "(rawget(" symbol ",\"bound\") and CL.T or n)")
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun makunbound (symbol)
  (if (symbolp symbol)
      (lua-push (symbol ".bound = nil") symbol)
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun fmakunbound (symbol)
  (if (symbolp symbol)
      (lua-push (symbol ".fbound = nil") nil)
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol))
  symbol)
(defun fboundp (symbol)
  (cond
    ((symbolp symbol) (lua "(rawget(" symbol ", 'fbound') and " symbol " or n)"))
    ((and (consp symbol) (eq 'setf (car symbol)) (symbolp (cadr symbol)))
     (lua "(rawget(CL.CADR(" symbol "), 'setfbound') and " symbol " or n)"))
    (t (error 'type-error :form 'symbol :datum symbol :expected-type 'function-name))))
(defun fdefinition (function-name)
  (if (functionp function-name)
      (lua-index-table function-name "fbound")
      (error 'type-error :form 'function-name :datum function-name :expected-type 'function)))
(defun symbol-function (symbol)
  (if (symbolp symbol)
      (fdefinition symbol)
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun symbol-macro-boundp (symbol) (lua "(rawget(" symbol ",\"sm_bound\") and CL.T or n)"))
(defun %symbol-macro (symbol) (lua "(rawget(" symbol ",\"sm_bound\") or n)"))
(defmacro define-symbol-macro (symbol expansion)
  (if (symbolp symbol)
      `(eval-when (:compile-toplevel :load-toplevel :execute)
         (lua-set-table ',symbol "sm_bound" ',expansion)
         ',symbol)
      (error "define-symbol-macro, not a symbol")))

;;; Hash


(defun hash-table-p (ht) (eq (lua-get-metatable ht) hash-table-metatable))
(defun %arrayp (array) (if (eq (lua-get-metatable array) array-metatable) t))
(defun arrayp (array) (or (%arrayp array) (%stringp array)))
(defun %structp (obj) (eq (lua-get-metatable struct) struct-metatable))

;;; Functions and calling convention

(defun functionp (f) (if (eq (lua-get-metatable f) function-metatable) t))
(defun lambda-list (f &optional fallback)
  (cond
    ((functionp f) (lua-index-table f "arglist"))
    ((and (symbolp f) (fboundp f)) (lua-index-table (fdefinition f) "arglist"))
    (t (if fallback fallback (error 'type-error :form 'f :datum f :expected-type '(or symbol function))))))

;; (defun values-gen-binds (vals) (mapcar (lambda (v) (list (gensym) v)) vals))
;; (defun values-gen-list (vals)
;;   (cond
;;     ((null vals) nil)
;;     ((= 1 (length vals)) vals)
;;     (t (list* (car vals) "," (values-gen-list (cdr vals))))))
;; (defmacro values (&rest vals)
;;   (let* ((binds (values-gen-binds vals))
;;          (vars (mapcar #'car binds)))
;;     `(let ,binds (lua ,@(values-gen-list vars)))))
(defun values-list (list) (lua-call "unpack_lisp_list" list))
(defun values (&rest vals) (values-list vals))

(progn
  (defmacro multiple-value-prog1 (first &body forms)
    (let ((val (gensym)))
      `(let ((,val (multiple-value-list ,first)))
         (progn ,@forms (values-list ,val)))))
  (defmacro multiple-value-call (function-form &rest forms)
    `(apply ,function-form (append ,@(mapcar (lambda (f) `(multiple-value-list ,f)) forms))))
  ;; (defmacro multiple-value-list (form)
  ;;   (if (%stringp form)
  ;;       form
  ;;       `(lua-call "l" (lua "unpack_mv(" ;; (funcall (lambda () ,form))
  ;;                           ,form ")"))))
  (defmacro nth-value (n form) `(nth ,n (multiple-value-list ,form)))
  (defmacro multiple-value-setq (varrs form)
    (let ((temp (mapcar (lambda (el) (gensym)) varrs)))
      `(multiple-value-bind ,temp ,form
         ,@(mapcar (lambda (var temp) (list 'setq var temp)) varrs temp)))))

;;; Macros

(defun macro-function (symbol &optional environment)
  (when environment
    ;; Local macro definitions from env
    (let ((macro (find symbol (env-mlocals environment) :key #'env-def-name)))
      (when macro (%return (lcl/compiler::env-def-target macro)))))
  (when (fboundp symbol)
    (let ((fn (fdefinition symbol)))
      (when (eq macro-metatable (lua-get-metatable fn))
        (lua-index-table fn "macro_function")))))
;; Place holder, let the macro fallback to the call-to-lua
;; Will be replaced later by an actual implementation
(defun macroexpand (form &optional env) (values form nil))
(defun macroexpand-1 (form &optional env) (values form nil))


;;; Math functions

(defun %+ (a b) (lua "(" a "+" b ")"))
(defun %- (a b) (lua "(" a "-" b ")"))
(defun %* (a b) (lua "(" a "*" b ")"))
(defun %/ (a b) (lua "(" a "/" b ")"))
(defun %// (a b) (lua "bit.intdiv(" a "," b ")"))
(defun %% (a b) (lua "(" a "%" b ")"))
(defun %logand (a b) (lua "bit.band(" a "," b ")"))
(defun %logior (a b) (lua "bit.bor(" a "," b ")"))
(defun %logxor (a b) (lua "bit.bxor(" a "," b ")"))
(defun %lsh (a b) (lua "bit.lshift(" a "," b ")"))
(defun %rsh (a b) (lua "bit.rshift(" a "," b ")"))
(defun %lognot (a) (lua "bit.bnot(" a ")"))
(defun %<= (a b) (lua "((" a "<=" b ") and CL.T or n)"))
(defun %< (a b) (lua "((" a "<" b ") and CL.T or n)"))
(defun %> (a b) (lua "((" a ">" b ") and CL.T or n)"))
(defun %>= (a b) (lua "((" a ">=" b ") and CL.T or n)"))
(defun %= (a b) (lua "((" a " == " b ") and CL.T or n)"))
(defun %/= (a b) (lua "((" a "~=" b ") and CL.T or n)"))
(defun %len (a) (lua "(# " a ")"))

(defun %sqrt (number) (lua-call "math.sqrt" number))
(defun %cos (number) (lua-call "math.cos" number))
(defun %sin (number) (lua-call "math.sin" number))
(defun %tan (number) (lua-call "math.tan" number))
(defun %cosh (number) (lua-call "math.cosh" number))
(defun %sinh (number) (lua-call "math.sinh" number))
(defun %tanh (number) (lua-call "math.tanh" number))

(defun %exp (number) (lua-call "math.exp" number))
(defun %expt (number power) (lua-call "math.pow" number power))
(defun %log (number) (lua-call "math.log" number))
(defun %floor (number) (lua-call "math.floor" number))
(defun %asin (y) (lua-call "math.asin" y))
(defun %acos (y) (lua-call "math.acos" y))
(defun %atan2 (y x) (lua-call "math.atan2" y x))
(defun %modf (x) (values-list (lua "l(math.modf(" x "))")))


;;; Complex
(defun realpart (number)
  (cond
    ((complexp number) (lua-index-table number "r"))
    ((numberp number) number)
    (t (error 'type-error :form 'number :datum number :expected-type 'number))))
(defun imagpart (number)
  (cond
    ((complexp number) (lua-index-table number "i"))
    ((numberp number) (* 0 number))
    (t (error 'type-error :form 'number :datum number :expected-type 'number))))

(defun numerator (number)
  (cond
    ((%rationalp number) (lua-index-table number "n"))
    ((%numberp number) number)
    (t (error 'type-error :form 'number :datum number :expected-type 'ratio))))
(defun denominator (number)
  (cond
    ((%rationalp number) (lua-index-table number "d"))
    ((%numberp number) 1)
    (t (error "Can't get denominator of non rational ~S" number))))

;;; Symbol value
(defun make-symbol (string)
  (if (stringp string)
      (funcall #'alloc-symbol nil (string string))
      (error 'type-error :form 'string :datum string :expected-type 'string)))
(defun symbol-value (symbol)
  (if (symbolp symbol)
      (lua symbol ".bound")
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun set (symbol value)
  (if (symbolp symbol)
      (lua-push (symbol ".bound = " value ";") value)
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
