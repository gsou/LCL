(in-package :cl-lib)
;;;; Array related functions

;;; Array creation

(defun alloc-array () (new array-metatable))

(defun %raw-aref (array ix)
  (if (%stringp array)
      (char array ix)
      (lua-index-table (lua-index-table array "data") ix (lua-index-table array "default"))))
(defun row-major-aset (array index value)
  (let ((ix (+ index (lua-index-table array "offset" 0))))
    (lua-set-table (lua-index-table array "data") ix value)))

(defun %adjust-array (array new-dimensions)
  "Change the dimensions of an array"
    (lua-set-table array "dim" (if (listp new-dimensions) new-dimensions (list new-dimensions))))

(defun %mapcar-depth (f depth contents)
  (if (zerop depth)
      (funcall f contents)
      ;; XXX
      (map 'list (lambda (c) (%mapcar-depth f (1- depth) c)) contents)))
(defun %set-initial-contents (array contents)
  (let ((depth (array-rank array))
        (ix 0))
    (%mapcar-depth
     (lambda (val)
       (row-major-aset array ix val)
       (incf ix))
     depth contents)))
(defun adjust-array (array new-dimensions &key (element-type t) (initial-element 0) initial-contents
                                            fill-pointer displaced-to (displaced-index-offset 0))
  (%adjust-array array new-dimensions)
  (lua-set-table array "type" element-type)
  (lua-set-table array "default" initial-element)
  (lua-set-table array "fp" fill-pointer)
  (if displaced-to
      (progn
        (unless (arrayp displaced-to) (error "adjust-array"))
        (lua-set-table array "offset" displaced-index-offset)
        (lua-set-table array "data" (lua-index-table displaced-to "data"))
        (lua-set-table array "disp" displaced-to))
      (progn
        (lua-set-table array "disp" nil)
        (lua-set-table array "offset" 0)
        (lua-set-table array "data" (lua "{}"))))
  (when initial-contents (%set-initial-contents array initial-contents))
  ;; TODO reuse existing data if any
  array)

(defun make-array (dimensions &key (element-type t) (initial-element 0) initial-contents adjustable
                                fill-pointer displaced-to (displaced-index-offset 0))
  (let ((array (alloc-array)))
    (adjust-array array dimensions :element-type element-type
                                   :initial-element initial-element
                                   :initial-contents initial-contents
                                   :fill-pointer fill-pointer
                                   :displaced-to displaced-to
                                   :displaced-index-offset displaced-index-offset)
    array))

;;; Manipulation and access

(defun array-dimension (array n) (nth n (array-dimensions array)))

(defun array-dimensions (array)
  (check-type array array)
  (if (%stringp array)
      (list (%len array))
      (lua-index-table array "dim")))

(defun array-displacement (array)
  (if (arrayp array)
      (values
       (lua-index-table array "disp")
       (lua-index-table array "offset"))
      (error "array-displacement, not an array")))

(defun array-element-type (array)
  (cond
    ((%stringp array) 'character)
    ((arrayp array) (lua-index-table array "type"))
    (t (error 'simple-type-error
              :format-control "array-element-type: ~S is not an array"
              :format-arguments (list array)
              :form 'array
              :datum array
              :expected-type 'array))))



(defun array-rank (array) (length (array-dimensions array)))

(defun array-row-major-index (array &rest subscripts)
  (if (apply #'array-in-bounds-p array subscripts)
      (let ((acc 1)
            (pos 0)
            (sub (reverse subscripts)))
        (dolist (n (reverse (array-dimensions array)))
          (incf pos (* (car sub) acc))
          (setq sub (cdr sub))
          (setq acc (* acc n)))
        pos)
      (error "array-row-major-index, not in bounds")))

(defun array-fit-ref (array sub)
  "Check if the diven subscripts fit the rank of the array. "
  (unless (= (length sub) (array-rank array)) (error "Invalid number of subscripts"))
  t)

(defun array-in-bounds-p (array &rest subscripts)
  (array-fit-ref array subscripts)
  (every #'< subscripts (array-dimensions array)))

(defun adjustable-array-p (array)
  (check-type array array)
  t)

(defun fill-pointer (array)
  (if (and (arrayp array) (array-has-fill-pointer-p array))
      (lua-index-table array "fp")
      (error "fill-pointer, array does not have a fill pointer")))

(defun array-has-fill-pointer-p (array)
  (if (arrayp array)
      (if (lua-index-table array "fp") t)
      (error "array-has-fill-pointer-p, not an array")))

(defun set-fill-pointer (array fp)
  (when (array-has-fill-pointer-p array)
    (lua-set-table array "fp" fp)))

(defun array-total-size (array)
  (apply #'* (array-dimensions array)))

(defun aset (array value &rest subscripts)
  (let ((ix (+ (apply #'array-row-major-index array subscripts) (lua-index-table array "offset"))))
    (lua-set-table (lua-index-table array "data") ix value)))



(defun aref (array &rest subscripts)
  (let ((ix (+ (apply #'array-row-major-index array subscripts) (lua-index-table array "offset" 0))))
    (%raw-aref array ix)))
;; TODO Constraints on bit type
(function-setq  'bit #'aref)
(function-setq 'sbit #'aref)


(defun row-major-aref (array index)
  (let ((ix (+ index (lua-index-table array "offset" 0))))
    (%raw-aref array ix)))

(defun copy-array (array)
  (let ((arr (make-array (array-dimensions array)
                         :element-type (array-element-type array)
                         :fill-pointer (if (array-has-fill-pointer-p array) (fill-pointer array))))
        (ix 0)
        (len (array-total-size array)))
    (%loop
     (unless (< ix len) (%break))
     (row-major-aset arr ix (row-major-aref array ix))
     (setq ix (+ 1 ix)))
    arr))

(defun char (string index)
  (check-type string string)
  (if (%stringp string)
      (%char string index)
      (aref string index)))

(defun %raw-alength (array) (apply #'* (array-dimensions array)))


;;; Vectors

(defun vectorp (v) (and (arrayp v) (= 1 (array-rank v))))

(defun stringp (string)
  (or (%stringp string)
      (and (vectorp string) (subtypep (array-element-type string) 'character))))

(defun vector-push (new-element vector)
  (if (vectorp vector)
      (let ((fp (fill-pointer vector)))
        (when (array-in-bounds-p vector fp)
          (aset vector new-element fp)
          (set-fill-pointer vector (+ fp 1))
          fp))
      (error "vector-push, not a vector")))

(defun vector-push-extend (new-element vector &optional (extension 128))
  (when (and (vectorp vector) (not (array-in-bounds-p vector (fill-pointer vector))))
    (%adjust-array vector  (max (+ 1 (fill-pointer vector)) (+ (array-total-size vector) extension))))
  (vector-push new-element vector))

(defun vector-pop (vector)
  (if (vectorp vector)
      (if (> (fill-pointer vector) 0)
          (progn
            (set-fill-pointer vector (- (fill-pointer vector) 1))
            (aref vector (fill-pointer vector)))
          (error "vector-pop, no elements left"))
      (error "vector-pop, not a vector")))

(defun make-string (size &key (initial-element #\Space) element-type)
  (lua-call "string.rep" (string initial-element) size))

(defun %lisp-string (string)
  "Convert the string to a lisp-string. If the string is already a lisp string,
this is a no-op but this will convert to an array if the string is a raw lisp string."
  (cond
    ((%stringp string) (copy-seq string))
    ((stringp string) string)
    (t (error "%lisp-string: ~A is not a string." string))))

(defun %lua-string (string)
  "Convert the string to a lua-string. In most cases this will be a no-op, but
this will convert vectors of characters to printable strings."
  (cond
    ((%stringp string) string)
    ((stringp string)
     (if (<= (length string) 0)
         ""
         (let ((char-strings (map 'vector #'char-string string)))
           (concat-string
            (aref char-strings 0)
            ;; Optimise fold of the inner table.
            (lua-call "table.concat" (lua-index-table char-strings "data"))))))
    (t (error "%lua-string: ~A is not a string" string))))


;; Bit-arrays

(defun bit-vector-p (array) (and (arrayp array) (eq 'bit (array-element-type array))))

(defun bit-op (op bit-array1 bit-array2 &optional opt-arg)
  (check-type bit-array1 (array bit))
  (check-type bit-array2 (array bit))
  (unless (equal (array-dimensions bit-array1) (array-dimensions bit-array2))
    (error "Dimensions of ~S and ~S are not the same" bit-array1 bit-array2))
  (let ((dest (cond
                ((eq opt-arg t) bit-array1)
                ((null opt-arg) (make-array (array-dimensions bit-array1) :element-type 'bit))
                ((typep opt-arg '(array bit))
                 (unless (equal (array-dimensions bit-array1) (array-dimensions opt-arg))
                   (error "Dimensions of ~S and ~S are not the same" bit-array1 opt-arg))
                 opt-arg)
                (t (error "Invalid argument for opt-arg ~S" opt-arg)))))
    (dotimes (i (%raw-alength bit-array1))
      (row-major-aset dest i
         (funcall op (%raw-aref bit-array1 i)
                  (%raw-aref bit-array2 i))))
    dest))

(defun bit-and (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logand bit-array1 bit-array2 opt-arg))
(defun bit-andc1 (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logandc1 bit-array1 bit-array2 opt-arg))
(defun bit-andc2 (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logandc2 bit-array1 bit-array2 opt-arg))
(defun bit-eqv (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logeqv bit-array1 bit-array2 opt-arg))
(defun bit-ior (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logior bit-array1 bit-array2 opt-arg))
(defun bit-nand (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'lognand bit-array1 bit-array2 opt-arg))
(defun bit-nor (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'lognor bit-array1 bit-array2 opt-arg))
(defun bit-orc1 (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logorc1 bit-array1 bit-array2 opt-arg))
(defun bit-orc2 (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logorc2 bit-array1 bit-array2 opt-arg))
(defun bit-xor (bit-array1 bit-array2 &optional opt-arg)
  (bit-op #'logxor bit-array1 bit-array2 opt-arg))
(defun bit-not (bit-array &optional opt-arg)
  (bit-op (lambda (a b) (if (plusp a) 0 1)) bit-array bit-array opt-arg))
