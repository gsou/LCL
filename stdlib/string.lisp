(in-package :cl-lib)
;;;; String and Character

(defun %char (string index)
  "Char for a native string"
  (unless (stringp string) (error "char, ~S not a string" string))
  (unless (integerp index) (error "char, index ~S not a number" index))
  (make-char string index))
(defun char (string index) (%char string index))

(defun %set-char (string index char)
  (if (%stringp string)
      (concat-string
       (subseq string 0 index)
       (char-string char)
       (subseq string (+ index 1)))
      (progn
        (row-major-aset string index char)
        string)))

(defun char-int (char) (char-code char))

(defun schar (string index) (char string index))

(defun string (x)
  (cond
    ((characterp x) (char-string x))
    ((symbolp x) (symbol-name x))
    ((%stringp x) x)
    ((vectorp x)
     (if (zerop (length x))
         ""
         (if (subtypep (array-element-type x) 'character)
             (%lua-string x)
             (error 'type-error :form 'x :datum x :expected-type 'string))))
    ((stringp x) x)
    (t (error 'type-error :form 'x :datum x :expected-type 'string))))

(defmacro def-character-comp (name comp &optional key)
  `(defun ,name (c &rest cs &aux next)
     ,@(if key `((setq c (,key c))))
     (setq next (car cs))
     (%loop
      (if cs
          (progn
            ,@(if key `((setq next (,key next))))
            (unless (,comp c next) (%return nil))
            (setq c next)
            (setq next (cadr cs))
            (setq cs (cdr cs)))
          (%break)))
     t))

(def-character-comp char=  =  char-code)
(def-character-comp char<  <  char-code)
(def-character-comp char<= <= char-code)
(def-character-comp char>  <  char-code)
(def-character-comp char>= <= char-code)

;; TODO start1 start2 end1 end2
(def-character-comp string=  %=  string)
(def-character-comp string<  %<  string)
(def-character-comp string<= %<= string)
(def-character-comp string>  %<  string)
(def-character-comp string>= %<= string)

(defun char/= (c &rest cs)
  (if cs
      (and
       (apply #'char/= cs)
       (let ((ret t))
         (dolist (el cs)
           (when (char= c el)
             (setq ret nil)
             (return)))
         ret))
      t))

(defun string/= (c &rest cs)
  (if cs
      (and
       (apply #'string/= cs)
       (let ((ret t))
         (dolist (el cs)
           (when (string= c el)
             (setq ret nil)
             (return)))
         ret))
      t))

(defun simple-string-p (str) (stringp str))

(defun standard-char-p (character)
  (and
   (charp character)
   (let ((code (char-code character)))
     (or
      (and (>= code 32)                 ; space
           (<= code 126)                ; ~
           )
      (= code 10)))))

(defun upper-case-p (char)
  (if (charp char)
      (and (<= 65 (char-code char)) (<= (char-code char) 90))
      (error 'type-error :form 'char :datum char :expected-type 'character)))
(defun lower-case-p (char)
  (if (charp char)
      (and (<= 97 (char-code char)) (<= (char-code char) 122))
      (error 'type-error :form 'char :datum char :expected-type 'character)))
(defun both-case-p (char)
  (or (upper-case-p char) (lower-case-p char)))

(defun digit-char (weight &optional (radix 10))
  (cond
    ((> radix 36) (error "digit-char, radix must be <= 36"))
    ((< weight 0) (error "digit-char, weight must be >= 0"))
    ((>= weight radix) nil)
    ((< weight 10) (code-char (+ weight 48)))
    ((< weight 36) (code-char (+ weight 55)))
    (t (error "digit-char, Invalid arguments"))))
(defun char-digit (weight &optional (radix 10) &aux (tmp nil))
  (setq weight (char-code weight))
  (cond
    ((> radix 36) (error "char-digit, radix must be <= 36"))
    ((< weight 48) nil)
    ((< weight 58) (setq tmp (- weight 48)))
    ((< weight 65) nil)
    ((< weight 91) (setq tmp (- weight 55)))
    ((< weight 97) nil)
    ((< weight 123) (setq tmp (- weight 87))))
  (and tmp (< tmp radix) tmp))
(defun digit-char-p (char &optional (radix 10))
  (cond
    ((> radix 36) (error "digit-char-p, radix must be <= 36"))
    ((< radix 2) (error "digit-char-p, radix must be >= 2"))
    ((upper-case-p char) (< (- (char-code char) 55) radix))
    ((lower-case-p char) (< (- (char-code char) 87) radix))
    (t (let ((code (char-code char)))
         (if (and (<= code 57) (>= code 48))
             (< (- code 48) radix))))))
(defun alpha-char-p (char) (both-case-p char))
(defun alphanumericp (char)
  (or (alpha-char-p char) (digit-char-p char)))

(defun graphic-char-p (char)
  (let ((code (char-code char)))
    (and (>= code 32) (< code 128))))

(defun string-upcase (string &key (start 0) end)
  (setq string (string string))
  (if (and (not end) (= 0 start))
      (lua-call "string.upper" string)
      (concat-string
       (subseq string 0 start)
       (lua-call "string.upper" (subseq string start end))
       (if end (subseq string end) ""))))
(defun string-downcase (string &key (start 0) end)
  (setq string (string string))
  (if (and (not end) (= 0 start))
      (lua-call "string.lower" string)
      (concat-string
       (subseq string 0 start)
       (lua-call "string.lower" (subseq string start end))
       (if end (subseq string end) ""))))

(defun char-upcase   (character) (char (string-upcase   (char-string character)) 0))
(defun char-downcase (character) (char (string-downcase (char-string character)) 0))

(defun nstring-upcase (string &key (start 0) end) (string-upcase string :start start :end end))
(defun nstring-downcase (string &key (start 0) end) (string-downcase string :start start :end end))

(defmacro def-character-comp-ci (name wrapper)
  `(defun ,name (c &rest cs) (apply (function ,wrapper) (mapcar #'char-downcase (cons c cs)))))
(def-character-comp-ci char-equal char=)
(def-character-comp-ci char-not-equal char/=)
(def-character-comp-ci char-lessp char<)
(def-character-comp-ci char-greaterp char>)
(def-character-comp-ci char-not-greaterp char<=)
(def-character-comp-ci char-not-lessp char>=)

(defmacro def-string-comp-ci (name wrapper)
  `(defun ,name (c &rest cs) (apply (function ,wrapper) (mapcar #'string-downcase (cons c cs)))))
(def-string-comp-ci string-equal string=)
(def-string-comp-ci string-not-equal string/=)
(def-string-comp-ci string-lessp string<)
(def-string-comp-ci string-greaterp string>)
(def-string-comp-ci string-not-greaterp string<=)
(def-string-comp-ci string-not-lessp string>=)

(defvar *character-names* (make-hash-table))

;; This allow us to borrow the char-names from the bootstraping implementation
(defmacro def-all-chars (upto)
  (let (push)
    (let ((i 0))
      ;; dotimes does not exist yet
      (tagbody
       next
         (when (>= i upto) (go break))
         (let ((name (char-name (code-char i))))
           (setq push (cons `(sethash ,(string-upcase name) *character-names* ,i) push))
           (setq push (cons `(sethash ,i *character-names* ,name) push)))
         (setq i (+ i 1))
         (go next)
       break))
    `(progn ,@push)))

(def-all-chars 256)

;; Manually add alternative names
(sethash "LINEFEED" *character-names* 10)

(defun char-name (char) (gethash (char-code char) *character-names*))
(defun name-char (name) (let ((code (gethash (string-upcase name) *character-names*))) (and code (code-char code))))
