;; Destructuring and macro destructuring
(cl:in-package :cl-lib)

(defmacro destructuring-bind (lambda-list expression &body forms)
  (let* ((expr (gensym))
        (unused (gensym))
        (bindings (list (list expr expression))))
    (labels ((add (el) (setq bindings (cons el bindings)))
             (des-to (ll sym)
               (when (eq '&whole (car ll))
                 (add (list (cadr ll) expr))
                 (setq ll (cddr ll)))
               (let ((positional (lambda-get-pos ll))
                     (optional (lambda-get-optional ll))
                     (rest (or (lambda-get-rest ll) (cdr (last ll))))
                     (keys (lambda-get-key ll))
                     (aux (lambda-get-aux ll)))
                 (dolist (p positional)
                   (if (consp p)
                       (let ((e (gensym)))
                         (add `(,e (pop ,sym)))
                         (des-to p e))
                       (add `(,p (pop ,sym)))))
                 (dolist (o optional)
                   (let ((os (if (consp o) (car o) o))
                         (od (if (consp o) (cadr o)))
                         (o-opt (if (consp o) (caddr o)))
                         (ol (gensym)))
                     (when o-opt (add `(,o-opt (if ,sym t))))
                     (if (consp os)
                         (progn
                           (add `(,ol (if ,sym (pop ,sym) ,od)))
                           (des-to os ol))
                         (add `(,os (if ,sym (pop ,sym) ,od))))))
                 (when rest
                   (if (consp rest)
                       (des-to rest sym)
                       (add `(,rest ,sym))))
                 (dolist (k keys)
                   (let ((ks (if (consp k) (car k) k))
                         (kd (if (consp k) (cadr k)))
                         (k-opt (if (consp k) (caddr k))))
                     (let ((kv (gensym)))
                       (add `(,kv (getf ,sym ,(intern (symbol-name ks) :keyword) ',unused)))
                       (add `(,ks (if (eq ',unused ,kv) ,kd ,kv)))
                       (when k-opt
                         (add `(,k-opt (not (eq ',unused ,kv))))))))
                 (dolist (a aux) (add a)))))
      ;; (push `(,expr ,expression) bindings)
      (des-to lambda-list expr)
      `(let* ,(reverse bindings) ,@forms))))


;;; Macros

(defmacro lambda-macro (name arglist &body body)
  (labels ((get-env-arglist (list)
             (if list
                 (if (eq (car list) '&environment)
                     (cadr list)
                     (get-env-arglist (cdr list)))))
           (clear-env-arglist (list)
             (if list
                 (if (eq (car list) '&environment)
                     (cddr arglist)
                     (cons (car list) (clear-env-arglist (cdr list)))))))
    (let ((form (gensym)))
      ;; XXX Change the env gensym to any possible environment argument in the arglist
      ;; And in destructuring-bind, simply ignore &environment for now as a cheat ?
      `(function (nlambda ,name (,form &optional ,(or (get-env-arglist arglist) (gensym)))
                   (destructuring-bind ,(clear-env-arglist arglist) (cdr ,form) ,@body))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro defmacro (name arglist &body body)
    (let ((form (gensym)))
      `(eval-when (:compile-toplevel :load-toplevel :execute)
         (setq (macro-function ',name) (lambda-macro ,name ,arglist ,@body))
         ',name))))

(defun macroexpand-1 (form &optional env)
  (if (atom form)
      (values form nil)
      (let ((call (car form)))
        (if (atom call)
            (let ((macro (macro-function (car form) env)))
              (if macro
                  (values (funcall *macroexpand-hook* macro form env) t)
                  (values form nil)))
            ;; HACK, we should not reach here anyway ?
            (values form nil)))))

(defun macroexpand (form &optional env &aux expanded-p)
  (%loop
   (multiple-value-bind (new-form exp) (macroexpand-1 form env)
     (if exp
         (progn
           (setq form new-form)
           (setq expanded-p t))
         (%break))))
  (values form expanded-p))


;;; Compiler macros

(defvar *compiler-macros* (make-hash-table))

(defun compiler-macro-function (name &optional environment)
  (gethash name *compiler-macros*))

(eval-when (:compile-toplevel :execute)
  (defmacro define-compiler-macro (name lambda-list &body body)
    #+lcl
    (let ((arg (if (eq (car lambda-list) '&whole)
                   (cadr lambda-list)
                   (gensym)))
          (ll (if (eq (car lambda-list) '&whole)
                  (cddr lambda-list)
                  lambda-list))
          (env (gensym)))
      `(sethash ',name *compiler-macros*
                (lambda (,arg &optional ,env)
                  (destructuring-bind ,ll (cdr ,arg)
                    ,@body))))))

(defun call-to-ir (ast env parent)
  (multiple-value-bind (form expanded-p) (macroexpand ast env)
    (if expanded-p
        ;; Try again.
        (compile-to-ir form env parent)
        ;; Actual function
        (let ((fn (compiler-macro-function (car ast) env)))
          (if fn
              (let ((res (funcall fn ast env)))
                (if (eq res ast)
                    (function-call-to-ir ast env parent)
                    (compile-to-ir res env parent)))
              (function-call-to-ir ast env parent))))))

;; With profiling, these are the compiler macro that are the
;; most worth it to include directly here

;; (define-compiler-macro + (&whole w &rest args)
;;   (if (every #'numberp args)
;;       (apply #'+ args)
;;       (case (length args)
;;         (0 0)
;;         (1 (car args))
;;         (2 `(lua "(" ,(car args) " + " ,(second args) ")"))
;;         (3 `(lua "(" ,(first args) " + "
;;                  ,(second args) " + "
;;                  ,(third args) ")"))
;;         (4 `(lua "(" ,(first args) " + "
;;                  ,(second args) " + "
;;                  ,(third args) " + "
;;                  ,(fourth args) ")"))
;;         (t w))))
;; (define-compiler-macro * (&whole w &rest args)
;;   (if (every #'numberp args)
;;       (apply #'* args)
;;       (case (length args)
;;         (0 1)
;;         (1 (car args))
;;         (2 `(lua "(" ,(car args) " * " ,(second args) ")"))
;;         (3 `(lua "(" ,(first args) " * "
;;                  ,(second args) " * "
;;                  ,(third args) ")"))
;;         (4 `(lua "(" ,(first args) " * "
;;                  ,(second args) " * "
;;                  ,(third args) " * "
;;                  ,(fourth args) ")"))
;;         (t w))))
;; (define-compiler-macro - (&whole w &rest args)
;;   (if (every #'numberp args)
;;       (apply #'- args)
;;       (case (length args)
;;         (0 0)
;;         (1 `(lua "- " ,(car args)))
;;         (2 `(lua "(" ,(car args) " - " ,(second args) ")"))
;;         (3 `(lua "(" ,(first args) " - "
;;                  ,(second args) " - "
;;                  ,(third args) ")"))
;;         (4 `(lua "(" ,(first args) " - "
;;                  ,(second args) " - "
;;                  ,(third args) " - "
;;                  ,(fourth args) ")"))
;;         (t w))))

;; Optimize car and cdr, this makes the error message less clear tho
;; TODO Typing
;; (define-compiler-macro car (&whole c list)
;;   (if list `(lua "(" ,list ")[1]")))
;; (define-compiler-macro cdr (&whole c list)
;;   (if list `(lua "(" ,list ")[2]")))

(define-compiler-macro = (&whole w &rest args)
  (if (every #'numberp args)
      (apply #'= args)
      (case (length args)
        (2 `(eq ,(car args) ,(second args)))
        (t w))))

(define-compiler-macro < (&whole w &rest args)
  (if (every #'numberp args)
      (apply #'< args)
      (case (length args)
        (2 `(%generic-comp #'%< ,(car args) ,(second args)))
        (t w))))

;; (define-compiler-macro funcall (&whole w function &rest args)
;;   (if (symbolp function)
;;       ;; Convert to funcall-lua + array at least
;;       ;; XXX Possibly even shortcall here ?
;;       `(lua "CL_LIB['FUNCALL-LUA'](" ,function "," (lua-array ,@args) ")")
;;       w))
