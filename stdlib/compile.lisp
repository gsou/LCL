(in-package :cl-lib)

(defun lua-error (&optional (reason "")) "" (concat-string "(error(" (string-to-lua reason) "))"))
(defun lambda-get-pos (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&optional) nil)
    ((eq (car arglist) '&key) nil)
    ((eq (car arglist) '&rest) nil)
    ((eq (car arglist) '&body) nil)
    ((eq (car arglist) '&aux) nil)
    (t (cons (car arglist) (lambda-get-pos (cdr arglist))))))
(defun lambda-grab-optional (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&key) nil)
    ((eq (car arglist) '&rest) nil)
    ((eq (car arglist) '&body) nil)
    ((eq (car arglist) '&aux) nil)
    (t (cons (car arglist) (lambda-grab-optional (cdr arglist))))))
(defun lambda-get-optional (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&optional) (lambda-grab-optional (cdr arglist)))
    (t (lambda-get-optional (cdr arglist)))))
(defun lambda-get-rest (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&rest) (cadr arglist))
    ((eq (car arglist) '&body) (cadr arglist))
    (t (lambda-get-rest (cdr arglist)))))
(defun lambda-grab-key (arglist)
  (cond
    ((null arglist) nil)
    ((eq (car arglist) '&allow-other-keys) nil)
    ((eq (car arglist) '&optional) nil)
    ((eq (car arglist) '&rest) nil)
    ((eq (car arglist) '&body) nil)
    ((eq (car arglist) '&aux) nil)
    (t (cons (car arglist) (lambda-grab-key (cdr arglist))))))
(defun lambda-has-key (arglist) (find '&key arglist))
(defun lambda-get-key (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&key) (lambda-grab-key (cdr arglist)))
    (t (lambda-get-key (cdr arglist)))))
(defun lambda-grab-aux (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&optional) nil)
    ((eq (car arglist) '&rest) nil)
    ((eq (car arglist) '&body) nil)
    ((eq (car arglist) '&key) nil)
    (t (cons (car arglist) (lambda-grab-aux (cdr arglist))))))
(defun lambda-get-aux (arglist)
  (cond
    ((atom arglist) nil)
    ((eq (car arglist) '&aux) (lambda-grab-key (cdr arglist)))
    (t (lambda-get-aux (cdr arglist)))))

(defun group (list) (if list (acons (car list) (cadr list) (group (cddr list)))))

(defun char-to-lua (char)
  "Convert a character to lua code generating the char"
  (concat-string "LCL['CL-LIB']['MAKE-CHAR'](" (string-to-lua (string char)) ")"))

(defun interned-p (sym)
  (or
   (null sym)
   (let* ((pn (package-name (symbol-package sym)))
          (name (symbol-name sym))
          (package (lua-index-table (lua "LCL") pn)))
     (and
      package
      (eq sym (lua "rawget(" package ", " name ") or n"))))))

(defun symbol-to-lua (sym &optional env)
  "Get the lua expression that will index a symbol"
  ;; FIXME Proper escaping of symbols
  (let ((pn (package-name (symbol-package sym))))
    (cond
      ;; Nil shorthands
      ((not sym) "n")
      ;; Uninterned symbol that needs to be copied (only available in :eval mode).
      ((and (eq *compile-mode* :eval) (not (interned-p sym)))
       (force-quote-to-lua sym env))
      ((string= pn "CL-LIB")
       (concat-string "CL_LIB[\"" (symbol-name sym) "\"]"))
      ((string= pn "COMMON-LISP")
       (concat-string "CL[\"" (symbol-name sym) "\"]"))
      (t (concat-string "LCL[\"" (package-name (symbol-package sym)) "\"][\"" (symbol-name sym) "\"]")))))
(defun atom-to-lua-p (obj) (or (numberp obj) (%stringp obj) (symbolp obj) (charp obj)))
(defun atom-to-lua (obj &optional env)
  (cond
    ((numberp obj) (number-to-lua obj))
    ((%stringp obj) (string-to-lua obj))
    ((symbolp obj) (symbol-to-lua obj env))
    ((charp obj) (char-to-lua obj))
    ;; If we have any other object we could just send it as it at this point
    ;; FIXME ERROR HERE, should not happen
    (t (%quote-to-lua obj nil))
    ;; (t (lua-error (concat-string "Invalid atom" (tostring obj))))
    ))
(defun %serialize-table-to-lua (name obj env saved &optional metatable)
  (let ((acc ""))
    (%pairs key val obj
            (setq acc (concat-string
                       acc
                       (serialize-to-lua (concat-string name "[" (atom-to-lua key env) "]") val env saved))))
    (concat-string
     name " = {} "
     (if metatable (concat-string "setmetatable(" name ", " (symbol-value-to-lua metatable env) ")
") "")
     acc)))
(defun string-to-lua (str)
  "String to a lua literal."
  (funcall (lua "string.format") "%q" str))

(defun function-call-to-ir (ast env parent)
  ;; (long-function-call-to-ir ast env parent)
 (or
   (let ((arglist (lambda-list (car ast) :undefined)))
     (cond
       ((not (eq arglist :undefined)) (gen-short-call-to-ir ast env parent arglist))))
   (long-function-call-to-ir ast env parent)))
