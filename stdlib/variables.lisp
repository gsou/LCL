(in-package :cl-lib)
;;;; Variables, constants and symbols defined in CL

(defvar *package* (lua "LCL['CL-LIB']"))
(defvar *features* (list :lcl
                         #+sbcl :lcl-boot1
                         #+lcl-boot1 :lcl-boot2
                         :64-bit
                         :ansi-cl
                         :common-lisp
                         :ieee-floating-point
                         :little-endian))


(defvar *macroexpand-hook* #'funcall)
;; (defvar cl-lib::*gensym-counter* 0)



;; TODO 32bits
(defconstant most-positive-fixnum #x+10000000000000)
(defconstant most-negative-fixnum #x-10000000000000)

(defconstant call-arguments-limit 50)
(defconstant lambda-parameters-limit 50)
(defconstant lambda-list-keywords '(&allow-other-keys &aux &body &environment &key &optional &rest &whole))
(defconstant multiple-values-limit 253)
(defconstant array-dimension-limit 1024)
(defconstant array-total-size-limit 1024)
(defconstant array-rank-limit 8)


(defconstant char-code-limit 256)
(defconstant pi 3.141592653589793)

(defconstant boole-1     2)
(defconstant boole-2     3)
(defconstant boole-andc1 12)
(defconstant boole-andc2 13)
(defconstant boole-and   6)
(defconstant boole-c1    4)
(defconstant boole-c2    5)
(defconstant boole-clr   0)
(defconstant boole-eqv   9)
(defconstant boole-ior   7)
(defconstant boole-nand  10)
(defconstant boole-nor   11)
(defconstant boole-orc1  14)
(defconstant boole-orc2  15)
(defconstant boole-set   1)
(defconstant boole-xor   8)

(defconstant most-positive-short-float              #.most-positive-short-float)
(defconstant least-positive-short-float             #.least-positive-short-float)
(defconstant least-positive-normalized-short-float  #.least-positive-normalized-short-float)
(defconstant most-positive-single-float             #.most-positive-single-float)
(defconstant least-positive-single-float             #.least-positive-single-float)
(defconstant least-positive-normalized-single-float #.least-positive-normalized-single-float)
(defconstant most-positive-double-float             #.most-positive-double-float)
(defconstant least-positive-double-float            #.least-positive-double-float)
(defconstant least-positive-normalized-double-float #.least-positive-normalized-double-float)
(defconstant most-positive-long-float               #.most-positive-long-float)
(defconstant least-positive-long-float              #.least-positive-long-float)
(defconstant least-positive-normalized-long-float   #.least-positive-normalized-long-float)
(defconstant most-negative-short-float              #.most-negative-short-float)
(defconstant least-negative-short-float             #.least-negative-short-float)
(defconstant least-negative-normalized-short-float  #.least-negative-normalized-short-float)
(defconstant most-negative-single-float             #.most-negative-single-float)
(defconstant least-negative-single-float             #.least-negative-single-float)
(defconstant least-negative-normalized-single-float #.least-negative-normalized-single-float)
(defconstant most-negative-double-float             #.most-negative-double-float)
(defconstant least-negative-double-float            #.least-negative-double-float)
(defconstant least-negative-normalized-double-float #.least-negative-normalized-double-float)
(defconstant most-negative-long-float               #.most-negative-long-float)
(defconstant least-negative-long-float              #.least-negative-long-float)
(defconstant least-negative-normalized-long-float   #.least-negative-normalized-long-float)

(defconstant short-float-epsilon #.short-float-epsilon)
(defconstant short-float-negative-epsilon #.short-float-negative-epsilon)
(defconstant single-float-epsilon #.single-float-epsilon)
(defconstant single-float-negative-epsilon #.single-float-negative-epsilon)
(defconstant double-float-epsilon #.double-float-epsilon)
(defconstant double-float-negative-epsilon #.double-float-negative-epsilon)
(defconstant long-float-epsilon #.long-float-epsilon)
(defconstant long-float-negative-epsilon #.long-float-negative-epsilon)
