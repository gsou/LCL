(cl:in-package :cl-lib)

;;; Construction
(defun acons (key datum alist)
  (cons (cons key datum) alist))
(defun pairlis (keys data &optional alist)
  (if (and keys data)
      (acons (car keys) (car data) (pairlis (cdr keys) (cdr data) alist))
      alist))

(defun append-list (list)
  (if (cdr list)
      (let ((curr (car list))
            (rst (cdr list)))
        (if curr
            (cons (car curr) (append-list (cons (cdr curr) rst)))
            (append-list rst)))
      (car list)))
(defun append (&rest list)
  (append-list list))

(defun %list*-rec (list)
  (cond
    ((endp list) nil)
    ((null (cdr list)) (car list))
    (t (cons (car list) (%list*-rec (cdr list))))))
(defun list* (&rest list)
  (cond
    ((endp list) nil)
    ((null (cdr list)) (car list))
    (t (cons (car list) (%list*-rec (cdr list))))))

;;; Access

(defun rest (list) (cdr list))

(defun caar (list) (car (car list)))
(defun cadr (list) (car (cdr list)))
(defun cdar (list) (cdr (car list)))
(defun cddr (list) (cdr (cdr list)))

(defun caaar (list) (car (car (car list))))
(defun caadr (list) (car (car (cdr list))))
(defun cadar (list) (car (cdr (car list))))
(defun caddr (list) (car (cdr (cdr list))))
(defun cdaar (list) (cdr (car (car list))))
(defun cdadr (list) (cdr (car (cdr list))))
(defun cddar (list) (cdr (cdr (car list))))
(defun cdddr (list) (cdr (cdr (cdr list))))

(defun caaaar (list) (car (car (car (car list)))))
(defun caaadr (list) (car (car (car (cdr list)))))
(defun caadar (list) (car (car (cdr (car list)))))
(defun caaddr (list) (car (car (cdr (cdr list)))))
(defun cadaar (list) (car (cdr (car (car list)))))
(defun cadadr (list) (car (cdr (car (cdr list)))))
(defun caddar (list) (car (cdr (cdr (car list)))))
(defun cadddr (list) (car (cdr (cdr (cdr list)))))
(defun cdaaar (list) (cdr (car (car (car list)))))
(defun cdaadr (list) (cdr (car (car (cdr list)))))
(defun cdadar (list) (cdr (car (cdr (car list)))))
(defun cdaddr (list) (cdr (car (cdr (cdr list)))))
(defun cddaar (list) (cdr (cdr (car (car list)))))
(defun cddadr (list) (cdr (cdr (car (cdr list)))))
(defun cdddar (list) (cdr (cdr (cdr (car list)))))
(defun cddddr (list) (cdr (cdr (cdr (cdr list)))))

(defun rest (list) (cdr list))

;; TODO
(defun nthcdr (n list)
  (cond
    ((or (not (integerp n)) (< n 0)) (error 'type-error :form 'n :datum n :expected-type '(integer 0)))
    ((not (listp list)) (error 'type-error :form 'list :datum list :expected-type 'list))
    ((= n 0) list)
    (t (nthcdr (- n 1) (cdr list)))))
(defun nth (n list)
  (car (nthcdr n list)))

(defun first   (list) (nth 0 list))
(defun second  (list) (nth 1 list))
(defun third   (list) (nth 2 list))
(defun fourth  (list) (nth 3 list))
(defun fifth   (list) (nth 4 list))
(defun sixth   (list) (nth 5 list))
(defun seventh (list) (nth 6 list))
(defun eighth  (list) (nth 7 list))
(defun ninth   (list) (nth 8 list))
(defun tenth   (list) (nth 9 list))

(defun %assoc (item alist)
  (if alist
      (if (eq item (caar alist))
          (car alist)
          (assoc item (cdr alist)))))
(defun assoc-if (predicate alist &key (key #'identity))
  (if alist
      (cond
        ((not (listp (car alist)))
         (error 'type-error :form '(car alist) :datum (car alist) :expected-type 'list))
        ((and (car alist) (funcall predicate (funcall key (caar alist))))
         (car alist))
        (t (assoc-if predicate (cdr alist) :key key)))))
(defun assoc-if-not (predicate alist &key (key #'identity))
  (assoc-if (complement predicate) alist :key key))
(defun assoc (item alist &key (key #'identity) test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eq)))
    (assoc-if (lambda (el) (funcall fn item el)) alist :key key)))
(defun rassoc-if (predicate alist &key (key #'identity))
  (if alist
      (cond
        ((not (listp (car alist)))
         (error 'type-error :form '(car alist) :datum (car alist) :expected-type 'list))
        ((and (car alist) (funcall predicate (funcall key (cdar alist))))
         (car alist))
        (t (rassoc-if predicate (cdr alist) :key key)))))
(defun rassoc-if-not (predicate alist &key key)
  (rassoc-if (complement predicate) alist :key (or key #'identity)))
(defun rassoc (item alist &key (key #'identity) test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eq)))
    (rassoc-if (lambda (el) (funcall fn item el)) alist :key key)))

(defun dlist-length (list)
  (if (consp list)
      (+ 1 (dlist-length (cdr list)))
      0))

(defun butlast (list &optional (n 1))
  (cond
    ((or (not (integerp n)) (minusp n)) (error 'type-error :form 'n :datum n :expected-type '(integer 0)))
    ((null list) nil)
    ((not (consp list)) (error 'simple-type-error
                               :format-control "butlast, ~S is not a list"
                               :format-arguments (list list)
                               :form 'list :datum list :expected-type 'list))
    ((<= (dlist-length list) n) nil)
    (t (cons (car list) (butlast (cdr list) n)))))
(defun nbutlast (list &optional (n 1)) (butlast list n))

(defun %last (list)
  (cond
    ((null list) list)
    ((and (consp list) (not (consp (cdr list)))) list)
    (t (%last (cdr list)))))

(defun last (list &optional (n 1))
  (if (or (not (integerp n)) (minusp n))
      (error 'type-error :form 'n :datum n :expected-type '(integer 0))
      (nthcdr (max 0 (- (dlist-length list) n)) list)))

;;; Plists
(defun getf (plist indicator &optional default)
  (cond
    ((null plist) default)
    ((eq indicator (car plist)) (cadr plist))
    (t (getf (cddr plist) indicator default))))
(defun setgetf (plist indicator value)
  (let ((iter plist)
        (ret plist))
    (%loop
     (cond
       ((null iter)
        (setq ret (cons indicator (cons value plist)))
        (%break))
       ((eq indicator (car iter))
        (rplaca (cdr iter) value)
        (%break))
       (t (setq iter (cddr iter)))))
    ret))
(defun symbol-plist (symbol)
  (if (symbolp symbol)
      (let ((ret (lua-index-table symbol "plist" symbol)))
        (if (eq ret symbol)
            (progn (lua-set-table symbol "plist" nil) nil)
            ret))
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))
(defun get (symbol indicator &optional default)
  (getf (symbol-plist symbol) indicator default))

(defun %get-properties (plist indicator-list)
  (if plist
      (if (member (car plist) indicator-list)
          plist
          (%get-properties (cddr plist) indicator-list))))

(defun get-properties (plist indicator-list)
  (let ((p (%get-properties plist indicator-list)))
    (values (car p) (cadr p) p)))

;;; Copy
(defun copy-list (list)
  (if (consp list)
      (if list (cons (car list) (copy-list (cdr list))))
      list))
(defun copy-alist (list)
  (if (consp list)
      (if list (cons (copy-list (car list)) (copy-alist (cdr list))))
      list))
(defun copy-tree (list)
  (if (consp list)
      (if list (cons (copy-tree (car list)) (copy-tree (cdr list))))
      list))

(defun copy-symbol (symbol &optional copy-properties)
  (if (symbolp symbol)
      (let ((new (make-symbol (symbol-name symbol))))
        (when copy-properties
          (let ((cl (copy-list (symbol-plist symbol))))
            (lua-push (new ".bound = rawget(" symbol ", 'bound'); "
                       new ".fbound = rawget(" symbol ",'fbound'); "
                       new ".plist = " cl)
                      nil)))
        new)
      (error 'type-error :form 'symbol :datum symbol :expected-type 'symbol)))

;;; Test
(defun endp (list)
  (if list
      (if (consp list) nil (error 'simple-type-error
                                  :format-control "endp, ~S is not a list"
                                  :format-arguments (list list)
                                  :form 'list
                                  :datum list
                                  :expected-type 'list))
      t))
(defun atom (atom) (not (consp atom)))

(defun listp (list)
  (lua "(n == " list " or ('table' == type(" list ") and nil == getmetatable(" list "))) and CL.T or n")
  ;; (or (consp list) (null list))
  )
(defun proper-list-p (list)
  (cond
    ((null list) t)
    ((consp list) (proper-list-p (cdr list)))
    (t nil)))

;; (defun list-length (list)
;;   (if (listp list)
;;       (if list
;;           (+ 1 (list-length (cdr list)))
;;           0)
;;       (error "list-length, ~S is not a list" list)))



;;; Operations

(defun nconc (&rest lists)
  (let ((l (length lists)))
    (case l
      (0 nil)
      (1 (car lists))
      (t (cond
           ((consp (car lists))
            (rplacd (last (car lists)) (apply #'nconc (cdr lists)))
            (car lists))
           ((null (car lists)) (apply #'nconc (cdr lists)))
           (t (error "nconc"))
           )))))
(defun revappend (list tail) (nconc (reverse list) tail))
(defun nreconc (list tail) (nconc (nreverse list) tail))

(defmacro push (val place)
  `(setf ,place (cons ,val ,place)))
(defmacro pushnew (val place &rest options &key key test test-not)
  (let ((v (gensym)) (p (gensym)) (o (gensym)))
    `(let ((,v ,val) (,p ,place) (,o (list ,@options)))
       (if (member ,(if (getf options :key) `(funcall (getf ,o :key) ,v) v) ,p ,@options)
           ,p
           (push ,v ,place)))))
(defmacro pop (place)
  (let ((val (gensym)))
    `(let ((,val ,place)) (prog1 (car ,val) (setf ,place (cdr ,val))))))

;; TODO Incomplete
(defmacro dolist (bind &body body)
  (let ((var (car bind))
        (list (cadr bind)))
    `(block nil
       (mapcar (lambda (,var) ,@body) ,list)
       nil)))


(defun group (list) (if list (acons (car list) (cadr list) (group (cddr list)))))

(defun %any-done (lists &aux (ret nil))
  (%loop
   (if lists
       (if (car lists)
           (setq lists (cdr lists))
           (progn (setq ret t) (%break)))
       (%break)))
  ret)
(defun mapone (func list)
  (let ((acc nil)
        (last nil))
    (%loop
     (if list
         (let* ((next (cons (funcall func (car list)) nil)))
           (if last
               (progn (rplacd last next) (setq last next))
               (progn (setq acc next) (setq last next)))
           (setq list (cdr list)))
         (%break)))
    acc))

(defun mapcar (func &rest lists)
  (cond
    ((null lists) nil)
    ;; ((null (car lists)) nil)
    ((null (cdr lists)) (mapone func (car lists)))
    (t
     (let ((acc nil)
           (last nil))
       (%loop
        (if (or (null lists) (%any-done lists))
            (%break)
            (let ((next (cons (apply func (mapone #'car lists)) nil)))
              (if last
                  (progn (rplacd last next) (setq last next))
                  (progn (setq acc next) (setq last next)))
              (setq lists (mapone #'cdr lists)))))
       acc))))
(defun maplist (func &rest lists)
  (cond
    ((null lists) nil)
    ;; ((null (car lists)) nil)
    (t
     (let ((acc nil)
           (last nil))
       (%loop
        (if (or (null lists) (%any-done lists))
            (%break)
            (let ((next (cons (apply func lists) nil)))
              (if last
                  (progn (rplacd last next) (setq last next))
                  (progn (setq acc next) (setq last next)))
              (setq lists (mapone #'cdr lists)))))
       acc))))
(defun mapc (func &rest lists)
  (apply #'mapcar func lists)
  (car lists))
(defun mapl (func &rest lists)
  (apply #'maplist func lists)
  (car lists))
(defun mapcan (func &rest lists) (apply #'nconc (apply #'mapcar func lists)))
(defun mapcon (func &rest lists) (apply #'nconc (apply #'maplist func lists)))

(defun tailp (object list)
  (if (listp list)
      (%loop
       (cond
         ((eq object list) (%return t))
         ((atom list) (%return nil))
         (t (setq list (cdr list)))))
      (error "tailp, not a list")))
(defun ldiff (list object)
  (if (eq list object) nil
      (if (listp list)
          (let* ((copy (copy-list list))
                 (iter (cons nil copy)))
            (%loop
             (cond
               ((eq object list) (rplacd iter nil) (%return copy))
               ((atom list) (%return copy))
               (t (setq list (cdr list))
                  (setq iter (cdr iter))))))
          (error 'simple-type-error
                 :format-control "ldiff, not a list"
                 :format-arguments nil
                 :form 'list
                 :datum list
                 :expected-type 'list))))

(defun member-if (predicate list &key (key #'identity))
  (cond
    ((null list) nil)
    ((consp list)
     (if (funcall predicate (funcall (or key #'identity) (car list)))
         list
         (member-if predicate (cdr list) :key key)))))
(defun member-if-not (predicate list &key (key #'identity))
  (member-if (complement predicate) list :key key))
(defun member (item list &key (key #'identity) test test-not)
  (let ((func (or test (and test-not (complement test-not)) #'eq)))
    (member-if (lambda (el) (funcall func item el)) list :key key)))

;; Maybe place that somewhere else
;; (defmacro eval-when (situation &body body)
;;   (if (or (member :execute situation)
;;           (member 'cl:eval situation))
;;       `(progn ,@body)))
(defvar *eval-when-compile-or-load* nil)
(defvar *eval-when-enable-compile* nil)
(defvar *eval-when-enable-load* nil)
(defmacro eval-when (situation &body body)
  (if *eval-when-compile-or-load*
      `(%eval-when ,situation ,@body)
      (progn
        (when (and *eval-when-enable-compile*
                   (or (member :compile-toplevel situation)
                       (member "COMPILE" situation :key #'symbol-name)))
          (dolist (form body) (eval form)))
        (when (and *eval-when-enable-load*
                   (or (member :load-toplevel situation)
                       (member "LOAD" situation :key #'symbol-name)))
          (dolist (form body) (eval form)))
        (if (or (member :execute situation)
                (member 'cl:eval situation))
            `(progn ,@body)))))


(defun adjoin (item list &key (key #'identity) test test-not)
  (let ((func (or test (and test-not (complement test-not)) #'eq)))
    (if (member item list :key key :test func)
        list
        (cons item list))))
(defun intersection (list-1 list-2 &key (key #'identity) test test-not)
  (let ((func (or test (and test-not (complement test-not)) #'eq))
        list)
    (dolist (item list-1)
      (when (member (funcall key item) list-2 :key key :test func)
        (setq list (cons item list))))
    list))
(defun nintersection (list-1 list-2 &key (key #'identity) test test-not)
  (intersection list-1 list-2 :key key :test test :test-not test-not))
(defun union (list-1 list-2 &key (key #'identity) test test-not)
  (let ((func (or test (and test-not (complement test-not)) #'eq))
        (list list-2))
    (dolist (item list-1)
      (unless (member item list-2 :key key :test func)
        (setq list (cons item list))))
    list))
(defun nunion (list-1 list-2 &key (key #'identity) test test-not)
  (union list-1 list-2 :key key :test test :test-not test-not))

(defun count-if (predicate list &key from-end (start 0) end key)
  ;; TODO Start end
  (let ((count 0))
    (mapcar (lambda (el) (when (funcall predicate (funcall (or key #'identity) el)) (setq count (+ 1 count))))
            (if from-end
                (reverse list)
                list))
    count))
(defun count-if-not (predicate sequence &key from-end (start 0) end key)
  (count-if (complement predicate) sequence :from-end from-end :start start :end end :key key))
(defun count (item sequence &key from-end (start 0) end key test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eq)))
    (count-if (lambda (el) (funcall fn item el))
              sequence
              :from-end from-end
              :start start
              :end end
              :key key)))

(defun max (&rest reals)
  (if reals
      (let ((acc (car reals)))
        (dolist (n (cdr reals))
          (if (> n acc) (setq acc n)))
        acc)
      (error "Taking max of an empty list")))
(defun min (&rest reals)
  (if reals
      (let ((acc (car reals)))
        (dolist (n (cdr reals))
          (if (< n acc) (setq acc n)))
        acc)
      (error "Taking min of an empty list")))

(defun %reverse-accumulator (list acc)
  (if (endp list)
      acc
      (%reverse-accumulator (cdr list) (cons (car list) acc))))
(defun reverse (list) (%reverse-accumulator list ()))
(defun nreverse (list) (reverse list))

(let ((none (gensym)))
  (defun reduce (function sequence &key key from-end start end (initial-value none))
    ;; TODO Support start and end
    ;; TODO Support other sequences than lists
    (when (or start end) (error "TODO Support start and end for reduce"))
    (when from-end (setq sequence (reverse sequence)))
    (when key (setq sequence (mapcar key sequence)))
    (let (acc)
      (if (eq initial-value none)
          (progn
            (setq acc (car sequence))
            (setq sequence (cdr sequence)))
          (setq acc initial-value))
      (mapcar
       (if from-end
           (lambda (elem) (setq acc (funcall function elem acc)))
           (lambda (elem) (setq acc (funcall function acc elem))))
       sequence)
      acc)))

;; List versions, will be replaced by sequence functions
(defun every (predicate &rest seqs)
  (cond
    ((null seqs) (error "every, specify at least one seq"))
    ((null (car seqs)) t)
    (t (if (apply predicate (mapone #'car seqs))
           (apply #'every predicate (mapone #'cdr seqs))))))
(defun some (predicate &rest seqs)
  (cond
    ((null seqs) (error "some, specify at least one seq"))
    ((null (car seqs)) nil)
    (t (or (apply predicate (mapone #'car seqs))
           (apply #'some predicate (mapone #'cdr seqs))))))
(defun notevery (predicate &rest seqs) (apply #'some (complement predicate) seqs))
(defun notany (predicate &rest seqs) (apply #'every (complement predicate) seqs))

(defun set-difference (list-1 list-2 &key key test test-not)
  (remove-if (lambda (el) (member (if key (funcall key el) el) list-2 :test (or test (and test-not (complement test-not))))) list-1))
(defun nset-difference (list-1 list-2 &key key test test-not)
  (delete-if (lambda (el) (member (if key (funcall key el) el) list-2 :test (or test (and test-not (complement test-not))))) list-1))

(defun subsetp (list-1 list-2 &key key test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eql)))
    (every (lambda (el) (member (if key (funcall key el) el) list-2 :key key :test fn)) list-1)))

(defun tree-equal (tree-1 tree-2 &key test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eql)))
    (block nil
      (tagbody
       again
         (cond
           ((and (atom tree-1) (atom tree-2))
            (return (not (not (funcall fn tree-1 tree-2)))))
           ((and (consp tree-1) (consp tree-2))
            (when (not (tree-equal (car tree-1) (car tree-2) :test fn))
              (return nil))
            (setq tree-1 (cdr tree-1))
            (setq tree-2 (cdr tree-2))
            (go again))
           (t (return nil)))))))

(defun special-operator-p (symbol)
  (and (member symbol
               '(block catch eval-when flet function go if labels let let*
                 load-time-value locally macrolet multiple-value-call multiple-value-prog1
                 progn progv quote return-from setq symbol-macrolet tagbody
                 the throw unwind-protect))
       t))


(defun %nsublis-if (alist tree &key key)
  (when tree
    (let ((ret t) (iter alist))
      (%loop
       (unless iter (%break))
       (if (funcall (cdar iter) (if key (funcall key (car tree)) (car tree)))
           (progn
             (setq ret nil)
             (rplaca tree (caar iter))
             (%break)))
       (setq iter (cdr iter)))
      (if ret (if (listp (car tree)) (%nsublis-if alist (car tree) :key key))))
    (let ((ret t) (iter alist))
      (%loop
       (unless iter (%break))
       (if (funcall (cdar iter) (if key (funcall key (cdr tree)) (cdr tree)))
           (progn
             (setq ret nil)
             (rplacd tree (caar iter))
             (%break)))
       (setq iter (cdr iter)))
      (if ret (if (listp (cdr tree)) (%nsublis-if alist (cdr tree) :key key))))
    tree))
(defun nsublis-if (alist tree &key key)
  (let ((ret t) retval (iter alist))
      (%loop
       (unless iter (%break))
       (if (funcall (cdar iter) (if key (funcall key (cdr tree)) (cdr tree)))
           (progn
             (setq ret nil)
             (setq retval (caar iter))
             (%break)))
       (setq iter (cdr iter)))
    (if ret
        (%nsublis-if alist tree :key key)
        retval)))
(defun nsublis (alist tree &key key test test-not)
  (let ((fn (or test (and test-not (complement test-not)) #'eql)))
    (nsublis-if (mapcar (lambda (x) (cons (cdr x) (lambda (y) (funcall fn (car x) y)))) alist)
                tree
                :key key)))
(defun sublis (alist tree &key key test test-not)
  (nsublis alist (copy-tree tree) :key key :test test :test-not test-not))

(defun %nsubst-if (new predicate tree &key key)
  (when (consp tree)
    (if (funcall predicate (if key (funcall key (car tree)) (car tree)))
        (rplaca tree new)
        (if (listp (car tree)) (%nsubst-if new predicate (car tree) :key key)))
    (if (funcall predicate (if key (funcall key (cdr tree)) (cdr tree)))
        (rplacd tree new)
        (if (listp (cdr tree)) (%nsubst-if new predicate (cdr tree) :key key))))
  tree)
(defun nsubst-if (new predicate tree &key key)
  (if (funcall predicate (if key (funcall key tree) tree))
      new
      (%nsubst-if new predicate tree :key key)))
(defun nsubst-if-not (new predicate tree &key key) (nsubst-if new (complement predicate) tree :key key))
(defun nsubst (new old tree &key key test test-not)
  (let ((func (or test (and test-not (complement test-not)) #'eql)))
    (nsubst-if new (lambda (x) (funcall func old x)) tree :key key)))
(defun subst-if (new predicate tree &key key) (nsubst-if new predicate (copy-tree tree) :key key))
(defun subst-if-not (new predicate tree &key key) (nsubst-if-not new predicate (copy-tree tree) :key key))
(defun subst (new old tree &key key test test-not) (nsubst new old (copy-tree tree) :key key :test test :test-not test-not))
