(in-package :cl-lib)
;;;; Byte/Bytespec

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defstruct (bytespec (:conc-name :byte-))
    size
    position))

(defun byte (size position)
  (check-type size integer)
  (check-type position integer)
  (make-bytespec :size size :position position))

(defun byte-mask (bytespec) (ash (1- (ash 1 (byte-size bytespec))) (byte-position bytespec)))

(defun mask-field (bytespec integer) (logand (byte-mask bytespec) integer))
(defun deposit-field (newbyte bytespec integer)
  (logior
   (logandc1 (byte-mask bytespec) integer)
   (mask-field bytespec newbyte)))

(defun ldb (bytespec integer) (ash (mask-field bytespec integer) (- (byte-position bytespec))))
(defun ldb-test (bytespec integer) (/= 0 (ldb bytespec integer)))
(defun dpb (byte bytespec integer) (deposit-field (ash byte (byte-position bytespec)) bytespec integer))

(define-setf-expander mask-field (bytespec integer)
  (multiple-value-bind (dummies vals newval setters getter)
      (%get-setf-expansion integer)
    (let ((store (gensym))
          (bs (gensym)))
      (values (append dummies (list bs))
              (append vals (list bytespec))
              (list store)
              (%traverse-replace (car newval) `(deposit-field ,store ,bs ,getter) setters)
              `(mask-field ,bs ,getter)))))
(define-setf-expander ldb (bytespec integer)
  (multiple-value-bind (dummies vals newval setters getter)
      (%get-setf-expansion integer)
    (let ((store (gensym))
          (bs (gensym)))
      (values (append dummies (list bs))
              (append vals (list bytespec))
              (list store)
              (%traverse-replace (car newval) `(dpb ,store ,bs ,getter) setters)
              `(ldb ,bs ,getter)))))
