;; Structures support
(cl:in-package :cl-lib)

(defun alloc-struct () (new struct-metatable))
(defun copy-structure (struct)
  (let ((this (alloc-struct)))
    (%pairs key val struct (lua-set-table this key val))
    this))

(%regtype structure-object (t) (eq (lua-get-metatable el) struct-metatable))

(defparameter *structure-slots-table* (make-hash-table))
(defparameter *structure-slots-type* (make-hash-table))
(defparameter *structure-printer-functions* (make-hash-table))

(defun structure-name (struct)
  (if (eq (lua-get-metatable struct) struct-metatable)
      (lua-index-table struct "name")
      (error 'simple-type-error :format-control "STRUCTURE-NAME: ~S not a structure."
                                :format-arguments (list struct)
                                :form 'struct :datum struct :expected-type 'structure-object)))

(defun %check-struct-type (struct type)
  (let ((expected-type (gethash type *structure-slots-type*)))
    (cond
      ((typep struct expected-type)
       ;; XXX Also check for list vs vector
       (eq type (elt struct 0)))
      ((and (null expected-type) (eq (lua-get-metatable struct) struct-metatable))
       (let ((name (lua-index-table struct "name")))
         (or (eq name type)
             (subtypep name type)))))))

;; Temporary definition
(eval-when (:compile-toplevel :execute)
  (defun defstruct-pre-init (name &optional parents)))

(defmacro defstruct (name-and-options &body slots)
  (let*
      ((name (if (consp name-and-options) (car name-and-options) name-and-options))
       (options (if (consp name-and-options) (cdr name-and-options)))
       (conc-name (concat-string (symbol-name name) "-"))
       (constructor (concat-string "MAKE-" (symbol-name name)))
       constructor-arglist
       (copier (concat-string "COPY-" (symbol-name name)))
       include
       include-additional-slots
       (initial-offset 0)
       named
       (predicate (concat-string (symbol-name name) "-P"))
       print-function
       print-object
       type)
    (dolist (option options)
      (when (atom option) (setf option (list option)))
      (case (car option)
        (:conc-name
         (let ((s (cadr option)))
           (cond
             ((null s) (setq conc-name ""))
             ((symbolp s) (setq conc-name (symbol-name s)))
             (t (setq conc-name s)))))
        (:constructor
            (let ((s (cadr option))
                  (arglist (caddr option)))
              (cond
                ((null s) (setq constructor s))
                ((symbolp s) (setq constructor (symbol-name s)))
                (t (setq constructor s)))
              (setq constructor-arglist arglist)))
        (:copier
         (let ((s (cadr option)))
           (cond
             ((null s) (setq copier s))
             ((symbolp s) (setq copier (symbol-name s)))
             (t (setq copier s)))))
        (:predicate
         (let ((s (cadr option)))
           (cond
             ((null s) (setq predicate s))
             ((symbolp s) (setq predicate (symbol-name s)))
             (t (setq predicate s)))))
        (:type (setf type (cadr option)))
        (:named (setf named t))
        (:include
         (let* ((parent (cadr option))
                (override-slots (cddr option))
                (parent-slots (gethash parent *structure-slots-table* t)))
           (setq include parent)
           ;; Parent struct does not exist
           (if (eq t parent-slots) (error "Parent structure does not exist"))
           (setq include-additional-slots
                 (mapcar (lambda (slot)
                           (let ((ass (assoc (car slot) override-slots)))
                             (if ass (setf (car (cdr slot)) (cadr ass)))
                             slot))
                         (copy-list parent-slots)))))
        (:initial-offset (setf initial-offset (or (cadr option) 0)))
        (:print-function (setq print-function (or (cadr option) t)))
        (:print-object (setq print-object (or (cadr option) t)))
        ))
    (setq slots
          (mapcar (lambda (x) (if (consp x) x (list x nil)))
                  (append include-additional-slots slots)))

    #+sbcl (progn (sethash name *structure-slots-table* slots)
                  (sethash name *structure-slots-type* type))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (sethash ',name *structure-slots-table* ',slots)
       (sethash ',name *structure-slots-type* ',type)
       ,(when constructor
          `(defun ,(intern constructor) ,(or constructor-arglist `(&key ,@(mapcar (lambda (x) (list (car x) (cadr x))) slots)))
             ,(cond
                ((eq type 'list)
                 ;; List structure
                 `(list ,@(make-list initial-offset) ,@(if named `((quote ,name))) ,@(mapcar #'car slots)))
                ((or (eq type 'vector) (and (consp type) (eq 'vector (car type))))
                 `(vector ,@(make-list initial-offset) ,@(if named `((quote ,name))),@(mapcar #'car slots)))
                ((null type)
                 ;; Normal struct
                 (let ((struct (gensym)))
                   `(let ((,struct (alloc-struct)))
                      ;; Set name
                      (lua-set-table ,struct "name" ',name)
                      ;; Set fields
                      ,@(mapcar (lambda (x) `(lua-set-table ,struct ',(car x) ,(car x))) slots)
                      ,struct))))))
       ,(when copier
          `(defun ,(intern copier) (struct)
             ,(if type
                  `(copy-seq struct)
                  `(copy-structure struct))))

       ,@(when (or (null type) named)
           `((%regtype ,name (,@(if include (list include)) structure-object ,@(if (eq type 'list) '(list))
                              ,@(if (or (eq type 'vector) (and (listp type) (eq 'vector (car type)))) '(vector)) t)
              (%check-struct-type el ',name))
             (defstruct-pre-init ',name ,@(if include `((quote (,include)))))))

       ,@(when (and predicate (or (null type) named))
           `((defun ,(intern predicate) (struct)
               (typep struct ',name))))

       ,@(cond
           ((and print-function (not (eq t print-function)))
            `((defmethod print-object ((object ,name) stream)
                (funcall (function ,print-function) object stream *current-level*))))
           ((and print-object (not (eq t print-object)))
            `((defmethod print-object ((object ,name) stream)
                (funcall (function ,print-object) object stream))))
           ((or print-function print-object)
            `((defmethod print-object ((object ,name) stream)
                (call-next-method)))))

       ,@(let ((ix -1)
               (check (or (null type) named)))
           (when named (incf ix))
           (mapcar (lambda (s)
                     (let* ((slot-name (car s))
                            ;; (slot-default (cadr s))
                            (slot-options (cddr s))
                            (fn-name (intern (concat-string conc-name (symbol-name slot-name)))))
                       (incf ix)
                       `(progn
                          ,(unless (getf slot-options :read-only)
                             `(defsetf ,fn-name
                                  (lambda (place new)
                                    ,@(when check
                                        `((unless (%check-struct-type place ',name)
                                            (error 'simple-type-error
                                                   :format-control "In (setf ~A), ~S is not a structure of type ~A"
                                                   :format-arguments (list ',fn-name place ',name)
                                                   :form 'place
                                                   :datum place
                                                   :expected-type ',name))))
                                    ,(if type
                                         `(%setelt place ,(+ initial-offset ix) new)
                                         `(lua-set-table place ',slot-name new)))))
                          (defun ,fn-name
                              (struct)
                            ,@(when check
                                `((unless (%check-struct-type struct ',name)
                                    (error 'simple-type-error
                                           :format-control "In ~A, ~S is not a structure of type ~A"
                                           :format-arguments (list ',fn-name struct ',name)
                                           :form 'struct
                                           :datum struct
                                           :expected-type ',name))))
                            ,(if type
                                 `(elt struct ,(+ initial-offset ix))
                                 `(lua-index-table struct ',slot-name))))))
                   slots))
       ',name)))

#+lcl
(defmacro with-accessors ((&rest slot-entries) instance &body body)
  (let ((it (gensym "INSTANCE")))
    `(let ((,it ,instance))
       (symbol-macrolet ,(mapcar
                          (lambda (entry)
                            (destructuring-bind (name accessor) entry
                              `(,name (,accessor ,it))))
                          slot-entries)
         ,@body))))
