(in-package :cl-lib)
;;;; Math functions and macros

(defun zerop (number)
  (if (numberp number)
      (eq 0 number)
      (error "zerop, not a number")))
(defun plusp (number) (> number 0))
(defun minusp (number) (< number 0))


;; TODO Fix these
(defun %integerp (number) (and (%numberp number) (eq number (%// number 1))))
(defun fixnump (number) (%integerp number))
(defun rationalp (number)
  (or (%integerp number) (%rationalp number)))
(defun numberp (number)
  (or (realp number) (complexp number)))
;; XXX Can't really know the difference with a lua number
(defun integerp (number) (%numberp number))
(defun floatp (number) (%numberp number))
(defun realp (number)
  (or (rationalp number) (floatp number)))

(defun evenp (integer)
  (if (integerp integer)
      (eq 0 (%% integer 2))
      (error "EVENP expects an integer, got ~S" integer)))
(defun oddp (integer)
  (if (integerp integer)
      (eq 1 (%% integer 2))
      (error "ODDP expects an integer, got ~S" integer)))


;; Arithmetic & Logic
(defun + (&rest numbers)
  (let ((acc 0))
    (dolist (n numbers)
      (if (numberp n)
          (setq acc (%+ acc n))
          (error 'type-error :form 'n :datum n :expected-type 'number)))
    acc))
(defun - (&rest numbers)
  (cond
    ((= (length numbers) 1)
     (unless (numberp (car numbers))
         (error 'type-error :form '(car numbers) :datum acc :expected-type 'number))
     (%- 0 (car numbers)))
    ((= (length numbers) 0) (error "-, no arguments"))
    (t (let ((acc (car numbers)))
         (unless (numberp acc) (error 'type-error :form '(car numbers) :datum acc :expected-type 'number))
         (dolist (n (cdr numbers))
           (if (numberp n)
               (setq acc (%- acc n))
               (error 'type-error :form 'n :datum n :expected-type 'number)))
         acc))))
(defun * (&rest numbers)
  (let ((acc 1))
    (dolist (n numbers)
      (if (numberp n)
          (setq acc (%* acc n))
          (error 'type-error :form 'n :datum n :expected-type 'number)))
    acc))
(defun / (&rest numbers)
  (cond
    ((= (length numbers) 1)
     (unless (numberp (car numbers))
         (error 'type-error :form '(car numbers) :datum acc :expected-type 'number))
     (%/ 1 (car numbers)))
    ((= (length numbers) 0) (error "/, no arguments"))
    (t (let ((acc (car numbers)))
         (unless (numberp acc) (error 'type-error :form '(car numbers) :datum acc :expected-type 'number))
         (dolist (n (cdr numbers))
           (if (numberp n)
               (setq acc (%/ acc n))
               (error 'type-error :form 'n :datum n :expected-type 'number)))
         acc))))
(defun floor (number &optional (divisor 1))
  (let* ((quotient (%// (float number 1.0) (float divisor 1.0)))
         (remainder (- number (* quotient divisor))))
    (values quotient remainder)))
(defun ffloor (number &optional (divisor 1.0)) (floor number divisor))
(defun truncate (number &optional (divisor 1))
  (let* ((quotient (* (signum number) (signum divisor) (%// (float (abs number) 1.0) (float (abs divisor) 1.0))))
         (rem (- number (* quotient divisor))))
    (values quotient rem)))
(defun ftruncate (number &optional (divisor 1.0)) (truncate number divisor))
(defun ceiling (number &optional (divisor 1))
  (let* ((quotient (lua-call "math.ceil" (float (%/ number divisor) 1.0)))
         (rem (- number (* quotient divisor))))
    (values quotient rem)))
(defun fceiling (number &optional (divisor 1.0)) (ceiling number divisor))
(defun round (number &optional (divisor 1))
  (let* ((align (+ 0.5 (%/ (float number 1.0) (float divisor 1.0))))
         (rem (%modf align))
         (quotient (cond
                     ((not (zerop rem)) (lua-call "math.floor" (float align 1.0)))
                     ((evenp align) align)
                     (t (- align 1))))
         (rem (- number (* quotient divisor))))
    (values quotient rem)))
(defun fround (number &optional (divisor 1.0)) (round number divisor))
(defun mod (number divisor) (nth-value 1 (floor number divisor)))
(defun rem (number divisor) (nth-value 1 (truncate number divisor)))

(defun 1+ (number) (+ number 1))
(defun 1- (number) (- number 1))

(defun %generic-comp-fallback (fn a b)
  (funcall fn (float a 1.0) (float b 1.0)))
(defun %generic-comp (fn a b)
  (if (eq (lua-get-metatable a) (lua-get-metatable b))
      (funcall fn a b)
      (%generic-comp-fallback fn a b)))
(defun %compare-with (func numbers)
  (let ((len (length numbers)))
    (cond
      ((%= 0 len) (simple-error 'simple-program-error "Can't compare 0 elements"))
      ((%= 1 len) t)
      (t (if (%generic-comp func (car numbers) (cadr numbers))
             (%compare-with func (cdr numbers)))))))
(defun < (&rest numbers) (%compare-with  #'%< numbers))
(defun <= (&rest numbers) (%compare-with #'%<= numbers))
(defun > (&rest numbers) (%compare-with #'%> numbers))
(defun >= (&rest numbers) (%compare-with #'%>= numbers))
(defun = (&rest numbers) (%compare-with #'%= numbers))
(defun /= (a b) (%/= a b))

(defun logand (&rest integers)
  (if integers
      (let ((acc (car integers)))
        (dolist (n (cdr integers)) (setq acc (%logand acc n)))
        acc)
      -1))
(defun logandc1 (int1 int2) (%logand (%lognot int1) int2))
(defun logandc2 (int1 int2) (%logand int1 (%lognot int2)))
;; TODO LOGEQV
(defun logeqv (&rest integers)
  (let ((acc -1)) (dolist (n integers) (setq acc (logand (logorc1 acc n) (logorc2 acc n)))) acc))
(defun logior (&rest integers)
  (let ((acc 0)) (dolist (n integers) (setq acc (%logior acc n))) acc))
(defun lognand (int1 int2) (%lognot (%logand int1 int2)))
(defun lognor (int1 int2) (%lognot (%logior int1 int2)))
(defun lognot (int) (%lognot int))
(defun logorc1 (int1 int2) (%logior (%lognot int1) int2))
(defun logorc2 (int1 int2) (%logior int1 (%lognot int2)))
(defun logxor (&rest integers)
  (let ((acc 0)) (dolist (n integers) (setq acc (%logxor acc n))) acc))

(defun logbitp (index number)
  (if (integerp number)
      (/= 0 (logand (%lsh 1 index) number))
      (error "logbitp, not an integer")))
(defun logtest (int1 int2) (not (zerop (logand int1 int2))))

(defun boole (op int1 int2)
  (unless (and (integerp int1) (integerp int2)) (error "boole, not integers"))
  (cond
    ((= boole-1 op) int1)
    ((= boole-2 op) int2)
    ((= boole-andc1 op) (logandc1 int1 int2))
    ((= boole-andc2 op) (logandc2 int1 int2))
    ((= boole-and op) (logand int1 int2))
    ((= boole-c1 op) (%lognot int1))
    ((= boole-c2 op) (%lognot int2))
    ((= boole-clr op) 0)
    ((= boole-eqv op) (logxor int1 (%lognot int2)))
    ((= boole-ior op) (logior int1 int2))
    ((= boole-nand op) (lognand int1 int2))
    ((= boole-nor op) (lognor int1 int2))
    ((= boole-orc1 op) (logorc1 int1 int2))
    ((= boole-orc2 op) (logorc2 int1 int2))
    ((= boole-set op) -1)
    ((= boole-xor op) (logxor int1 int2))))

(defun complex (real &optional (imag 0))
  (if (= imag 0) real (make-complex real imag)))
(defun conjugate (number)
  (cond
    ((complexp number) (complex (realpart number) (- (imagpart number))))
    ((%numberp number) number)
    (t (error "conjugate, not a number"))))
(defun abs (number)
  (cond
    ((complexp number) (let ((r (realpart number))
                             (i (imagpart number)))
                         (%sqrt (float (+ (* r r) (* i i)) 1.0))))
    ((not (numberp number)) (error "abs, not a number"))
    ((< number 0) (- number))
    (t number)))
(defun phase (number) (atan (imagpart number) (realpart number)))
(defun ash (number count)
  (if (%numberp number)
      (if (< count 0)
          (%rsh number (- count))
          (%lsh number count))
      (error "ash, not a number")))
(defun signum (x) (if (zerop x) x (/ x (abs x))))
(defun sqrt (number)
  (cond
    ((complexp number) (expt number 0.5))
    ((< number 0) (complex 0 (%sqrt (- number))))
    (t (%sqrt number))))
(defun isqrt (number)
  (if (and (integerp number) (>= number 0))
      (%floor (%sqrt number))
      (error "isqrt, not a number")))
(defun cis (radians) (exp (* #C(0.0 1.0) radians)))


;;; Trig

(defun exp (number)
  (if (complexp number)
      (let ((r (realpart number))
            (i (imagpart number)))
        (* (exp r) (+ (cos i) (* #C(0.0 1.0) (sin i)))))
      (%exp (float number 1.0))))
(defun log (number &optional base)
  (cond
    ((null base)
     (if (or (complexp number) (< number 0))
         (complex (log (abs number)) (phase number))
         (%log (float number 1.0))))
    ((= 0 base) 0)
    (t (/ (log number) (log base)))))
(defun expt (base power)
  (cond
    ;; FIXME Still wrong.
    ((or (complexp base) (complexp power) (and (< base 0) (not (%integerp power))))
     (exp (* power (log base))))
    ((= base 0) (if (> power 0) 0 (error "expt, invalid operands")))
    (t (%expt (float base 1.0) (float power 1.0)))))
(defun cos (rad)
  (cond
    ((complexp rad)
     (let ((ix (* #C(0.0 1.0) rad)))
       (/ (+ (exp ix) (exp (- ix))) 2)))
    ((numberp rad) (%cos rad))
    (t
     (error 'simple-type-error
            :format-control "cos: ~S is not a number"
            :format-arguments (list rad)
            :form 'rad :datum rad :expected-type 'number))))
(defun sin (rad)
  (cond
    ((complexp rad)
     (let ((ix (* #C(0.0 1.0) rad)))
       (/ (- (exp ix) (exp (- ix))) #C(0 2))))
    ((numberp rad) (%sin rad))
    (t
     (error 'simple-type-error
            :format-control "sin: ~S is not a number"
            :format-arguments (list rad)
            :form 'rad :datum rad :expected-type 'number))))
(defun tan (rad)
  (cond
    ((complexp rad) (/ (sin rad) (cos rad)))
    ((numberp rad) (%tan rad))
    (t
     (error 'simple-type-error
            :format-control "tan: ~S is not a number"
            :format-arguments (list rad)
            :form 'rad :datum rad :expected-type 'number))))
(defun asin (number)
  (cond
    ((or (complexp number) (< number -1) (> number 1))
     (* #C(0.0 -1.0) (log (+ (* #C(0 1) number) (sqrt (- 1 (* number number)))))))
    ((numberp number) (%asin number))
    (t
     (error 'simple-type-error
            :format-control "asin: ~S is not a number"
            :format-arguments (list number)
            :form 'number :datum number :expected-type 'number))))
(defun acos (number)
  (cond
    ((or (complexp number) (< number -1) (> number 1))
     (- (/ pi 2) (asin number)))
    ((numberp number) (%acos number))
    (t
     (error 'simple-type-error
            :format-control "acos: ~S is not a number"
            :format-arguments (list number)
            :form 'number :datum number :expected-type 'number))))
(defun atan (number &optional x)
  (if x
      (%atan2 (float number 1.0) (float x 1.0))
      (* #C(0.0 -1.0) (log (* (+ 1 (* #C(0.0 1.0) number))
                              (sqrt (/ (+ 1 (* number number)))))))))


(defun sinh (x) (/ (- (exp x) (exp (- x))) 2))
(defun cosh (x) (/ (+ (exp x) (exp (- x))) 2))
(defun tanh (x)
  (let ((ex (exp x))
        (emx (exp (- x))))
    (/ (- ex emx) (+ ex emx))))
(defun asinh (x) (log (+ x (sqrt (+ 1 (* x x))))))
(defun acosh (x) (* 2 (log (+ (sqrt (/ (+ 1 x) 2)) (sqrt (/ (- x 1) 2))))))
(defun atanh (x) (/ (- (log (+ 1 x)) (log (- 1 x))) 2))

;;; Float
(defun float (number &optional prototype)
  (cond
    ((%rationalp number) (%/ (numerator number) (denominator number)))
    ((%numberp number) number)
    (t (error "Bad argument to float: ~A" number))))
(defun decode-float (float)
  (let (significand exponent)
    (lua-push (" " significand ", " exponent " = math.frexp(" float ")") nil)
    (values significand exponent (if (>= float 0.0) 1.0 -1.0))))
(defun float-radix (float) 2)
(defun scale-float (float integer)
  (lua "math.ldexp(" float "," integer ")")
  ;; (* float (expt (float-radix float) integer))
  )
(defun float-sign (float-1 &optional (float-2 1.0))
  (let ((a (abs float-2)))
    (if (>= float-1 0.0) a (- a))))
;; Only 64 bit floats in Lua
(defun float-digits (float)
  #+64-bit 53
  #-64-bit 24
  )
;; TODO Denormalized
(defun float-precision (float) (if (= 0.0 float) 0 (float-digits float)))
(defun integer-decode-float (float)
  (multiple-value-bind (m e s) (decode-float float)
    (let* ((dig (float-digits float))
           (sf (scale-float m dig)))
      (values (lua "math.floor(0.5+" sf ")") (- e dig) s))))
(defun integer-length (integer)
  (values (ceiling (log (if (minusp integer)
                            (- integer)
                            (1+ integer))
                        2))))

(defun logcount (integer)
  (let ((acc 0)
        (which (if (plusp integer) 1 0)))
    (dotimes (i (integer-length integer))
      (when (= which (ldb (byte 1 i) integer))
        (incf acc)))
    acc))

(defun first-bit-set (x &aux (acc 0))
  (%loop
   (cond
     ((zerop x) (%break))
     ((not (evenp x)) (%break))
     (t (incf acc) (setq x (floor x 2)))))
  acc)

;; FIXME
;; "Borrowed" from SBCL
(defun rational (x)
  "RATIONAL produces a rational number for any real numeric argument. This is
  more efficient than RATIONALIZE, but it assumes that floating-point is
  completely accurate, giving a result that isn't as pretty."
  (cond
    ((%numberp x)
     (multiple-value-bind (bits exp sign) (integer-decode-float x)
       (if (eql bits 0)
           0
           (let ((int (if (minusp sign) (- bits) bits)))
             (cond ((minusp exp)
                    ;; Instead of division (which also involves GCD)
                    ;; find the first set bit of the numerator and shift accordingly,
                    ;; as the denominator is a power of two.
                    (let* ((pexp (- exp))
                           (set (first-bit-set bits)))
                      (if (> pexp set)
                          (make-rational (ash int (- set))
                                         (let ((shift (- pexp set)))
                                           (ash 1 shift)))
                          (ash int exp))))
                   (t
                    (ash int exp)))))))
    ((%rationalp x) x)
    (t (error "Invalid argument to rational: ~A." x))))

;; TODO
(defun rationalize (x) (rational x))


;;; NT
(defun gcd (&rest integers)
  (when (notevery #'integerp integers) (error "gcd, not a number"))
  ;; TODO Reduce (lua "gcd(x,y)")
  (reduce (lambda (x y) (lua "gcd(" x "," y ")")) integers :initial-value 0 :key #'abs))
(defun lcm (&rest integers)
  (when (notevery #'integerp integers) (error "lcm, not a number"))
  (if integers
      (if (cdr integers)
          (let ((a (car integers))
                (b (apply #'lcm (cdr integers))))
            (%// (abs (* a b)) (gcd a b)))
          (car integers)
          )
      1)
  )


;;;; Time

(defconstant +unix-ts-to-universal+ 2209006800
  "Commmon Lisp Universal time at unix epoch")

(defun unix->universal-time (unix) (+ unix +unix-ts-to-universal+))
(defun universal-time->unix (unix) (- unix +unix-ts-to-universal+))
(defun get-universal-time () (unix->universal-time (lua "os.time()")))
