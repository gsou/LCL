(in-package :cl-lib)
;;;; SETF and SETF definitions

;; Setf expander
(defvar *setf-expander* (make-hash-table))
(defun %register-setf-expander (key function)
  (sethash key *setf-expander* function))
(defun %get-setf-expansion (place &optional env)
  (cond
    ((symbolp place)
     (let ((ss (gensym)))
       (values nil nil (list ss) (list 'setq place ss) place)))
    ((and (listp place) (gethash (car place) *setf-expander*))
     (apply (gethash (car place) *setf-expander*) (cdr place)))
    ((and (listp place) (not (listp (car place))) (fboundp `(setf ,(car place))))
     (if (cdr place)
         (let ((obj (gensym))
               (new (gensym)))
           ;; FIXME Incorrect
           (values (list obj) (list (cadr place)) (list new)
                   `(funcall (function (setf ,(car place))) ,new ,obj ,@(cddr place))
                   `(,(car place) ,obj ,@(cddr place))))
         (let ((new (gensym)))
           (values () () (list new)
                   `(funcall (function (setf ,(car place))) ,new)
                   `(,(car place))))))
    (t (error "Invalid place ~S, could not %GET-SETF-EXPANSION" place))))
(defun get-setf-expansion (place &optional env) (%get-setf-expansion place env))
(defun get-setf-expansion-or-eval (place &optional env)
  (cond
    ((symbolp place)
     (let ((ss (gensym)))
       (values nil nil (list ss) (list 'setq place ss) place)))
    ((and (listp place) (gethash (car place) *setf-expander*))
     (apply (gethash (car place) *setf-expander*) (cdr place)))
    (t (values nil nil nil nil place))))
(defun get-setf-all-expansion (places &optional env)
  (let (dummies vals newval setter getter)
    (dolist (p places)
      (multiple-value-bind (d v n s g) (get-setf-expansion-or-eval p)
        (setq dummies (append d dummies))
        (setq vals (append v vals))
        (setq newval (append n newval))
        (setq setter (append (list s) setter))
        (setq getter (append (list g) getter))))
    (values dummies vals newval setter getter)))
(defun gen-sym-all-getters (getters dummies vals)
  (let (syms)
    (dolist (g getters)
      (let ((s (gensym)))
        (setq syms (cons s syms))
        (setq dummies (cons s dummies))
        (setq vals (cons g vals))))
    (values syms dummies vals)))

(defun fdefinition (function-name)
  (cond
    ((symbolp function-name) (lua-index-table function-name "fbound"))
    ((and (consp function-name) (eq 'setf (car function-name)) (symbolp (cadr function-name)))
     (lua-index-table (cadr function-name) "setfbound"))
    (t (error 'type-error :form 'function-name :datum function-name :expected-type '(or symbol cons)))))

(defmacro define-setf-expander (access-fn lambda-list &body body)
  ;; Required for bootstrapping, as when bootstrapping eval-when is not correctly managed
  #+sbcl (%register-setf-expander access-fn (eval `(lambda ,lambda-list (block ,access-fn ,@body))))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (%register-setf-expander (quote ,access-fn) (lambda ,lambda-list (block ,access-fn ,@body)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun gen-params (ll &optional key)
    (cond
      ((null ll) (list nil))
      ((eq (car ll) '&aux) (list nil))
      ((eq (car ll) '&optional) (gen-params (cdr ll) key))
      ((eq (car ll) '&allow-other-keys) (gen-params (cdr ll) key))
      ((eq (car ll) '&key) (gen-params (cdr ll) t))
      ((eq (car ll) '&rest) (list (cadr ll)))
      ;; Optional or keyword arg
      ((listp (car ll))
       `(,@(if key (list (intern (symbol-name (caar ll)) :keyword))) ,(caar ll)
         ,@(gen-params (cdr ll) key)))
      ((atom (car ll))
       `(,@(if key (list (intern (symbol-name (car ll)) :keyword))) ,(car ll)
         ,@(gen-params (cdr ll) key)))
      (t (error "Unsupported lambda list ~S in define-modify-macro" ll)))))

(defmacro defsetf (access update &body body)
  (if (and body (or (> (length body) 1) (not (stringp (car body)))))
      (let ((store (car body))
            (b (cdr body))
            (x (gensym)))

        `(define-setf-expander ,access (&rest ,x)
           (multiple-value-bind (dummies vals newval setters getter) (get-setf-all-expansion ,x)
             (destructuring-bind ,update getter
               (let ,(mapcar (lambda (s) (list s '(gensym))) store)
                 (values dummies
                         vals
                         (list ,@store)
                         (progn ,@b)
                         (list 'apply (list 'function ',access) ,@(gen-params update))))))))
      `(define-setf-expander ,access (&rest x)
         (multiple-value-bind (dummies vals newval setters getter)
             (get-setf-all-expansion x)
           (multiple-value-bind (syms dummies-get vals-get)
               (gen-sym-all-getters getter dummies vals)
             (let ((store (gensym)))
               (values dummies-get
                       vals-get
                       (list store)
                       (append (list (quote ,update)) syms (list store))
                       (append (list (quote ,access)) syms))))))))

(defmacro define-modify-macro (name lambda-list fn &optional documentation)
  (let ((ref (gensym))
        (rest (gensym)))
    `(eval-when (:compile-toplevel :execute)
       (defmacro ,name (,ref &rest ,rest)
         ,@(if documentation (list documentation))
         (multiple-value-bind (dummies vals newval setter getter) (%get-setf-expansion ,ref)
           (list 'let (mapcar #'list dummies vals)
                 (list 'destructuring-bind ',lambda-list (cons 'list ,rest)
                       (list 'let (list (list (car newval) (list 'apply (list 'function ',fn) getter ,@(mapcar (lambda (x) `(quote ,x)) (gen-params lambda-list)))))
                             setter))))))))

(defun %is-setf-function-defined (place)
  (and (consp place) (fboundp `(setf ,(car place)))))
(defmacro %setf-one (place newvalue)
  (if (%is-setf-function-defined place)
      ;; Setf function
      `(funcall (function (setf ,(car place)))
        ,newvalue
        ,@(cdr place))
      ;; Setf expansion
      (multiple-value-bind (dummies vals newval setter getter)
          (%get-setf-expansion place)
        ;; (dbg "On expand: " dummies vals newval setter getter)
        `(let* ,(reverse (mapcar #'list dummies vals))
           (let ((,(car newval) ,newvalue))
             ,setter
             ,(car newval))))))

;; Add the (defun (setf name) ...) style definition
(defmacro %defun-setf (name arglist &body body)
  `(define-setf-expander ,name (&rest x)
     (multiple-value-bind (dummies vals newval setters getter)
         (get-setf-all-expansion x)
       (multiple-value-bind (syms dummies-get vals-get)
           (gen-sym-all-getters getter dummies vals)
         (let ((store (gensym)))
           (values dummies-get
                   vals-get
                   (list store)
                   (append (list 'funcall (list* 'lambda ',arglist ',body)) (list store) syms)
                   (append (list (quote ,name)) syms)))))))


;; XXX No need anymore, setf proper function name.
(defmacro defun (name arglist &body body)
  `(progn
     (setq (function ,name) (function (nlambda ,name ,arglist ,@body)))
     ',name))
;; (defmacro %defun (name arglist &body body)
;;   `(setq (function ,name) (function (nlambda ,name ,arglist ,@body))))
;; (defmacro defun (name arglist &body body)
;;   (cond
;;     ((and (consp name) (eq 'setf (car name)))
;;      `(%defun-setf ,(cadr name) ,arglist ,@body))
;;     ;; FIXME block name, block nil and make efficient
;;     ((symbolp name) `(%defun ,name ,arglist ,@body))
;;     (t (error "Invalid name ~S for defun" name))))

(defmacro setf (&rest pairs)
  (let ((place (car pairs))
        (newvalue (cadr pairs))
        (rst (cddr pairs)))
    `(progn
       (%setf-one ,place ,newvalue)
       ,@(if rst (list `(setf ,@rst))))))

(defmacro psetf (&rest rest)
  (let (preset postset)
    (dolist (l (group rest))
      (let ((sym (gensym)))
        (push (list sym (cdr l)) preset)
        (push sym postset)
        (push (car l) postset)))
    `(let ,(reverse preset) (setf ,@postset) nil)))

(defmacro psetq (&rest rest) `(psetf ,@rest))

(defmacro incf (place &optional (delta 1) )
  (multiple-value-bind (dummies vals newval setter getter)
      (%get-setf-expansion place)
    `(let ,(mapcar #'list dummies vals)
       (let ((,(car newval) (+ ,delta ,getter)))
         ,setter ,(car newval)))))
(defmacro decf (place &optional (delta 1))
  `(incf ,place (- ,delta)))

(defmacro rotatef (&rest places)
  (if places
      (let* ((getters nil)
             (builder (reduce (lambda (code next-place)
                                (multiple-value-bind (dummies vals newval setter getter)
                                    (%get-setf-expansion next-place)
                                  ;; TODO What if more than 1 newval
                                  (setq getters (cons (list (car newval) getter setter) getters))
                                  (lambda (inside)
                                    (funcall code
                                             `(let ,(mapcar #'list dummies vals)
                                                (let ,newval ,inside))))))
                              places
                              :initial-value (lambda (x) x))))
        (funcall
         builder
         `(progn
            ;; Gets
            ,@(let* ((storeto (mapcar #'car getters))
                     (storetorot (append (cdr storeto) (list (car storeto))))
                     (gets (mapcar #'cadr getters))
                     ;; (gettersrot (append (cdr gets) (list (car gets))))
                     )
                (mapcar (lambda (var val) `(setq ,var ,val)) storetorot gets))
            ;; Sets
            ,@(mapcar #'caddr getters)
            nil)))))

(defmacro shiftf (&rest places)
  (if (< (length places) 2)
      (error "SHIFTF requires at least two arguments")
      (let ((tmp (gensym)))
        `(let ((,tmp ,(car (last places))))
           (rotatef ,@(butlast places) ,tmp)
           ,tmp))))
