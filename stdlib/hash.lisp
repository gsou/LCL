(in-package :cl-lib)
;;;; Hash-table support

(defun clrhash (hashtable) (lua-set-table hashtable "data" (new)) hashtable)
(defun make-hash-table (&key test size rehash-size rehash-threshold)
  (clrhash (new hash-table-metatable)))
(defun sethash (key hashtable value)
  (lua-set-table (lua-index-table hashtable "data") key value)
  value)
(defun gethash (key hashtable &optional default)
  (let ((data (lua-index-table hashtable "data")))
    (if (lua "nil == " data "[ " key "] and CL.T or n")
        (values default nil)
        (values (lua data "[" key "]") t))))

(defun hash-table-count (hash-table)
  (let ((acc 0))
    (%pairs k v (lua-index-table hash-table "data") (setq acc (+ acc 1)))
    acc))
(defun hash-table-size (ht)
  (if (hash-table-p ht)
      0
      (error 'type-error :form 'ht :datum ht :expected-type 'hash-table)))
(defun hash-table-rehash-size (ht)
  (if (hash-table-p ht)
      1
      (error 'type-error :form 'ht :datum ht :expected-type 'hash-table)))
(defun hash-table-rehash-threshold (ht)
  (if (hash-table-p ht)
      0.5
      (error 'type-error :form 'ht :datum ht :expected-type 'hash-table)))
(defun hash-table-test (ht)
  (if (hash-table-p ht)
      'eql
      (error 'type-error :form 'ht :datum ht :expected-type 'hash-table)))

(let ((none (gensym)))
  (defun remhash (key hash-table)
    (let ((val (gethash key hash-table none)))
      (sethash key hash-table (lua "nil"))
      (not (eq val none)))))

(defun maphash (function hash-table)
  (%pairs key val (lua-index-table hash-table "data")
    (funcall function key val))
  nil)

(defun sxhash (x) x)

(defun %pairs-iterator (table)
  (let (next tbl (x (new)))
    (lua-push (next "," tbl "," x "[1] = pairs(" table ")")
              (lambda ()
                (setq x (lua "{" next "(" tbl ", " x "[1])}"))
                (if (lua x "[1] == nil" )
                    (values nil nil nil)
                    (values (lua x "[1]") (lua x "[2]") t))))))

(defun call-with-hash-table-iterator (hash-table function)
  (unless (hash-table-p hash-table)
    (error 'type-error :form 'hash-table :datum hash-table :expected-type 'hash-table))
  (let ((it (%pairs-iterator (lua-index-table hash-table "data"))))
    (funcall function it)))

(defmacro with-hash-table-iterator (namel &body body)
  (let ((it (gensym "IT")))
    (destructuring-bind (name hash-table) namel
      `(call-with-hash-table-iterator ,hash-table
        (lambda (,it)
          (macrolet ((,name ()
                       (list 'multiple-value-bind '(key value more) ',(list 'funcall it)
                             '(if more (values more key value) nil))))
            ,@body))))))
