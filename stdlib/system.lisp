(in-package :cl-lib)
;;;; System functions

(defstruct (random-state (:constructor %make-random-state)) seed)
(defun make-random-state (&optional state)
  (cond
    ((eq state t) (%make-random-state :seed (lua "os.time()")))
    ((eq state nil) (copy-random-state *random-state*))
    ((random-state-p state) (copy-random-state state))))
(defun %random (&optional (limit 4611686018427387903)) (lua "math.random(" limit ")"))
(defvar *random-state* (%make-random-state :seed (%random)))
(defun random (limit &optional (random-state *random-state*))
  (let ((seed (random-state-seed random-state)))
    (lua "math.randomseed(" seed ")")
    (prog1 (1- (%random limit))
      (setf (random-state-seed random-state) (%random)))))

(defun getenv (name)
  "Get the environment variable with the given name."
  (lua-call "os.getenv" (string name)))

(defun command-line-arguments ()
  "Get the command-line-arguments that were passed to the process."
  (let* ((ret ())
         (args (lua "arg"))
         (argl (lua "#arg"))
         (ix -1))
    (%loop
     (if (> ix argl) (%break))
     (push (lua-index-table args ix) ret)
     (incf ix))
    (nreverse ret)))

(defun gc () (lua "collectgarbage('collect')"))

(defun quit (&optional (code 0))
  "Stop the Lua process with the given CODE."
  (lua-call "os.exit" code))
