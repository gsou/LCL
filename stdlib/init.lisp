(in-package :cl-lib)
;;;; Must be loaded before anything else

(setq (macro-function 'defmacro)
      (function (lambda (form &optional env)
        (%let ((name    (second form))
              (arglist (third form))
              (body    (cdddr form)))
          `(setq (macro-function ',name)
                 (function (lambda (form &optional env) (apply (function (lambda ,arglist ,@body)) (cdr form)))))))))
