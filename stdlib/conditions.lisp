(in-package :cl-lib)
;;;; Condition structure and functions

(eval-when (:compile-toplevel :load-toplevel :execute) (use-package :closette))

(defvar cl::*break-on-signals* nil)

(defclass cl::condition () ())
(defclass cl::simple-condition (condition)
  ((format :initarg :format-control)
   (args :initarg :format-arguments :initform nil)))
(defun cl::simple-condition-format-arguments (condition)
  (slot-value condition 'args))
(defun cl::simple-condition-format-control (condition)
  (slot-value condition 'format))
(defclass cl::serious-condition (condition) ())
(defclass cl::storage-condition (serious-condition) ())
(defclass cl::error (serious-condition) ())
(defclass cl::type-error (error)
  ((form :initarg :form)
   (datum :initarg :datum
          :reader cl::type-error-datum)
   (expected-type
    :initarg :expected-type
    :reader cl::type-error-expected-type)))
(defclass cl::simple-error (simple-condition error) ())
(defclass cl::simple-type-error (simple-condition type-error) ())
(defclass cl::warning (condition) ())
(defclass cl::style-warning (warning) ())
(defclass cl::simple-warning (simple-condition warning) ())
(defclass cl::arithmetic-error (error)
  ((operation :initarg :operation
              :reader cl::arithmetic-error-operation)
   (operands :initarg :operands
             :reader cl::arithmetic-error-operands)))
(defclass cl::division-by-zero (arithmetic-error) ())
(defclass cl::cell-error (error)
  ((name :initarg :name
              :reader cl::cell-error-name)))
(defclass cl::undefined-function (cell-error) ())
(defclass cl::unbound-variable (cell-error) ())
(defclass cl::control-error (error) ())
(defclass cl::parse-error (error) ())
(defclass cl::program-error (error) ())
(defclass cl::stream-error (error)
  ((stream :initarg :stream
           :reader cl::stream-error-stream)))
(defclass cl::end-of-file (stream-error) ())
(defclass cl::reader-error (parse-error stream-error) ())
(defclass cl::package-error (error)
  ((package :initarg :package
            :reader cl::package-error-package)))

;; Custom LCL error types
(defclass simple-program-error (simple-error program-error) ())

(defgeneric report-condition (condition stream))
(defmethod report-condition ((c condition) stream)
  (write-string (concat-string "#<" (string (class-name (class-of c))) ">") stream))
(defmethod report-condition ((c simple-condition) stream)
  (write-string (concat-string "#<" (string (class-name (class-of c))) " "
                               (apply 'format nil (simple-condition-format-control c) (simple-condition-format-arguments c))
                               ">")
                stream))
(defmethod report-condition ((c type-error) stream)
  (write-string (concat-string "#<" (string (class-name (class-of c))) " "
                               "The value of "
                               (princ-to-string (slot-value c 'form)) ", "
                               (prin1-to-string (type-error-datum c)) ", "
                               "is not "
                               (princ-to-string (type-error-expected-type c)) ">")
                stream))
(defmethod report-condition ((c cell-error) stream)
  (format stream "#<~A ~S>" (string (class-name (class-of c))) (slot-value c 'name)))

(defmacro cl::define-condition (name types slots &rest options)
  (unless (some (lambda (e) (subclassp (find-class e) (find-class 'condition))) types)
    (pushnew 'condition types))
  (let* ((report (assoc :report options))
         (def `(progn
                (defclass ,name ,types ,slots ,@(remove :report options :key #'car))
                ,(when report
                   `(defmethod report-condition ((c ,name) stream)
                      (funcall ,(cadr report) c stream)))
                ',name)))
    `(eval-when (:compile-toplevel :load-toplevel :execute) ,def)))

(defmethod print-object ((instance condition) stream)
  (if *print-escape* (call-next-method) (report-condition instance stream)))

;;; TODO Thread storage for currently bound restarts and handlers

(defstruct thread-storage
  (handlers nil)
  (restarts nil))

(defvar *thread* (make-thread-storage))

'cl::restart-name
(defstruct cl::restart
  (name)
  (function)
  (interactive-function)
  (report-function)
  (test-function))



;;; handlers/signaling

(defmacro %with-thread-storage-handlers ((handlers) &body body)
  (let ((restore (gensym "RESTORE")))
    `(let ((,restore (thread-storage-handlers *thread*)))
       (unwind-protect
            (progn
              (setf (thread-storage-handlers *thread*) ,handlers)
              ,@body)
         (setf (thread-storage-handlers *thread*) ,restore)))))

(defmacro cl::handler-bind (binds &body forms)
  `(unwind-protect
        (progn
          (push
           (list
            ,@(mapcar (lambda (b) `(list ',(car b) ,(cadr b))) binds))
           (thread-storage-handlers *thread*))
          ,@forms)
     (pop (thread-storage-handlers *thread*))))
;; (defmacro cl::handler-bind (binds &body forms)
;;   `(unwind-protect
;;         (progn
;;           (push
;;            (list
;;             ,@(mapcar (lambda (b) `(list ',(car b) ,(cadr b))) binds))
;;            (thread-storage-handlers *thread*))
;;           ,@forms)
;;      (pop (thread-storage-handlers *thread*))))
(defmacro cl::ignore-errors (&body forms)
  (let ((err (gensym)))
    `(catch ',err
       (handler-bind
           ((error #'(lambda (condition) (throw ',err nil))))
         ,@forms))))
;; no-error-clause
(defmacro cl::handler-case (form &body specs)
  (let* ((err (gensym))
         (ok (gensym))
         (binds
           (mapcar
            (lambda (bind)
              `(,(car bind) #'(lambda ,(or (cadr bind) (list (gensym "CONDITION"))) (throw ',err (progn ,@(cddr bind))))))
            (remove-if (lambda (n) (eq :no-error (car n))) specs)))
         (no-error-clause (remove-if-not (lambda (n) (eq :no-error (car n))) specs))
         (no-error
           (if no-error-clause
               `#'(lambda ,@(cdar no-error-clause)))))
    `(catch ',err
       ,(if no-error
            `(apply ,no-error
                   (catch ',ok
                     (handler-bind ,binds
                       (throw ',ok (multiple-value-list ,form)))))
            `(handler-bind ,binds ,form)))))

;;; TODO Bind restarts to conditions
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro cl::restart-bind (restarts &body forms)
    `(unwind-protect
          (progn
            (push
             (list
              ,@(mapcar
                 (lambda (r)
                   (destructuring-bind (name function &rest kv-pairs) r
                     `(make-restart :name ',name :function ,function ,@kv-pairs)))
                 restarts))
             (thread-storage-restarts *thread*))
            ,@forms)
       (pop (thread-storage-restarts *thread*)))))

(defmacro cl::restart-case (restartable-form &body clauses)
  (let ((ret (gensym))
        (call (gensym)))
    `(catch ',ret
       (funcall
        (catch ',call
          (restart-bind
              ,(mapcar
                (lambda (clause)
                  (let ((name (first clause))
                        (lambdalist (second clause))
                        report
                        interactive
                        test
                        (rest (cddr clause)))
                    (%loop
                     (cond
                       ((eq (car rest) :report) (setq report (second rest)))
                       ((eq (car rest) :interactive) (setq interactive (second rest)))
                       ((eq (car rest) :test) (setq test (second rest)))
                       (t (%break)))
                     (setq rest (cddr rest)))
                    `(,name (function (lambda ,lambdalist (throw ',call (lambda () ,@rest))))
                      ,@(when report
                          `(:report-function
                            ,(if (stringp report)
                                 `(lambda (stream) (write-string ,report stream))
                                 `(function ,report))))
                      ,@(when interactive `(:interactive-function #',interactive))
                      ,@(when test `(:test-function #',test)))))
                clauses)
            (throw ',ret ,restartable-form)))))))
(defun cl::compute-restarts (&optional condition)
  (apply #'concatenate 'list (thread-storage-restarts *thread*)))
(defun cl::find-restart (identifier &optional condition)
  (if (restart-p identifier)
      identifier
      (find identifier (compute-restarts condition) :key #'restart-name)))
(defun cl::invoke-restart (restart &rest arguments)
  (let ((fn (find-restart restart)))
    (if fn
        (apply (restart-function fn) arguments)
        (error "Can't find restart ~S" restart))))
(defun cl::invoke-restart-interactively (restart)
  (let ((r (find-restart restart)))
    (if r
        (let ((int (restart-interactive-function r)))
          (if int
              (apply (restart-function r) (funcall int))
              (funcall (restart-function r))))
        (error "Can't find restart ~S" restart))))

(defmacro cl::with-simple-restart ((restart-name format-control
                                &rest format-arguments)
                               &body forms)
  `(restart-case (progn ,@forms)
     (,restart-name ()
       :report (lambda (stream)
                 (format stream ,format-control ,@format-arguments))
       (values nil t))))

(defvar cl::*debugger-hook* nil)
(defun cl::invoke-debugger (condition)
  ;; By default just use lua error system
  (let ((d cl::*debugger-hook*))
    (when d
      (let ((cl::*debugger-hook* nil))
        (funcall d condition d)))
    ;; Might as well show a stack trace since we're here.
    (lua "print('DEBUGGER TRACEBACK')")
    (lua "print(debug.traceback())")
    ;; And propagate the error
    (lua "error(" condition ")")))

;; XXX Probably wont work under resignaling
(defun %%signal (condition)
  (let ((class (class-of condition)))
    (mapl (lambda (hts)
            ;; One handler scope
            (dolist (hdl (car hts))
              ;; One handler
              ;; Maybe warning here ?
              (when (ignore-errors (subclassp class (find-class (car hdl))))
                ;; Handler matches, call it, with the handlers under it.
                (%with-thread-storage-handlers ((cdr hts))
                  (funcall (cadr hdl) condition))
                ;; If we are here, the handler declined, switch to next handler block
                (return))))
          (thread-storage-handlers *thread*))
    nil))

(defun %signal (condition)
  (when (typep condition *break-on-signals*)
    (restart-case (invoke-debugger (make-condition 'simple-condition :format-control "[*break-on-signals*]: ~A" :format-arguments (list condition)))
      (continue ()
        :report (lambda (s) (format s "Continue to signal")))))
  (%%signal condition))

(defun cl::make-condition (type &rest slot-init)
  (apply #'make-instance type slot-init))

(defun %make-condition-signal (signal-class datum arguments)
  (typecase datum
    (symbol (apply #'make-condition datum arguments))
    (string (make-condition signal-class :format-control datum :format-arguments arguments))
    (t (error "Can't make condition"))))


(defun cl::signal (datum &rest arguments)
  (%signal (%make-condition-signal 'simple-condition datum arguments)))

(defun cl::error (datum &rest arguments)
  (let ((condition (%make-condition-signal 'simple-error datum arguments)))
    (%signal condition)
    (invoke-debugger condition)))
(defun cl::cerror (continue-format-control datum &rest arguments)
  (restart-case (apply #'error datum arguments)
    (continue () :report (lambda (s) (apply #'format s continue-format-control arguments))))
  nil)

(defun simple-error (type datum &rest arguments)
  (error type :format-control datum :format-arguments arguments))
(defun simple-type-error (datum &rest arguments)
  (error 'simple-type-error :format-control datum :format-arguments arguments))

(defun cl::warn (datum &rest arguments)
  (block muffled
    (let ((condition (%make-condition-signal 'simple-warning datum arguments)))
      (restart-bind ((cl::muffle-warning (lambda () (return-from muffled nil))))
        (%signal condition)
        (fresh-line *error-output*)
        (write-string "WARNING: " *error-output*)
        (report-condition condition *error-output*)))))



;;; Default conditions and restarts

(defun cl::abort (&optional condition) (invoke-restart 'abort))
(defun cl::continue (&optional condition) (let ((r (find-restart 'continue))) (if r (invoke-restart r))))
(defun cl::muffle-warning (&optional condition) (invoke-restart 'muffle-warning))
(defun cl::store-value (&optional condition) (let ((r (find-restart 'store-value))) (if r (invoke-restart r x))))
(defun cl::use-value (&optional condition) (let ((r (find-restart 'use-value))) (if r (invoke-restart r x))))
