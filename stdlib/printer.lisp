(cl:in-package :cl-lib)
;;; Common Lisp Printer


;; Printer variables

(defvar *print-readably* nil)
(defvar *print-radix* nil)
(defvar *print-base* 10)
(defvar *print-circle* nil)
(defvar *print-pretty* nil)
(defvar *print-level* nil)
(defvar *print-lines* nil)
(defvar *print-length* nil)
(defvar *print-miser-width* nil)
(defvar *print-case* :upcase)
(defvar *print-pprint-dispatch* nil)
(defvar *print-gensym* t)
(defvar *print-escape* t)
(defvar *print-array* t)
(defvar *print-right-margin* nil)


;; Print object function


;; Default implementation, should be replaced when CLOS is loaded
;; This is why it must use a &rest argument
;; (defun print-object (object stream) (%print-object object stream) object)
(defun %print-object (object stream))
(defun print-object (&rest rest) (%print-object (first rest) (second rest)) (first rest))

(defun print-symbol-name (object)
  (if (and (eq (symbol-package :keyword) (symbol-package object))
           *print-escape*)
      (concat-string ":" (symbol-name object))
      (symbol-name object)))

(defun print-apply-case (string &optional (print-case *print-case*))
  ;; TODO
  string)

(defun %print-lua-table (object &optional (stream *standard-output*) &aux f)
  (write-string "#<{" stream)
  (%pairs key val object
          (when f (write-string ", " stream))
          (setq f t)
          (print-object key stream)
          (write-string " = " stream)
          (print-object val stream))
  (write-string "}>" stream)
  object)

(defun %print-string-escape (object stream)
  (write-string "\"" stream)
  (dotimes (n (length object))
    (let ((c (char object n)))
      (when (or (eq #\" c) (readtable-singleescapep c))
        (write-string "\\" stream))
      (write-char c stream)))
  (write-string "\"" stream))

(defun %print-int-radix (object stream &optional (radix *print-base*))
  (when *print-radix*
    (case radix
      (10)
      (2 (write-string "#b" stream))
      (8 (write-string "#o" stream))
      (16 (write-string "#x" stream))
      (t
       (write-string "#" stream)
       (write-string (tostring radix) stream)
       (write-string "r" stream))))
  (if (= radix 10)
      (write-string (tostring object) stream)
      (if (= 0 object)
          (write-string "0" stream)
          (let ((c ""))
            (when (< object 0)
              (write-string "-" stream)
              (setq object (- object)))
            (%loop
             (if (= 0 object) (%break))
             (multiple-value-bind (next mod) (floor object radix)
               (setq c (concat-string (char-string (digit-char mod radix)) c))
               (setq object next)))
            (write-string c stream))))
  (when (and *print-radix* (= 10 radix)) (write-string "." stream))
  object)

(defun %print-array-dims (stream object
                          &optional
                            (dims (array-dimensions object)) (refs nil) (last t))
  (if dims
      (progn
        (write-string "(" stream)
        (dotimes (ix (car dims))
          (%print-array-dims stream object (cdr dims) (cons ix refs)
                             (= ix (1- (car dims)))))
        (write-string ")" stream))
      (print-object (apply 'aref object (reverse refs)) stream))
  (unless last (write-string " " stream)))
(defun %print-object (object stream)
  (let ((escaping (or *print-readably* *print-escape*)))
    (typecase object
      (integer
       (%print-int-radix object stream))
      (ratio
       (print-object (numerator object) stream)
       (write-string "/" stream)
       (print-object (denominator object) stream))
      (float
       ;; TODO
       (write-string (tostring object) stream))
      (complex
       (write-string "#C(" stream)
       (print-object (realpart object) stream)
       (write-string " " stream)
       (print-object (imagpart object) stream)
       (write-string ")" stream))
      (character
       (if escaping
           (progn
             (write-string "#\\" stream)
             (if (readtable-constituentp object)
                 (write-char object stream)
                 (write-string (char-name object) stream)))
           (write-char object stream)))
      (symbol (write-string (print-symbol-name object) stream))
      (native-string
       (if escaping
           (%print-string-escape object stream)
           (write-string object stream)))
      (string
       (%print-object (%lua-string object) stream))
      (cons
       ;; TODO Detect more invalid lists and pass to print table
       (if (%consp object)
           (progn
             (write-string "(" stream)
             (let ((current object))
               (%loop
                (print-object (car current) stream)
                (cond
                  ((consp (cdr current))
                   (progn
                     (setq current (cdr current))
                     (write-string " " stream)))
                  ((null (cdr current)) (%break))
                  (t (write-string " . " stream)
                     (print-object (cdr current) stream)
                     (%break)))))
             (write-string ")" stream))
           (%print-lua-table object stream)))
      (vector
       (if (eq 'bit (array-element-type object))
           (if *print-array*
               (progn
                 (write-string "#*" stream)
                 (dotimes (n (length object))
                   (print-object (aref object n) stream)))
               (progn
                 (write-string "#<BIT-VECTOR (" stream)
                 (print-object (length object) stream)
                 (write-string ")>" stream)))
           (if *print-array*
               (progn
                 (write-string "#(" stream)
                 (dotimes (n (length object))
                   (if (> n 0) (write-string " " stream))
                   (print-object (aref object n) stream))
                 (write-string ")" stream))
               (progn
                 (write-string "#<ARRAY (" stream)
                 (print-object (length object) stream)
                 (write-string ")>" stream)))))
      (array
       (if *print-array*
           (progn
             (write-string "#" stream)
             (write-string (tostring (array-rank object)) stream)
             (write-string "A" stream)
             (%print-array-dims stream object))
           (progn
             (write-string "#<ARRAY " stream)
             (print-object (array-dimensions object) stream)
             (write-string ">" stream))
           ))
      (hash-table
       (write-string "#<HASH-TABLE {" stream)
       (write-string (tostring object) stream)
       (write-string "}>" stream))
      (pathname
       (write-string "#P" stream)
       (%print-string-escape (namestring object) stream)
       )
      (package
       (write-string "#<PACKAGE " stream)
       (write-string (package-name object) stream)
       (write-string ">" stream))
      ;; TODO Compiler scopes
      (lcl/compiler::scope
       (write-string "#<SCOPE " stream)
       (write-string (string (lcl/compiler::scope-name object)) stream)
       (write-string " :kind " stream)
       (write-string (string (lcl/compiler::scope-kind object)) stream)
       (when (lcl/compiler::scope-nlr object)
         (write-string " :nlr " stream)
         (write-string (prin1-to-string (lcl/compiler::scope-nlr object)) stream))
       (when (lcl/compiler::scope-arglist object)
         (write-string " &arglist " stream)
         (write-string (prin1-to-string (lcl/compiler::scope-arglist object)) stream))
       (write-string " &body " stream)
       (write-string (prin1-to-string (lcl/compiler::scope-body object)) stream)
       (write-string ">" stream))
      (structure-object
       ;; Use natively defined function for this one
       ;; Except if it's an object. In that case, it's supposed
       ;; to be managed by the generic function defined in CLOS
       ;; No need to check for standard-instance, we will never reach here with an object.
       (write-string "#S(" stream)
       (write-string (lua-index-table (lua-index-table object "name") "name") stream)
       (%pairs key val object
               (when (symbolp key)
                 (write-string " :" stream)
                 (write-string (symbol-name key) stream)
                 (write-string " " stream)
                 (print-object val stream)))
       (write-string ")" stream))
      (function
       (write-string "#<FUNCTION " stream)
       (if (lua-index-table object "debugName")
           (write-string (lua-index-table object "debugName") stream)
           (progn
             (write-string "{" stream)
             (write-string (tostring (lua-index-table object "func")) stream)
             (write-string "}" stream)))
       (write-string ">" stream))
      (t
       ;; Use lua printer in any other cases
       (write-string "#<" stream)
       (write-string (tostring object) stream)
       (write-string ">" stream)))))



;;; Printer functions

(defun write (object &key
                       (array nil arrayp)
                       (base nil basep)
                       (case nil casep)
                       (circle nil circlep)
                       (escape nil escapep)
                       (gensym nil gensymp)
                       (length nil lengthp)
                       (level nil levelp)
                       (lines nil linesp)
                       (miser-width nil widthp)
                       (pprint-dispatch nil dispatchp)
                       (pretty nil prettyp)
                       (radix nil radixp)
                       (readably nil readablyp)
                       (right-margin nil marginp)
                       (stream *standard-output*))
  ;; XXX Optimize
  (let ((print-array *print-array*)
        (print-base *print-base*)
        (print-case *print-case*)
        (print-circle *print-circle*)
        (print-escape *print-escape*)
        (print-gensym *print-gensym*)
        (print-length *print-length*)
        (print-level *print-level*)
        (print-lines *print-lines*)
        (print-miser-width *print-miser-width*)
        (print-pprint-dispatch *print-pprint-dispatch*)
        (print-pretty *print-pretty*)
        (print-radix *print-radix*)
        (print-readably *print-readably*)
        (print-right-margin *print-right-margin*))
    (if arrayp (setq *print-array* array))
    (if basep (setq *print-base* base))
    (if casep (setq *print-case* case))
    (if circlep (setq *print-circle* circle))
    (if escapep (setq *print-escape* escape))
    (if gensymp (setq *print-gensym* gensym))
    (if lengthp (setq *print-length* length))
    (if levelp (setq *print-level* level))
    (if linesp (setq *print-lines* lines))
    (if widthp (setq *print-miser-width* miser-width))
    (if dispatchp (setq *print-pprint-dispatch* pprint-dispatch))
    (if prettyp (setq *print-pretty* pretty))
    (if radixp (setq *print-radix* radix))
    (if readablyp (setq *print-readably* readably))
    (if marginp  (setq *print-right-margin* right-margin))
    (unwind-protect (print-object object stream)
      (if arrayp (setq *print-array* print-array))
      (if basep (setq *print-base* print-base))
      (if casep (setq *print-case* print-case))
      (if circlep (setq *print-circle* print-circle))
      (if escapep (setq *print-escape* print-escape))
      (if gensymp (setq *print-gensym* print-gensym))
      (if lengthp (setq *print-length* print-length))
      (if levelp (setq *print-level* print-level))
      (if linesp (setq *print-lines* print-lines))
      (if widthp (setq *print-miser-width* print-miser-width))
      (if dispatchp (setq *print-pprint-dispatch* print-pprint-dispatch))
      (if prettyp (setq *print-pretty* print-pretty))
      (if radixp (setq *print-radix* print-radix))
      (if readablyp (setq *print-readably* print-readably))
      (if marginp  (setq *print-right-margin* print-right-margin)))))

(defun prin1 (object &optional (output-stream *standard-output*))
  (write object :stream output-stream :escape t))

(defun princ (object &optional (output-stream *standard-output*))
  (write object :stream output-stream :escape nil :readably nil))

(defun print (object &optional (output-stream *standard-output*))
  (write-string "
" output-stream)
  (write object :stream output-stream)
  (write-string " " output-stream)
  object)

(defun pprint (object &optional (output-stream *standard-output*))
  (write-string "
" output-stream)
  (write object :stream output-stream :pretty t))

(defun prin1-to-string (object)
  (let ((stream (make-string-output-stream)))
    (prin1 object stream)
    (get-output-stream-string stream)))

(defun princ-to-string (object)
  (let ((stream (make-string-output-stream)))
    (princ object stream)
    (get-output-stream-string stream)))

(defun write-to-string (object &rest args)
  (let ((stream (make-string-output-stream)))
    (apply #'write object :stream stream args)
    (get-output-stream-string stream)))

(defun format (stream fmt &rest args)
  ;; Temp implementation of format
  (if (eq stream t) (setq stream *standard-output*))
  (if stream
      (progn
        (write-string fmt stream)
        (dolist (a args)
          (write-string " ")
          (print-object a stream)))
      (apply 'concat-string fmt (mapcar (lambda (a) (concat-string " " (princ-to-string a))) args))))
