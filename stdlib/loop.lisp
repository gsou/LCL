(in-package :cl-lib)
;;;; Iteration macros

(defmacro do-type (lett cmd binds end-res &body body)
  (let ((end-test-form (car end-res))
        (result-forms (cdr end-res))
        (lets (mapcar (lambda (x) (list (car x) (cadr x))) binds))
        iter)
    (push cmd iter)
    (dolist (b binds)
      ;; Rebind only if there is a third element.
      (when (cddr b)
        (push (car b) iter)
        (push (caddr b) iter)))
    `(block nil
       (,lett ,lets
              (tagbody
               again
                 (when ,end-test-form (go end))
                 ,@body
                 ,(reverse iter)
                 (go again)
               end)
              ,@result-forms))))
;; (defmacro do-type (lett cmd binds end-res &body body)
;;   (let ((end-test-form (car end-res))
;;         (result-forms (cdr end-res))
;;         (lets (mapcar (lambda (x) (list (car x) (cadr x))) binds))
;;         iter
;;         (next (gensym)))
;;     (push cmd iter)
;;     (dolist (b binds)
;;       ;; Rebind only if there is a third element.
;;       (when (cddr b)
;;         (push (car b) iter)
;;         (push (caddr b) iter)))
;;     `(,lett ,lets
;;        (block nil
;;          (tagbody
;;             ,next
;;             (when ,end-test-form (return (progn ,@result-forms)))
;;             ,@body
;;             ,(reverse iter)
;;             (go ,next))))))
(defmacro do (binds end &body body)
  `(do-type let psetf ,binds ,end ,@body))
(defmacro do* (binds end &body body)
  `(do-type let* setf ,binds ,end ,@body))

(defmacro dolist (var &body body)
  (let ((tmp (gensym))
        (v (car var))
        (list (cadr var))
        (ret (caddr var)))
    `(do* ((,tmp ,list (cdr ,tmp))
           (,v (car ,tmp) (car ,tmp)))
          ((not ,tmp) ,ret)
       ,@body)))
(defmacro dotimes (var &body body)
  (let ((v (car var))
        (cnt (cadr var))
        (res (caddr var))
        (cnt-var (gensym "COUNT")))
    `(let ((,cnt-var ,cnt))
       (do ((,v 0 (+ 1 ,v)))
           ((>= ,v ,cnt-var) ,res)
         ,@body))))


;; TODO Placement
(defun string-capitalize (string &key (start 0) end)
  (setq string (string string))
  (let ((l (length string))
        (state 0))
    (do* ((ix start (1+ ix)))
         ((or (and end (>= ix end)) (>= ix l)))
      (let ((ch (char string ix)))
        (ecase state
          (0
           ;; Waiting start of word
           (when (alphanumericp ch)
             (setq state 1)
             (setq string (%set-char string ix (char-upcase ch)))))
          (1
           ;; In word
           (if (alphanumericp ch)
               (setq string (%set-char string ix (char-downcase ch)))
               (setq state 0))))))
    string))
(defun nstring-capitalize (string &key (start 0) end) (string-capitalize string :start start :end end))

(defmacro until (cond &body body) `(while (not ,cond) ,@body))
