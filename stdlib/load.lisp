(cl:in-package :cl-lib)
;;; Compile file and load

(defvar *compile-file-pathname* nil)
(defvar *compile-file-truename* nil)
(defvar *compile-print* nil)
(defvar *compile-verbose* nil)

(defvar *load-pathname* nil)
(defvar *load-truename* nil)
(defvar *load-print* nil)
(defvar *load-verbose* nil)

;; FIXME
(defun %write-compile-form (stream form)
  (let ((*compile-mode* :serialize))
    (write-string "do " stream)
    (write-string (compile-to-lua form nil) stream)
    (write-string "; end
" stream)))

;; In list
;; (defvar *eval-when-compile-or-load* nil)
;; (defmacro eval-when (situation &body body)
;;   (if *eval-when-compile-or-load*
;;       `(%eval-when ,situation ,@body)
;;       (if (or (member :execute situation)
;;               (member 'cl:eval situation))
;;           `(progn ,@body))))

(defun compiler-print-form-print (form &optional (stream *standard-output*))
  (cond
    ((atom form) (%print-object form stream))
    ((eq 'defun (car form)) (%print-object `(defun ,(second form) ,(third form) '|...|) stream))
    ((eq 'defmacro (car form)) (%print-object `(defmacro ,(second form) ,(third form) '|...|) stream))
    ((eq 'eval-when (car form)) (%print-object `(eval-when ,(second form) '|...|) stream))
    ((eq 'in-package (car form)) (%print-object form stream))
    ((eq 'setq (car form)) (%print-object `(setq ,(second form) '|...|) stream))
    ((null form) (write-string "NIL" stream))
    (t (%print-object `(,(car form) '|...|) stream))))

(defun compile-file-pathname (input-file &key output-file &allow-other-keys)
  (or output-file (merge-pathnames (make-pathname :type "lua") input-file)))

(defun compile-file (input-file &key
                                  output-file
                                  (verbose *compile-verbose*)
                                  (print *compile-print*)
                                  external-format)
  (unless output-file (setq output-file (merge-pathnames (make-pathname :type "lua") input-file)))
  ;; (setq output-file (compile-file-pathname input-file :output-file output-file))
  (if (stringp input-file) (setq input-file (parse-namestring input-file)))

  (labels
      ((toplevel-compile-form (form register-load &optional (compile-eval nil) (compile-load t))
         "This processes the toplevel-form FORM. When the form needs to be loaded, it is passed to REGISTER-LOAD.
"
         (setq form (let ((*eval-when-compile-or-load* t)) (macroexpand form)))
         (cond
           ((and (listp form) (or (eq 'eval-when (car form)) (eq '%eval-when (car form))))
            (let ((situation (second form)))
              (when
                  ;; Need at least one of these...
                  (or (member :compile-toplevel situation)
                      (member "COMPILE" situation :key #'symbol-name)
                      (member :load-toplevel situation)
                      (member "LOAD" situation :key #'symbol-name))
                (dolist (b (cddr form))
                  (toplevel-compile-form
                   b
                   register-load
                   ;; XXX This is probably incorrect...
                   (or (member :compile-toplevel situation)
                       (member "COMPILE" situation :key #'symbol-name))
                   t
                   ;; (or (member :load-toplevel situation)
                   ;;     (member "LOAD" situation :key #'symbol-name))
                   )))))
           ((and (listp form) (eq '%let (car form)) (null (second form)))
            ;; Empty lets/progn get loaded separately.
            (dolist (b (cddr form)) (toplevel-compile-form b register-load compile-eval compile-load)))
           (t
            (when compile-eval (eval form))
            (when compile-load (funcall register-load form))))))

      (let ((bak-pn *compile-file-pathname*)
            (bak-tn *compile-file-truename*)
            (rt-bak *readtable*)
            (pak-bak *package*))

        (setq *compile-file-pathname* input-file)
        (setq *compile-file-truename* (truename input-file))

        (when verbose
          (write-string "; [COMPILER] Starting compilation of ")
          (write-string *compile-file-truename*))
        ;; TODO Verbose and print
        (unwind-protect
             (let ((done (gensym)))
               (with-open-file (i input-file)
                 (with-open-file (o output-file :direction :output)
                   (do ((form (read i nil done) (read i nil done)))
                       ((eq form done))
                     (when print
                       (write-string "; [COMPILER] Processing form: ")
                       (compiler-print-form-print form)
                       (write-string "
"))
                     (toplevel-compile-form form (lambda (f) (%write-compile-form o f)))
                     ;; (progn
                     ;;   (let ((*eval-when-enable-compile* t))
                     ;;     (setq form (nth-value 0 (macroexpand form))))
                     ;;   (%write-compile-form o form))
                     ))))

          (setq *compile-file-pathname* bak-pn)
          (setq *compile-file-truename* bak-tn)
          (setq *readtable* rt-bak)
          (setq *package* pak-bak))

        (truename output-file))))

(defun load (filespec &key (verbose *load-verbose*) (print *load-print*) if-does-not-exist external-format)
  (if (stringp filespec) (setq filespec (parse-namestring filespec)))

  (labels ((printing (form)
             (when print
               (write-string "; [COMPILER] Loading form: ")
               (compiler-print-form-print form)
               (write-string "
"))
             )
           (toplevel-load-form (form)
             (printing form)
             (setq form (let ((*eval-when-compile-or-load* t)) (macroexpand form)))
             (cond
               ((and (listp form) (or (eq 'eval-when (car form)) (eq '%eval-when (car form))))
                (when (or (member :load-toplevel (second form))
                          (member "LOAD" (second form) :key #'symbol-name)
                          (member :execute (second form))
                          (member "EVAL" (second form) :key #'symbol-name))
                  (dolist (b (cddr form)) (toplevel-load-form b))))
               ((and (listp form) (eq '%let (car form)) (null (second form)))
                ;; Empty lets/progn get loaded separately.
                (dolist (b (cddr form)) (toplevel-load-form b)))
               (t
                (eval form))))
           (load-all-stream (stream)
             (let ((done (gensym)))
               (do ((form (read stream nil done)
                          (read stream nil done)))
                   ((eq done form))
                 (toplevel-load-form form)))))
    (let ((rt-bak *readtable*)
          (pak-bak *package*))
      (when verbose
        (write-string "; [COMPILER] Loading from ")
        (princ filespec))
      (unwind-protect
           (etypecase filespec
             (pathname
              (let ((bak-pn *load-pathname*)
                    (bak-tn *load-truename*))
                (setq *load-pathname* filespec)
                (setq *load-truename* (truename filespec))
                (if (string= "lua" (pathname-type filespec))
                    (let ((ns (namestring filespec)))
                      (lua "dofile(" ns ")"))
                    (with-open-file (f filespec) (load-all-stream f)))
                (setq *load-pathname* bak-pn)
                (setq *load-truename* bak-tn)))
             (stream
              (load-all-stream filespec)))
        (setq *readtable* rt-bak)
        (setq *package* pak-bak))
      t)))

(defmacro with-compilation-unit (options &body body)
  `(progn ,@body))
