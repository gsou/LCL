(in-package :cl-lib)
;;; Equality functions

(defun neq (x y) (not (eq x y)))

(defun eql (x y)
  (eq x y))

(defun equal (x y)
  (cond
    ((eql x y) t)
    ((consp x) (and (consp y)
                    (equal (car x) (car y))
                    (equal (cdr x) (cdr y))))
    ((stringp x) (and (stringp y) (string= x y)))
    ;; TODO Paths
    ;; TODO Bit vectors
    (t nil)))

(defun equalp (x y)
  (cond ((eq x y) t)
        ((characterp x) (and (characterp y) (char-equal x y)))
        ((stringp x) (and (stringp y) (eq (string-downcase (string x)) (string-downcase (string y)))))
        ((numberp x) (and (numberp y) (= x y)))
        ((consp x)
         (and (consp y)
              (equalp (car x) (car y))
              (equalp (cdr x) (cdr y))))
        ;; TODO Structures
        ((arrayp x)
         (and (arrayp y)
              ;; string-equal is nearly 2x the speed of array-equalp for comparing strings
              (cond ((and (stringp x) (stringp y)) (string-equal x y))
                    ((and (bit-vector-p x) (bit-vector-p y)) (bit-vector-= x y))
                    (t (array-equalp x y)))))
        (t nil)))
