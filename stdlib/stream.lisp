(cl:in-package :cl-lib)
;;; Non-CLOS implementation of streams.

;;; Class hierarchy

;; (defclass concatenated-stream (stream)
;;   ((streams :initarg :streams
;;             :reader concatenated-stream-streams
;;             :type list
;;             :documentation "List of the streams to read from")))

;; (defclass echo-stream (stream)
;;   ((input-stream :initarg :input-stream
;;                  :reader echo-stream-input-stream
;;                  :type stream
;;                  :documentation "The input stream of the echo stream")
;;    (output-stream :initarg :output-stream
;;                   :reader echo-stream-output-stream
;;                   :type stream
;;                   :documentation "The output stream of the echo stream")))

;; (defclass synonym-stream (stream)
;;   ((symbol :initarg :symbol
;;            :reader synonym-stream-symbol
;;            :type symbol
;;            :documentation "Symbol representing the global variable for which to forward the stream operations")))

(defstruct stream (state t) (column 0))

;; XXX Do we also need column on input streams ? Now it is only considered in output streams.

(defstruct (two-way-stream (:constructor make-two-way-stream (input-stream output-stream &aux (state t) (column 0)))
                           #-sbcl (:include stream))
  (input-stream nil :type stream)
  (output-stream nil :type stream))

(defstruct (file-stream (:include stream))
  (unread nil)
  (last nil)
  (file nil)
  (pathname nil))

(defstruct (string-stream (:include stream))
  (string "")
  (pos 0))
(defun make-string-input-stream (string &optional (start 0) end) (make-string-stream :string (subseq string start end)))
(defun make-string-output-stream (&key element-type) (make-string-stream))
(defun get-output-stream-string (stream) (string-stream-string stream))

(defstruct (broadcast-stream (:constructor %make-broadcast-stream (streams &aux (state t) (column 0))) (:include stream))
  (streams nil))
(defun make-broadcast-stream (&rest streams) (%make-broadcast-stream streams))



;;; Standard streams

(defparameter *lua-stdin* (make-file-stream :file (lua "io.stdin")))
(defparameter *lua-stdout* (make-file-stream :file (lua "io.stdout")))
(defparameter *lua-stderr* (make-file-stream :file (lua "io.stderr")))
(defparameter *lua-io* (make-two-way-stream *lua-stdin* *lua-stdout*))

(defparameter *debug-io* *lua-io*)
(defparameter *query-io* *lua-io*)
(defparameter *terminal-io* *lua-io*)
(defparameter *error-output* *lua-stderr*)
(defparameter *standard-input* *lua-stdin*)
(defparameter *standard-output* *lua-stdout*)
(defparameter *trace-output* *lua-stdout*)


;;; Generic Stream operations

;; TODO stream-element-type
(defun stream-element-type (stream) 'character)

;; TODO
(defun stream-line-length (stream) 80)

(defun streamp (object) (typep object 'stream))

(defun close (stream &key abort)
  (setf (stream-state stream) nil)
  (etypecase stream
    (file-stream
     (not (not (lua-call (lua "pcall") (lambda () (lua-mcall (file-stream-file stream) "close"))))))
    (two-way-stream
     (close (two-way-stream-input-stream stream))
     (close (two-way-stream-output-stream stream)))
    (broadcast-stream
     (mapc #'close (broadcast-stream-streams stream)))))

(defun input-stream-p (stream)
  (typecase stream
    (file-stream t)
    (two-way-stream t)
    (string-stream t)
    (t nil)))

(defun output-stream-p (stream)
  (typecase stream
    (file-stream t)
    (two-way-stream t)
    (string-stream t)
    (broadcast-stream t)
    (t nil)))

(defun interactive-stream-p ())

(defun open-stream-p (stream)
  (if (streamp stream)
      (stream-state stream)
      (error 'type-error
             :form 'stream
             :datum stream
             :expected-type 'stream)))


;;; Input stream commads

(defun clear-input-g (stream)
  (etypecase stream
    (file-stream
     ;; TODO
     ;; (lua-mcall (file-stream-file stream) "read" "*a")
     )
    (two-way-stream
     (clear-input-g (two-way-stream-input-stream stream)))))
(defun clear-input (&optional (input-stream *standard-input*)) (clear-input-g input-stream))

;;; Read

(defun try-read-char (stream)
  (etypecase stream
    (file-stream
     (let ((unread (file-stream-unread stream)))
       (if unread
           (prog1 unread
             (setf (file-stream-unread stream) nil))
           (let* ((raw (lua-mcall (file-stream-file stream) "read" 1))
                  (c (lua raw " or n")))
             (if c (char c 0))))))
    (two-way-stream (try-read-char (two-way-stream-input-stream stream)))
    (string-stream
     (if (< (string-stream-pos stream) (length (string-stream-string stream)))
         (prog1
             (char (string-stream-string stream) (string-stream-pos stream))
           (incf (string-stream-pos stream)))))))

;;; Unread char
(defun push-unread-char (char stream)
  (etypecase stream
    (file-stream
     (setf (file-stream-unread stream) char))
    (two-way-stream
     (push-unread-char char (two-way-stream-input-stream stream)))
    (string-stream (decf (string-stream-pos stream)))))
(defun unread-char (char &optional (stream *standard-input*)) (push-unread-char char stream))


(defun read-char (&optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  (or (try-read-char input-stream)
      (if eof-error-p
          (error "EOF")
          eof-value)))
(defun read-byte (stream &optional (eof-error-p t) eof-value)
  (char-code (read-char stream eof-error-p eof-value)))

(defun read-char-no-hang (&optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  ;; TODO
  (read-char input-stream eof-error-p eof-value recursive-p))

(defun %peek-char (stream)
  (let ((char (read-char stream)))
    (unread-char char)
    char))

(defun read-sequence (sequence stream &key (start 0) end)
  (check-type sequence sequence)
  (unless end (setq end (length sequence)))
  (do ((i start (+ 1 i)))
      ((>= i end) i)
    ;; TODO When not a char stream, read byte
    (let ((char (read-char stream nil nil)))
      (if char
          (setf (elt sequence i) char)
          (return-from read-sequence i)))))

(defun write-sequence (sequence stream &key (start 0) end)
  (check-type sequence sequence)
  (unless end (setq end (length sequence)))
  (do ((i start (+ 1 i)))
      ((>= i end) sequence)
    ;; TODO When not a char stream, read byte
    (let ((e (elt sequence i)))
      (etypecase e
        (char (write-char e stream))
        ((unsigned-byte 8) (write-byte e stream))))))


;;; Output stream commands

(defun flush-output (stream)
  (etypecase stream
    (file-stream (lua-mcall (file-stream-file stream) "flush"))
    (two-way-stream (flush-output (two-way-stream-output-stream stream)))
    (broadcast-stream (dolist (s (broadcast-stream-streams stream))
                        (flush-output s)))))

(defun finish-output (&optional (output-stream *standard-output*))
  (flush-output output-stream))
(defun force-output (&optional (output-stream *standard-output*))
  (flush-output output-stream))
(defun clear-output (&optional (output-stream *standard-output*)))

(defun stream-consider-char-column (stream char)
  (if (streamp stream)
      (if (char= char #\Newline)
          (setf (stream-column stream) 0)
          (incf (stream-column stream)))
      (error 'type-error
             :form 'stream
             :datum stream
             :expected-type 'stream)))

(defun try-write-char (char stream)
  (if (charp char)
      (progn
        ;; HACK to be able to retrieve column
        (stream-consider-char-column stream char)
        (etypecase stream
          (file-stream
           (setf (file-stream-last stream) char)
           (lua-mcall (file-stream-file stream) "write" (char-string char)))
          (try-write-char
           (try-write-char char (two-way-stream-output-stream stream)))
          (string-stream
           (setf (string-stream-string stream) (concat-string (string-stream-string stream) (char-string char))))
          (broadcast-stream
           (dolist (s (broadcast-stream-streams stream))
             (try-write-char char s)))))
      (error "try-write-char, ~S not a char" char)))

(defun try-write-string (string stream)
  (if (stringp string)
      (progn
        (setq string (string string))
        ;; HACK to be able to retrieve column
        ;; (mapcar (lambda (x) (stream-consider-char-column stream x)) (coerce string 'list))
        (let ((pos (lua "({string.find(" string ", \".*\\n\")})[2] or n")))
          (if pos
              (setf (stream-column stream) (- (length string) pos))
              (incf (stream-column stream) (length string))))
        (etypecase stream
          (file-stream
           (setf (file-stream-last stream) (char string (- (length string) 1)))
           (lua-mcall (file-stream-file stream) "write" string))
          (two-way-stream
           (try-write-string string (two-way-stream-output-stream stream)))
          (string-stream
           (setf (string-stream-string stream) (concat-string (string-stream-string stream) string)))
          (broadcast-stream
           (dolist (s (broadcast-stream-streams stream))
             (try-write-string string s)))))
      (error "try-write-string, ~S not a string" string)))

(defun fresh-line-g (stream)
  (etypecase stream
    (file-stream
     (if (eq 10 (char-code (file-stream-last stream)))
         nil
         (progn (terpri stream) t)))
    (two-way-stream (fresh-line-g (two-way-stream-output-stream stream)))
    (string-stream
     (let ((str (string-stream-string stream)))
       (if (eq 10 (char-code (char str (- (length str) 1))))
           nil
           (progn (terpri stream) t))))
    (broadcast-stream
     (dolist (s (broadcast-stream-streams stream))
       (fresh-line-g s)))))

(defun write-char (char &optional (output-stream *standard-output*))
  (try-write-char char output-stream))
(defun write-byte (byte &optional (output-stream *standard-output*))
  (try-write-char (code-char byte) output-stream))
(defun terpri (&optional (output-stream *standard-output*))
  (write-char (code-char 10) output-stream)
  nil)
(defun write-string (string &optional (output-stream *standard-output*) &key (start 0) end)
  (try-write-string
   (if (or end (/= start 0))
       (subseq string start end)
       string)
   output-stream))
(defun write-line (string &optional (output-stream *standard-output*) &key (start 0) end)
  (write-string (if (or end (/= start 0))
                    (subseq string start end)
                    string)
                output-stream)
  (terpri output-stream))
(defun fresh-line (&optional (output-stream *standard-output*))
  (fresh-line-g output-stream))


;;; Pathnames

(defstruct (pathname (:predicate pathnamep) (:constructor %make-pathname))
  (host nil)
  (device nil)
  (directory nil)
  (name nil)
  (type nil)
  (version nil))


(defvar *default-pathname-defaults* (%make-pathname :directory (list :relative)))

(defun %ensure-string (n) (if (symbolp n) n (string n)))


(defun make-pathname (&key host device directory name type version defaults (case :local))
  (%make-pathname
   :host (%ensure-string (or host (and defaults (pathname-host defaults)) (pathname-host *default-pathname-defaults*)))
   :device (%ensure-string (or device (and defaults (pathname-device defaults))))
   :directory (mapcar #'%ensure-string (or directory (and defaults (pathname-directory defaults))))
   :name (%ensure-string (or name (and defaults (pathname-name defaults))))
   :type (%ensure-string (or type (and defaults (pathname-type defaults))))
   :version (%ensure-string (or version (and defaults (pathname-version defaults))))))


;; TODO
(defun wild-pathname-p (pathname &optional field-key) nil)

;; translate-pathname
;; pathname-match-p

(defun parse-namestring (thing &optional host (default-pathname *default-pathname-defaults*) &key (start 0) end junk-allowed)
  (if (file-stream-p thing) (setq thing (file-stream-pathname thing)))
  (cond
    ((pathnamep thing) thing)
    ((stringp thing)
     (setq thing (string thing))
     (if (or end (/= start 0)) (setq thing (subseq thing start end)))
     (setq end (- (or end (length thing)) start))
     (let ((dir nil)
           (accstart 0)
           (acc 0))
       (if (char= #\/ (char thing 0))
           (progn
             (push :absolute dir)
             (incf accstart)
             (incf acc))
           ;; XXX Use relative to default-pathname ??
           (setq dir (reverse (pathname-directory *default-pathname-defaults*)))
           ;; (push :relative dir)
           )
       (%loop
        (if (eq acc end)
            ;; At end of file
            (progn
              (push (subseq thing accstart acc) dir)
              (%break))
            (if (char= #\/ (char thing acc))
                ;; New directory
                (progn
                  (push (subseq thing accstart acc) dir)
                  (setq accstart (+ 1 acc)))
                ;; Nothing, part of name
                ))
        (incf acc))

       (if (string= "" (car dir))
           (make-pathname :directory (reverse (cdr dir)))
           ;; Parse the filename
           (let* ((car (car dir))
                  (pos (lua "({string.find(" car ", '.*%.')}) [2] or n"))
                  (name (if (string= "" (car dir)) nil (if pos (subseq (car dir) 0 (- pos 1)) (car dir))))
                  (type (if (string= "" (car dir)) nil (if pos (subseq (car dir) pos)))))
             (make-pathname
              :directory (reverse (cdr dir))
              :name name
              :type type)))))
    (t (error "Can't convert ~S to pathname" thing))))

(defun merge-pathnames (pathname &optional (default-pathname *default-pathname-defaults*) (default-version :newest))
  (if (stringp pathname) (setq pathname (parse-namestring pathname)))
  (if (stringp default-pathname) (setq default-pathname (parse-namestring default-pathname)))

  (make-pathname
   :host (or (pathname-host pathname) (pathname-host default-pathname))
   :device (or (pathname-device pathname) (pathname-device default-pathname))
   :name (or (pathname-name pathname) (pathname-name default-pathname))
   :version (or (pathname-version pathname) (pathname-version default-pathname) default-version)
   :type (or (pathname-type pathname) (pathname-type default-pathname))
   :directory
   (if (and (listp (pathname-directory pathname))
            (eq :relative (car (pathname-directory pathname))))
       (append (pathname-directory default-pathname)
               (cdr (pathname-directory pathname)))
       (or (pathname-directory pathname) (pathname-directory default-pathname)))))

(defun host-namestring (pathname)
  (if (stringp pathname) (setq pathname (parse-namestring pathname)))
  (pathname-host pathname))

(defun file-namestring (pathname)
  (if (stringp pathname) (setq pathname (parse-namestring pathname)))
  (let ((stream (make-string-output-stream)))
    ;; Write name
    (let ((name (pathname-name pathname)))
      (if (eq :wild name)
          (write-string "*" stream)
          (write-string (pathname-name pathname) stream)))
    ;; Write ext
    (when (pathname-type pathname)
      (write-string "." stream)
      (write-string (pathname-type pathname) stream))

    (get-output-stream-string stream)))

(defun directory-namestring (pathname)
  (if (stringp pathname) (setq pathname (parse-namestring pathname)))
  (let ((stream (make-string-output-stream)))
    ;; Write directory
    (let ((dir (pathname-directory pathname)))
      (%loop
       (let ((el (car dir))
             (next (cadr dir)))
         (cond
           ((eq el :relative) nil)
           ((eq el :absolute)
            (let ((device (pathname-device pathname)))
              (when device
                (write-string device stream)))
            (write-string "/" stream))
           ((eq el :wild) (write-string "**/" stream))
           ((string= ".." next) (setq dir (cdr dir)))
           ((string= "." next))
           ((null el) (%break))
           (t (write-string el stream)
              (write-string "/" stream)))
         (setq dir (cdr dir)))))

    (get-output-stream-string stream)))


(defun namestring (pathname)
  (if (stringp pathname) (setq pathname (parse-namestring pathname)))
  (let ((stream (make-string-output-stream)))
    (write-string (directory-namestring pathname) stream)
    (if (pathname-name pathname) (write-string (file-namestring pathname) stream))
    (get-output-stream-string stream)))

(defun enough-namestring (pathname) (namestring pathname))

;; TODO
(defun truename (filespec)
  (etypecase filespec
    (string (parse-namestring filespec))
    (pathname filespec)
    (stream (file-stream-pathname filespec))))

(defun pathname (pathspec)
  (etypecase pathspec
    (string (parse-namestring pathspec))
    (pathname pathspec)
    (stream (file-stream-pathname pathspec))))

(defun user-homedir-pathname (&optional host)
  (if host
      nil
      ;; Windows ?
      (let ((home (getenv "HOME")))
        (unless (char= #\/ (char home (1- (length home))))
          (setq home (concat-string home "/")))
        (parse-namestring home))))



;;; Logical pathnames

(defun logical-pathname (pathname) (error "LOGICAL-PATHNAME: Unsupported"))
(defun load-logical-pathname-translations (pathname) (error "LOAD-LOGICAL-PATHNAME-TRANSLATIONS: UNSUPPORTED"))

(defun logical-pathname-translations (host) (error "LOGICAL-PATHNAME-TRANSLATIONS: Unsupported"))
(defun (setf logical-pathname-translations) (new-value host) (error "LOGICAL-PATHNAME-TRANSLATIONS: Unsupported"))


;;; Files

(defun open (filespec &key (direction :input)
                        element-type
                        (if-exists :default)
                        (if-does-not-exist :default)
                        (external-format :default))
  ;; XXX if-exists, if-does-not-exist
  (let* ((lua-dir (ecase direction
                    (:input "r")
                    (:output "w")
                    (:io "rw")
                    (:probe "r")))
         (ns (namestring (truename filespec)))
         (file (lua "io.open(" ns ", " lua-dir ") or n")))
    (if file
        (make-file-stream :file file :pathname filespec)
        (error "Could not open file ~S" filespec))))

(defun %file-position-designator (position)
  (cond
    ((eq :start position) 0)
    ((eq :end position) "end")
    ((integerp position) position)
    (t (error 'type-error
             :form 'position
             :datum position
             :expected-type '(or (integer 0) (member :start :end))))))

(defun file-position (stream &optional position)
  (check-type stream file-stream)
  (if position
      (lua-mcall (file-stream-file stream) "seek" "set" (%file-position-designator position))
      (+
       (if (file-stream-unread stream) -1 0)
       (lua-mcall (file-stream-file stream) "seek"))))

(defun file-string-length (stream object)
  (+ (file-position stream)
     (etypecase object
       (character 1)
       (string (length object)))))

(defun file-author (pathspec) nil)
(defun file-string-length (stream object) nil)
(defun file-write-date (pathspec) nil)

(defun file-length (stream)
  (let* ((current (lua-mcall (file-stream-file stream) "seek"))
         (end "end")
         (size (lua-mcall (file-stream-file stream) "seek" end))
         (set "set"))
    (lua-mcall (file-stream-file stream) "seek" set current)
    size))

(defun probe-file (pathspec)
  (let* ((ns (namestring (truename pathspec)))
         (file (lua "io.open(" ns ", \"r\") or n")))
    (if file (progn (lua-mcall file "close") (truename pathspec)))))

(defun directory (pathspec &key key)
  ;; TODO Unimplemented
  nil)

(defun delete-file (filespec)
  (let ((ns (namestring filespec)))
    (if (lua "os.remove(" ns ") and CL.T or CL.NIL")
        t (error "File error: ~A does not exist" filespec))))

(defmacro with-open-file (param &body body)
  `(let ((,(car param) (open ,@(cdr param))))
     (unwind-protect
          (progn ,@body)
       (close ,(car param)))))


;;; Macros

(defmacro with-open-stream (bind &body body)
  `(let (,bind)
     (unwind-protect
          (progn ,@body)
       (close ,(car bind)))))

(defmacro with-output-to-string (vars &body body)
  (if (second vars)
      (let ((res (gensym)))
        ;; XXX Fix this, close enough for now.
        `(let (,res)
           (setf ,(second vars)
                 (concat-string
                  ,(second vars)
                  (with-output-to-string (,(car vars))
                    (setq ,res (multiple-value-list (progn ,@body))))))
           (values-list ,res)))
      `(let ((,(car vars) (make-string-output-stream)))
         ,@body
         (get-output-stream-string ,(car vars)))))

;; with-input-from-string
