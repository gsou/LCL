(in-package :cl-lib)
(defpackage :common-lisp-user (:nicknames :cl-user) (:use :cl))

;; Load libs, should be builtin 
;; (load "ext/split-sequence.lisp")
;; (load "ext/usocket.lisp")
;; (load "asdf.lisp")
;; (push (cl-lib:lua "os.getenv('PWD')") asdf:*central-registry*)

(defvar cl::* nil)
(defvar cl::** nil)
(defvar cl::*** nil)
(defvar cl::+ nil)
(defvar cl::++ nil)
(defvar cl::+++ nil)
(defvar cl::/ nil)
(defvar cl::// nil)
(defvar cl::/// nil)
(defvar cl::- nil)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro %pcall (&body body)
    (let ((fn (gensym)))
      `(let ((,fn (lambda () ,@body)))
         (lua "pcall(" ,fn ")"))))

  (defmacro safe-eval (&body body)
    `(multiple-value-bind (ok err) (%pcall ,@body)
       (unless (eq (lua "true") ok)
         (handler-case
             ;; There's an error...
             (format t "*UNCAUGHT-NLR* ~S" err)
           (error (c) (format t "*ERROR WHILE PRINTING ERROR*")))))))


(defun repl (&optional (nest 0) restarts)
  (block repl
    (restart-bind
        ((abort #'(lambda () (return-from repl))))
      (let ((*debugger-hook*
              (lambda (condition d)
                (write-string "
*UNCAUGHT ERROR of type " *error-output*)
                (write-string (string (class-name (class-of condition))) *error-output*)
                (write-line "*" *error-output*)
                (handler-case
                    (report-condition condition *error-output*)
                  (error (e) (write-line "(Error while printing error)" *error-output*)))
                (write-line "" *error-output*)
                (write-line "restarts (number to call interactively): " *error-output*)

                (let ((restarts (compute-restarts condition))
                      (ix 0))
                  (dolist (r (compute-restarts condition))
                    (write-string (concat-string (prin1-to-string ix) ": [" (string (restart-name r)) "] ") *error-output*)
                    (if (restart-report-function r) (funcall (restart-report-function r) *error-output*))
                    (fresh-line *error-output*)
                    (incf ix))

                  (fresh-line *error-output*)
                  (write-line "STACK TRACE" *error-output*)
                  (lua "print(debug.traceback())")

                  (repl (+ 1 nest) restarts)
                  (invoke-restart-interactively (elt restarts (read)))))))
        (%loop
         (fresh-line)
         (write-string
          (if (> nest 0)
              (concat-string (prin1-to-string nest) "] ")
              (concat-string (package-name *package*) "> ")))
         (psetf +++ ++
                ++  +
                +   -
                -  (read))

         (psetf /// //
                //  /
                /  (multiple-value-list
                    (if (and (numberp -) (> nest 0))
                        (invoke-restart-interactively (elt restarts -))
                        (eval -))))
         (psetf *** **
                **  *
                *   (car /))
         (dolist (p /) (print p)))))))

(defun main ()
  (setq *package* :cl-user)
  (let ((args (lua "l((table.unpack or unpack)(arg or {}))")))
    ;; TODO Parse args
    (tagbody
     next
       (safe-eval (repl))
       (go next))))
