(in-package :cl-lib)
;;;; Sequence related functions

(defun svref (vector index) (aref vector index))
(defun %setsvref (vector index new-value) (aset vector new-value index))

;;; Raw element access
(defun elt (sequence index)
  (cond
    ((listp sequence) (nth index sequence))
    ((stringp sequence) (char sequence index))
    ((vectorp sequence) (aref sequence index))))
(defun %setelt (sequence index new-value)
  (cond
    ((listp sequence)
     (if (< index (length sequence))
         (rplaca (nthcdr index sequence) new-value)
         (error "setting element, out of bounds")))
    ((vectorp sequence) (aset sequence new-value index))
    (t (error "setting element, not a sequence"))))



;;; Generic sequence operations
(defmacro do-from-to (varft &body body)
  (let ((var (car varft))
        (from (cadr varft))
        (to (caddr varft)))
    `(let ((,var ,from))
       (%loop
        (when (>= ,var ,to) (%break))
        ,@body
        (incf ,var)))))

(defun fill (sequence item &key (start 0) end)
  (unless end (setf end (length sequence)))
  (do-from-to (ix start end)
    (%setelt sequence ix item))
  sequence)


(defun map-into (result-sequence function &rest sequences)
  (let ((end (apply #'min
                    (length result-sequence)
                    (mapcar #'length sequences))))
    (do-from-to (ix 0 end)
      (%setelt result-sequence ix
               (apply function
                      (mapcar #'(lambda (seq) (elt seq ix))
                              sequences))))
    result-sequence))


(defun %list-length (list &aux (acc 0))
  (%loop
   (if list
       (progn
         (setq acc (+ 1 acc))
         (setq list (cdr list)))
       (%return acc))))
(defun list-length (list)
  (cond
    ((consp list)
     (let ((acc 1)
           (slow list)
           (fast (cdr list)))
       (%loop
        ;; If circular return nil
        (when (eq slow fast) (%return nil))
        (cond
          ((null fast) (%return acc))
          ((consp fast)
           (cond
             ((consp (cdr fast))
              (setf fast (cddr fast)
                    slow (cdr slow))
              (incf acc 2))
             ((null (cdr fast)) (%return (1+ acc)))
             (t (error 'simple-type-error
                       :format-control "list-length: Must be a proper list"
                       :format-arguments nil
                       :form 'list
                       :datum list
                       :expected-type 'proper-list))))
          (t (error 'simple-type-error
                       :format-control "list-length: Must be a proper list"
                       :format-arguments nil
                       :form 'list
                       :datum list
                       :expected-type 'proper-list))))))
    ((null list) 0)
    (t (error 'type-error :form 'list :datum list :expected-type 'list))))

(defun length (list)
  (cond
    ((listp list) (%list-length list))
    ((%stringp list) (%len list))
    ((vectorp list) (if (array-has-fill-pointer-p list)
                        (fill-pointer list)
                        (array-dimension list 0)))
    (t (error 'simple-type-error
              :format-control "length: Not a valid sequence: ~S"
              :format-arguments (list list)
              :form 'list :datum list :expected-type 'list))))

(defun reverse (list)
  (cond
    ((listp list) (%reverse-accumulator list ()))
    ((%stringp list) (lua "string.reverse(" list ")"))
    ((vectorp list) (let ((arr (copy-array list)))
                      (do-from-to (v 0 (length list))
                        (%setsvref arr v (svref list (- (length list) v 1))))
                      arr))
    (t (error 'simple-type-error
              :format-control "reverse: Not a valid sequence: ~S"
              :format-arguments (list list)
              :form 'list :datum list :expected-type 'list))))

(defun %sequence-type (sequence)
  (cond
    ((listp sequence) 'list)
    ((stringp sequence) 'string)
    ((vectorp sequence) 'vector)
    (t (error "Unknown sequence type: ~A" sequence))))
(defun %substr (string &optional (start 0) (end (length sequence)))
  (lua-call "string.sub" string (+ 1 start) end))
(defun subseq (sequence start &optional end)
  (if (%stringp sequence)
      (%substr sequence start (or end (length sequence)))
      ;; (lua-call "string.sub" sequence (+ 1 start) (or end (length sequence)))
      (let* ((len (length sequence))
             (newlen (- (if end (min end len) len) start))
             (new (make-sequence (%sequence-type sequence) newlen)))
        (do ((ix 0 (+ ix 1)))
            ((>= ix newlen))
          (%setelt new ix (elt sequence (+ ix start))))
        new)))
(defun replace (sequence new-subsequence &key (start1 0) end1 (start2 0) end2)
  (unless end1 (setq end1 (length sequence)))
  (unless end2 (setq end2 (length new-subsequence)))
  (when (eq sequence new-subsequence)
    ;; XXX Optimize check for overlap, only needed if overlap
    (setq new-subsequence (copy-seq new-subsequence)))
  ;; XXX Strings
  (when (%stringp sequence) (setq sequence (copy-seq sequence)))
  (do ((i start1 (+ i 1))
       (j start2 (+ j 1)))
      ((or (>= i end1) (>= j end2)) sequence)
    (%setelt sequence i (elt new-subsequence j))))

(defun (setf subseq) (new-subsequence sequence start &optional end)
  (let* ((len (length sequence))
         (stop (min (length new-subsequence) (- (if end (min end len) len) start))))
    (dotimes (ix stop)
      (%setelt sequence (+ ix start) (elt new-subsequence ix)))
    sequence))

(defun position-if (predicate sequence &key from-end (start 0) end key)
  (unless end (setq end (length sequence)))
  (do ((ix (if from-end (- end 1) start) (if from-end (- ix 1) (+ ix 1))))
      ((or (< ix 0) (>= ix (length sequence)) (if from-end (< ix start) (and end (>= ix end)))))
    (when (funcall predicate (funcall (or key #'identity) (elt sequence ix)))
      (%return ix))))
(defun position-if-not (predicate sequence &key from-end (start 0) end key)
  (position-if (complement predicate) sequence :from-end from-end :start start :end end :key key))
(defun position (item sequence &key from-end test test-not (start 0) end key)
  (let ((func (or test (and test-not (complement test-not)) #'eq)))
    (position-if (lambda (el) (funcall func item el)) sequence :from-end from-end :start start :end end  :key key)))

(let ((skip (gensym)))
  (defun %delete-skips (sequence)
    (cond
      ((null sequence) ())
      ((listp sequence)
       (if (eq skip (car sequence)) (%delete-skips (cdr sequence)) (cons (car sequence) (%delete-skips (cdr sequence)))))
      ((vectorp sequence)
       (let ((l (make-list (length sequence))))
         (map-into l #'identity sequence)
         (setq l (%delete-skips l))
         (let ((v (make-array (length l))))
           (map-into v #'identity l)
           v)
         ))
      (t (error "delete/remove, not a sequence"))))
  (defun delete-if (test sequence &key from-end (start 0) end count key)
    (unless end (setq end (length sequence)))
    (let ((cnt 0))
      (do ((ix (if from-end (- end 1) start) (if from-end (- ix 1) (+ ix 1))))
          ((or (and count (>= cnt count))
               (if from-end (< ix start) (>= ix end))))
        (when (funcall test (funcall (or key #'identity) (elt sequence ix)))
          (setq cnt (+ 1 cnt))
          (%setelt sequence ix skip)))
      (%delete-skips sequence)))
  (defun delete-if-not (test sequence &key from-end (start 0) end count key)
    (delete-if (complement test) sequence :from-end from-end :start start :end end :count count :key key))
  (defun delete (item sequence &key from-end test test-not (start 0) end count key)
    (let ((func (or test (and test-not (complement test-not)) #'eq)))
      (delete-if (lambda (el) (funcall func item el)) sequence :from-end from-end :start start :end end :key key)))

  (defun remove-if (test sequence &key from-end (start 0) end count key)
    (delete-if test (copy-seq sequence) :from-end from-end :start start :end end :count count :key key))
  (defun remove-if-not (test sequence &key from-end (start 0) end count key)
    (delete-if-not test (copy-seq sequence) :from-end from-end :start start :end end :count count :key key))
  (defun remove (item sequence &key from-end test test-not (start 0) end count key)
    (delete item (copy-seq sequence) :from-end from-end :test test :test-not test-not :start start :end end :count count :key key))

  (defun delete-duplicates (sequence &key from-end test test-not (start 0) end key)
    (delete-if (lambda (el) (> (count-if (lambda (other)
                                           (funcall (or test (and test-not (complement test-not)) #'eql)
                                                    (if key (funcall key el) el)
                                                    (if key (funcall key other) other)))
                                         sequence)
                               1))
               sequence
               :from-end from-end))
  (defun remove-duplicates (sequence &key from-end test test-not (start 0) end key)
    (delete-duplicates (copy-seq sequence) :from-end from-end :test test :test-not test-not :start start :end end :key key)))

(defun nsubstitute-if (newitem predicate sequence &key from-end (start 0) end count key)
  (let* ((l (length sequence))
         (first start)
         (last (or end l))
         (cnt 0))
    (do ((i (if from-end (1- last) first) (+ i (if from-end -1 1))))
        ((or (and count (>= cnt count)) (if from-end (< i first) (>= i last))) sequence)
      (when (funcall predicate (if key (funcall key (elt sequence i)) (elt sequence i)))
        (incf cnt)
        (%setelt sequence i newitem)))))
(defun nsubstitute-if-not (newitem predicate sequence &key from-end (start 0) end count key)
  (nsubstitute-if newitem (complement predicate) sequence :from-end from-end :start start :end end :count count :key key))
(defun nsubstitute (newitem olditem sequence &key from-end test test-not (start 0) end count key)
  (let ((func (or test (and test-not (complement test-not)) #'eq)))
    (nsubstitute-if newitem (lambda (x) (funcall func olditem x)) sequence :from-end from-end :start start :end end :count count :key key)))

(defun %sort-list (list predicate key)
  (if (not (zerop (length list)))
      (let* ((pivot (elt list 0))
             (pivot-key (if key (funcall key pivot) pivot))
             (rest (subseq list 1))
             (fn (lambda (p) (funcall predicate pivot-key p)))
             (lower (remove-if     fn rest :key key))
             (upper (remove-if-not fn rest :key key)))
        (append (%sort-list lower predicate key)
                (list pivot)
                (%sort-list upper predicate key)))))
(defun sort (sequence predicate &key key)
  (if (listp sequence)
      (%sort-list sequence predicate key)
      (map-into sequence #'identity (%sort-list sequence predicate key))))

(defun stable-sort (sequence predicate &key key)
  (sort sequence predicate :key key))

(defun find-if (predicate sequence &key from-end (start 0) end (key #'identity))
  (let ((last (or end (length sequence))))
    (do-from-to (ix 0 (- last start))
      (let* ((i (if from-end (- last ix 1) (+ start ix)))
             (el (elt sequence i)))
        (when (funcall predicate (funcall key el)) (%return el))))
    nil))
(defun find-if-not (predicate sequence &key from-end (start 0) end (key #'identity))
  (find-if (complement predicate) sequence :from-end from-end :start start :end end :key key))
(defun find (item sequence &key from-end test test-not (start 0) end (key #'identity))
  (let ((func (or test (and test-not (complement test-not)) #'eq)))
    (find-if (lambda (el) (funcall func item el)) sequence :from-end from-end :start start :end end :key key)))

(defun member-if (predicate list &key (key #'identity))
  (cond
    ((null list) nil)
    ((consp list)
     (if (funcall predicate (if key (funcall key (car list)) (car list)))
         list
         (member-if predicate (cdr list) :key key)))
    ;; NOTE: Those two are not standard
    ((sequencep list)
     (let (ret)
       (dotimes (i (length list))
         (when (funcall predicate (funcall key (elt list i)))
           (setq ret (subseq list i))
           (return)))
       ret))
    (t (error 'type-error :form 'list :datum list :expected-type 'list))))

(defun mismatch (sequence-1 sequence-2 &key from-end test test-not key
                                         (start1 0) (start2 0) end1 end2)
  (block ret
    (let ((fn (or test (and test-not (complement test-not)) #'eql)))
      (unless end1 (setq end1 (length sequence-1)))
      (unless end2 (setq end2 (length sequence-2)))
      (do ((i (if from-end (1- end1) start1) (if from-end (1- i) (1+ i)))
           (j (if from-end (1- end2) start2) (if from-end (1- j) (1+ j))))
          ((or (>= i end1) (>= j end2) (< i start1) (< j start2))
           (if (or (and (>= i end1) (>= j end2)) (and (< i start1) (< j start2))) nil
               (if from-end (+ 1 i) i)))
        (unless
            (if key
                (funcall fn (funcall key (elt sequence-1 i))
                         (funcall key (elt sequence-2 j)))
                (funcall fn (elt sequence-1 i) (elt sequence-2 j)))
          (return-from ret (if from-end (+ 1 i) i)))))))

(defun search (sequence-1 sequence-2 &key from-end test test-not key
                                       (start1 0) (start2 0) end1 end2)
  (unless end1 (setq end1 (length sequence-1)))
  (unless end2 (setq end2 (length sequence-2)))
  (let ((s1len (- end1 start1)))
    (do ((i (if from-end (1- (or end2 (length sequence-2))) 0) (if from-end (1- i) (1+ i))))
        ((if from-end (< i start2) (>= i end2)) nil)
      (if from-end
          (let* ((e2 (1+ i))
                 (s2 (- e2 s1len)))
            (unless (mismatch sequence-1 sequence-2 :test test :test-not test-not :key key
                                                    :start1 start1 :end1 end1 :start2 s2 :end2 e2)
              (return s2)))
          (let* ((s2 i)
                 (e2 (+ s2 s1len)))
            (unless (mismatch sequence-1 sequence-2 :test test :test-not test-not :key key
                                                    :start1 start1 :end1 end1 :start2 s2 :end2 e2)
              (return s2)))))))

(defun string-left-trim (bag string)
  (setq string (string string))
  (let ((len (length string)))
    (do ((i 0 (1+ i)))
        ((or (>= i len) (not (member (char string i) bag :test #'char=))) (subseq string i)))))

(defun string-right-trim (bag string)
  (setq string (string string))
  (let ((len (length string)))
    (do ((i (1- len) (1- i)))
        ((or (< i 0) (not (member (char string i) bag :test #'char=))) (subseq string 0 (1+ i))))))

(defun string-trim (bag string)
  (string-left-trim bag (string-right-trim bag string)))

(defun nset-exclusive-or (list-1 list-2 &key key test test-not)
  (let (acc)
    (flet ((iter (from to collect)
             (%loop
              (if (cdr from)
                  (if (member (if key (funcall key (cadr from)) (cadr from)) to :key key :test test :test-not test-not)
                      (progn
                        (if collect (push (cadr from) acc))
                        (rplacd from (cddr from)))
                      (setq from (cdr from)))
                  (%break)))))
      (iter list-1 list-2 t)
      (when (member (if key (funcall key (car list-1)) (car list-1)) list-2 :key key :test test :test-not test-not) (push (car list-1) acc) (setq list-1 (cdr list-1)))
      (iter list-2 acc nil)
      (when (member (if key (funcall key (car list-2)) (car list-2)) acc :key key :test test :test-not test-not) (setq list-2 (cdr list-2)))
      (append list-1 list-2))))
(defun set-exclusive-or (list-1 list-2 &key key test test-not)
  (nset-exclusive-or (copy-list list-1) (copy-list list-2) :key key :test test :test-not test-not))

(defun every (predicate &rest seqs)
  (when (null seqs) (error "every, specify at least one seq"))
  (dotimes (ix (apply #'min (mapcar #'length seqs)) t)
    (unless (apply predicate (mapcar (lambda (s) (elt s ix)) seqs))
      (return nil))))
(defun some (predicate &rest seqs)
  (when (null seqs) (error "some, specify at least one seq"))
  (dotimes (ix (apply #'min (mapcar #'length seqs)) nil)
    (when (apply predicate (mapcar (lambda (s) (elt s ix)) seqs))
      (return t))))
(defun notevery (predicate &rest seqs) (apply #'some (complement predicate) seqs))
(defun notany (predicate &rest seqs) (apply #'every (complement predicate) seqs))



;;; Constructing sequences

(defun make-list (size &key initial-element)
  (cond
    ((or
      (not (integerp size))
      (minusp size))
     (error 'type-error :form 'size :datum size :expected-type '(integer 0)))
    ((zerop size) nil)
    ((plusp size)
     (let ((acc nil))
       (dotimes (i size) (setq acc (cons initial-element acc)))
       acc))
    (t (error "Can't make list of size ~A" size))))

(defun make-sequence (result-type size &key initial-element)
  (cond
    ((subtypep result-type 'list) (make-list size :initial-element initial-element))
    ((subtypep result-type 'string) (make-array size :initial-element initial-element :element-type 'character))
    ((subtypep result-type 'vector) (make-array size :initial-element initial-element))
    (t (error "Invalid sequence type ~A" result-type))))

(defun copy-seq (sequence)
  (cond
    ((typep sequence 'list) (copy-list sequence))
    ((typep sequence 'vector) (copy-array sequence))))

(defun vector (&rest list)
  ;; (make-array (length list))
  (map-into (make-array (length list)) #'identity list))

;; TODO String
(defun map (result-type function &rest sequences)
  (let* ((len (apply #'min (mapcar #'length sequences)))
         (seq (if result-type (make-sequence result-type len))))
    (if result-type
        (dotimes (i len)
          (%setelt seq i (apply function (mapcar (lambda (s) (elt s i)) sequences))))
        (dotimes (i len)
          (apply function (mapcar (lambda (s) (elt s i)) sequences))))
    seq))



(let ((none (gensym)))
  ;; TODO Optimise for lists like list-only version
  (defun reduce (function sequence &key key from-end start end (initial-value none))
    (when (or start end) (setq sequence (subseq sequence (or start 0) end)))
    (when from-end (setq sequence (reverse sequence)))
    (when key (setq sequence (map 'vector key sequence)))
    (let (acc (offset 0))
      (if (eq initial-value none)
          (progn
            (setq acc (elt sequence 0))
            (setq offset 1))
          (setq acc initial-value))
      (dotimes (i (- (length sequence) offset))
        (let ((elem (elt sequence (+ i offset))))
          (if from-end (setq acc (funcall function elem acc)) (setq acc (funcall function acc elem))))
       sequence)
      acc)))

;; TODO Proper support for strings
(defun concatenate (result-type &rest sequences)
  (if (subtypep result-type 'string)
      (reduce 'concat-string (apply #'concatenate 'vector sequences) :key #'char-string)
      (let* ((len (apply #'+ (mapcar #'length sequences)))
             (seq (make-sequence result-type len))
             (ix 0))
        (dolist (s sequences)
          (setf (subseq seq ix) s)
          (incf ix (length s)))
        seq)))



(defun substitute-if (newitem predicate sequence &key from-end (start 0) end count key) (nsubstitute-if newitem predicate (copy-seq sequence) :from-end from-end :start start :end end :count count :key key))
(defun substitute-if-not (newitem predicate sequence &key from-end (start 0) end count key) (nsubstitute-if newitem (complement predicate) (copy-seq sequence) :from-end from-end :start start :end end :count count :key key))
(defun substitute (newitem olditem sequence &key from-end test test-not (start 0) end count key)
  (nsubstitute newitem olditem (copy-seq sequence) :from-end from-end :test test :test-not test-not :start start :end end :count count :key key))

(defun merge (result-type seq1 seq2 predicate &key key)
  (let* ((l1 (length seq1))
         (l2 (length seq2))
         (l (+ l1 l2))
         (new (make-sequence result-type l))
         (j 0)
         (k 0))
    (if (null key) (setq key #'identity))
    (do ((i 0 (+ 1 i)))
        ((>= i l) new)
      (let* ((el1 (funcall key (elt seq1 j)))
             (el2 (funcall key (elt seq2 k)))
             (which (or (>= k l2) (and (< j l1) (or (funcall predicate el1 el2) (not (funcall predicate el2 el1)))))))
        (if which
            (progn
              ;; Take from seq1
              (%setelt new i (elt seq1 j))
              (incf j))
            (progn
              ;; Take from seq2
              (%setelt new i (elt seq2 k))
              (incf k)))))))
