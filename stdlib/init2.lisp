(in-package :cl-lib)
;;;; Must be loaded before anything else

;; Temporary let without any special variable handling
(defmacro let (&rest rest) `(%let ,@rest))

(defmacro defun (name arglist &body body)
  `(setq (function ,name) (function (lambda ,arglist ,@(if (stringp (car body)) (cdr body) body)))))

(defmacro the (type value) value)

(defmacro progn (&rest forms)
  `(let () ,@forms))

(defmacro cond (&body clauses)
  (if clauses
      (let ((car (car clauses))
            (rst (cdr clauses)))
        `(if ,(car car) (progn ,@(cdr car)) (cond ,@rst)))))

(defmacro and (&rest clauses)
  (cond
    ((null clauses) t)
    ((endp (cdr clauses)) (car clauses))
    (t `(if ,(car clauses) (and ,@(cdr clauses))))))

(defmacro or (&rest clauses)
  (cond
    ((null clauses) nil)
    ((null (cdr clauses)) (car clauses))
    (t (let ((res (gensym)))
         `(let ((,res ,(car clauses)))
            (if ,res ,res (or ,@(cdr clauses))))))))

(defmacro when (c &body body) `(if ,c (progn ,@body)))
(defmacro unless (c &body body) `(if ,c nil (progn ,@body)))

(defmacro lambda (arglist &body body)
  `(function (lambda ,arglist ,@body)))

(defmacro case (keyform &body clauses)
  (let ((keysym (gensym)))
    `(let ((,keysym ,keyform))
       (cond
         ,@(mapcar (lambda (clause)
                     (cons (cond
                             ((or (eq (car clause) t) (eq (car clause) 'otherwise)) 't)
                             ((listp (car clause)) (list 'member keysym (list 'quote (car clause))))
                             (t (list 'eql keysym (list 'quote (car clause)))))
                           (cdr clause)))
                   clauses)))))
(defmacro ccase (keyform &body clauses)
  `(ecase ,keyform ,@clauses))
(defmacro ecase (keyform &body clauses)
  `(case ,keyform ,@clauses (t (error "ECASE"))))

(defmacro defvar (var &optional val doc) (let ((sym (gensym))) `(let ((,sym ,val)) (lua "CL_LIB.DEF(" (quote ,var) "," ,sym ")"))))
(defmacro defparameter (var &optional val doc) (let ((sym (gensym))) `(let ((,sym ,val)) (lua "CL_LIB.DEF(" (quote ,var) "," ,sym ")"))))
(defmacro defconstant (var &optional val doc) (let ((sym (gensym))) `(let ((,sym ,val)) (lua "CL_LIB.DEF(" (quote ,var) "," ,sym ")"))))

;;; Prog family of functions
(defmacro prog1 (form &rest rest)
  (let ((form1 (gensym)))
    `(let ((,form1 ,form))
       ,@rest
       ,form1)))

(defmacro prog2 (form form2 &rest rest)
  `(prog1 (progn ,form ,form2) ,@rest))

(defmacro locally (&body body) `(progn ,@body))

(defmacro let* (assignments &body rest)
  (if assignments
      `(let (,(car assignments)) (let* ,(cdr assignments) ,@rest))
      `(progn ,@rest)))

(defmacro prog (vars &body body)  `(block nil (let  ,vars (tagbody ,@body))))
(defmacro prog* (vars &body body) `(block nil (let* ,vars (tagbody ,@body))))

;; TODO Declarations
(defmacro declare (&rest no) nil)
(defmacro declaim (&rest no) nil)
(defmacro proclaim (&rest no) nil)
