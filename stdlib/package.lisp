(in-package :cl-lib)
;;; Package functions and support

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun packagep (package) (eq package-metatable (lua-get-metatable package)))
  (%regtype package () (packagep el))

  (defun find-package (name)
    (cond
      ((symbolp name) (setq name (symbol-name name)))
      ((stringp name) (setq name (string name)))
      ((packagep name) (return-from find-package name))
      (t (error 'type-error :form 'name :datum name :expected-type '(or symbol string package))))
    (lua-index-table (lua "LCL") name))

  (defun %find-package (name)
    (let ((p (find-package name)))
      (if p
          p
          (error "Package ~S (~A) does not exist" name (tostring name)))))
  (defun %in-package (package)
    (setq *package* (%find-package package))))


(defmacro in-package (package)
  (let ((p (find-package package)))
    (if p
        (progn
          (setq *package* p)
          `(%in-package ',package))
        (error "Can't switch to package ~S, package does not exist" package))))

(defun list-all-packages ()
  (let (lst)
    (%pairs key val (lua "LCL")
            (if (string= key (package-name val)) (push val lst)))
    lst))

(defun package-name (package) (lua-index-table (%find-package package) "name"))
(defun package-nicknames (package) (lua-index-table (%find-package package) "nicknames"))
(defun package-use-list (package) (lua-index-table (%find-package package) "used"))
(defun package-used-by-list (package) (lua-index-table (%find-package package) "used_by"))
(defun package-shadowing-symbols (package) (lua-index-table (%find-package package) "shadowing"))
(defun package-external-symbols (package) (%pairs-value-list (lua-index-table (%find-package package) "external") :test #'symbolp))

(defun special-package-symbol-p (string)
  (member (string string) '("name" "external" "shadowing" "nicknames" "used" "used_by")))
(defmacro do-symbols (varres &body body)
  (let ((key (gensym "KEY")))
    `(block nil
       (%pairs ,key ,(car varres) (%find-package ,(or (cadr varres) *package*))
         (when (and (symbolp ,(car varres))
                    (not (special-package-symbol-p ,key)))
           ,@body))
       ,(caddr varres))))

(defmacro do-external-symbols (varres &body body)
  `(block nil
     (%pairs ,(gensym) ,(car varres) (lua-index-table (%find-package ,(or (cadr varres) '*package*)) "external")
             (when (symbolp ,(car varres)) ,@body))
     ,(caddr varres)))

;; RETURN should return from all
(defmacro do-all-symbols (varres &body body)
  (let ((pack (gensym)))
    `(progn
       (dolist (,pack (list-all-packages) ,(cadr varres))
         (do-symbols (,(car varres) ,pack)
           (when (symbolp ,(car varres)) ,@body))))))

(defun rename-package (package new-name &optional new-nicknames)
  (let* ((p (%find-package package))
         (n (package-name p)))
    (lua-set-table p "name" new-name)
    (lua-set-table (lua "LCL") n (lua "nil"))
    (dolist (nick (package-nicknames p))
      (lua-set-table (lua "LCL") nick (lua "nil")))
    (lua-set-table p "nicknames" new-nicknames)
    (dolist (nick (cons new-name (package-nicknames p)))
      (if (lua-index-table (lua "LCL") nick)
          (error "Can't rename-package, package ~S already exist" nick)
          (lua-set-table (lua "LCL") nick p)))))

(defun %find-symbol-external (string package)
  (if (eq :none
          (lua-index-table (lua-index-table package "external")
                           (if (symbolp string) (symbol-name string) string)
                           :none))
      (if (eq package (find-package :keyword)) :external :internal) :external))
(let ((none (gensym)))
  (defun %find-symbol-inherited (string packs)
    (if packs
        (let* ((pack (%find-package (car packs)))
               (found (lua "rawget(" pack ".external, " string ") or " none)))
          (if (eq found none)
              (%find-symbol-inherited string (cdr packs))
              (values found nil)))
        (values nil t))
    ;; (lua "rawget(" pack ".external, " string ") or true")
    ;; (lua-index-table (lua-index-table pack "external") string)
    ;; (find string (package-external-symbols pack) :key #'symbol-name)
    ))

(defun find-symbol (string &optional (package *package*))
  ;; Supposed to be a string, but same examples use symbols, so here it is
  (setq string (string string))
  (let* ((p (%find-package package))
         (sh-sym (find string (package-shadowing-symbols p) :key #'symbol-name)))
    (if sh-sym
        ;; Shadowed symbol
        (values sh-sym
                (%find-symbol-external sh-sym p))
        (let ((raw-sym (lua "rawget(" p ", " string ") or true")))
          (if (eq (lua "true") raw-sym)
              ;; Check for use-package symbols:
              (multiple-value-bind (use-sym not-there) (%find-symbol-inherited string (package-use-list p))
                (if not-there
                    (values nil nil)
                    (values use-sym :inherited)))
              ;; Symbol in package
              (values raw-sym
                      (%find-symbol-external raw-sym p)))))))

(defun find-all-symbols (string)
  (let (ret)
    (dolist (pack (list-all-packages) ret)
      (multiple-value-bind (sym type) (find-symbol string pack)
        (if (and type (not (eq type :inherited)))
            (pushnew sym ret))))))



;;; Defpackage

(defun %defpackage (name options)
  (let*
      ((name (if (symbolp name) (symbol-name name) name))
       (nicknames nil)
       (use nil)
       ;; TODO
       ;; (shadow nil)
       ;; (shadowing-import-from nil)
       (import-from nil)
       (export nil)
       (intern nil)
       (package (or (find-package name) (make-package name))))

    (dolist (op options)
      (case (car op)
        (:nicknames (setq nicknames (append nicknames (mapcar (lambda (o) (if (symbolp o) (symbol-name o) o)) (cdr op)))))
        (:use (setq use (append use (mapcar #'string (cdr op)))))
        (:export (setq export (append export (mapcar #'string (cdr op)))))
        (:intern (setq intern (append intern (mapcar #'string (cdr op)))))
        (:import-from (push (cdr op) import-from))
        (:shadow (warn "defpackage: :SHADOW not supported"))
        (:shadowing-import-from (warn "defpackage: :SHADOWING-IMPORT-FROM not supported"))
        (:documentation)
        (:size)
        (t (error "defpackage: Invalid option ~S" op))))

    (rename-package package name nicknames)
    ;; TODO Shadows
    (use-package use package)
    (dolist (i import-from)
      (import (mapcar (lambda (s) (find-symbol s (car i))) (cdr i)) package))
    (dolist (i intern)
      (intern i package))
    (export (mapcar (lambda (x) (intern x package)) export) package)
    package))

(defmacro defpackage (name &body options)
  `(eval-when (:compile-toplevel :load-toplevel :execute) (%defpackage ',name ',options)))



;;; Modules
(defvar *modules* nil)

(defun provide (module-name) (pushnew module-name *modules*))
(defun require (module-name &optional pathname-list)
  (if (member module-name *modules*) t (error "Can't require module ~A, module not found" module-name)))


;;; Access management
;;; FIXME Everything related to shadowing is most likely wrong here.

(defun export (symbols &optional (package *package*))
  (unless (listp symbols) (setq symbols (list symbols)))
  (setq package (%find-package package))

  (dolist (s symbols)
    (unless (member s (package-external-symbols package))
      ;; Check conflict
      (dolist (p (package-used-by-list package))
        (when (nth-value 0 (find-symbol (symbol-name s) p))
          (warn "export conflict for symbol ~S in package ~S" s p)))
      (lua-set-table (lua-index-table package "external") (symbol-name s)
                     (let ((p (symbol-package s)))
                       (if (or (eq (find-package :nil) p) (eq (find-package :keyword) p))
                           (lua-index-table package (symbol-name s))
                           s)))))
  t)
(defun unexport (symbols &optional (package *package*))
  (unless (listp symbols) (setq symbols (list symbols)))
  (setq package (%find-package package))

  (dolist (s symbols)
    (let ((type (nth-value 1 (find-symbol (symbol-name s) package))))
      (ecase type
        (:external (lua-set-table (lua-index-table package "external") (symbol-name s) (lua "nil")))
        (:internal (warn "Symbol ~S was already internal to package ~S" s package))
        (:inherited (error "Can't unexport symbol ~S, not present in package ~S" s package))
        (nil (error "Can't unexport symbol ~S, not present in package ~S" s package)))))
  t)

(defun shadow (symbols &optional (package *package*))
  (unless (listp symbols) (setq symbols (list symbols)))
  (setq package (%find-package package))
  (dolist (s symbols)
    (multiple-value-bind (found type) (find-symbol s package)
      (cond
        ((eq type :inherit)
         (let ((new-symbol (funcall #'alloc-symbol package (symbol-name s))))
           (let ((ss (symbol-name s)))
             (lua "rawset(" package "," ss "," new-symbol ")"))
           (lua-set-table package "shadowing" (adjoin new-symbol (package-shadowing-symbols package)
                                                      :test #'equal :key #'symbol-name))))
        (found
         (lua-set-table package "shadowing" (adjoin found (package-shadowing-symbols package)
                                                    :test #'equal :key #'symbol-name))))))
  t)

;; shadowing-import

(defun use-package (packages-to-use &optional (package *package*))
  (unless (listp packages-to-use) (setq packages-to-use (list packages-to-use)))
  (setq package (%find-package package))

  (dolist (p packages-to-use)
    (setq p (%find-package p))
    ;; TODO Check conflicts
    (let ((l (package-use-list package)))
      (pushnew p l)
      (lua-set-table package "used" l))
    (let ((l (package-used-by-list p)))
      (pushnew package l)
      (lua-set-table p "used_by" l)))
  t)

(defun unuse-package (packages-to-unuse &optional (package *package*))
  (unless (listp packages-to-unuse) (setq packages-to-unuse (list packages-to-unuse)))
  (setq package (%find-package package))

  (dolist (p packages-to-unuse)
    (setq p (%find-package p))
    (lua-set-table package "used" (remove p (package-use-list package)))
    (lua-set-table p "used_by" (remove package (package-used-by-list p))))
  t)

(defun import (symbols &optional (package *package*))
  (unless (listp symbols) (setq symbols (list symbols)))
  (setq package (%find-package package))

  (dolist (s symbols)
    (multiple-value-bind (found type) (find-symbol (symbol-name s) package)
      (cond
        ((and type (eq found s))
         ;; That's fine it's the same symbol.
         )
        (type (error "Can't import symbol ~S, a symbol of the same name is already accessible from package ~S" s package))
        (t (lua-set-table package (symbol-name s) s))))))

;; Rewrite intern
(defun intern (string &optional (package *package*))
  (if (stringp string)
      (progn
        (setq string (string string))
        (multiple-value-bind (sym loc) (find-symbol string package)
          (if loc
              sym
              (lua-index-table (%find-package package) string))))
      (error "Argument to intern must be a string, got ~S instead" string)))

(defvar *gentemp-counter* 0)
(defun gentemp (&optional (prefix "T") (package *package*))
  (unless (stringp prefix) (error 'type-error :form 'prefix :datum prefix :expected-type 'string))
  (setq prefix (string prefix))
  (%loop
   (if (nth-value 1 (find-symbol (concat-string prefix *gentemp-counter*) package))
       (incf *gentemp-counter*)
       (%break)))
  (intern (concat-string prefix *gentemp-counter*) package))

;; TODO return nil if symbol not found.
(defun unintern (symbol &optional (package *package*))
  (setq package (%find-package package))
  (when (eq package (symbol-package symbol))
    (lua-set-table symbol "package" (lua "nil")))
  (lua-set-table package (symbol-name symbol) (lua "nil"))
  t)

;; Export all symbols of CL
(lua-set-table (lua "CL") "external" (lua "CL"))
;; (%pairs key sym (lua "CL") (when (symbolp sym) (export sym :cl)))

(defun delete-package (package)
  (setq package (%find-package package))

  (dolist (p (package-used-by-list package)) (unuse-package package p))
  (unuse-package (package-use-list package) package)
  (rename-package package (package-name package))
  (do-symbols (sym package) (unintern sym package))
  (lua-set-table (lua "LCL") (package-name package) (lua "nil")))

(defun %pairs-iterator (table)
  (let (next tbl (x (new)))
    (lua-push (next "," tbl "," x "[1] = pairs(" table ")")
              (lambda ()
                (setq x (lua "{" next "(" tbl ", " x "[1])}"))
                (if (lua x "[1] == nil" )
                    (values nil nil nil)
                    (values (lua x "[1]") (lua x "[2]") t))))))
(defun %pairs-iterator-combine (iterators)
  (labels ((it ()
             (if iterators
                 (multiple-value-bind (key value ok) (funcall (car iterators))
                   (if ok
                       (if (symbolp value)
                           (values key value ok)
                           (it))
                       (progn
                         (setq iterators (cdr iterators))
                         (it))))
                 (values nil nil nil))))
    #'it))
(defun call-with-package-iterator (packages filter function)
  (let ((it (%pairs-iterator-combine (mapcar #'%pairs-iterator (mapcar #'%find-package packages)))))
    (funcall function (lambda ()
                        (block nil
                          (tagbody
                           again
                             (multiple-value-bind (k v ok) (funcall it)
                               (if ok
                                   (let ((kind (nth-value 1 (find-symbol (symbol-name v) (symbol-package v)))))
                                     (if (member kind filter)
                                         ;; XXX Add the package
                                         (return (values t v kind))
                                         (go again)))
                                   (return nil)))))))))
(defmacro with-package-iterator (namel &body body)
  (destructuring-bind (name package-list-form &rest symbol-types) namel
    (let ((it (gensym "IT")))
      `(call-with-package-iterator
        (list ,@(if (listp package-list-form) package-list-form (list package-list-form)))
        (list ,@symbol-types)
        (lambda (,it)
          (macrolet ((,name () ',(list 'funcall it)))
            ,@body))))))
