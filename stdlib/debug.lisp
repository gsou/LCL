(in-package :cl-lib)

(defvar *trace-functions* nil)
;; We swap the __call metamethod so we don't have the overhead whenever
;; there are no traces
(defvar *func-metatable-default* (lua-index-table function-metatable "__call"))
(defvar *func-metatable-trace*
  (lua "function (fbound, ...) if fbound.trace then CL_LIB['%TRACE-HOOK-CALL'](fbound, l(...)) end local ret = fbound.func(...) if fbound.trace then CL_LIB['%TRACE-HOOK-RET'](fbound, ret) end return ret end"))

(defun %trace-hook-call (function &rest arguments)
  (fresh-line *trace-output*)
  (write-string "TRACE CALL: " *trace-output*)
  (print-object function *trace-output*)
  (write-string " " *trace-output*)
  (print-object arguments *trace-output*)
  (fresh-line *trace-output*))

(defun %trace-hook-ret (function return)
  (fresh-line *trace-output*)
  (write-string "TRACE RETURN: " *trace-output*)
  (print-object function *trace-output*)
  (write-string " " *trace-output*)
  (print-object return *trace-output*)
  (fresh-line *trace-output*))

(defun attach-trace-function () (lua-set-table function-metatable "__call" *func-metatable-trace*))
(defun detach-trace-function () (lua-set-table function-metatable "__call" *func-metatable-default*))

(defmacro cl::trace (&rest functions)
  (let ((lst (gensym)))
    `(let ((,lst ',functions))
       (unless *trace-functions* (attach-trace-function))
       (dolist (l ,lst)
         (let ((fn (cond
                     ((symbolp l) (fdefinition l))
                     ((functionp l) l)
                     (t (error "Can't trace, not a function ~S" l)))))
           (pushnew
            fn
            *trace-functions*)
           (lua-set-table fn "trace" t)))
       ,lst)))

(defmacro cl::untrace (&rest functions)
  (if functions
      (let ((lst (gensym)))
        `(let ((,lst ',functions))
           (dolist (l ,lst)
             (let ((fn (cond
                         ((symbolp l) (fdefinition l))
                         ((functionp l) l)
                         (t (error "Can't trace, not a function ~S" l)))))
               (pushnew
                fn
                *trace-functions*)
               (lua-set-table fn "trace" (lua "nil"))))
           (unless *trace-functions* (detach-trace-function))
           t))
      `(progn
         (dolist (l *trace-functions*)
           (lua-set-table l "trace" (lua "nil")))
         (setq *trace-functions* nil)
         (detach-trace-function)
         t)))

(defun cl::break (&optional (format-control "Break") &rest format-arguments)
  (with-simple-restart (continue "Return from BREAK.")
    (invoke-debugger
     (make-condition 'simple-condition
                     :format-control format-control
                     :format-arguments format-arguments)))
  nil)

;; TODO
(defmacro cl::assert (test-form &optional place (datum-form "ASSERT") &rest argument-form)
  `(unless ,test-form
    (with-simple-restart (continue "Ignore ASSERT and continue.")
      (error ,datum-form ,@argument-form))))
