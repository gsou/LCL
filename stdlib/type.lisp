(in-package :cl-lib)
;;;; Typing related functions
(defun keywordp (symbol) (and (symbolp symbol) (string= "KEYWORD" (package-name (symbol-package symbol)))))

(defun sequencep (seq) (or (listp seq) (stringp seq) (vectorp seq)))

;;;; Type specifier map
;;; The hash-table binds the specifier symbol to
;;; a list whose car is a function used by typep
;;; and its cdr contains in order its supertypes
(defvar *type-specifiers* (make-hash-table))

(defun %register-type-specifier (symbol function super)
  (sethash symbol *type-specifiers* (cons function super)))

(defmacro %regtype (typ supers &body body)
  (let ((typsym (if (consp typ) (car typ) typ))
       (arglist (if (consp typ) (cdr typ) nil)))
    `(%register-type-specifier ',typsym (lambda ,(cons 'el arglist) ,@body) ',supers)))

(defmacro deftype (name arglist &body body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (%register-type-specifier ',name (lambda ,(cons (gensym) arglist) ,@body) '(t))))

;;;; Type specifier definition
(%regtype (and &rest conds) ()
  (if conds
      (and (typep el (car conds))
           (typep el (cons 'and (cdr conds))))
      t))
(%regtype (eql object) () (eql object el))
(%regtype (member &rest objects) () (if (member el objects :test #'eql) t))
(%regtype (mod n) () `(integer 0 ,(- n 1)))
(%regtype (not spec) () (not (typep el spec)))
(%regtype (or &rest conds) ()
  (if conds
      (or (typep el (car conds))
          (typep el (cons 'or (cdr conds))))))
(%regtype (satisfies func) () (if (funcall func el) t))


(%regtype t () t)
(%regtype otherwise () t)
(%regtype (nil) () nil)
(%regtype symbol (t) (symbolp el))
(%regtype character (t) (charp el))
(%regtype base-char (character) (charp el))
(%regtype standard-char (base-char) (standard-char-p el))
(%regtype keyword (symbol t) (keywordp el))
(%regtype boolean (symbol t) (or (eq el t) (eq el nil)))
(%regtype null (symbol list sequence t) (null el))
(%regtype (cons &optional (car '*) (cdr '*)) (list sequence t)
  (and (consp el)
       (or (eq car '*) (typep (car el) car))
       (or (eq cdr '*) (typep (cdr el) car))
       t))
(%regtype list (sequence t) (listp el))

(%regtype atom (t) (atom el))
(%regtype hash-table (t) (hash-table-p el))

;;; Numeric types
(%regtype fixnum (integer rational real number t) (%integerp el))
(%regtype bignum (integer rational real number t) `(and integer (not fixnum)))
(%regtype bit (unsigned-byte signed-byte integer rational real number t) (list 'integer 0 1))
(%regtype (unsigned-byte &optional (s '*)) (signed-byte integer rational real number t)
  (if (eq s '*)
      `(integer 0 *)
      `(integer 0 ,(- (expt 2 s) 1))))
(%regtype (signed-byte &optional (s '*)) (integer rational real number t)
  (if (eq s '*)
      'integer
      (let ((m (expt 2 (- s 1))))
        `(integer ,(- m) ,(- m 1)))))
(%regtype (integer &optional (lower '*) (upper '*)) (rational real number t)
    (and
     (integerp el)
     (or (eq lower '*) (if (consp lower)
                           (> el (car lower))
                           (>= el lower)))
     (or (eq upper '*) (if (consp upper)
                           (< el (car upper))
                           (<= el upper)))
     t))
(%regtype ratio (rational real number t) (%rationalp el))
(%regtype rational (real number t) (rationalp el))
(%regtype (short-float &optional (lower '*) (upper '*)) (float real number t) `(float ,lower ,upper))
(%regtype (single-float &optional (lower '*) (upper '*)) (float real number t) `(float ,lower ,upper))
(%regtype (double-float &optional (lower '*) (upper '*)) (float real number t) `(float ,lower ,upper))
(%regtype (long-float &optional (lower '*) (upper '*)) (float real number t) `(float ,lower ,upper))
(%regtype (float &optional (lower '*) (upper '*)) (real number t)
  (and
   (floatp el)
   (or (eq lower '*) (>= el lower))
   (or (eq upper '*) (if (consp upper)
                         (< el (car upper))
                         (<= el upper)))
   t))
(%regtype (real &optional (lower '*) (upper '*)) (number t)
  (and
   (realp el)
   (or (eq lower '*) (>= el lower))
   (or (eq upper '*) (if (consp upper)
                         (< el (car upper))
                         (<= el upper)))
   t))
(%regtype (complex &optional (spec '*)) (number t)
  (and
   (complexp el)
   (or (eq spec '*)
       (and
        (typep (realpart el) spec)
        (typep (imagpart el) spec)))
   t))
(%regtype number (t) (numberp el))

;;; Function types
(%regtype (function &optional (arg-type '*) (value-type '*)) (t)
  ;; TODO Validate the arg and return
  (functionp el))
(%regtype compiled-function (function t) 'function)

;;; Sequence/Array types
(%regtype sequence (t) '(or vector list string))
(%regtype (array &optional (element-type '*) (dimension-spec '*)) (t)
  (and
   (arrayp el)
   (or (eq '* element-type) (eql (array-element-type el) element-type))
   (cond
     ((eq dimension-spec '*) t)
     ((integerp dimension-spec) (eql (array-rank el) dimension-spec))
     ((listp dimension-spec) (equal (array-dimensions el) dimension-spec))
     ;; XXX Should throw error ?
     (t nil))))
(%regtype (simple-array &optional (element-type '*) (dimension-spec '*))
          (array t)
          (and
           (arrayp el)
           (null (fill-pointer el))
           (null (array-displacement el))
           (list 'array element-type dimension-spec)))
(%regtype (vector &optional (element-type '*) (size '*)) (array sequence t)
  (and
   (vectorp el)
   (or (eq '* element-type) (eql (array-element-type el) element-type))
   (or (eq '* size) (eql (length el) size))))
(%regtype (simple-vector &optional (size '*))
          (vector simple-array array sequence t)
          `(simple-array t (,size)))
(defun simple-vector-p (arr) (typep arr 'simple-vector))
(%regtype (bit-vector &optional (size '*))
          (vector array sequence t)
          `(vector bit ,size))
(%regtype (simple-bit-vector &optional (size '*))
          (bit-vector vector simple-array array sequence t)
          `(simple-array bit (,size)))
(defun simple-bit-vector-p (arr) (typep arr 'simple-bit-vector))
(%regtype (string &optional (size '*)) (vector array sequence t)
  (and
   (stringp el)
   (or (eq '* size) (= size (length el)))))
(%regtype (simple-string &optional (size '*)) (string vector array sequence t)
          `(string ,size))
(%regtype (cl:simple-base-string &optional (size '*)) (base-string simple-string string vector simple-array array sequence t)
          `(string ,size))
(%regtype (base-string &optional (size '*)) (string vector array sequence t) (%stringp el))
(%regtype (native-string &optional (size '*)) (simple-string string vector simple-array array sequence t)
          (and (%stringp el) (or (eq '* size) (= size (%len el)))))


;; TODO Missing built-in type spec

(defmacro check-type (place typespec &optional (string nil stringp))
  `(unless (typep ,place ',typespec)
    (error 'type-error :form ',place :datum ,place :expected-type ',(if stringp string typespec))))

(defun typep-ext (object symbol args)
  ;; TODO Until the type system is more complete, let's just return nil if the type
  ;; is not found
  ;; (error)
  )

;;;; Type operations
(defun typep (object type-specifier &optional environment)
  (let* ((spec-symbol (if (consp type-specifier) (car type-specifier) type-specifier))
         (spec-args (if (consp type-specifier) (cdr type-specifier)))
         (spec-fn (car (gethash spec-symbol *type-specifiers*))))
    (if spec-fn
        (let ((val (apply spec-fn object spec-args)))
          (cond
            ((eq val t) t)
            ((eq val nil) nil)
            (t (typep object val environment))))
        (typep-ext object spec-symbol spec-args))))

(defun %integral-type-p (type) (member (if (consp type) (car type) type) '(bit unsigned-byte signed-byte fixnum integer)))
(defun %integral-type (type &rest args)
  "Convert the type to the INTEGER type if possible return the bounds as extra values"
  (if (consp type) (apply #'%integral-type type)
      (case type
        (bit (values 'integer 0 1))
        (unsigned-byte
         (if (integerp (first args))
             (values 'integer 0 (1- (expt 2 (first args))))
             (values 'integer 0 '*)))
        (signed-byte)
        (fixnum (values 'integer most-negative-fixnum most-positive-fixnum))
        (integer
         (values
          'integer
          (cond
            ((integerp (first args)) (first args))
            ((consp (first args)) (1+ (car (first args))))
            (t '*))
          (cond
            ((integerp (second args)) (second args))
            ((consp (second args)) (1- (car (second args))))
            (t '*))))
        (t nil))))

(defun %real-type-p (type)
  (or (%integral-type-p type) (member type '(rational real))))
;; XXX Ranges don't work correctly this way
(defun %real-type (type &rest args)
  (if (consp type) (apply #'%real-type type)
      (if (%integral-type-p type)
          (apply #'%integral-type type args)
          (case type
            ((real rational)
             (values
              'integer
              (cond
                ((integerp (first args)) (first args))
                ((consp (first args)) (car (first args)))
                (t '*))
              (cond
                ((integerp (second args)) (second args))
                ((consp (second args)) (car (second args)))
                (t '*))))))))

(defun %find-class (cls))

(defun subtypep (type-1 type-2 &optional environment)
  (if type-1
      ;; TODO Other cases
      (cond
        ((and (consp type-1) (symbolp type-2) (eq (car type-1) type-2)) (values t t))
        ((eq type-1 type-2) (values t t))
        ((and (%find-class type-1) (%find-class type-2))
         (values (%subclassp (%find-class type-1) (%find-class type-2)) t))
        ((and (symbolp type-1) (symbolp type-2))
         (let ((spec-super (cdr (gethash type-1 *type-specifiers*))))
           (values (some (lambda (el) (subtypep el type-2)) spec-super) t)))
        ((eq type-2 'character)
         ;; No compound types are valid here, and due to string compare this happens ofter.
         (values nil t))
        ((and (%integral-type-p type-1) (%integral-type-p type-2))
         (multiple-value-bind (_ low1 high1) (%integral-type type-1)
           (multiple-value-bind (_ low2 high2) (%integral-type type-2)
             (values (and
                      (or
                       (eq '* low2)
                       (and (integerp low1)
                            (integerp low2)
                            (<= low2 low1)))
                      (or
                       (eq '* high2)
                       (and (integerp high1)
                            (integerp high2)
                            (>= high2 high1))))
                     t))))
        ((and (%real-type-p type-1) (%real-type-p type-2))
         (multiple-value-bind (_ low1 high1) (%real-type type-1)
           (multiple-value-bind (_ low2 high2) (%real-type type-2)
             (values (and
                      (or
                       (eq '* low2)
                       (and (integerp low1)
                            (integerp low2)
                            (<= low2 low1)))
                      (or
                       (eq '* high2)
                       (and (integerp high1)
                            (integerp high2)
                            (>= high2 high1))))
                     t))))
        ((and (eq 'number type-1) (equal type-2 '(complex float)))
         (values nil t))
        (t
         (warn "Unable to subtypep type ~S to ~S." type-1 type-2)
         (values nil nil)))
      (values t t)))

(defmacro typecase (keyform &body clauses)
  (let ((keysym (gensym)))
    `(let ((,keysym ,keyform))
       (cond
         ,@(mapcar (lambda (clause)
                     (cons (list 'typep keysym (list 'quote (car clause))) (cdr clause)))
                   clauses)))))

(defmacro ctypecase (keyform &body clauses)
  `(etypecase ,keyform ,@clauses))

(defmacro etypecase (keyform &body clauses)
  `(typecase ,keyform ,@clauses (t (error "etypecase, no matching branches"))))



(defun coerce (object result-type)
  (cond
    ((subtypep result-type 'string)
     (cond
       ((or (typep object 'symbol) (typep object 'string)) (string object))
       ((typep object 'sequence) (concatenate 'string object))
       (t (error "Can't coerce to string"))))
    ((subtypep result-type 'sequence)
     (if (sequencep object)
         (concatenate result-type object)
         (error "Can't coerce non sequence to list")))
    ((subtypep result-type 'character)
     (cond
       ((characterp object) object)
       ((and (stringp object) (= 1 (length object))) (char (string object) 0))
       (t (error 'simple-type-error :format-control "Can't coerce non character designator ~S"
                                    :format-arguments (list object)
                                    :form 'object :datum object :expected-type 'character
                                    ))))
    ;; XXX Inacurate
    ((subtypep result-type 'complex)
     (cond
       ((integerp object) object)
       ((realp object) (complex object))
       (t (error "Can't coerce ~S to complex" object))))
    ((subtypep result-type 'float)
     (cond
       ((rationalp object)
        (+ 0.0 (/ (numerator object) (denominator object))))
       ((realp object) (+ 0.0 object))
       (t (error "Can't coerce non real to float"))))
    ((subtypep result-type 'function)
     (cond
       ((and (listp object) (eq 'lambda (car object))) (eval object))
       ((symbolp object) (fdefinition object))
       (t (error "Can't coerce ~S to function" result-type))))
    ((eq result-type t) object)
    (t (error "Can't coerce to result-type ~S" result-type))))

(defun character (character) (coerce character 'character))
