(cl:in-package :cl-lib)
;;; Common Lisp Reader

(defstruct (readtable (:predicate readtablep) (:copier %copy-readtable))
  (table (make-hash-table))
  (macros (make-hash-table))
  (dispatch (make-hash-table))
  (case :upcase))

(defvar *readtable* nil)

(defvar *read-base* 10)
(defvar *read-suppress* nil)
(defvar *standard-syntax-readtable* (make-readtable))
(defvar *read-eval* t)
(defvar *read-default-float-format* 'double-float)

;; XXX Deep copy
(defun copy-readtable (&optional (from-readtable *readtable*) to-readtable)
  (when to-readtable (error "copy-readtable unsupported"))
  (%copy-readtable from-readtable))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *read-label-expression* nil)

  (defmacro with-read-recursive (rec &body body)
    `(if ,rec
         #+lcl (if *read-label-expression*
             (progn ,@body)
             (error "Invoked read function recursively (~A) in a non-recursive context." ,rec))
         #-lcl (progn ,@body)
         (let ((*read-label-expression* (make-hash-table)))
           ,@body))))


;;; Reader

(defun readtable-whitespacep (char &optional (readtable *readtable*))
  (eq :white-space (gethash (char-code char) (readtable-table readtable) :invalid)))
(defun readtable-constituentp (char &optional (readtable *readtable*))
  (eq :constituent (gethash (char-code char) (readtable-table readtable) :invalid)))
(defun readtable-singleescapep (char &optional (readtable *readtable*))
  (eq :single-escape (gethash (char-code char) (readtable-table readtable) :invalid)))
(defun readtable-terminatingmacrop (char &optional (readtable *readtable*))
  (eq :terminating-macro-character (gethash (char-code char) (readtable-table readtable) :invalid)))


(defun readtable-apply-case (char &optional (readtable *readtable*))
  (ecase (readtable-case readtable)
    (:upcase (lua-call "string.upper" (char-string char)))
    (:downcase (lua-call "string.lower" (char-string char)))
    (:invert
     (let* ((cs (char-string char))
            (u (lua-call "string.upper" cs)))
       (if (eq u cs) (lua-call "string.lower" cs) u)))
    (:preserve (char-string char))))

;; FIXME This is most likely still incorrect
;; TODO Add number of char reads from token so that parse-integer can have the junk allowed
#-lcl (defun read-number (token &optional (base *read-base*) (error-p t) error-value)
        (lua "tonumber(" token ") or n"))
#+lcl
(defun read-number (token &optional (base *read-base*) (error-p t) error-value)
  (labels ((valid-int (char) (or (char= char #\+) (char= char #\-) (digit-char-p char base)))
           (valid-rational (char) (or (char= char #\/) (valid-int char)))
           (valid-exp (char)
             (or
              (char= char #\e) (char= char #\E)   ; fdefault
              (char= char #\s) (char= char #\S)   ; f16
              (char= char #\f) (char= char #\F)   ; f32
              (char= char #\d) (char= char #\D)   ; f64
              (char= char #\l) (char= char #\L))) ; f128
           (valid-float (char) (or (char= char #\.) (valid-exp char) (valid-int char)))
           (read-denum (start)
             (let ((acc 0) (factor 1))
               (dotimes (ix-raw (- (length token) start) (* factor acc))
                 (let* ((ix (+ start ix-raw))
                        (cix (char token ix)))
                   (cond
                     ((and (zerop ix) (char= #\- cix)) (setq factor -1))
                     ((and (zerop ix) (char= #\+ cix)) (setq factor 1))
                     (t
                      (let ((c (char-digit cix base)))
                        (if c
                            (setq acc (+ c (* acc base)))
                            ;; Error
                            (return nil)))))))))
           (read-num ()
             (let ((acc 0) (factor 1))
               (dotimes (ix (length token) (* factor acc))
                 (let ((cix (char token ix)))
                   (cond
                     ((and (zerop ix) (char= #\- cix)) (setq factor -1))
                     ((and (zerop ix) (char= #\+ cix)) (setq factor 1))
                     (t
                      (let ((c (char-digit cix base)))
                        (cond
                          (c (setq acc (+ c (* acc base))))
                          ((char= #\/ cix)
                           (let ((den (read-denum (1+ ix))))
                             (if den
                                 (return (make-rational (* factor acc) den))
                                 (return nil))))
                          ;; Error
                          (t (return nil)))))))))))
    (let ((ret (cond
                 ((zerop (length token)) nil)
                 ;; We need at least a digit in here
                 ((not (some (lambda (char) (digit-char-p char base)) token)) nil)
                 ;; We are reading an integer
                 ((every #'valid-int token) (read-denum 0))
                 ;; We are reading a rational
                 ((every #'valid-rational token)
                  (if (eq #\/ (char token 0)) nil (read-num)))
                 ((every #'valid-float token)
                  ;; We are reading a float. If the read-base allows for D or E in the number and the exponent both,
                  ;; what do we do ?
                  ;; FIXME
                  ;; We cheat, replace the exponent delimiter with a "e" and ask lua to parse it
                  ;; Also... this ignores read-base
                  (let ((p (position-if #'valid-exp token :from-end t)))
                    (when p (setf (char token (position-if #'valid-exp token :from-end t)) #\e)))
                  (lua "tonumber(" token ") or n")))))
      (or ret (if error-p (error "Number ~A is invalid in base ~S" token base) error-value)))))

;; FIXME
(defun parse-integer (string &key (start 0) end (radix 10) junk-allowed)
  (read-number (subseq string start end) radix t))

(defun finalize-token (token escaped &optional package package-internal-p)
  (if *read-suppress*
      nil
      (or
       (and (not escaped) (read-number token *read-base* nil nil))
       (cond
         (package
          ;; TODO External/internal
          (intern token package))
         ((eq #\: (char token 0)) (intern (subseq token 1) 'keyword))
         (t (intern token)))
       ;; (let* ((pos (lua "{string.find(" token ", '::?')}"))
       ;;        (start-pos (lua pos "[1] or n"))
       ;;        (end-pos (lua pos "[2] or n")))
       ;;   (if (eq #\: (char token 0))
       ;;       (intern (subseq token 1) 'keyword)
       ;;       (if start-pos
       ;;           (intern
       ;;            (subseq token end-pos)
       ;;            (subseq token 0 (- start-pos 1)))
       ;;           (intern token))))
       )))

(defun %read-token (finalize stream token &optional symbol-can-read-package escaping (escaped escaping))
  (let (package package-internal)
    (labels
        ((done (token escaped)
           (if package
               (funcall finalize token escaped package package-internal)
               (funcall finalize token escaped)))
         (run (token escaping escaped)
           (if escaping
               ;; Case 9
               (let* ((char (read-char stream t nil t))
                      (chartype (gethash (char-code char) (readtable-table *readtable*) :invalid)))
                 (cond
                   ((eq :invalid chartype) (error "Char ~A (~A) is invalid " char (char-code char)))
                   ((eq :single-escape chartype)
                    (run (concat-string token (char-string (read-char stream t nil t))) nil t))
                   ((eq :multiple-escape chartype) (run token nil t))
                   (t (run (concat-string token (char-string char)) t t))))
               ;; Case 8
               (let* ((char (read-char stream nil nil t))
                      (chartype (gethash (char-code char) (readtable-table *readtable*) :invalid)))
                 (if char
                     (cond
                       ((and symbol-can-read-package (char= #\: char))
                        (if package
                            (error "Too many colons")
                            (progn
                              ;; This is a special case inserted here to presplit the package
                              (let ((next (read-char stream)))
                                (if (char= next #\:)
                                    (setq package-internal t)
                                    (unread-char next stream)))
                              (setq package token)
                              (run "" nil escaped))))
                       ((eq :invalid chartype) (error "Char ~A (~A) is invalid" char (char-code char)))
                       ((eq :single-escape chartype)
                        (run (concat-string token (char-string (read-char stream t nil t))) nil t))
                       ((eq :multiple-escape chartype) (run token t t))
                       ((eq :terminating-macro-character chartype) (unread-char char stream) (done token escaped))
                       ((eq :white-space chartype)
                        ;; TODO preserving whitespace
                        (done token escaped))
                       (t (run (concat-string token (readtable-apply-case char)) nil escaped)))
                     (done token escaped))))))
      (run token escaping escaped))))

(defun read-token (stream token &optional escaping) (%read-token #'finalize-token stream token t escaping))

(defun %read (loop &optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  ;; Step1
  (with-read-recursive recursive-p
    (let ((char (read-char stream nil nil t)))
      (if char
          (let ((chartype (gethash (char-code char) (readtable-table *readtable*) :invalid)))
            (cond
              ;; Case 2
              ((eq :invalid chartype) (error "Char ~A (~A) is invalid" char (char-code char)))
              ;; Case 3
              ((eq :white-space chartype) (read stream eof-error-p eof-value t))
              ;; Case 4
              ((or (eq :terminating-macro-character chartype)
                   (eq :non-terminating-macro-character chartype))
               (let ((ret (multiple-value-list
                           (funcall (gethash (char-code char) (readtable-macros *readtable*) :no-readtable-macro)
                                    stream char))))
                 (if ret
                     (car ret)
                     (if loop
                         loop
                         (read stream eof-error-p eof-value t)))))
              ;; Case 5
              ((eq :single-escape chartype)
               (read-token stream (char-string (read-char stream t nil t))))
              ;; Case 6
              ((eq :multiple-escape chartype)
               (read-token stream "" t))
              ;; Case 7
              ((eq :constituent chartype)
               (read-token stream (readtable-apply-case char)))
              (t (error "Invalid chartype ~A ~A" char chartype))))
          (if eof-error-p (error "EOF") eof-value)))))

(defun read (&optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  (%read nil stream eof-error-p eof-value recursive-p))

(defun read-preserving-whitespace (&optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  ;; TODO Preserving whitespace
  (read stream eof-error-p eof-value recursive-p))

;; TODO peek-char
(let ((done (gensym)))
  (defun peek-char (&optional peek-type (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p)
    (with-read-recursive recursive-p
      (let ((c (read-char input-stream eof-error-p done t)))
        (if (eq c done)
            eof-value
            (cond
              ((characterp peek-type)
               (if (char= c peek-type)
                   (progn (unread-char c input-stream) c)
                   (peek-char peek-type input-stream eof-error-p eof-value recursive-p)))
              ((eq t peek-type) (if (readtable-whitespacep c) (peek-char peek-type input-stream eof-error-p eof-value t)
                                    (progn (unread-char c input-stream) c)))
              ((eq nil peek-type)
               (unread-char c input-stream)
               c)))))))

(defun read-line (&optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  (let ((line nil))
    (labels ((rec ()
               (let ((char (read-char stream nil nil t)))
                 (when char
                   (when (null line) (setq line ""))
                   (when (not (eq 10 (char-code char)))
                     (setq line (concat-string line (char-string char)))
                     (rec))))))
      (rec))
    (or line
        (if eof-error-p
            (error "EOF")
            eof-value))))

(let ((none (gensym)))
  (defun read-delimited-list (end &optional (stream *standard-input*) recursive-p)
    (with-read-recursive recursive-p
      (let (acc)
        (block nil
          (tagbody
           again
             (let ((char (read-char stream t nil t)))
               (cond
                 ((eq end char) (return (reverse acc)))
                 ((readtable-whitespacep char) (go again))
                 (t (unread-char char stream)
                    (let ((elt (%read none stream t nil t)))
                      (unless (eq elt none) (push elt acc))
                      (go again)))))))))))

(defun read-from-string (string &optional (eof-error-p t) eof-value &key (start 0) end preserve-whitespace)
  (let ((stream (make-string-input-stream string start end)))
    (funcall (if preserve-whitespace #'read-preserving-whitespace #'read)
             stream
             eof-error-p
             eof-value
             nil)))


;;; Readtable macros and chars

(defun %set-char-syntax (char chartype &optional (readtable *readtable*))
  (sethash (char-code char) (readtable-table readtable) chartype))

(defmacro %set-all-syntax (readtable &body syntax)
  (labels ((%%set-all-syntax (readtable syntax)
           (if syntax
               `((%set-char-syntax ,(car syntax) ,(cadr syntax) ,readtable)
                 ,@(%%set-all-syntax readtable (cddr syntax))))))
    (if syntax `(progn ,@(%%set-all-syntax readtable syntax)))))

(%set-all-syntax *standard-syntax-readtable*
                 ;; #\Backspace :constituent
                 (char "	" 0) :white-space (char "
" 0) :white-space
                 #\Linefeed :white-space
                 (char "" 0) :white-space
                 ;; #\Return :white-space
                 (char " " 0) :white-space
                 #\! :constituent #\$ :constituent #\% :constituent #\& :constituent #\* :constituent
                 #\+ :constituent #\- :constituent #\. :constituent #\/ :constituent
                 #\0 :constituent #\1 :constituent #\2 :constituent #\3 :constituent #\4 :constituent
                 #\5 :constituent #\6 :constituent #\7 :constituent #\8 :constituent #\9 :constituent
                 #\: :constituent #\< :constituent #\= :constituent #\> :constituent #\? :constituent
                 #\@ :constituent #\A :constituent #\B :constituent #\C :constituent #\D :constituent
                 #\E :constituent #\F :constituent #\G :constituent #\H :constituent #\I :constituent
                 #\J :constituent #\K :constituent #\L :constituent #\M :constituent #\N :constituent
                 #\O :constituent #\P :constituent #\Q :constituent #\R :constituent #\S :constituent
                 #\T :constituent #\U :constituent #\V :constituent #\W :constituent #\X :constituent
                 #\Y :constituent #\Z :constituent #\[ :constituent #\\ :single-escape #\] :constituent
                 #\^ :constituent #\_ :constituent #\a :constituent #\b :constituent #\c :constituent
                 #\d :constituent #\e :constituent #\f :constituent #\g :constituent #\h :constituent
                 #\i :constituent #\j :constituent #\k :constituent #\l :constituent #\m :constituent
                 #\n :constituent #\o :constituent #\p :constituent #\q :constituent #\r :constituent
                 #\s :constituent #\t :constituent #\u :constituent #\v :constituent #\w :constituent
                 #\x :constituent #\y :constituent #\z :constituent #\{ :constituent #\| :multiple-escape
                 #\} :constituent #\~ :constituent
                 ;; #\Rubout :constituent
                 )

(defun get-macro-character (char &optional (readtable *readtable*))
  (let ((chartype (gethash (char-code char) (readtable-table readtable) :invalid)))
    (if (or (eq chartype :terminating-macro-character)
            (eq chartype :non-terminating-macro-character))
        (values (gethash (char-code char) (readtable-macros readtable)) (eq chartype :non-terminating-macro-character))
        (values nil nil))))

(defun set-macro-character (char fn &optional non-terminating-p (readtable *readtable*))
  (setf (gethash (char-code char) (readtable-table readtable))
        (if non-terminating-p :non-terminating-macro-character :terminating-macro-character))
  (setf (gethash (char-code char) (readtable-macros readtable))
        fn)
  t)

(set-macro-character
 #\"
 (lambda (stream unused-char)
   (let ((str ""))
     (labels ((rec ()
                (let ((char (read-char stream t nil t)))
                  (cond
                    ((eql #\" char) str)
                    ((readtable-singleescapep char)
                     (setq str (concat-string str (char-string (read-char stream t nil t))))
                     (rec))
                    (t
                     (setq str (concat-string str (char-string char)))
                     (rec))))))
       (rec))))
 nil
 *standard-syntax-readtable*)

(set-macro-character
 #\)
 (lambda (stream unused-char) (error "Unmatched closing parens"))
 nil
 *standard-syntax-readtable*)
(let ((none (gensym)))
  (set-macro-character
   #\(
   (lambda (stream unused-char)
     (block nil
      (let (acc dot-el dot-el-p)
        (tagbody
         next
           (let ((char (read-char stream t nil t)))
             (cond
               ((eq #\) char) (go done))
               ((and (eq #\. char) (not dot-el-p))
                (let ((nc (peek-char nil stream t nil t)))
                  (if (member (gethash (char-code nc) (readtable-table *readtable*) :invalid) '(:invalid :white-space :terminating-macro-character))
                      (go dot)
                      (progn
                        (push (%read-token #'finalize-token stream "." t nil nil)
                         acc)
                        (go next)))))
               ((readtable-whitespacep char) (go next))
               (t (unread-char char stream)
                  (let ((elt (%read none stream t nil t)))
                    (when (eq elt none) (go next))
                    (if dot-el-p
                        (error "Multiple elements after dot")
                        (push elt acc))))))
           (go next)
         dot
           (if dot-el-p
               (error "Multiple dots in list")
               (let ((el (%read none stream t nil t)))
                 (when (eq el none) (go dot))
                 (setq dot-el-p t)
                 (setq dot-el el)))
           (go next)
         done
           (return
             (if dot-el-p
                 (let ((rev (reverse acc)))
                   (setf (cdr (last rev)) dot-el)
                   (return rev))
                 (reverse acc)))))))
   nil
   *standard-syntax-readtable*))
(set-macro-character #\' (lambda (stream char) (list 'quote (read stream t nil t))) nil *standard-syntax-readtable*)
(set-macro-character #\` (lambda (stream char) (list 'quasiquote (read stream t nil t))) nil *standard-syntax-readtable*)
(set-macro-character #\, (lambda (stream char)
                           (let ((c (read-char stream t nil t)))
                             (list (if (eq #\@ c) 'unquote-list (progn (unread-char c stream) 'unquote))
                                   (read stream t nil t))))
                     nil *standard-syntax-readtable*)
(set-macro-character
 #\;
 (lambda (stream unused-char)
   (labels ((rec ()
              (let ((char (read-char stream nil nil t)))
                (if (and char (not (eq 10 (char-code char)))) (rec)))))
     (rec)
     (values)))
 nil
 *standard-syntax-readtable*)

(defun read-digits (stream)
  (let ((number nil))
    (labels ((rec ()
               (let ((char (read-char stream t nil t)))
                 (if (digit-char-p char)
                     (progn
                       (unless number (setq number 0))
                       (setq number (+ (* 10 number) (- (char-code char) 48)))
                       (rec))
                     (unread-char char stream)))))
      (rec)
      number)))
(defun dispatch-macro-character-function (stream disp-char)
  (let* ((n (read-digits stream))
         (sub-char (read-char stream t nil t))
         (fn (get-dispatch-macro-character disp-char sub-char)))
    (if fn
        (funcall fn stream sub-char n)
        (error "Dispatch macro ~C~C, does not exist"  disp-char sub-char))))

;; *read-eval*

(defun make-dispatch-macro-character (char &optional non-terminating-p (readtable *readtable*))
  (set-macro-character char #'dispatch-macro-character-function non-terminating-p readtable)
  (setf (gethash (char-code char) (readtable-dispatch readtable)) (make-hash-table)))

(defun set-dispatch-macro-character (disp-char sub-char fn &optional (readtable *readtable*))
  (let ((charu (if (lower-case-p sub-char)
                   (- (char-code sub-char) 32)
                   (char-code sub-char))))
    (setf (gethash charu (gethash (char-code disp-char) (readtable-dispatch readtable)))
          fn)))

(defun get-dispatch-macro-character (disp-char sub-char &optional (readtable *readtable*))
  (let ((charu (if (lower-case-p sub-char)
                   (- (char-code sub-char) 32)
                   (char-code sub-char))))
    (gethash charu (gethash (char-code disp-char) (readtable-dispatch readtable)))))

(make-dispatch-macro-character #\# t *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\'
 (lambda (stream char n) (list 'function (read stream t nil t)))
 *standard-syntax-readtable*)
(defun %read-char-name-or-char (stream &optional tok)
  (if tok
      (let ((c (read-char stream t nil t)))
        (if (or (readtable-whitespacep c)
                (readtable-terminatingmacrop c))
            (progn
              (unread-char c stream)
              (if (= (length tok) 1)
                  (char tok 0)
                  (name-char tok)))
            (%read-char-name-or-char stream (concat-string tok (char-string c)))))
      (%read-char-name-or-char stream (char-string (read-char stream t nil t)))))
(set-dispatch-macro-character
 #\# #\\
 (lambda (stream char n) (%read-char-name-or-char stream))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\(
 (lambda (stream unused-char n)
   (let ((list (read-delimited-list #\) stream t)))
     (let* ((el (car (last list)))
            (arr (make-array (or n (length list)) :initial-element el)))
       (replace arr list)
       arr)))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\A
 (lambda (stream unused-char n)
   (let* ((list (read stream t nil t))
          (dims
            (let ((acc list) c)
              (dotimes (i n)
                (push (length acc) c)
                (setq acc (car acc)))
              (reverse c))))
     (make-array dims :initial-contents list)))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\C
 (lambda (stream unused-char n)
   (let ((list (read stream t nil t)))
     (if (and (listp list) (= 2 (length list )))
         (eval `(complex (quote ,(first list)) (quote ,(second list))))
         (error "#C argument is not a list of length 2"))))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\P
 (lambda (stream unused-char n)
   (let ((str (read stream t nil t)))
     (if (stringp str)
         (eval `(parse-namestring (quote ,str)))
         (error "#P argument is not a string"))))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\.
 (lambda (stream char n)
   (if *read-suppress*
       (read stream t nil t)
       (if *read-eval*
           (eval (read stream t nil t))
           (error "Can't run #\\. when *read-eval* is nil"))))
 *standard-syntax-readtable*)
(defun %interpret-feature-expression (fexp)
  (cond
    (*read-suppress* nil)
    ((symbolp fexp) (member (intern (symbol-name fexp) :keyword) *features*))
    ((not (listp fexp)) (error "Invalid feature expression ~S" fexp))
    ((eq (car fexp) 'not) (not (%interpret-feature-expression (cadr fexp))))
    ((eq (car fexp) 'and) (every #'%interpret-feature-expression (cdr fexp)))
    ((eq (car fexp) 'or) (some #'%interpret-feature-expression (cdr fexp)))
    (t (error "Invalid feature expression ~S" fexp))))
(set-dispatch-macro-character
 #\# #\+
 (lambda (stream char n)
   (if (%interpret-feature-expression (read stream t nil t))
       (read stream t nil t)
       (let ((*read-suppress* t))
         (read stream t nil t)
         (values))))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\-
 (lambda (stream char n)
   (if (not (%interpret-feature-expression (read stream t nil t)))
       (read stream t nil t)
       (let ((*read-suppress* t))
         (read stream t nil t)
         (values))))
 *standard-syntax-readtable*)
;; TODO
(defun read-number-in-base (stream n)
  (%read-token
   (lambda (name escaped) (read-number name n t))
   stream
   ""))
(set-dispatch-macro-character
 #\# #\b
 (lambda (stream char n) (read-number-in-base stream 2))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\o
 (lambda (stream char n) (read-number-in-base stream 8))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\x
 (lambda (stream char n) (read-number-in-base stream 16))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\r
 (lambda (stream char n) (read-number-in-base stream n))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\:
 (lambda (stream char n)
   (%read-token
    ;; (lambda (name) (lua "CL_LIB['ALLOC-SYMBOL'](" nil "," name ")"))
    (lambda (name escaped) (alloc-symbol nil name))
    stream ""))
 *standard-syntax-readtable*)
#-sbcl
(set-dispatch-macro-character
 #\# #\*
 (lambda (stream char n)
   (let (arr)
     (tagbody
      again
        (let ((c (read-char stream t nil t)))
          (if (readtable-constituentp c)
              (progn
                (cond
                  ((char= #\0 c) (push 0 arr))
                  ((char= #\1 c) (push 1 arr))
                  (t (error "illegal element ~S for bit-vector" c)))
                (go again))
              (unread-char c stream))))
     (let ((bv (make-array (length arr) :element-type 'bit)))
       (replace bv (reverse arr))
       bv)))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\|
 (lambda (stream char n)
   (let ((count 1))
     (do ((char1 (read-char stream t nil t) char2)
          (char2 (read-char stream t nil t) (read-char stream t nil t)))
         ((= count 0))
       (when (and (char= char1 #\#) (char= char2 #\|)) (incf count))
       (when (and (char= char1 #\|) (char= char2 #\#))
         (decf count) (if (= count 0) (return (values))))))
   (values))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\=
 (lambda (stream char n)
   (cond
     ((null *read-label-expression*) (error "In #=, not in a read context."))
     ((null n) (error "In #=, no number specified"))
     ((gethash n *read-label-expression*)
      (error "In #=, label appeared twice (first was ~A)" (gethash n *read-label-expression*)))
     (t
      (let ((temp (new) ;; (gensym "TEMPORARY")
                  ))
        (sethash n *read-label-expression* temp)
        (let* ((r (read stream t nil t))
               ;; XXX That's not correct, but enough for simple cycles.
               (r* (nsubst r temp r)))
          (sethash n *read-label-expression* r*)
          r*)))))
 *standard-syntax-readtable*)
(set-dispatch-macro-character
 #\# #\#
 (lambda (stream char n)
   (cond
     ((null *read-label-expression*) (error "In ##, not in a read context."))
     ((null n) (error "In ##, no number specified"))
     (t (multiple-value-bind (r has) (gethash n *read-label-expression*)
          (unless has (error "In ##, forward references not permitted."))
          r))))
 *standard-syntax-readtable*)

;; TODO unimplemented dispatch
;; S -> structure


(defmacro with-standard-io-syntax (&body forms)
  ;; TODO Restore read/print state
  `(progn ,@forms))

(defun set-syntax-from-char (to-char from-char &optional (to-readtable *readtable*) from-readtable)
  ;; TODO
  )



(setq *readtable* (copy-readtable *standard-syntax-readtable*))
