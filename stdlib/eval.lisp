(in-package :cl-lib)
;;;; Evaluate lisp code

(defun eval (forms)
  #-lcl
  (let ((comp (let ((*compile-mode* nil)) (compile-to-lua forms))))
    (lua "load(" comp ")()"))
  #+lcl
  (multiple-value-bind (code env) (let ((*compile-mode* :eval)) (compile-to-lua forms))
    (let ((q (env-eval-state env)))
      (lua "load(" code ")()" "(" q ".data)"))))

(defun eval-in-lexical-environment (forms env)
  "Evaluate the forms in the given lexical environment (as much as possible)"
  (multiple-value-bind (code env) (let ((*compile-mode* :eval)) (compile-to-lua-in-env forms env))
    (let ((q (env-eval-state env)))
      (lua "load(" code ")()" "(" q ".data)"))))

(defun compile (name &optional definition)
  (cond
    ((and name definition) (%set-fdefinition name (eval definition)))
    ((and (not name) definition) (eval definition))
    ((symbolp name) name)
    (t (error "Invalid argument to compile ~A" name))))

(defun progv-declare (var unbound &aux (ret unbound))
  (when (boundp var) (setq ret (symbol-value var)))
  ret)
(defun progv-declare-all (vars unbound)
  (mapcar (lambda (var) (progv-declare var unbound)) vars))
(defun progv-restore (vars restore unbound)
  (mapcar (lambda (var r) (if (eq r unbound) (makunbound var) (set var r) )) vars restore))
(defun progv-set-all (vars values)
  (mapcar #'set vars values))
(defmacro progv (symbols values &body forms)
  (let ((symbol-list (gensym))
        (restore-list (gensym))
        (unbound (gensym)))

    `(let* ((,unbound (new))
            (,symbol-list ,symbols)
            (,restore-list (progv-declare-all ,symbol-list ,unbound)))
       (unwind-protect
            (progn
              (progv-set-all ,symbol-list ,values)
              ,@forms)
         (progv-restore ,symbol-list ,restore-list ,unbound)))))
