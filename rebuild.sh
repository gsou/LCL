#!/usr/bin/env bash
set -e
lua="luajit"

$lua boot/lcl-compile.lua

./assemble-ext.sh

echo "Done"
